package browser.javascript;

import javax.annotation.Nonnull;

import de.inetsoftware.jwebassembly.api.annotation.Import;
import de.inetsoftware.jwebassembly.web.DOMString;

public class JS {

	public static final String MODULE = "JS";

	//=========================
	// Param Adapters
	//=========================

	public static DOMString param(String str) {
		return str == null ? null : de.inetsoftware.jwebassembly.web.JSObject.domString(str);
	}

	public static Object param(Peered peered) {
		return peered == null ? null : peered.peer;
	}

	//=========================
	// newObject -> new Object()
	//=========================

	@Import(module = MODULE, js = "()=>new Object()")
	private static native Object newObject0();

	public static Object newObject() {
		return newObject0();
	}

	//=========================
	// get -> o[p]
	//=========================

	@Import(module = MODULE, js = "(o,p)=>o[p]")
	private static native <T> T get0(@Nonnull Object peer, @Nonnull DOMString propName);

	public static Object get(@Nonnull Object peer, @Nonnull String propName) {
		return get0(peer, param(propName));
	}

	public static int getInt(@Nonnull Object peer, @Nonnull String propName) {
		return get0(peer, param(propName));
	}

	public static double getDouble(@Nonnull Object peer, @Nonnull String propName) {
		return get0(peer, param(propName));
	}

	public static boolean getBoolean(@Nonnull Object peer, @Nonnull String propName) {
		return get0(peer, param(propName));
	}

	//=========================
	// set -> o[p]=v
	//=========================

	@Import(module = MODULE, js = "(o,p,v)=>o[p]=v")
	private static native void set0(Object peer, @Nonnull DOMString propName, Object value);

	public static void set(@Nonnull Object peer, @Nonnull String propName, Object value) {
		set0(peer, param(propName), value);
	}

	public static void set(@Nonnull Object peer, @Nonnull String propName, String value) {
		set0(peer, param(propName), param(value));
	}

	//=========================
	// delete -> delete o[p]
	//=========================

	@Import(module = MODULE, js = "(o,p)=>delete o[p]")
	private static native void delete0(Object peer, @Nonnull DOMString propName);

	public static void delete(Object peer, String propName) {
		delete0(peer, param(propName));
	}

	//=========================
	// in -> p in o
	//=========================

	@Import(module = MODULE, js = "(o,p)=>p in o")
	private static native boolean in0(Object peer, @Nonnull DOMString propName);

	public static boolean in(Object peer, String propName) {
		return in0(peer, param(propName));
	}

	//=========================
	// invoke -> o[m](p1,p2,...)
	//=========================

	@Import(module = MODULE, js = "(o,m)=>o[m]()")
	private static native <T> T invoke0(Object peer, DOMString methodName);

	@Import(module = MODULE, js = "(o,m,p1)=>o[m](p1)")
	private static native <T> T invoke1(Object peer, DOMString methodName, Object param1);

	@Import(module = MODULE, js = "(o,m,p1,p2)=>o[m](p1,p2)")
	private static native <T> T invoke2(Object peer, DOMString methodName, Object param1, Object param2);

	@Import(module = MODULE, js = "(o,m,p1,p2,p3)=>o[m](p1,p2,p3)")
	private static native <T> T invoke3(Object peer, DOMString methodName, Object param1, Object param2, Object param3);

	//==========
	// No Param

	public static <T> T invoke(Object peer, String methodName) {
		return invoke0(peer, param(methodName));
	}

	//==========
	// 1 Param

	public static <T> T invoke(Object peer, String methodName, int param1) {
		return invoke1(peer, param(methodName), param1);
	}

	public static <T> T invoke(Object peer, String methodName, double param1) {
		return invoke1(peer, param(methodName), param1);
	}

	public static <T> T invoke(Object peer, String methodName, boolean param1) {
		return invoke1(peer, param(methodName), param1);
	}

	// TODO mudar pra String
	public static <T> T invoke(Object peer, String methodName, DOMString param1) {
		return invoke1(peer, param(methodName), param1);
	}

	// TODO mudar pra Peered
	public static <T> T invoke(Object peer, String methodName, Object param1) {
		return invoke1(peer, param(methodName), param1);
	}

	//==========
	// 2 Param


	public static <T> T invoke(Object peer, String methodName, Object param1, Object param2) {
		return invoke2(peer, param(methodName), param1, param2);
	}

	//==========
	// 3 Param

	public static <T> T invoke(Object peer, String methodName, Object param1, Object param2, Object param3) {
		return invoke3(peer, param(methodName), param1, param2, param3);
	}

	//=========================
	// TODO Callbacks
	//=========================

	//=========================
	// Other Stuff
	//=========================

	@Import(module = MODULE, js = "()=>window")
	public static native Object window();

	@Import(module = MODULE, js = "()=>document")
	public static native Object document();

	@Import(module = MODULE, js = "()=>console")
	public static native Object console();


}
