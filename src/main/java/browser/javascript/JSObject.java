/*
 * Copyright 2019 - 2022 Volker Berlin (i-net software)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package browser.javascript;

import browser.dom.Wrap;

public class JSObject extends Peered {

	public JSObject() {
		super(JS.newObject());
	}

	public JSObject(Object peer) {
		super(peer);
	}

	public String getString(String propName) {
		return Wrap.String(JS.get(peer, propName));
	}

	public int getInt(String propName) {
		return JS.getInt(peer, propName);
	}

	public double getDouble(String propName) {
		return JS.getDouble(peer, propName);
	}

	public boolean getBoolean(String propName) {
		return JS.getBoolean(peer, propName);
	}

	public Object getRaw(String propName) {
		return JS.get(peer, propName);
	}

	public JSObject getJSObject(String propName) {
		return new JSObject(JS.get(peer, propName));
	}

	public void set(String propName, String value) {
		JS.set(peer, propName, value);
	}

	public void set(String propName, int value) {
		JS.set(peer, propName, value);
	}

	public void set(String propName, double value) {
		JS.set(peer, propName, value);
	}

	public void set(String propName, boolean value) {
		JS.set(peer, propName, value);
	}

	public void set(String propName, Peered value) {
		JS.set(peer, propName, value.peer);
	}

	public boolean has(String propName) {
		return JS.in(peer, propName);
	}

	public void remove(String propName) {
		JS.delete(peer, propName);
	}

}
