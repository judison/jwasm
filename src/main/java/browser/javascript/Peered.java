package browser.javascript;

public class Peered {

	/**
	 * Native object from JavaScript.
	 */
	public final Object peer;

	public Peered(Object peer) {
		this.peer = peer;
	}

}
