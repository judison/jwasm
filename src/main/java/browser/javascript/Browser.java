package browser.javascript;

import browser.dom.Console;
import browser.dom.Document;
import browser.dom.Window;
import browser.dom.Wrap;

public class Browser {

	public static final Window window = Wrap.Window(JS.window());
	public static final Document document = Wrap.Document(JS.document());
	public static final Console console = Wrap.Console(JS.console());

}
