package browser.dom;

import browser.javascript.Peered;

/**
 * <a href="https://developer.mozilla.org/en-US/docs/Web/API/Event">Online Doc</a>
 */
public class Event extends Peered {

	/**
	 * Create a Java instance as wrapper of the JavaScript object.
	 *
	 * @param peer the native JavaScript object
	 */
	public Event(Object peer) {
		super(peer);
	}

}
