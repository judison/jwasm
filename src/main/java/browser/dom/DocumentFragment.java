package browser.dom;

import browser.javascript.JS;

/**
 * <p>The <strong><code>DocumentFragment</code></strong> interface represents a minimal document object that has no parent.
 * It is used as a lightweight version of {@link Document} that stores a segment of a document structure comprised of nodes just like a standard document. The key difference is due to the fact that the document fragment isn't part of the active document tree structure. Changes made to the fragment don't affect the document.</p>
 */
@SuppressWarnings({"unused", "DanglingJavadoc"})
public class DocumentFragment extends Node {

	protected DocumentFragment(Object peer) {
		super(peer);
	}

	//=========================
	// Properties
	//=========================

	/**
	 * <p>The <strong><code>Document.childElementCount</code></strong> read-only property
	 * returns the number of child elements of a <code>DocumentFragment</code>.
	 * To get the number of children of a specific element, see {@link Element#childElementCount}.</p>
	 * <h2>Value</h2>
	 * <p>A number representing the number of children of the element.</p>
	 */
	public int getChildElementCount() {
		return JS.getInt(peer, "childElementCount");
	}

	/**
	 * <p>The read-only <strong><code>children</code></strong> property returns a live {@link HTMLCollection}
	 * which contains all of the child {@link Element} of the document fragment upon which it was called.</p>
	 * <h2>Value</h2>
	 * <p>An  domxref(&quot;HTMLCollection&quot;)  which is a live, ordered collection of the DOM
	 * elements which are children of the document fragment. You can access the
	 * individual child nodes in the collection by using either the
	 * {@link HTMLCollection.item()} method on the collection, or by using
	 * JavaScript array-style notation.
	 * If the document fragment has no element children, then <code>children</code> is an empty list with a
	 * <code>length</code> of <code>0</code>.</p>
	 */
	public HTMLCollection getChildren() {
		return Wrap.HTMLCollection(JS.get(peer, "children"));
	}

	/**
	 * <p>The <strong><code>DocumentFragment.firstElementChild</code></strong> read-only property
	 * returns the document fragment's first child {@link Element}, or <code>null</code> if there
	 * are no child elements.</p>
	 * <h2>Value</h2>
	 * <p>An {@link Element} that is the first child <code>Element</code> of the object, or <code>null</code> if there are none.</p>
	 */
	public Element getFirstElementChild() {
		return Wrap.Element(JS.get(peer, "firstElementChild"));
	}

	/**
	 * <p>The <strong><code>DocumentFragment.lastElementChild</code></strong> read-only property
	 * returns the document fragment's last child {@link Element}, or <code>null</code> if there
	 * are no child elements.</p>
	 * <h2>Value</h2>
	 * <p>An {@link Element} that is the last child <code>Element</code> of the object, or <code>null</code> if there are none.</p>
	 */
	public Element getLastElementChild() {
		return Wrap.Element(JS.get(peer, "lastElementChild"));
	}

	//=========================
	// Methods
	//=========================

	/**
	 * <p>The <strong><code>DocumentFragment.append()</code></strong> method
	 * inserts a set of {@link Node} objects or string objects after
	 * the last child of the document fragment. String objects
	 * are inserted as equivalent {@link Text} nodes.
	 * This method appends a child to a <code>DocumentFragment</code>. To append to an arbitrary element in the tree, see {@link Element#append()}.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>param1</code>, …, <code>paramN</code>
	 * <ul>
	 * <li>: A set of {@link Node} or string objects to insert.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>None (<code>undefined</code>).</p>
	 */
	//TODO check it
//	public void append() {
//		JS.invoke(peer, "append");
//	}

	/**
	 * <p>The <strong><code>DocumentFragment.prepend()</code></strong> method
	 * inserts a set of {@link Node} objects or string objects before
	 * the first child of the document fragment. String objects
	 * are inserted as equivalent {@link Text} nodes.
	 * This method prepends a child to a <code>DocumentFragment</code>. To prepend to an arbitrary element in the tree, see {@link Element#prepend()}.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>param1</code>, …, <code>paramN</code>
	 * <ul>
	 * <li>: A set of {@link Node} or string objects to insert.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>None (<code>undefined</code>).</p>
	 */
	//TODO check it
//	public void prepend() {
//		JS.invoke(peer, "prepend");
//	}

	/**
	 * <p>The <strong><code>DocumentFragment.querySelector()</code></strong> method returns the
	 * first element, or <code>null</code> if no matches are found, within the
	 * {@link DocumentFragment} (using depth-first pre-order traversal of the
	 * document's nodes) that matches the specified group of selectors.
	 * If the selector matches an ID and this ID is erroneously used several times in the
	 * document, it returns the first matching element.
	 * If the selectors specified in parameter are invalid a {@link DOMException} with
	 * a <code>SYNTAX_ERR</code> value is raised.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>selectors</code>
	 * <ul>
	 * <li>: A string containing one or more CSS selectors separated by
	 * commas.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>An {@link Element} object representing the first element in the document
	 * that matches the specified set of CSS selectors, or <code>null</code> is returned if there are no matches.</p>
	 */
	public Element querySelector(String selectors) {
		return Wrap.Element(JS.invoke(peer, "querySelector", JS.param(selectors)));
	}

	/**
	 * <p>The <strong><code>DocumentFragment.querySelectorAll()</code></strong> method returns a
	 * {@link NodeList} of elements within the {@link DocumentFragment} (using
	 * depth-first pre-order traversal of the document's nodes) that matches the specified
	 * group of selectors.
	 * If the selectors specified in parameter are invalid a {@link DOMException} with
	 * a <code>SYNTAX_ERR</code> value is raised.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>selectors</code>
	 * <ul>
	 * <li>: A string containing one or more CSS selectors separated by
	 * commas.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>A non-live {@link NodeList} containing one {@link Element} object for
	 * each element that matches at least one of the specified selectors or an empty
	 * {@link NodeList} in case of no matches.</p>
	 */
	public NodeList querySelectorAll(String selectors) {
		return Wrap.NodeList(JS.invoke(peer, "querySelectorAll", JS.param(selectors)));
	}

	/**
	 * <p>The <strong><code>DocumentFragment.replaceChildren()</code></strong> method replaces the
	 * existing children of a <code>DocumentFragment</code> with a specified new set of children. These
	 * can be string or {@link Node} objects.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>param1</code>, …, <code>paramN</code>
	 * <ul>
	 * <li>: A set of {@link Node} or string objects to replace the
	 * <code>DocumentFragment</code>'s existing children with. If no replacement objects are
	 * specified, then the <code>DocumentFragment</code> is emptied of all child nodes.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>None (<code>undefined</code>).</p>
	 */
	//TODO check it
//	public void replaceChildren() {
//		JS.invoke(peer, "replaceChildren");
//	}

}
