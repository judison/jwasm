package browser.dom;

import browser.javascript.JS;
import browser.javascript.JSPromisse;

/**
 * <p>The <strong><code>HTMLVideoElement</code></strong> interface provides special properties and methods for manipulating video objects. It also inherits properties and methods of {@link HTMLMediaElement} and {@link HTMLElement}.
 * The list of supported media formats varies from one browser to the other. You should either provide your video in a single format that all the relevant browsers supports, or provide multiple video sources in enough different formats that all the browsers you need to support are covered.</p>
 */
@SuppressWarnings({"unused", "JavadocReference"})
public class HTMLVideoElement extends HTMLMediaElement {

	HTMLVideoElement(Object peer) {
		super(peer);
	}

	//=========================
	// Properties
	//=========================

	/**
	 * <p>The {@link HTMLVideoElement}
	 * <strong><code>autoPictureInPicture</code></strong> property reflects the HTML
	 * attribute indicating whether the video should enter or leave picture-in-picture mode
	 * automatically.</p>
	 * <h2>Value</h2>
	 * <p>A boolean value that is <code>true</code> if the video should enter or
	 * leave picture-in-picture mode automatically when changing tab and/or application.</p>
	 */
	public boolean isAutoPictureInPicture() {
		return JS.getBoolean(peer, "autoPictureInPicture");
	}

	/**
	 * <p>The {@link HTMLVideoElement}
	 * <strong><code>autoPictureInPicture</code></strong> property reflects the HTML
	 * attribute indicating whether the video should enter or leave picture-in-picture mode
	 * automatically.</p>
	 * <h2>Value</h2>
	 * <p>A boolean value that is <code>true</code> if the video should enter or
	 * leave picture-in-picture mode automatically when changing tab and/or application.</p>
	 */
	public void setAutoPictureInPicture(boolean value) {
		JS.set(peer, "autoPictureInPicture", value);
	}

	/**
	 * <p>The {@link HTMLVideoElement}
	 * <strong><code>disablePictureInPicture</code></strong> property reflects the HTML
	 * attribute indicating whether the user agent should suggest the
	 * picture-in-picture feature to users, or request it automatically.</p>
	 * <h2>Value</h2>
	 * <p>A boolean value that is <code>true</code> if the user agent should
	 * suggest that feature to users.</p>
	 */
	public boolean isDisablePictureInPicture() {
		return JS.getBoolean(peer, "disablePictureInPicture");
	}

	/**
	 * <p>The {@link HTMLVideoElement}
	 * <strong><code>disablePictureInPicture</code></strong> property reflects the HTML
	 * attribute indicating whether the user agent should suggest the
	 * picture-in-picture feature to users, or request it automatically.</p>
	 * <h2>Value</h2>
	 * <p>A boolean value that is <code>true</code> if the user agent should
	 * suggest that feature to users.</p>
	 */
	public void setDisablePictureInPicture(boolean value) {
		JS.set(peer, "disablePictureInPicture", value);
	}

	// msHorizontalMirror NON-STANDARD

	// msIsBoxed NON-STANDARD

	// msIsLayoutOptimalForPlayback NON-STANDARD

	// msIsStereo3D NON-STANDARD

	// msSetVideoRectangle NON-STANDARD

	// msStereo3DPackingMode NON-STANDARD

	// msStereo3DRenderMode NON-STANDARD

	// msZoom NON-STANDARD

	// onMSVideoFormatChanged NON-STANDARD

	// onMSVideoFrameStepCompleted NON-STANDARD

	// onMSVideoOptimalLayoutChanged NON-STANDARD

	/**
	 * <p>The {@link HTMLVideoElement} interface's read-only <strong><code>videoHeight</code></strong> property indicates the <a href="#about_intrinsic_width_and_height">intrinsic height</a> of the video, expressed in CSS pixels.
	 * In simple terms, this is the height of the media in its natural size.</p>
	 * <h2>Value</h2>
	 * <p>An integer value specifying the intrinsic height of the video in CSS pixels.
	 * If the element's {@link HTMLMediaElement.readyState} is <code>HTMLMediaElement.HAVE_NOTHING</code>, then the value of this property is 0, because neither video nor poster frame size information is yet available.</p>
	 */
	public int getVideoHeight() {
		return JS.getInt(peer, "videoHeight");
	}

	/**
	 * <p>The {@link HTMLVideoElement} interface's read-only <strong><code>videoWidth</code></strong> property indicates the {@link HTMLVideoElement/videoHeight#about_intrinsic_width_and_height} of the video, expressed in CSS pixels.
	 * In simple terms, this is the width of the media in its natural size.
	 * See {@link HTMLVideoElement/videoHeight#about_intrinsic_width_and_height} for more details.</p>
	 * <h2>Value</h2>
	 * <p>An integer value specifying the intrinsic width of the video in CSS pixels.
	 * If the element's {@link HTMLMediaElement.readyState} is <code>HTMLMediaElement.HAVE_NOTHING</code>, then the value of this property is 0, because neither video nor poster frame size information is yet available.</p>
	 */
	public int getVideoWidth() {
		return JS.getInt(peer, "videoWidth");
	}

	//=========================
	// Methods
	//=========================

	/**
	 * <p>APIRef(&quot;HTML DOM&quot;)
	 * The <strong>{@link HTMLVideoElement}</strong> method
	 * <strong><code>getVideoPlaybackQuality()</code></strong> creates and returns a
	 * {@link VideoPlaybackQuality} object containing metrics including how many
	 * frames have been lost.
	 * The data returned can be used to evaluate the quality of the video stream.</p>
	 * <h3>Parameters</h3>
	 * <p>None.</p>
	 * <h3>Return value</h3>
	 * <p>A {@link VideoPlaybackQuality} object providing information about the video
	 * element's current playback quality.</p>
	 */
	public VideoPlaybackQuality getVideoPlaybackQuality() {
		return new VideoPlaybackQuality(JS.invoke(peer, "getVideoPlaybackQuality"));
	}

	// msFrameStep NON-STANDARD

	// msInsertVideoEffect NON-STANDARD

	/**
	 * <p>APIRef(&quot;HTML DOM&quot;)
	 * The <strong>{@link HTMLVideoElement}</strong> method
	 * <strong><code>requestPictureInPicture()</code></strong> issues an asynchronous request
	 * to display the video in picture-in-picture mode.
	 * It's not guaranteed that the video will be put into picture-in-picture. If permission
	 * to enter that mode is granted, the returned <code>Promise</code> will resolve and the
	 * video will receive a {{domxref(&quot;HTMLVideoElement.enterpictureinpicture_event&quot;,
	 * &quot;enterpictureinpicture&quot;)}} event to let it know that it's now in picture-in-picture.</p>
	 * <h3>Parameters</h3>
	 * <p>None.</p>
	 * <h3>Return value</h3>
	 * <p>A <code>Promise</code> that will resolve to a {@link PictureInPictureWindow}
	 * object that can be used to listen when a user will resize that floating window.</p>
	 */
	public JSPromisse requestPictureInPicture() {
		return new JSPromisse(JS.invoke(peer, "requestPictureInPicture"));
	}

}
