package browser.dom;

/**
 * <p><code>HTML DOM</code>
 * The <strong><code>HTMLDListElement</code></strong> interface provides special properties (beyond those of the regular {@link HTMLElement} interface it also has available to it by inheritance) for manipulating definition list (<code>dl</code>) elements.</p>
 */
public class HTMLDListElement extends HTMLElement {

	HTMLDListElement(Object peer) {
		super(peer);
	}

	//=========================
	// Properties
	//=========================

	//=========================
	// Methods
	//=========================

}
