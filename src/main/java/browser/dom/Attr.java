package browser.dom;

import browser.javascript.JS;

/**
 * <p>The <strong><code>Attr</code></strong> interface represents one of an element's attributes as an object. In most situations, you will directly retrieve the attribute value as a string (e.g., {@link Element#getAttribute()}), but certain functions (e.g., {@link Element#getAttributeNode()}) or means of iterating return <code>Attr</code> instances.
 * The core idea of an object of type <code>Attr</code> is the association between a <em>name</em> and a <em>value</em>. An attribute may also be part of a <em>namespace</em> and, in this case, it also has a URI identifying the namespace, and a prefix that is an abbreviation for the namespace.
 * The name is deemed <em>local</em> when it ignores the eventual namespace prefix and deemed <em>qualified</em> when it includes the prefix of the namespace, if any, separated from the local name by a colon (<code>:</code>). We have three cases: an attribute outside of a namespace, an attribute inside a namespace without a prefix defined, an attribute inside a namespace with a prefix:
 * | Attribute | Namespace name | Namespace prefix | Attribute local name | Attribute qualified name |
 * | --------- | -------------- | ---------------- | -------------------- | ------------------------ |
 * | <code>myAttr</code>  | <em>none</em>         | <em>none</em>           | <code>myAttr</code>             | <code>myAttr</code>                 |
 * | <code>myAttr</code>  | <code>mynamespace</code>  | <em>none</em>           | <code>myAttr</code>             | <code>myAttr</code>                 |
 * | <code>myAttr</code>  | <code>mynamespace</code>  | <code>myns</code>           | <code>myAttr</code>             | <code>myns:myAttr</code>            |</p>
 * <blockquote>
 * <p><strong>Note:</strong> This interface represents only attributes present in the tree representation of the {@link Element&quot;)}}, being a SVG, an HTML or a MathML element. It doesn't represent the <em>property</em> of an interface associated with such element, such as {@link HTMLTableElement} for a <code>table</code> element. (See {{Glossary(&quot;Attribute} for more information about attributes and how they are <em>reflected</em> into properties.)</p>
 * </blockquote>
 */
@SuppressWarnings("unused")
public class Attr extends Node {

	protected Attr(Object peer) {
		super(peer);
	}

	//=========================
	// Properties
	//=========================

	/**
	 * <p>The read-only <strong><code>localName</code></strong> property of the {@link Attr} interface returns the <em>local part</em> of the <em>qualified name</em> of an attribute, that is the name of the attribute, stripped from any namespace in front of it. For example, if the qualified name is <code>xml:lang</code>, the returned local name is <code>lang</code>, if the element supports that namespace.
	 * The local name is always in lower case, whatever case at the attribute creation.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> HTML only supports a fixed set of namespaces on SVG and MathML elements. These are <code>xml</code> (for the <code>xml:lang</code> attribute), <code>xlink</code> (for the <code>xlink:href</code>, <code>xlink:show</code>, <code>xlink:target</code> and <code>xlink:title</code> attributes) and <code>xpath</code>.</p>
	 * <p>That means that the local name of an attribute of an HTML element is always be equal to its qualified name: Colons are treated as regular characters. In XML, like in SVG or MathML, the colon denotes the end of the prefix and what is before is the namespace; the local name may be different from the qualified name.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A string representing the local part of the attribute's qualified name.</p>
	 */
	public String getLocalName() {
		return Wrap.String(JS.get(peer, "localName"));
	}

	/**
	 * <p>The read-only <strong><code>name</code></strong> property of the {@link Attr} interface returns the <em>qualified name</em> of an attribute, that is the name of the attribute, with the namespace prefix, if any, in front of it. For example, if the local name is <code>lang</code> and the namespace prefix is <code>xml</code>, the returned qualified name is <code>xml:lang</code>.
	 * The qualified name is always in lower case, whatever case at the attribute creation.</p>
	 * <h2>Value</h2>
	 * <p>A string representing the attribute's qualified name.</p>
	 */
	public String getName() {
		return Wrap.String(JS.get(peer, "name"));
	}

	/**
	 * <p>The read-only <strong><code>namespaceURI</code></strong> property of the {@link Attr} interface returns the namespace URI of the attribute,
	 * or <code>null</code> if the element is not in a namespace.
	 * The namespace URI is set at the {@link Attr} creation and cannot be changed.
	 * An attribute with a namespace can be created using {@link Element#setAttributeNS()}.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> an attribute does not inherit its namespace from the element it is attached to.
	 * If an attribute is not explicitly given a namespace, it has no namespace.
	 * The browser does not handle or enforce namespace validation per se. It is up to the JavaScript
	 * application to do any necessary validation. Note, too, that the namespace prefix, once it
	 * is associated with a particular attribute node, cannot be changed.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A string containing the URI of the namespace, or <code>null</code> if the attribute is not in a namespace.</p>
	 */
	public String getNamespaceURI() {
		return Wrap.String(JS.get(peer, "namespaceURI"));
	}

	/**
	 * <p>The read-only <strong><code>ownerElement</code></strong> property of the {@link Attr} interface returns the {@link Element} the attribute belongs to.</p>
	 * <h2>Value</h2>
	 * <p>The {@link Element} the attribute belongs to, or <code>null</code> if the attribute is not linked to an element.</p>
	 */
	public String getOwnerElement() {
		return Wrap.String(JS.get(peer, "ownerElement"));
	}

	/**
	 * <p>The read-only <strong><code>prefix</code></strong> property of the {@link Attr} returns the namespace prefix of the attribute, or <code>null</code> if no prefix is specified.
	 * The prefix is always in lower case, whatever case is used at the attribute creation.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> Only XML supports namespaces. HTML does not. That means that the prefix of an attribute of an HTML element will always be <code>null</code>.
	 * Also, only the <code>xml</code> (for the <code>xml:lang</code> attribute), <code>xlink</code> (for the <code>xlink:href</code>, <code>xlink:show</code>, <code>xlink:target</code> and <code>xlink:title</code> attributes) and <code>xpath</code> namespaces are supported, and only on SVG and MathML elements.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A string containing the prefix of the namespace the attribute belongs too. If none, it returns <code>null</code>.</p>
	 */
	public String getPrefix() {
		return Wrap.String(JS.get(peer, "prefix"));
	}

	// specified DEPRECATED

	/**
	 * <p>The <strong><code>value</code></strong> property of the {@link Attr} interface contains the value of the attribute.</p>
	 * <h2>Value</h2>
	 * <p>A string representing the attribute value.</p>
	 */
	public String getValue() {
		return Wrap.String(JS.get(peer, "value"));
	}

	/**
	 * <p>The <strong><code>value</code></strong> property of the {@link Attr} interface contains the value of the attribute.</p>
	 * <h2>Value</h2>
	 * <p>A string representing the attribute value.</p>
	 */
	public void setValue(String value) {
		JS.set(peer, "value", JS.param(value));
	}

	//=========================
	// Methods
	//=========================

}
