package browser.dom;

import browser.javascript.JS;

/**
 * <p>The <strong><code>CharacterData</code></strong> abstract interface represents a {@link Node&quot;)}} object that contains characters# This is an abstract interface, meaning there aren't any objects of type <code>CharacterData</code>: it is implemented by other interfaces like {@link Text}, {{domxref(&quot;Comment}, {@link CDATASection}, or {@link ProcessingInstruction}, which aren't abstract.</p>
 */
@SuppressWarnings({"unused", "DanglingJavadoc"})
public class CharacterData extends Node {

	protected CharacterData(Object peer) {
		super(peer);
	}

	//=========================
	// Properties
	//=========================

	/**
	 * <p>The <strong><code>data</code></strong> property of the {@link CharacterData} interface represent the value of the current object's data.</p>
	 * <h2>Value</h2>
	 * <p>A string with the character information contained in the {@link CharacterData} node.</p>
	 */
	public String getData() {
		return Wrap.String(JS.get(peer, "data"));
	}

	/**
	 * <p>The <strong><code>data</code></strong> property of the {@link CharacterData} interface represent the value of the current object's data.</p>
	 * <h2>Value</h2>
	 * <p>A string with the character information contained in the {@link CharacterData} node.</p>
	 */
	public void setData(String value) {
		JS.set(peer, "data", JS.param(value));
	}

	/**
	 * <p>The read-only <strong><code>CharacterData.length</code></strong> property
	 * returns the number of characters in the contained data, as a positive integer.</p>
	 * <h2>Value</h2>
	 * <p>A positive integer with the length of the {@link CharacterData#getData()} string.</p>
	 */
	public int getLength() {
		return JS.getInt(peer, "length");
	}

	/**
	 * <p>The read-only <strong><code>nextElementSibling</code></strong> property of the {@link CharacterData} interface
	 * returns the first {@link Element} node following the specified one in its parent's
	 * children list, or <code>null</code> if the specified element is the last one in the list.</p>
	 * <h2>Value</h2>
	 * <p>A {@link Element} object, or <code>null</code> if no sibling has been found.</p>
	 */
	public String getNextElementSibling() {
		return Wrap.String(JS.get(peer, "nextElementSibling"));
	}

	/**
	 * <p>The read-only <strong><code>previousElementSibling</code></strong> of the {@link CharacterData} interface
	 * returns the first {@link Element} before the current node in its parent's children list,
	 * or <code>null</code> if there is none.</p>
	 * <h2>Value</h2>
	 * <p>A {@link Element} object, or <code>null</code> if no sibling has been found.</p>
	 */
	public String getPreviousElementSibling() {
		return Wrap.String(JS.get(peer, "previousElementSibling"));
	}

	//=========================
	// Methods
	//=========================

	/**
	 * <p>The <strong><code>after()</code></strong> method of the {@link CharacterData} interface
	 * inserts a set of {@link Node} objects or strings in the children list of the
	 * object's parent, just after the object itself.
	 * Strings are inserted as {@link Text&quot;)}} nodes; the string is being passed as argument to the {{domxref(&quot;Text/Text} constructor.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>nodes</code>
	 * <ul>
	 * <li>: A set of {@link Node} or strings to insert.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	//TODO how to?
//	public void after(Node... nodes) {
//		JS.invoke(peer, "after", nodes);
//	}

	/**
	 * <p>The <strong><code>appendData()</code></strong> method of the {@link CharacterData} interface
	 * adds the provided data to the end of the node's current data.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>data</code>
	 * <ul>
	 * <li>: The data to append to the current node.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>None.</p>
	 */
	public void appendData(String data) {
		JS.invoke(peer, "appendData", JS.param(data));
	}

	/**
	 * <p>The <strong><code>before()</code></strong> method of the {@link CharacterData} interface
	 * inserts a set of {@link Node} objects and strings
	 * in the children list of the <code>CharacterData</code>'s parent, just before the <code>CharacterData</code> node.
	 * Strings are inserted as {@link Text&quot;)}} nodes; the string is being passed as argument to the {{domxref(&quot;Text/Text} constructor.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>nodes</code>
	 * <ul>
	 * <li>: A set of {@link Node} or strings to insert.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	//TODO "set of"
//	public void before() {
//		JS.invoke(peer, "before");
//	}

	/**
	 * <p>The <strong><code>deleteData()</code></strong> method of the {@link CharacterData} interface
	 * removes all or part of the data from this <code>CharacterData</code> node.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>offset</code>
	 * <ul>
	 * <li>: The number of bytes from the start of the data to remove from.
	 * <code>0</code> is the first character of the string.</li>
	 * </ul>
	 * </li>
	 * <li><code>count</code>
	 * <ul>
	 * <li>: The number of bytes to remove.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>None.</p>
	 */
	public void deleteData(int offset, int count) {
		JS.invoke(peer, "deleteData", offset, count);
	}

	/**
	 * <p>The <strong><code>insertData()</code></strong> method of the {@link CharacterData} interface
	 * inserts the provided data into this <code>CharacterData</code> node's current data,
	 * at the provided offset from the start of the existing data.
	 * The provided data is spliced into the existing data.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>offset</code>
	 * <ul>
	 * <li>: The offset number of characters to insert the provided data at.
	 * <code>0</code> is the first character of the string.</li>
	 * </ul>
	 * </li>
	 * <li><code>data</code>
	 * <ul>
	 * <li>: The data to insert.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>None.</p>
	 */
	public void insertData(int offset, String data) {
		JS.invoke(peer, "insertData", offset, JS.param(data));
	}

	/**
	 * <p>The <strong><code>remove()</code></strong> method of the {@link CharacterData} removes the text contained in the node.</p>
	 * <h3>Parameters</h3>
	 * <p>None.</p>
	 */
	public void remove() {
		JS.invoke(peer, "remove");
	}

	/**
	 * <p>The <strong><code>replaceData()</code></strong> method of the {@link CharacterData} interface removes a certain number of characters of the existing text in a given <code>CharacterData</code> node and replaces those characters with the text provided.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>offset</code>
	 * <ul>
	 * <li>: The number of characters from the start of the data to insert at.
	 * <code>0</code> is the first character of the string.</li>
	 * </ul>
	 * </li>
	 * <li><code>count</code>
	 * <ul>
	 * <li>: The number of characters to replace with the provided data.</li>
	 * </ul>
	 * </li>
	 * <li><code>data</code>
	 * <ul>
	 * <li>: The data to insert.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>None.</p>
	 */
	public void replaceData(int offset, int count, String data) {
		JS.invoke(peer, "replaceData", offset, count, JS.param(data));
	}

	/**
	 * <p>The <strong><code>replaceWith()</code></strong> method of the {@link CharacterData} interface
	 * replaces this node in the children list of its parent
	 * with a set of {@link Node} objects or string.
	 * Strings are inserted as {@link Text&quot;)}} nodes; the string is being passed as argument to the {{domxref(&quot;Text/Text} constructor.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>nodes</code> optional_inline
	 * <ul>
	 * <li>: A comma-separated list of {@link Node} objects or strings that will replace the current node.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <blockquote>
	 * <p><strong>Note:</strong> If there no argument is passed, this method acts just remove the node from the DOM tree.</p>
	 * </blockquote>
	 * <h3>Return value</h3>
	 * <p>None (<code>undefined</code>).</p>
	 */
	//TODO varargs
//	public void replaceWith() {
//		JS.invoke(peer, "replaceWith");
//	}

	/**
	 * <p>The <strong><code>substringData()</code></strong> method of the {@link CharacterData} interface
	 * returns a portion of the existing data,
	 * starting at the specified index
	 * and extending for a given number of characters afterwards.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>offset</code>
	 * <ul>
	 * <li>: The index of the first character to include in the returned substring.
	 * <code>0</code> is the first character of the string.</li>
	 * </ul>
	 * </li>
	 * <li><code>count</code>
	 * <ul>
	 * <li>: The number of characters to return.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>A string with the substring.</p>
	 */
	public String substringData(int offset, int count) {
		return Wrap.String(JS.invoke(peer, "substringData", offset, count));
	}

}
