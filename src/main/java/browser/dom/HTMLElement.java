package browser.dom;

import browser.javascript.JS;
import browser.javascript.JSObject;

/**
 * <p>The <strong><code>HTMLElement</code></strong> interface represents any HTML element. Some elements directly implement this interface, while others implement it via an interface that inherits it.</p>
 */
@SuppressWarnings({"unused"})
public class HTMLElement extends Element {

	/**
	 * Create a Java instance as wrapper of the DOM object.
	 *
	 * @param peer the native DOM object
	 */
	protected HTMLElement(Object peer) {
		super(peer);
	}

	/**
	 * Create a wrapper for a HTML peer element.
	 *
	 * @param <T>     the return type
	 * @param tagName the tag name of the element
	 * @param peer    the native DOM object
	 * @return the wrapper
	 */
	@SuppressWarnings("unchecked")
	public static <T extends HTMLElement> T createWrapper(String tagName, Object peer) {
		switch (tagName) {
			case "a":
				return (T)new HTMLAnchorElement(peer);
			case "area":
				return (T)new HTMLAreaElement(peer);
			case "button":
				return (T)new HTMLButtonElement(peer);
			case "canvas":
				return (T)new HTMLCanvasElement(peer);
			case "div":
				return (T)new HTMLDivElement(peer);
			default:
				return (T)new HTMLElement(peer);
		}
	}

	public static <T extends HTMLElement> T createWrapper(Object peer) {
		String tagName = Wrap.String(JS.get(peer, "tagName"));
		return createWrapper(tagName, peer);
	}

	//=========================
	// Properties
	//=========================

	/**
	 * <p>The <strong><code>HTMLElement.accessKey</code></strong> property sets the keystroke which a user can press to jump to a given element.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> The <code>HTMLElement.accessKey</code> property is seldom used because of its multiple conflicts with already present key bindings in browsers. To work around this, browsers implement accesskey behavior if the keys are pressed with other &quot;qualifying&quot; keys (such as <kbd>Alt</kbd> + accesskey).</p>
	 * </blockquote>
	 */
	public String getAccessKey() {
		return Wrap.String(JS.get(peer, "accessKey"));
	}

	/**
	 * <p>The <strong><code>HTMLElement.accessKey</code></strong> property sets the keystroke which a user can press to jump to a given element.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> The <code>HTMLElement.accessKey</code> property is seldom used because of its multiple conflicts with already present key bindings in browsers. To work around this, browsers implement accesskey behavior if the keys are pressed with other &quot;qualifying&quot; keys (such as <kbd>Alt</kbd> + accesskey).</p>
	 * </blockquote>
	 */
	public void setAccessKey(String value) {
		JS.set(peer, "accessKey", value);
	}

	/**
	 * <p>The <strong><code>HTMLElement.accessKeyLabel</code></strong>
	 * read-only property returns a string containing the element's
	 * browser-assigned access key (if any); otherwise it returns an empty string.</p>
	 */
	public String getAccessKeyLabel() {
		return Wrap.String(JS.get(peer, "accessKeyLabel"));
	}

	/**
	 * <p>The <strong><code>contentEditable</code></strong> property of
	 * the {@link HTMLElement} interface specifies whether or not the element is
	 * editable.
	 * This enumerated attribute can have the following values:</p>
	 * <ul>
	 * <li>'<code>true</code>' indicates that the element is <code>contenteditable</code>.</li>
	 * <li>'<code>false</code>' indicates that the element cannot be edited.</li>
	 * <li>'<code>inherit</code>' indicates that the element inherits its parent's editable
	 * status.
	 * You can use the {@link #isContentEditable()} property to test the
	 * computed boolean value of this property.</li>
	 * </ul>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public String getContentEditable() {
		return Wrap.String(JS.get(peer, "contentEditable"));
	}

	/**
	 * <p>The <strong><code>contentEditable</code></strong> property of
	 * the {@link HTMLElement} interface specifies whether or not the element is
	 * editable.
	 * This enumerated attribute can have the following values:</p>
	 * <ul>
	 * <li>'<code>true</code>' indicates that the element is <code>contenteditable</code>.</li>
	 * <li>'<code>false</code>' indicates that the element cannot be edited.</li>
	 * <li>'<code>inherit</code>' indicates that the element inherits its parent's editable
	 * status.
	 * You can use the {@link #isContentEditable()} property to test the
	 * computed boolean value of this property.</li>
	 * </ul>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public void setContentEditable(String value) {
		JS.set(peer, "contentEditable", value);
	}

	// contextMenu DEPRECATED

	/**
	 * <p>The <strong><code>dataset</code></strong> read-only property
	 * of the  interface provides read/write access to custom data attributes
	 * (<code>data-*</code>) on elements. It exposes a map of strings
	 * ({@link DOMStringMap}) with an entry for each <code>data-*</code> attribute.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> The <code>dataset</code> property itself can be read, but not directly written.
	 * Instead, all writes must be to the individual properties within the
	 * <code>dataset</code>, which in turn represent the data attributes.
	 * An HTML <code>data-*</code> attribute and its corresponding DOM
	 * <code>dataset.property</code> modify their shared name according to where
	 * they are read or written:</p>
	 * </blockquote>
	 * <ul>
	 * <li>In HTML
	 * <ul>
	 * <li>: The attribute name begins with <code>data-</code>. It can contain only letters,
	 * numbers, dashes (<code>-</code>), periods (<code>.</code>), colons (<code>:</code>),
	 * and underscores (<code>_</code>). Any ASCII capital letters (<code>A</code> to
	 * <code>Z</code>) are converted to lowercase.</li>
	 * </ul>
	 * </li>
	 * <li>In JavaScript
	 * <ul>
	 * <li>: The property name of a custom data attribute is the same as the HTML attribute
	 * without the <code>data-</code> prefix, and removes single dashes (<code>-</code>) for
	 * when to capitalize the property's &quot;camelCased&quot; name.
	 * In addition to the information below, you'll find a how-to guide for using HTML data
	 * attributes in our article <em>Using data attributes</em>.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h2>Value</h2>
	 * <p>A {@link DOMStringMap}.</p>
	 */
	public DOMStringMap getDataset() {
		return Wrap.DOMStringMap(JS.get(peer, "dataset"));
	}

	/**
	 * <p>The <strong><code>HTMLElement.dir</code></strong> property gets or sets the text
	 * writing directionality of the content of the current element.
	 * The text writing directionality of an element is which direction that text goes (for
	 * support of different language systems). Arabic languages and Hebrew are typical
	 * languages using the RTL directionality.
	 * An image can have its <code>dir</code> property set to &quot;<code>rtl</code>&quot; in which case
	 * the HTML attributes <code>title</code> and <code>alt</code> will be formatted and
	 * defined as &quot;<code>rtl</code>&quot;.
	 * When a table has its <code>dir</code> set to &quot;<code>rtl</code>&quot;, the column order is
	 * arranged from right to left.
	 * When an element has its dir set to &quot;<code>auto</code>&quot;, the direction of the element is
	 * determined based on its first strong directionality character, or default to the
	 * directionality of its parent element.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> Browsers might allow users to change the directionality of
	 * and s in order to assist with authoring content. Chrome
	 * and Safari provide a directionality option in the contextual menu of input fields
	 * while Internet Explorer and Edge use the key combinations <kbd>Ctrl</kbd> + <kbd>Left Shift</kbd> and <kbd>Ctrl</kbd> + <kbd>Right Shift</kbd>. Firefox uses <kbd>Ctrl</kbd>/<kbd>Cmd</kbd> + <kbd>Shift</kbd> + <kbd>X</kbd> but does NOT update
	 * the <strong><code>dir</code></strong> attribute value.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>One of the following:</p>
	 * <ul>
	 * <li><code>ltr</code>, for left-to-right;</li>
	 * <li><code>rtl</code>, for right-to-left;</li>
	 * <li><code>auto</code> for specifying that the direction of the element must be determined based on the contents of the element.</li>
	 * </ul>
	 */
	public String getDir() {
		return Wrap.String(JS.get(peer, "dir"));
	}

	/**
	 * <p>The <strong><code>HTMLElement.dir</code></strong> property gets or sets the text
	 * writing directionality of the content of the current element.
	 * The text writing directionality of an element is which direction that text goes (for
	 * support of different language systems). Arabic languages and Hebrew are typical
	 * languages using the RTL directionality.
	 * An image can have its <code>dir</code> property set to &quot;<code>rtl</code>&quot; in which case
	 * the HTML attributes <code>title</code> and <code>alt</code> will be formatted and
	 * defined as &quot;<code>rtl</code>&quot;.
	 * When a table has its <code>dir</code> set to &quot;<code>rtl</code>&quot;, the column order is
	 * arranged from right to left.
	 * When an element has its dir set to &quot;<code>auto</code>&quot;, the direction of the element is
	 * determined based on its first strong directionality character, or default to the
	 * directionality of its parent element.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> Browsers might allow users to change the directionality of
	 * and s in order to assist with authoring content. Chrome
	 * and Safari provide a directionality option in the contextual menu of input fields
	 * while Internet Explorer and Edge use the key combinations <kbd>Ctrl</kbd> + <kbd>Left Shift</kbd> and <kbd>Ctrl</kbd> + <kbd>Right Shift</kbd>. Firefox uses <kbd>Ctrl</kbd>/<kbd>Cmd</kbd> + <kbd>Shift</kbd> + <kbd>X</kbd> but does NOT update
	 * the <strong><code>dir</code></strong> attribute value.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>One of the following:</p>
	 * <ul>
	 * <li><code>ltr</code>, for left-to-right;</li>
	 * <li><code>rtl</code>, for right-to-left;</li>
	 * <li><code>auto</code> for specifying that the direction of the element must be determined based on the contents of the element.</li>
	 * </ul>
	 */
	public void setDir(String value) {
		JS.set(peer, "dir", value);
	}

	/**
	 * <p>The <strong><code>enterKeyHint</code></strong> property is an enumerated property defining
	 * what action label (or icon) to present for the enter key on virtual keyboards.
	 * It reflects the <code>enterkeyhint</code>
	 * HTML global attribute and is an enumerated property, only accepting the following values
	 * as a string:</p>
	 * <ul>
	 * <li><code>'enter'</code> typically indicating inserting a new line.</li>
	 * <li><code>'done'</code> typically meaning there is nothing more to input and the input method editor (IME) will be closed.</li>
	 * <li><code>'go'</code> typically meaning to take the user to the target of the text they typed.</li>
	 * <li><code>'next'</code> typically taking the user to the next field that will accept text.</li>
	 * <li><code>'previous'</code> typically taking the user to the previous field that will accept text.</li>
	 * <li><code>'search'</code> typically taking the user to the results of searching for the text they have typed.</li>
	 * <li><code>'send'</code> typically delivering the text to its target.
	 * If no <code>enterKeyHint</code> value has been specified or if it was set to a different value than the allowed ones, it will return an empty string.</li>
	 * </ul>
	 */
	public String getEnterKeyHint() {
		return Wrap.String(JS.get(peer, "enterKeyHint"));
	}

	/**
	 * <p>The <strong><code>enterKeyHint</code></strong> property is an enumerated property defining
	 * what action label (or icon) to present for the enter key on virtual keyboards.
	 * It reflects the <code>enterkeyhint</code>
	 * HTML global attribute and is an enumerated property, only accepting the following values
	 * as a string:</p>
	 * <ul>
	 * <li><code>'enter'</code> typically indicating inserting a new line.</li>
	 * <li><code>'done'</code> typically meaning there is nothing more to input and the input method editor (IME) will be closed.</li>
	 * <li><code>'go'</code> typically meaning to take the user to the target of the text they typed.</li>
	 * <li><code>'next'</code> typically taking the user to the next field that will accept text.</li>
	 * <li><code>'previous'</code> typically taking the user to the previous field that will accept text.</li>
	 * <li><code>'search'</code> typically taking the user to the results of searching for the text they have typed.</li>
	 * <li><code>'send'</code> typically delivering the text to its target.
	 * If no <code>enterKeyHint</code> value has been specified or if it was set to a different value than the allowed ones, it will return an empty string.</li>
	 * </ul>
	 */
	public void setEnterKeyHint(String value) {
		JS.set(peer, "enterKeyHint", value);
	}

	/**
	 * <p>The {@link HTMLElement} property
	 * <strong><code>hidden</code></strong> is a boolean value which is
	 * <code>true</code> if the element is hidden; otherwise the value is <code>false</code>.
	 * This is quite different from using the CSS property  to control
	 * the visibility of an element.
	 * The <code>hidden</code> property applies to all
	 * presentation modes and should not be used to hide content that is meant to be directly
	 * accessible to the user.
	 * Appropriate use cases for <code>hidden</code> include:</p>
	 * <ul>
	 * <li>Content that isn't yet relevant but may be needed later</li>
	 * <li>Content that was previously needed but is not any longer</li>
	 * <li>Content that is reused by other parts of the page in a template-like fashion</li>
	 * <li>Creating an offscreen canvas as a drawing buffer
	 * Inappropriate use cases include:</li>
	 * <li>Hiding panels in a tabbed dialog box</li>
	 * <li>Hiding content in one presentation while intending it to be visible in others</li>
	 * </ul>
	 * <blockquote>
	 * <p><strong>Note:</strong> Elements that are not <code>hidden</code> must not link to elements which are.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A Boolean which is <code>true</code> if the element is hidden from view; otherwise, the
	 * value is <code>false</code>.</p>
	 */
	public boolean isHidden() {
		return JS.getBoolean(peer, "hidden");
	}

	/**
	 * <p>The {@link HTMLElement} property
	 * <strong><code>hidden</code></strong> is a boolean value which is
	 * <code>true</code> if the element is hidden; otherwise the value is <code>false</code>.
	 * This is quite different from using the CSS property  to control
	 * the visibility of an element.
	 * The <code>hidden</code> property applies to all
	 * presentation modes and should not be used to hide content that is meant to be directly
	 * accessible to the user.
	 * Appropriate use cases for <code>hidden</code> include:</p>
	 * <ul>
	 * <li>Content that isn't yet relevant but may be needed later</li>
	 * <li>Content that was previously needed but is not any longer</li>
	 * <li>Content that is reused by other parts of the page in a template-like fashion</li>
	 * <li>Creating an offscreen canvas as a drawing buffer
	 * Inappropriate use cases include:</li>
	 * <li>Hiding panels in a tabbed dialog box</li>
	 * <li>Hiding content in one presentation while intending it to be visible in others</li>
	 * </ul>
	 * <blockquote>
	 * <p><strong>Note:</strong> Elements that are not <code>hidden</code> must not link to elements which are.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A Boolean which is <code>true</code> if the element is hidden from view; otherwise, the
	 * value is <code>false</code>.</p>
	 */
	public void setHidden(boolean value) {
		JS.set(peer, "hidden", value);
	}

	/**
	 * <p>The {@link HTMLElement} property <strong><code>inert</code></strong> is a boolean value that, when present, makes the browser &quot;ignore&quot; user input events for the element, including focus events and events from assistive technologies. The browser may also ignore page search and text selection in the element. This can be useful when building UIs such as modals where you would want to &quot;trap&quot; the focus inside the modal when it's visible.</p>
	 * <h2>Value</h2>
	 * <p>A Boolean which is <code>true</code> if the element is inert; otherwise, the value is <code>false</code>.</p>
	 */
	public boolean isInert() {
		return JS.getBoolean(peer, "inert");
	}

	/**
	 * <p>The {@link HTMLElement} property <strong><code>inert</code></strong> is a boolean value that, when present, makes the browser &quot;ignore&quot; user input events for the element, including focus events and events from assistive technologies. The browser may also ignore page search and text selection in the element. This can be useful when building UIs such as modals where you would want to &quot;trap&quot; the focus inside the modal when it's visible.</p>
	 * <h2>Value</h2>
	 * <p>A Boolean which is <code>true</code> if the element is inert; otherwise, the value is <code>false</code>.</p>
	 */
	public void setInert(boolean value) {
		JS.set(peer, "inert", value);
	}

	/**
	 * <p>The <strong><code>innerText</code></strong> property of the {@link HTMLElement} interface represents the rendered text content of a node and its descendants.
	 * As a getter, it approximates the text the user would get if they highlighted the contents of the element with the cursor and then copied it to the clipboard.
	 * As a setter this will replace the element's children with the given value, converting any line breaks into  elements.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> <code>innerText</code> is easily confused with {@link Node#getTextContent()}, but there are important differences between the two.
	 * Basically, <code>innerText</code> is aware of the rendered appearance of text, while <code>textContent</code> is not.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A string representing the rendered text content of an element.
	 * If the element itself is not <a href="https://html.spec.whatwg.org/multipage/rendering.html#being-rendered">being rendered</a> (for example, is detached from the document or is hidden from view), the returned value is the same as the {@link Node#getTextContent()} property.</p>
	 * <blockquote>
	 * <p><strong>Warning:</strong> Setting <code>innerText</code> on a node removes <em>all</em> of the node's children
	 * and replaces them with a single text node with the given string value.</p>
	 * </blockquote>
	 */
	public String getInnerText() {
		return Wrap.String(JS.get(peer, "innerText"));
	}

	/**
	 * <p>The <strong><code>innerText</code></strong> property of the {@link HTMLElement} interface represents the rendered text content of a node and its descendants.
	 * As a getter, it approximates the text the user would get if they highlighted the contents of the element with the cursor and then copied it to the clipboard.
	 * As a setter this will replace the element's children with the given value, converting any line breaks into  elements.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> <code>innerText</code> is easily confused with {@link Node#getTextContent()}, but there are important differences between the two.
	 * Basically, <code>innerText</code> is aware of the rendered appearance of text, while <code>textContent</code> is not.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A string representing the rendered text content of an element.
	 * If the element itself is not <a href="https://html.spec.whatwg.org/multipage/rendering.html#being-rendered">being rendered</a> (for example, is detached from the document or is hidden from view), the returned value is the same as the {@link Node#getTextContent()} property.</p>
	 * <blockquote>
	 * <p><strong>Warning:</strong> Setting <code>innerText</code> on a node removes <em>all</em> of the node's children
	 * and replaces them with a single text node with the given string value.</p>
	 * </blockquote>
	 */
	public void setInnerText(String value) {
		JS.set(peer, "innerText", value);
	}

	/**
	 * <p>The <strong><code>HTMLElement.isContentEditable</code></strong> read-only property
	 * returns a boolean value that is <code>true</code> if the contents of the element
	 * are editable; otherwise it returns <code>false</code>.</p>
	 * <h2>Value</h2>
	 * <p>A boolean value.</p>
	 */
	public boolean isContentEditable() {
		return JS.getBoolean(peer, "isContentEditable");
	}

	/**
	 * <p>The <strong><code>HTMLElement.lang</code></strong> property gets or sets the base
	 * language of an element's attribute values and text content.
	 * The language code returned by this property is defined in .
	 * Common examples include &quot;en&quot; for English, &quot;ja&quot; for
	 * Japanese, &quot;es&quot; for Spanish and so on. The default value of this attribute is
	 * <code>unknown</code>. Note that this attribute, though valid at the individual element
	 * level described here, is most often specified for the root element of the document.
	 * This also only works with the <code>lang</code> attribute and not with
	 * <code>xml:lang</code>.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public String getLang() {
		return Wrap.String(JS.get(peer, "lang"));
	}

	/**
	 * <p>The <strong><code>HTMLElement.lang</code></strong> property gets or sets the base
	 * language of an element's attribute values and text content.
	 * The language code returned by this property is defined in .
	 * Common examples include &quot;en&quot; for English, &quot;ja&quot; for
	 * Japanese, &quot;es&quot; for Spanish and so on. The default value of this attribute is
	 * <code>unknown</code>. Note that this attribute, though valid at the individual element
	 * level described here, is most often specified for the root element of the document.
	 * This also only works with the <code>lang</code> attribute and not with
	 * <code>xml:lang</code>.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public void setLang(String value) {
		JS.set(peer, "lang", value);
	}

	/**
	 * <p>The <strong><code>nonce</code></strong> property of the  interface returns the cryptographic number used once that is used by Content Security Policy to determine whether a given fetch will be allowed to proceed.
	 * In later implementations, elements only expose their <code>nonce</code> attribute to scripts (and not to side-channels like CSS attribute selectors).</p>
	 */
	public String getNonce() {
		return Wrap.String(JS.get(peer, "nonce"));
	}

	/**
	 * <p>The <strong><code>nonce</code></strong> property of the  interface returns the cryptographic number used once that is used by Content Security Policy to determine whether a given fetch will be allowed to proceed.
	 * In later implementations, elements only expose their <code>nonce</code> attribute to scripts (and not to side-channels like CSS attribute selectors).</p>
	 */
	public void setNonce(String value) {
		JS.set(peer, "nonce", value);
	}

	/**
	 * <p>The <strong><code>HTMLElement.offsetHeight</code></strong> read-only property returns
	 * the height of an element, including vertical padding and borders, as an integer.
	 * Typically, <code>offsetHeight</code> is a measurement in pixels of the element's CSS
	 * height, including any borders, padding, and horizontal scrollbars (if rendered). It does
	 * not include the height of pseudo-elements such as <code>::before</code> or
	 * <code>::after</code>. For the document body object, the measurement includes total
	 * linear content height instead of the element's CSS height. Floated elements extending
	 * below other linear content are ignored.
	 * If the element is hidden (for example, by setting <code>style.display</code> on the
	 * element or one of its ancestors to <code>&quot;none&quot;</code>), then <code>0</code> is
	 * returned.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> This property will round the value to an integer. If you need a fractional value, use
	 * .</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A number.</p>
	 */
	public double getOffsetHeight() {
		return JS.getDouble(peer, "offsetHeight");
	}

	/**
	 * <p>The <strong><code>HTMLElement.offsetLeft</code></strong> read-only property returns the number of pixels that the <em>upper left corner</em> of the current element is offset to the left within the {@link #getOffsetParent()} node.
	 * For block-level elements, <code>offsetTop</code>, <code>offsetLeft</code>, <code>offsetWidth</code>, and <code>offsetHeight</code> describe the border box of an element relative to the <code>offsetParent</code>.
	 * However, for inline-level elements (such as <strong>span</strong>) that can wrap from one line to the next, <code>offsetTop</code> and <code>offsetLeft</code> describe the positions of the <em>first</em> border box (use {@link Element#getClientRects()} to get its width and height), while <code>offsetWidth</code> and <code>offsetHeight</code> describe the dimensions of the <em>bounding</em> border box (use {@link Element#getBoundingClientRect()} to get its position). Therefore, a box with the left, top, width and height of <code>offsetLeft</code>, <code>offsetTop</code>, <code>offsetWidth</code> and <code>offsetHeight</code> will not be a bounding box for a span with wrapped text.</p>
	 * <h2>Value</h2>
	 * <p>An integer.</p>
	 */
	public int getOffsetLeft() {
		return JS.getInt(peer, "offsetLeft");
	}

	/**
	 * <p>The <strong><code>HTMLElement.offsetParent</code></strong> read-only property returns a
	 * reference to the element which is the closest (nearest in the containment hierarchy)
	 * positioned ancestor element.
	 * A positioned ancestor is either:</p>
	 * <ul>
	 * <li>an element with a non-static position, or</li>
	 * <li><code>td</code>, <code>th</code>, <code>table</code> in case the element itself is static positioned.
	 * If there is no positioned ancestor element, the <code>body</code> is returned.</li>
	 * </ul>
	 * <blockquote>
	 * <p><strong>Note:</strong> <code>offsetParent</code> returns <code>null</code> in the following
	 * situations:</p>
	 * <ul>
	 * <li>The element or its parent element has the <code>display</code> property set to
	 * <code>none</code>.</li>
	 * <li>The element has the <code>position</code> property set to <code>fixed</code>
	 * (Firefox returns <code>&lt;body&gt;</code>).</li>
	 * <li>The element is <code>&lt;body&gt;</code> or <code>&lt;html&gt;</code>.
	 * <code>offsetParent</code> is useful because
	 * {@link #getOffsetTop()} and
	 * {@link #getOffsetLeft()} are relative to its padding edge.</li>
	 * </ul>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>An object reference to the element in which the current element is offset.</p>
	 */
	public String getOffsetParent() {
		return Wrap.String(JS.get(peer, "offsetParent"));
	}

	/**
	 * <p>The <strong><code>HTMLElement.offsetTop</code></strong> read-only property returns the
	 * distance of the outer border of the current element relative to the inner border of
	 * the top of the {@link #getOffsetParent()}, the <em>closest positioned</em>
	 * ancestor element.</p>
	 * <h2>Value</h2>
	 * <p>A number.</p>
	 */
	public double getOffsetTop() {
		return JS.getDouble(peer, "offsetTop");
	}

	/**
	 * <p>The <strong><code>HTMLElement.offsetWidth</code></strong> read-only property returns
	 * the layout width of an element as an integer.
	 * Typically, <code>offsetWidth</code> is a measurement in pixels of the element's CSS
	 * width, including any borders, padding, and vertical scrollbars (if rendered). It does
	 * not include the width of pseudo-elements such as <code>::before</code> or
	 * <code>::after</code>.
	 * If the element is hidden (for example, by setting <code>style.display</code> on the
	 * element or one of its ancestors to <code>&quot;none&quot;</code>), then <code>0</code> is
	 * returned.</p>
	 * <h2>Value</h2>
	 * <p>An integer corresponding to the <code>offsetWidth</code> pixel value of the element. The <code>offsetWidth</code> property is a read-only.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> This property will round the value to an integer. If you need a fractional value, use
	 * .</p>
	 * </blockquote>
	 */
	public int getOffsetWidth() {
		return JS.getInt(peer, "offsetWidth");
	}

	/**
	 * <p>The <strong><code>outerText</code></strong> property of the {@link HTMLElement} interface returns the same value as {@link #getInnerText()}.
	 * When used as a setter it replaces the whole current node with the given text (this differs from <code>innerText</code>, which replaces the content <em>inside</em> the current node).
	 * See {@link #getInnerText()} for more information and examples showing how both properties are used as getters.</p>
	 * <h2>Value</h2>
	 * <p>A string representing the rendered text content of an element and its descendants.
	 * If the element itself is not <a href="https://html.spec.whatwg.org/multipage/rendering.html#being-rendered">being rendered</a> (for example, is detached from the document or is hidden from view), the returned value is the same as the {@link Node#getTextContent()} property.
	 * When used as a setter it replaces the current node with the given text, converting any line breaks into  elements.</p>
	 */
	public String getOuterText() {
		return Wrap.String(JS.get(peer, "outerText"));
	}

	/**
	 * <p>The <strong><code>outerText</code></strong> property of the {@link HTMLElement} interface returns the same value as {@link #getInnerText()}.
	 * When used as a setter it replaces the whole current node with the given text (this differs from <code>innerText</code>, which replaces the content <em>inside</em> the current node).
	 * See {@link #getInnerText()} for more information and examples showing how both properties are used as getters.</p>
	 * <h2>Value</h2>
	 * <p>A string representing the rendered text content of an element and its descendants.
	 * If the element itself is not <a href="https://html.spec.whatwg.org/multipage/rendering.html#being-rendered">being rendered</a> (for example, is detached from the document or is hidden from view), the returned value is the same as the {@link Node#getTextContent()} property.
	 * When used as a setter it replaces the current node with the given text, converting any line breaks into  elements.</p>
	 */
	public void setOuterText(String value) {
		JS.set(peer, "outerText", value);
	}

	/**
	 * <p>The <strong><code>style</code></strong> read-only property returns the <em>inline</em> style of an element in the form of a {@link CSSStyleDeclaration} object that contains a list of all styles properties for that element with values assigned for the attributes that are defined in the element's inline <code>style</code> attribute.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> See the CSS Properties Reference for a list of the CSS properties accessible via <code>style</code>. The <code>style</code> property has the same (and highest) priority in the CSS cascade as an inline style declaration set via the <code>style</code> attribute.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A {@link CSSStyleDeclaration} object, with the following properties:</p>
	 * <ul>
	 * <li>computed flag
	 * <ul>
	 * <li>: Unset.</li>
	 * </ul>
	 * </li>
	 * <li>parent CSS rule
	 * <ul>
	 * <li>: Null.</li>
	 * </ul>
	 * </li>
	 * <li>owner node
	 * <ul>
	 * <li>: <code>this</code></li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public CSSStyleDeclaration style() {
		return new CSSStyleDeclaration(JS.get(peer, "style"));
	}

	/**
	 * <p>The <strong><code>tabIndex</code></strong> property of the
	 * interface represents the tab order of the current element.
	 * Tab order is as follows:</p>
	 * <ol>
	 * <li>Elements with a positive <code>tabIndex</code>. Elements that have identical
	 * <code>tabIndex</code> values should be navigated in the order they appear. Navigation
	 * proceeds from the lowest <code>tabIndex</code> to the highest <code>tabIndex</code>.</li>
	 * <li>Elements that do not support the <code>tabIndex</code> attribute or support it and
	 * assign <code>tabIndex</code> to <code>0</code>, in the order they appear.
	 * Elements that are disabled do not participate in the tabbing order.
	 * Values don't need to be sequential, nor must they begin with any particular value. They
	 * may even be negative, though each browser trims very large values.</li>
	 * </ol>
	 * <h2>Value</h2>
	 * <p>An integer.</p>
	 */
	public int getTabIndex() {
		return JS.getInt(peer, "tabIndex");
	}

	/**
	 * <p>The <strong><code>tabIndex</code></strong> property of the
	 * interface represents the tab order of the current element.
	 * Tab order is as follows:</p>
	 * <ol>
	 * <li>Elements with a positive <code>tabIndex</code>. Elements that have identical
	 * <code>tabIndex</code> values should be navigated in the order they appear. Navigation
	 * proceeds from the lowest <code>tabIndex</code> to the highest <code>tabIndex</code>.</li>
	 * <li>Elements that do not support the <code>tabIndex</code> attribute or support it and
	 * assign <code>tabIndex</code> to <code>0</code>, in the order they appear.
	 * Elements that are disabled do not participate in the tabbing order.
	 * Values don't need to be sequential, nor must they begin with any particular value. They
	 * may even be negative, though each browser trims very large values.</li>
	 * </ol>
	 * <h2>Value</h2>
	 * <p>An integer.</p>
	 */
	public void setTabIndex(int value) {
		JS.set(peer, "tabIndex", value);
	}

	/**
	 * <p>The <strong><code>HTMLElement.title</code></strong> property
	 * represents the title of the element: the text usually displayed in a 'tooltip' popup
	 * when the mouse is over the node.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public String getTitle() {
		return Wrap.String(JS.get(peer, "title"));
	}

	/**
	 * <p>The <strong><code>HTMLElement.title</code></strong> property
	 * represents the title of the element: the text usually displayed in a 'tooltip' popup
	 * when the mouse is over the node.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public void setTitle(String value) {
		JS.set(peer, "title", value);
	}

	//=========================
	// Methods
	//=========================

	/**
	 * <p>The <strong><code>HTMLElement.attachInternals()</code></strong> method returns an {@link ElementInternals} object. This method allows a custom element to participate in HTML forms. The <code>ElementInternals</code> interface provides utilities for working with these elements in the same way you would work with any standard HTML form element, and also exposes the <a href="https://wicg.github.io/aom/explainer.html">Accessibility Object Model</a> to the element.</p>
	 * <h3>Parameters</h3>
	 * <p>None.</p>
	 * <h3>Return value</h3>
	 * <p>An {@link ElementInternals} object.</p>
	 */
	public ElementInternals attachInternals() {
		return Wrap.ElementInternals(JS.invoke(peer, "attachInternals"));
	}

	/**
	 * <p>The <strong><code>HTMLElement.blur()</code></strong> method removes keyboard focus from the current element.</p>
	 * <h3>Parameters</h3>
	 * <p>None.</p>
	 * <h3>Return value</h3>
	 * <p>None ().</p>
	 */
	public void blur() {
		JS.invoke(peer, "blur");
	}

	/**
	 * <p>The <strong><code>HTMLElement.click()</code></strong> method simulates a mouse click on
	 * an element.
	 * When <code>click()</code> is used with supported elements (such as an
	 * ), it fires the element's click event. This event then bubbles
	 * up to elements higher in the document tree (or event chain) and fires their click
	 * events.</p>
	 * <h3>Parameters</h3>
	 * <p>None.</p>
	 * <h3>Return value</h3>
	 * <p>None ().</p>
	 */
	public void click() {
		JS.invoke(peer, "click");
	}

	/**
	 * <p>The <strong><code>HTMLElement.focus()</code></strong> method sets focus on the specified element, if it can be focused.
	 * The focused element is the element that will receive keyboard and similar events by default.
	 * By default the browser will scroll the element into view after focusing it, and it may also provide visible indication of the focused element (typically by displaying a &quot;focus ring&quot; around the element).
	 * Parameter options are provided to disable the default scrolling and force visible indication on elements.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>options</code>
	 * <ul>
	 * <li>: An optional object for controlling aspects of the focusing process.
	 * This object may contain the following properties:
	 * <ul>
	 * <li><code>preventScroll</code>
	 * <ul>
	 * <li>: A boolean value indicating whether or not the browser should scroll the document to bring the newly-focused element into view.
	 * A value of <code>false</code> for <code>preventScroll</code> (the default) means that the browser will scroll the element into view after focusing it.
	 * If <code>preventScroll</code> is set to <code>true</code>, no scrolling will occur.</li>
	 * </ul>
	 * </li>
	 * <li><code>focusVisible</code>
	 * <ul>
	 * <li>: A boolean value that should be set to <code>true</code> to force visible indication that the element is focused.
	 * By default, or if the property is not <code>true</code>, a browser may still provide visible indication if it determines that this would improve accessibility for users.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * </li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>None ().</p>
	 */
	public void focus(JSObject options) {
		JS.invoke(peer, "focus", JS.param(options));
	}

	public void focus() {
		JS.invoke(peer, "focus");
	}

}
