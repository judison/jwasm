package browser.dom;

/**
 * <p>The <strong><code>HTMLBRElement</code></strong> interface represents an HTML line break element (<code>br</code>). It inherits from {@link HTMLElement}.</p>
 */
@SuppressWarnings({"unused"})
public class HTMLBRElement extends HTMLElement {

	HTMLBRElement(Object peer) {
		super(peer);
	}

	//=========================
	// Properties
	//=========================

	//=========================
	// Methods
	//=========================

}
