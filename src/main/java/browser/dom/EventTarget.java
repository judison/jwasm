package browser.dom;

import browser.javascript.JS;
import browser.javascript.Peered;
import de.inetsoftware.jwebassembly.api.annotation.Import;
import de.inetsoftware.jwebassembly.web.DOMString;

/**
 * <a href="https://developer.mozilla.org/en-US/docs/Web/API/EventTarget">Online Doc</a>
 */
public class EventTarget extends Peered {

	protected EventTarget(Object peer) {
		super(peer);
	}

	public void addEventListener(String type, EventListener listener, boolean useCapture) {
		addEventListener0(peer, JS.param(type), listener, useCapture);
	}

	public void addEventListener(String type, EventListener listener) {
		addEventListener0(peer, JS.param(type), listener, false);
	}

	private static void wasm_dispatchEvent(EventListener listener, Object event) {
		listener.handleEvent(new Event(event));
	}

	@Import(module = JS.MODULE,
		js = "(o,t,l,u)=>o.addEventListener(t,e=>{console.log('log: ' + e);wasmImports.instance.exports.wasm_dispatchEvent(l,e)},u)",
		callbacks = "browser/dom/event/EventTarget.wasm_dispatchEvent(Lbrowser/dom/event/EventListener;Ljava/lang/Object;)V")
	private native static void addEventListener0(Object peer, DOMString type, EventListener listener, boolean useCapture);

//	public void removeEventListener(String type, EventListener listener, boolean useCapture) {
//		removeEventListener0(peer, domString(type), listener, useCapture);
//	}
//
//	public void removeEventListener(String type, EventListener listener) {
//		removeEventListener0(peer, domString(type), listener, false);
//	}
//
//	// Probable won't work, check if it is the case....
//	// creates the "same" listener... but it is "the same", needs to be??
//	@Import(module = MODULE,
//		js = "(o,t,l,u)=>o.removeEventListener(t,e=>{console.log('log: ' + e);wasmImports.instance.exports.wasm_dispatchEvent(l,e)},u)",
//		callbacks = "browser/dom/event/EventTarget.wasm_dispatchEvent(Lbrowser/dom/event/EventListener;Ljava/lang/Object;)V")
//	private native static void removeEventListener0(Object peer, DOMString type, EventListener listener, boolean useCapture);
//
//  public boolean dispatchEvent(Event evt) throws EventException;

}
