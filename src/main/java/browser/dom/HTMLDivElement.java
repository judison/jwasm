package browser.dom;

/**
 * <p>The <strong><code>HTMLDivElement</code></strong> interface provides special properties (beyond the regular {@link HTMLElement} interface it also has available to it by inheritance) for manipulating <code>div</code> elements.</p>
 */
@SuppressWarnings("unused")
public class HTMLDivElement extends HTMLElement {

	protected HTMLDivElement(Object peer) {
		super(peer);
	}

	//=========================
	// Properties
	//=========================

	//=========================
	// Methods
	//=========================

}
