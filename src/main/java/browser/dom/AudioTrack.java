package browser.dom;

import browser.javascript.JS;
import browser.javascript.Peered;

/**
 * <p>The <strong><code>AudioTrack</code></strong> interface represents a single audio track from one of the HTML media elements, <code>audio</code> or <code>video</code>.
 * The most common use for accessing an <code>AudioTrack</code> object is to toggle its {@link AudioTrack.enabled} property in order to mute and unmute the track.</p>
 */
@SuppressWarnings("unused")
public class AudioTrack extends Peered {

	protected AudioTrack(Object peer) {
		super(peer);
	}

	//=========================
	// Properties
	//=========================

	/**
	 * <p>The <strong>{@link AudioTrack}</strong> property
	 * <strong><code>enabled</code></strong> specifies whether or not the described audio
	 * track is currently enabled for use. If the track is disabled by setting
	 * <code>enabled</code> to <code>false</code>, the track is muted and does not produce
	 * audio.</p>
	 * <h2>Value</h2>
	 * <p>The <code>enabled</code> property is a Boolean whose value is <code>true</code> if the
	 * track is enabled; enabled tracks produce audio while the media is playing. Setting
	 * <code>enabled</code> to <code>false</code> effectively mutes the audio track, preventing
	 * it from contributing to the media's audio performance.</p>
	 */
	public boolean isEnabled() {
		return JS.getBoolean(peer, "enabled");
	}

	/**
	 * <p>The <strong>{@link AudioTrack}</strong> property
	 * <strong><code>enabled</code></strong> specifies whether or not the described audio
	 * track is currently enabled for use. If the track is disabled by setting
	 * <code>enabled</code> to <code>false</code>, the track is muted and does not produce
	 * audio.</p>
	 * <h2>Value</h2>
	 * <p>The <code>enabled</code> property is a Boolean whose value is <code>true</code> if the
	 * track is enabled; enabled tracks produce audio while the media is playing. Setting
	 * <code>enabled</code> to <code>false</code> effectively mutes the audio track, preventing
	 * it from contributing to the media's audio performance.</p>
	 */
	public void setEnabled(boolean value) {
		JS.set(peer, "enabled", value);
	}

	/**
	 * <p>The <strong><code>id</code></strong> property contains a
	 * string which uniquely identifies the track represented by the
	 * <strong>{@link AudioTrack}</strong>.
	 * This ID can be used with the
	 * {@link AudioTrackList#getTrackById()} method to locate a specific track within
	 * the media associated with a media element. The track ID can also be used as the fragment of a URL that loads the specific track
	 * (if the media supports media fragments).</p>
	 * <h2>Value</h2>
	 * <p>A string which identifies the track, suitable for use when calling
	 * {@link AudioTrackList.getTrackById} on an
	 * {@link AudioTrackList} such as the one specified by a media element's
	 * {@link HTMLMediaElement.audioTracks} property.</p>
	 */
	public String getId() {
		return Wrap.String(JS.get(peer, "id"));
	}

	/**
	 * <p>The <strong><code>id</code></strong> property contains a
	 * string which uniquely identifies the track represented by the
	 * <strong>{@link AudioTrack}</strong>.
	 * This ID can be used with the
	 * {@link AudioTrackList#getTrackById()} method to locate a specific track within
	 * the media associated with a media element. The track ID can also be used as the fragment of a URL that loads the specific track
	 * (if the media supports media fragments).</p>
	 * <h2>Value</h2>
	 * <p>A string which identifies the track, suitable for use when calling
	 * {@link AudioTrackList.getTrackById} on an
	 * {@link AudioTrackList} such as the one specified by a media element's
	 * {@link HTMLMediaElement.audioTracks} property.</p>
	 */
	public void setId(String value) {
		JS.set(peer, "id", JS.param(value));
	}

	/**
	 * <p>The <strong><code>kind</code></strong> property contains a
	 * string indicating the category of audio contained in the
	 * <strong>{@link AudioTrack}</strong>.
	 * The <code>kind</code> can be used
	 * to determine the scenarios in which specific tracks should be enabled or disabled. See
	 * <a href="#audio_track_kind_strings">Audio track kind strings</a> for a list of the kinds available for audio tracks.</p>
	 * <h2>Value</h2>
	 * <p>A string specifying the type of content the media represents. The
	 * string is one of those found in <a href="#audio_track_kind_strings">Audio track kind strings</a> below.</p>
	 */
	public String getKind() {
		return Wrap.String(JS.get(peer, "kind"));
	}

	/**
	 * <p>The <strong><code>kind</code></strong> property contains a
	 * string indicating the category of audio contained in the
	 * <strong>{@link AudioTrack}</strong>.
	 * The <code>kind</code> can be used
	 * to determine the scenarios in which specific tracks should be enabled or disabled. See
	 * <a href="#audio_track_kind_strings">Audio track kind strings</a> for a list of the kinds available for audio tracks.</p>
	 * <h2>Value</h2>
	 * <p>A string specifying the type of content the media represents. The
	 * string is one of those found in <a href="#audio_track_kind_strings">Audio track kind strings</a> below.</p>
	 */
	public void setKind(String value) {
		JS.set(peer, "kind", JS.param(value));
	}

	/**
	 * <p>The read-only <strong>{@link AudioTrack}</strong>
	 * property <strong><code>label</code></strong> returns a string specifying the audio
	 * track's human-readable label, if one is available; otherwise, it returns an empty
	 * string.</p>
	 * <h2>Value</h2>
	 * <p>A string specifying the track's human-readable label, if one is
	 * available in the track metadata. Otherwise, an empty string (<code>&quot;&quot;</code>) is
	 * returned.
	 * For example, a track whose {@link AudioTrack.kind} is
	 * <code>&quot;commentary&quot;</code> might have a <code>label</code> such as
	 * <code>&quot;Commentary with director Mark Markmarkimark and star Donna Donnalidon&quot;</code>.</p>
	 */
	public String getLabel() {
		return Wrap.String(JS.get(peer, "label"));
	}

	/**
	 * <p>The read-only <strong>{@link AudioTrack}</strong>
	 * property <strong><code>language</code></strong> returns a string identifying the
	 * language used in the audio track.
	 * For tracks that include multiple languages
	 * (such as a movie in English in which a few lines are spoken in other languages), this
	 * should be the video's primary language.</p>
	 * <h2>Value</h2>
	 * <p>A string specifying the BCP 47 (RFC(5646)) format language tag of
	 * the primary language used in the audio track, or an empty string (<code>&quot;&quot;</code>) if
	 * the language is not specified or known, or if the track doesn't contain speech.
	 * For example, if the primary language used in the track is United States English, this
	 * value would be <code>&quot;en-US&quot;</code>. For Brazilian Portuguese, the value would be
	 * <code>&quot;pt-BR&quot;</code>.</p>
	 */
	public String getLanguage() {
		return Wrap.String(JS.get(peer, "language"));
	}

	/**
	 * <p>The read-only <strong>{@link AudioTrack}</strong>
	 * property <strong><code>sourceBuffer</code></strong> returns the
	 * {@link SourceBuffer} that created the track, or null if the track was not
	 * created by a {@link SourceBuffer} or the {@link SourceBuffer} has been
	 * removed from the {@link MediaSource#sourceBuffers} attribute of its parent
	 * media source.</p>
	 * <h2>Value</h2>
	 * <p>A {@link SourceBuffer} or null.</p>
	 */
	public SourceBuffer getSourceBuffer() {
		return Wrap.SourceBuffer(JS.get(peer, "sourceBuffer"));
	}

	//=========================
	// Methods
	//=========================

}
