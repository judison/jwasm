package browser.dom;

import browser.javascript.JSObject;

public class Wrap {

	public static String String(Object peer) {
		return peer == null ? null : de.inetsoftware.jwebassembly.web.JSObject.toJavaString(peer);
	}

	public static JSObject JSObject(Object peer) {
		return peer == null ? null : new JSObject(peer);
	}

	public static DOMTokenList DOMTokenList(Object peer) {
		return peer == null ? null : new DOMTokenList(peer);
	}

	public static DOMStringMap DOMStringMap(Object peer) {
		return peer == null ? null : new DOMStringMap(peer);
	}

	public static NamedNodeMap NamedNodeMap(Object peer) {
		return peer == null ? null : new NamedNodeMap(peer);
	}

	public static ElementInternals ElementInternals(Object peer) {
		return peer == null ? null : new ElementInternals(peer);
	}

	public static NodeList NodeList(Object peer) {
		return peer == null ? null : new NodeList(peer);
	}

	public static Node Node(Object peer) {
		return peer == null ? null : new Node(peer);
	}

	public static Document Document(Object peer) {
		return peer == null ? null : new Document(peer);
	}

	public static Element Element(Object peer) {
		return peer == null ? null : new Element(peer);
	}

	public static HTMLCollection HTMLCollection(Object peer) {
		return peer == null ? null : new HTMLCollection(peer);
	}

	public static HTMLAnchorElement HTMLAnchorElement(Object peer) {
		return peer == null ? null : new HTMLAnchorElement(peer);
	}

	public static HTMLAreaElement HTMLAreaElement(Object peer) {
		return peer == null ? null : new HTMLAreaElement(peer);
	}

	public static HTMLAudioElement HTMLAudioElement(Object peer) {
		return peer == null ? null : new HTMLAudioElement(peer);
	}

	public static HTMLBRElement HTMLBRElement(Object peer) {
		return peer == null ? null : new HTMLBRElement(peer);
	}

	public static HTMLButtonElement HTMLButtonElement(Object peer) {
		return peer == null ? null : new HTMLButtonElement(peer);
	}

	public static HTMLCanvasElement HTMLCanvasElement(Object peer) {
		return peer == null ? null : new HTMLCanvasElement(peer);
	}

	public static HTMLDivElement HTMLDivElement(Object peer) {
		return peer == null ? null : new HTMLDivElement(peer);
	}

	public static HTMLSlotElement HTMLSlotElement(Object peer) {
		return peer == null ? null : new HTMLSlotElement(peer);
	}

	public static HTMLBodyElement HTMLBodyElement(Object peer) {
		return peer == null ? null : new HTMLBodyElement(peer);
	}

	public static AudioTrackList AudioTrackList(Object peer) {
		return peer == null ? null : new AudioTrackList(peer);
	}

	public static Window Window(Object peer) {
		return peer == null ? null : new Window(peer);
	}

	public static Console Console(Object peer) {
		return peer == null ? null : new Console(peer);
	}

	public static SourceBuffer SourceBuffer(Object peer) {
		return peer == null ? null : new SourceBuffer(peer);
	}

	public static AudioTrack AudioTrack(Object peer) {
		return peer == null ? null : new AudioTrack(peer);
	}

	public static ShadowRoot ShadowRoot(Object peer) {
		return peer == null ? null : new ShadowRoot(peer);
	}

	public static Attr Attr(Object peer) {
		return peer == null ? null : new Attr(peer);
	}

}
