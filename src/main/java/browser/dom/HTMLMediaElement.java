package browser.dom;

import browser.javascript.JS;

/**
 * <p>The <strong><code>HTMLMediaElement</code></strong> interface adds to {@link HTMLElement} the properties and methods needed to support basic media-related capabilities that are common to audio and video.
 * The {@link HTMLVideoElement} and {@link HTMLAudioElement} elements both inherit this interface.</p>
 */
@SuppressWarnings({"unused", "JavadocReference"})
public class HTMLMediaElement extends HTMLElement {

	HTMLMediaElement(Object peer) {
		super(peer);
	}

	//=========================
	// Properties
	//=========================

	/**
	 * <p>The read-only <strong><code>audioTracks</code></strong>
	 * property on {@link HTMLMediaElement} objects returns
	 * an {@link AudioTrackList} object listing all of the {@link AudioTrack}
	 * objects representing the media element's audio tracks.
	 * The media element may be
	 * either an <code>audio</code> element or a <code>video</code> element.
	 * The returned list is <em>live</em>; that is, as tracks are added to and removed from
	 * the media element, the list's contents change dynamically. Once you have a reference to
	 * the list, you can monitor it for changes to detect when new audio tracks are added or
	 * existing ones removed. See {{SectionOnPage(&quot;/en-US/docs/Web/API/AudioTrackList&quot;, &quot;Event
	 * handlers&quot;)}} to learn more about watching for changes to a media element's track list.</p>
	 * <h2>Value</h2>
	 * <p>An {@link AudioTrackList} object representing the list of audio tracks included
	 * in the media element. The list of tracks can be accessed using array notation, or using
	 * the object's {@link AudioTrackList.getTrackById} method.
	 * Each track is represented by an {@link AudioTrack} object which provides
	 * information about the track.</p>
	 */
	public AudioTrackList audioTracks() {
		return new AudioTrackList(JS.get(peer, "audioTracks"));
	}

	/**
	 * <p>The <strong><code>HTMLMediaElement.autoplay</code></strong>
	 * property reflects the <code>autoplay</code> HTML attribute, indicating
	 * whether playback should automatically begin as soon as enough media is available to do
	 * so without interruption.
	 * A media element whose source is a {@link MediaStream} and whose
	 * <code>autoplay</code> property is <code>true</code> will begin playback when it becomes
	 * active (that is, when {@link MediaStream.active} becomes <code>true</code>).</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> Sites which automatically play audio (or videos with an audio
	 * track) can be an unpleasant experience for users, so it should be avoided when
	 * possible. If you must offer autoplay functionality, you should make it opt-in
	 * (requiring a user to specifically enable it). However, autoplay can be useful when
	 * creating media elements whose source will be set at a later time, under user control.
	 * For a much more in-depth look at autoplay, autoplay blocking, and how to respond when
	 * autoplay is blocked by the user's browser, see our article Autoplay guide for media and Web Audio APIs.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A boolean value which is <code>true</code> if the media element will
	 * begin playback as soon as enough content has loaded to allow it to do so without
	 * interruption.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> Some browsers offer users the ability to override
	 * <code>autoplay</code> in order to prevent disruptive audio or video from playing
	 * without permission or in the background. Do not rely on <code>autoplay</code> actually
	 * starting playback and instead use {@link HTMLMediaElement.play_event}
	 * event.</p>
	 * </blockquote>
	 */
	public boolean isAutoplay() {
		return JS.getBoolean(peer, "autoplay");
	}

	/**
	 * <p>The <strong><code>HTMLMediaElement.autoplay</code></strong>
	 * property reflects the <code>autoplay</code> HTML attribute, indicating
	 * whether playback should automatically begin as soon as enough media is available to do
	 * so without interruption.
	 * A media element whose source is a {@link MediaStream} and whose
	 * <code>autoplay</code> property is <code>true</code> will begin playback when it becomes
	 * active (that is, when {@link MediaStream.active} becomes <code>true</code>).</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> Sites which automatically play audio (or videos with an audio
	 * track) can be an unpleasant experience for users, so it should be avoided when
	 * possible. If you must offer autoplay functionality, you should make it opt-in
	 * (requiring a user to specifically enable it). However, autoplay can be useful when
	 * creating media elements whose source will be set at a later time, under user control.
	 * For a much more in-depth look at autoplay, autoplay blocking, and how to respond when
	 * autoplay is blocked by the user's browser, see our article Autoplay guide for media and Web Audio APIs.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A boolean value which is <code>true</code> if the media element will
	 * begin playback as soon as enough content has loaded to allow it to do so without
	 * interruption.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> Some browsers offer users the ability to override
	 * <code>autoplay</code> in order to prevent disruptive audio or video from playing
	 * without permission or in the background. Do not rely on <code>autoplay</code> actually
	 * starting playback and instead use {@link HTMLMediaElement.play_event}
	 * event.</p>
	 * </blockquote>
	 */
	public void setAutoplay(boolean value) {
		JS.set(peer, "autoplay", value);
	}

	/**
	 * <p>The <strong><code>buffered</code></strong> read-only property of {@link HTMLMediaElement} objects returns a new static {@link TimeRanges#normalized_timeranges_objects} that represents the ranges of the media resource, if any, that the user agent has buffered at the moment the <code>buffered</code> property is accessed.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> This feature is not available in {@link Web_Workers_API}.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A new static {@link TimeRanges#normalized_timeranges_objects} that represents the ranges of the media resource, if any, that the user agent has buffered at the moment the <code>buffered</code> property is accessed.</p>
	 */
	public TimeRanges buffered() {
		return new TimeRanges(JS.get(peer, "buffered"));
	}

	// controller DEPRECATED

	/**
	 * <p>The <strong><code>HTMLMediaElement.controls</code></strong> property reflects the
	 * <code>controls</code> HTML attribute, which controls whether user
	 * interface controls for playing the media item will be displayed.</p>
	 * <h2>Value</h2>
	 * <p>A boolean value. A value of <code>true</code> means controls will be
	 * displayed.</p>
	 */
	public boolean isControls() {
		return JS.getBoolean(peer, "controls");
	}

	/**
	 * <p>The <strong><code>HTMLMediaElement.controls</code></strong> property reflects the
	 * <code>controls</code> HTML attribute, which controls whether user
	 * interface controls for playing the media item will be displayed.</p>
	 * <h2>Value</h2>
	 * <p>A boolean value. A value of <code>true</code> means controls will be
	 * displayed.</p>
	 */
	public void setControls(boolean value) {
		JS.set(peer, "controls", value);
	}

	/**
	 * <p>The <strong><code>controlsList</code></strong> property of the
	 * {@link HTMLMediaElement} interface returns a DOMTokenList that helps the user
	 * agent select what controls to show on the media element whenever the user agent shows
	 * its own set of controls. The DOMTokenList takes one or more of three possible values:
	 * <code>nodownload</code>, <code>nofullscreen</code>, and <code>noremoteplayback</code>.</p>
	 * <h2>Value</h2>
	 * <p>A {@link DOMTokenList}.</p>
	 */
	public DOMTokenList getControlsList() {
		return new DOMTokenList(JS.get(peer, "controlsList"));
	}

	/**
	 * <p>The <strong><code>controlsList</code></strong> property of the
	 * {@link HTMLMediaElement} interface returns a DOMTokenList that helps the user
	 * agent select what controls to show on the media element whenever the user agent shows
	 * its own set of controls. The DOMTokenList takes one or more of three possible values:
	 * <code>nodownload</code>, <code>nofullscreen</code>, and <code>noremoteplayback</code>.</p>
	 * <h2>Value</h2>
	 * <p>A {@link DOMTokenList}.</p>
	 */
	public void setControlsList(DOMTokenList value) {
		JS.set(peer, "controlsList", value);
	}

	/**
	 * <p>The <strong><code>HTMLMediaElement.crossOrigin</code></strong> property is the CORS setting for this media element. See CORS settings attributes for details.</p>
	 */
	public String getCrossOrigin() {
		return Wrap.String(JS.get(peer, "crossOrigin"));
	}

	/**
	 * <p>The <strong><code>HTMLMediaElement.crossOrigin</code></strong> property is the CORS setting for this media element. See CORS settings attributes for details.</p>
	 */
	public void setCrossOrigin(String value) {
		JS.set(peer, "crossOrigin", value);
	}

	/**
	 * <p>The <strong><code>HTMLMediaElement.currentSrc</code></strong> property contains the
	 * absolute URL of the chosen media resource. This could happen, for example, if the web
	 * server selects a media file based on the resolution of the user's display. The value
	 * is an empty string if the <code>networkState</code> property is <code>EMPTY</code>.</p>
	 * <h2>Value</h2>
	 * <p>A string object containing the absolute URL of the chosen media
	 * source; this may be an empty string if <code>networkState</code> is <code>EMPTY</code>;
	 * otherwise, it will be one of the resources listed by the
	 * {@link HTMLSourceElement} contained within the media element, or the value or src
	 * if no <code>source</code> element is provided.</p>
	 */
	public String getCurrentSrc() {
		return Wrap.String(JS.get(peer, "currentSrc"));
	}

	/**
	 * <p>The <strong><code>HTMLMediaElement.currentSrc</code></strong> property contains the
	 * absolute URL of the chosen media resource. This could happen, for example, if the web
	 * server selects a media file based on the resolution of the user's display. The value
	 * is an empty string if the <code>networkState</code> property is <code>EMPTY</code>.</p>
	 * <h2>Value</h2>
	 * <p>A string object containing the absolute URL of the chosen media
	 * source; this may be an empty string if <code>networkState</code> is <code>EMPTY</code>;
	 * otherwise, it will be one of the resources listed by the
	 * {@link HTMLSourceElement} contained within the media element, or the value or src
	 * if no <code>source</code> element is provided.</p>
	 */
	public void setCurrentSrc(String value) {
		JS.set(peer, "currentSrc", value);
	}

	/**
	 * <p>The {@link HTMLMediaElement} interface's
	 * <strong><code>currentTime</code></strong> property specifies the current playback time
	 * in seconds.
	 * Changing the value of <code>currentTime</code> seeks the media to
	 * the new time.</p>
	 * <h2>Value</h2>
	 * <p>A double-precision floating-point value indicating the current playback time in
	 * seconds.
	 * If the media is not yet playing, the value of <code>currentTime</code> indicates the
	 * time position within the media at which playback will begin once the
	 * {@link HTMLMediaElement.play} method is called.
	 * Setting <code>currentTime</code> to a new value seeks the media to the given time, if
	 * the media is available.
	 * For media without a known duration—such as media being streamed live—it's possible that
	 * the browser may not be able to obtain parts of the media that have expired from the
	 * media buffer. Also, media whose timeline doesn't begin at 0 seconds cannot be seeked to
	 * a time before its timeline's earliest time.
	 * The length of the media in seconds can be determined using the
	 * {@link HTMLMediaElement.duration} property.</p>
	 */
	public double getCurrentTime() {
		return JS.getDouble(peer, "currentTime");
	}

	/**
	 * <p>The {@link HTMLMediaElement} interface's
	 * <strong><code>currentTime</code></strong> property specifies the current playback time
	 * in seconds.
	 * Changing the value of <code>currentTime</code> seeks the media to
	 * the new time.</p>
	 * <h2>Value</h2>
	 * <p>A double-precision floating-point value indicating the current playback time in
	 * seconds.
	 * If the media is not yet playing, the value of <code>currentTime</code> indicates the
	 * time position within the media at which playback will begin once the
	 * {@link HTMLMediaElement.play} method is called.
	 * Setting <code>currentTime</code> to a new value seeks the media to the given time, if
	 * the media is available.
	 * For media without a known duration—such as media being streamed live—it's possible that
	 * the browser may not be able to obtain parts of the media that have expired from the
	 * media buffer. Also, media whose timeline doesn't begin at 0 seconds cannot be seeked to
	 * a time before its timeline's earliest time.
	 * The length of the media in seconds can be determined using the
	 * {@link HTMLMediaElement.duration} property.</p>
	 */
	public void setCurrentTime(double value) {
		JS.set(peer, "currentTime", value);
	}

	/**
	 * <p>The <strong><code>HTMLMediaElement.defaultMuted</code></strong> property reflects the <code>muted</code> HTML attribute, which indicates whether the media element's audio output should be muted by default. This property has no dynamic effect. To mute and unmute the audio output, use the {@link HTMLMediaElement.muted} property.</p>
	 * <h2>Value</h2>
	 * <p>A boolean value. A value of <code>true</code> means that the audio output will be muted by default.</p>
	 */
	public boolean isDefaultMuted() {
		return JS.getBoolean(peer, "defaultMuted");
	}

	/**
	 * <p>The <strong><code>HTMLMediaElement.defaultMuted</code></strong> property reflects the <code>muted</code> HTML attribute, which indicates whether the media element's audio output should be muted by default. This property has no dynamic effect. To mute and unmute the audio output, use the {@link HTMLMediaElement.muted} property.</p>
	 * <h2>Value</h2>
	 * <p>A boolean value. A value of <code>true</code> means that the audio output will be muted by default.</p>
	 */
	public void setDefaultMuted(boolean value) {
		JS.set(peer, "defaultMuted", value);
	}

	/**
	 * <p>The <strong><code>HTMLMediaElement.defaultPlaybackRate</code></strong> property indicates the default playback rate for the media.</p>
	 * <h2>Value</h2>
	 * <p>A double. <code>1.0</code> is &quot;normal speed,&quot; values lower than <code>1.0</code> make the media play slower than normal, higher values make it play faster.</p>
	 */
	public double getDefaultPlaybackRate() {
		return JS.getDouble(peer, "defaultPlaybackRate");
	}

	/**
	 * <p>The <strong><code>HTMLMediaElement.defaultPlaybackRate</code></strong> property indicates the default playback rate for the media.</p>
	 * <h2>Value</h2>
	 * <p>A double. <code>1.0</code> is &quot;normal speed,&quot; values lower than <code>1.0</code> make the media play slower than normal, higher values make it play faster.</p>
	 */
	public void setDefaultPlaybackRate(double value) {
		JS.set(peer, "defaultPlaybackRate", value);
	}

	/**
	 * <p>The <strong><code>HTMLMediaElement.disableRemotePlayback</code></strong> property
	 * determines whether the media element is allowed to have a remote playback UI.</p>
	 * <h2>Value</h2>
	 * <p>A boolean value indicating whether the media element may have a remote playback
	 * UI. (false means &quot;not disabled&quot;, which means &quot;enabled&quot;)</p>
	 */
	public boolean isDisableRemotePlayback() {
		return JS.getBoolean(peer, "disableRemotePlayback");
	}

	/**
	 * <p>The <strong><code>HTMLMediaElement.disableRemotePlayback</code></strong> property
	 * determines whether the media element is allowed to have a remote playback UI.</p>
	 * <h2>Value</h2>
	 * <p>A boolean value indicating whether the media element may have a remote playback
	 * UI. (false means &quot;not disabled&quot;, which means &quot;enabled&quot;)</p>
	 */
	public void setDisableRemotePlayback(boolean value) {
		JS.set(peer, "disableRemotePlayback", value);
	}

	/**
	 * <p>The <em>read-only</em> {@link HTMLMediaElement}
	 * property <strong><code>duration</code></strong> indicates the length of the element's
	 * media in seconds.</p>
	 * <h2>Value</h2>
	 * <p>A double-precision floating-point value indicating the duration of the media in
	 * seconds. If no media data is available, the value <code>NaN</code> is returned. If the
	 * element's media doesn't have a known duration—such as for live media streams—the value
	 * of <code>duration</code> is <code>+Infinity</code>.</p>
	 */
	public double getDuration() {
		return JS.getDouble(peer, "duration");
	}

	/**
	 * <p>The <strong><code>HTMLMediaElement.ended</code></strong> indicates whether the media
	 * element has ended playback.</p>
	 * <h2>Value</h2>
	 * <p>A boolean value which is <code>true</code> if the media contained in the
	 * element has finished playing.
	 * If the source of the media is a {@link MediaStream}, this value is
	 * <code>true</code> if the value of the stream's {{domxref(&quot;MediaStream.active&quot;,
	 * &quot;active&quot;)}} property is <code>false</code>.</p>
	 */
	public boolean isEnded() {
		return JS.getBoolean(peer, "ended");
	}

	/**
	 * <p>The <strong><code>HTMLMediaElement.ended</code></strong> indicates whether the media
	 * element has ended playback.</p>
	 * <h2>Value</h2>
	 * <p>A boolean value which is <code>true</code> if the media contained in the
	 * element has finished playing.
	 * If the source of the media is a {@link MediaStream}, this value is
	 * <code>true</code> if the value of the stream's {{domxref(&quot;MediaStream.active&quot;,
	 * &quot;active&quot;)}} property is <code>false</code>.</p>
	 */
	public void setEnded(boolean value) {
		JS.set(peer, "ended", value);
	}

	/**
	 * <p>The <strong><code>HTMLMediaElement.error</code></strong> is the
	 * {@link MediaError} object for the most recent error, or <code>null</code> if
	 * there has not been an error. When an {@link HTMLMediaElement/error_event} event is received by the
	 * element, you can determine details about what happened by examining this object.</p>
	 * <h2>Value</h2>
	 * <p>A {@link MediaError} object describing the most recent error to occur on the
	 * media element or <code>null</code> if no errors have occurred.</p>
	 */
	public MediaError getError() {
		return new MediaError(JS.get(peer, "error"));
	}

	/**
	 * <p>The <strong><code>HTMLMediaElement.error</code></strong> is the
	 * {@link MediaError} object for the most recent error, or <code>null</code> if
	 * there has not been an error. When an {@link HTMLMediaElement/error_event} event is received by the
	 * element, you can determine details about what happened by examining this object.</p>
	 * <h2>Value</h2>
	 * <p>A {@link MediaError} object describing the most recent error to occur on the
	 * media element or <code>null</code> if no errors have occurred.</p>
	 */
	public void setError(MediaError value) {
		JS.set(peer, "error", value);
	}

	/**
	 * <p>The <strong><code>HTMLMediaElement.loop</code></strong> property reflects the <code>loop</code> HTML attribute, which controls whether the media element should start over when it reaches the end.</p>
	 * <h2>Value</h2>
	 * <p>A boolean value.</p>
	 */
	public boolean isLoop() {
		return JS.getBoolean(peer, "loop");
	}

	/**
	 * <p>The <strong><code>HTMLMediaElement.loop</code></strong> property reflects the <code>loop</code> HTML attribute, which controls whether the media element should start over when it reaches the end.</p>
	 * <h2>Value</h2>
	 * <p>A boolean value.</p>
	 */
	public void setLoop(boolean value) {
		JS.set(peer, "loop", value);
	}

	// mediaGroup DEPRECATED

	// msClearEffects NON-STANDARD

	// msGraphicsTrustStatus NON-STANDARD

	// msPlayToDisabled NON-STANDARD

	// msPlayToPreferredSourceUri NON-STANDARD

	// msPlayToPrimary NON-STANDARD

	// msPlayToSource NON-STANDARD

	// msRealTime NON-STANDARD

	/**
	 * <p>The <strong><code>HTMLMediaElement.muted</code></strong> indicates whether the media
	 * element muted.</p>
	 * <h2>Value</h2>
	 * <p>A boolean value. <code>true</code> means muted and <code>false</code> means
	 * not muted.</p>
	 */
	public boolean isMuted() {
		return JS.getBoolean(peer, "muted");
	}

	/**
	 * <p>The <strong><code>HTMLMediaElement.muted</code></strong> indicates whether the media
	 * element muted.</p>
	 * <h2>Value</h2>
	 * <p>A boolean value. <code>true</code> means muted and <code>false</code> means
	 * not muted.</p>
	 */
	public void setMuted(boolean value) {
		JS.set(peer, "muted", value);
	}

	/**
	 * <p>The
	 * <strong><code>HTMLMediaElement.networkState</code></strong> property indicates the
	 * current state of the fetching of media over the network.</p>
	 * <h2>Value</h2>
	 * <p>An <code>unsigned short</code>. Possible values are:
	 * | Constant            | Value | Description                                                                           |
	 * | ------------------- | ----- | ------------------------------------------------------------------------------------- |
	 * | <code>NETWORK_EMPTY</code>     | 0     | There is no data yet. Also, <code>readyState</code> is <code>HAVE_NOTHING</code>.                           |
	 * | <code>NETWORK_IDLE</code>      | 1     | HTMLMediaElement is active and has selected a resource, but is not using the network. |
	 * | <code>NETWORK_LOADING</code>   | 2     | The browser is downloading HTMLMediaElement data.                                     |
	 * | <code>NETWORK_NO_SOURCE</code> | 3     | No HTMLMediaElement src found.                                                        |</p>
	 */
	public String getNetworkState() {
		return Wrap.String(JS.get(peer, "networkState"));
	}

	/**
	 * <p>The
	 * <strong><code>HTMLMediaElement.networkState</code></strong> property indicates the
	 * current state of the fetching of media over the network.</p>
	 * <h2>Value</h2>
	 * <p>An <code>unsigned short</code>. Possible values are:
	 * | Constant            | Value | Description                                                                           |
	 * | ------------------- | ----- | ------------------------------------------------------------------------------------- |
	 * | <code>NETWORK_EMPTY</code>     | 0     | There is no data yet. Also, <code>readyState</code> is <code>HAVE_NOTHING</code>.                           |
	 * | <code>NETWORK_IDLE</code>      | 1     | HTMLMediaElement is active and has selected a resource, but is not using the network. |
	 * | <code>NETWORK_LOADING</code>   | 2     | The browser is downloading HTMLMediaElement data.                                     |
	 * | <code>NETWORK_NO_SOURCE</code> | 3     | No HTMLMediaElement src found.                                                        |</p>
	 */
	public void setNetworkState(String value) {
		JS.set(peer, "networkState", value);
	}

	/**
	 * <p>The read-only <strong><code>HTMLMediaElement.paused</code></strong> property
	 * tells whether the media element is paused.</p>
	 * <h2>Value</h2>
	 * <p>A boolean value. <code>true</code> is paused and <code>false</code> is not
	 * paused.</p>
	 */
	public boolean isPaused() {
		return JS.getBoolean(peer, "paused");
	}

	/**
	 * <p>The <strong><code>HTMLMediaElement.playbackRate</code></strong> property sets the rate at which the media is being played back. This is used to implement user controls for fast forward, slow motion, and so forth. The normal playback rate is multiplied by this value to obtain the current rate, so a value of 1.0 indicates normal speed.
	 * If <code>playbackRate</code> is negative, the media is <strong>not</strong> played backwards.
	 * The audio is muted when the fast forward or slow motion is outside a useful range (for example, Gecko mutes the sound outside the range <code>0.25</code> to <code>4.0</code>).
	 * The pitch of the audio is corrected by default. You can disable pitch correction using the {@link HTMLMediaElement.preservesPitch} property.</p>
	 * <h2>Value</h2>
	 * <p>A <a href="https://en.wikipedia.org/wiki/Double-precision_floating-point_format"><code>double</code></a>. <code>1.0</code> is &quot;normal speed,&quot; values lower than <code>1.0</code> make the media play slower than normal, higher values make it play faster. (<strong>Default:</strong> <code>1.0</code>)</p>
	 */
	public double getPlaybackRate() {
		return JS.getDouble(peer, "playbackRate");
	}

	/**
	 * <p>The <strong><code>HTMLMediaElement.playbackRate</code></strong> property sets the rate at which the media is being played back. This is used to implement user controls for fast forward, slow motion, and so forth. The normal playback rate is multiplied by this value to obtain the current rate, so a value of 1.0 indicates normal speed.
	 * If <code>playbackRate</code> is negative, the media is <strong>not</strong> played backwards.
	 * The audio is muted when the fast forward or slow motion is outside a useful range (for example, Gecko mutes the sound outside the range <code>0.25</code> to <code>4.0</code>).
	 * The pitch of the audio is corrected by default. You can disable pitch correction using the {@link HTMLMediaElement.preservesPitch} property.</p>
	 * <h2>Value</h2>
	 * <p>A <a href="https://en.wikipedia.org/wiki/Double-precision_floating-point_format"><code>double</code></a>. <code>1.0</code> is &quot;normal speed,&quot; values lower than <code>1.0</code> make the media play slower than normal, higher values make it play faster. (<strong>Default:</strong> <code>1.0</code>)</p>
	 */
	public void setPlaybackRate(double value) {
		JS.set(peer, "playbackRate", value);
	}

	/**
	 * <p>The <strong><code>HTMLMediaElement.preservesPitch</code></strong> property determines whether or not the browser should adjust the pitch of the audio to compensate for changes to the playback rate made by setting {@link HTMLMediaElement.playbackRate}.</p>
	 * <h2>Value</h2>
	 * <p>A boolean value defaulting to <code>true</code>.</p>
	 */
	public boolean isPreservesPitch() {
		return JS.getBoolean(peer, "preservesPitch");
	}

	/**
	 * <p>The <strong><code>HTMLMediaElement.preservesPitch</code></strong> property determines whether or not the browser should adjust the pitch of the audio to compensate for changes to the playback rate made by setting {@link HTMLMediaElement.playbackRate}.</p>
	 * <h2>Value</h2>
	 * <p>A boolean value defaulting to <code>true</code>.</p>
	 */
	public void setPreservesPitch(boolean value) {
		JS.set(peer, "preservesPitch", value);
	}

	/**
	 * <p>The <strong><code>HTMLMediaElement.readyState</code></strong> property indicates the
	 * readiness state of the media.</p>
	 * <h2>Value</h2>
	 * <p>An <code>unsigned short</code>. Possible values are:</p>
	 * <table class="no-markdown">
	 *   <thead>
	 *     <tr>
	 *       <th scope="col">Constant</th>
	 *       <th scope="col">Value</th>
	 *       <th scope="col">Description</th>
	 *     </tr>
	 *   </thead>
	 *   <tbody>
	 *     <tr>
	 *       <td><code>HAVE_NOTHING</code></td>
	 *       <td>0</td>
	 *       <td>No information is available about the media resource.</td>
	 *     </tr>
	 *     <tr>
	 *       <td><code>HAVE_METADATA</code></td>
	 *       <td>1</td>
	 *       <td>
	 *         Enough of the media resource has been retrieved that the metadata
	 *         attributes are initialized. Seeking will no longer raise an exception.
	 *       </td>
	 *     </tr>
	 *     <tr>
	 *       <td><code>HAVE_CURRENT_DATA</code></td>
	 *       <td>2</td>
	 *       <td>
	 *         Data is available for the current playback position, but not enough to
	 *         actually play more than one frame.
	 *       </td>
	 *     </tr>
	 *     <tr>
	 *       <td><code>HAVE_FUTURE_DATA</code></td>
	 *       <td>3</td>
	 *       <td>
	 *         Data for the current playback position as well as for at least a little
	 *         bit of time into the future is available (in other words, at least two
	 *         frames of video, for example).
	 *       </td>
	 *     </tr>
	 *     <tr>
	 *       <td><code>HAVE_ENOUGH_DATA</code></td>
	 *       <td>4</td>
	 *       <td>
	 *         Enough data is available—and the download rate is high enough—that the
	 *         media can be played through to the end without interruption.
	 *       </td>
	 *     </tr>
	 *   </tbody>
	 * </table>
	 */
	public String getReadyState() {
		return Wrap.String(JS.get(peer, "readyState"));
	}

	/**
	 * <p>The <strong><code>HTMLMediaElement.readyState</code></strong> property indicates the
	 * readiness state of the media.</p>
	 * <h2>Value</h2>
	 * <p>An <code>unsigned short</code>. Possible values are:</p>
	 * <table class="no-markdown">
	 *   <thead>
	 *     <tr>
	 *       <th scope="col">Constant</th>
	 *       <th scope="col">Value</th>
	 *       <th scope="col">Description</th>
	 *     </tr>
	 *   </thead>
	 *   <tbody>
	 *     <tr>
	 *       <td><code>HAVE_NOTHING</code></td>
	 *       <td>0</td>
	 *       <td>No information is available about the media resource.</td>
	 *     </tr>
	 *     <tr>
	 *       <td><code>HAVE_METADATA</code></td>
	 *       <td>1</td>
	 *       <td>
	 *         Enough of the media resource has been retrieved that the metadata
	 *         attributes are initialized. Seeking will no longer raise an exception.
	 *       </td>
	 *     </tr>
	 *     <tr>
	 *       <td><code>HAVE_CURRENT_DATA</code></td>
	 *       <td>2</td>
	 *       <td>
	 *         Data is available for the current playback position, but not enough to
	 *         actually play more than one frame.
	 *       </td>
	 *     </tr>
	 *     <tr>
	 *       <td><code>HAVE_FUTURE_DATA</code></td>
	 *       <td>3</td>
	 *       <td>
	 *         Data for the current playback position as well as for at least a little
	 *         bit of time into the future is available (in other words, at least two
	 *         frames of video, for example).
	 *       </td>
	 *     </tr>
	 *     <tr>
	 *       <td><code>HAVE_ENOUGH_DATA</code></td>
	 *       <td>4</td>
	 *       <td>
	 *         Enough data is available—and the download rate is high enough—that the
	 *         media can be played through to the end without interruption.
	 *       </td>
	 *     </tr>
	 *   </tbody>
	 * </table>
	 */
	public void setReadyState(String value) {
		JS.set(peer, "readyState", value);
	}

	/**
	 * <p>The <strong><code>seekable</code></strong> read-only property of {@link HTMLMediaElement} objects returns a new static {@link TimeRanges#normalized_timeranges_objects} that represents the ranges of the media resource, if any, that the user agent is able to seek to at the time <code>seekable</code> property is accessed.</p>
	 * <h2>Value</h2>
	 * <p>A new static {@link TimeRanges#normalized_timeranges_objects} that represents the ranges of the media resource, if any, that the user agent is able to seek to at the time <code>seekable</code> property is accessed.</p>
	 */
	public TimeRanges seekable() {
		return new TimeRanges(JS.get(peer, "seekable"));
	}

	/**
	 * <p><code>HTML DOM</code>
	 * The <strong><code>HTMLMediaElement.sinkId</code></strong> read-only property returns a
	 * string that is the unique ID of the audio device delivering output. If
	 * it is using the user agent default, it returns an empty string. This ID should be one of
	 * the {@link MediaDeviceInfo.deviceId} values returned from
	 * {@link MediaDevices.enumerateDevices()}, <code>id-multimedia</code>, or
	 * <code>id-communications</code>.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public String getSinkId() {
		return Wrap.String(JS.get(peer, "sinkId"));
	}

	/**
	 * <p>The <strong><code>HTMLMediaElement.src</code></strong> property reflects the value of
	 * the HTML media element's <code>src</code> attribute, which indicates the URL of a media
	 * resource to use in the element.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> The best way to know the URL of the media resource currently
	 * in active use in this element is to look at the value of the
	 * {@link HTMLMediaElement.currentSrc} attribute, which also takes
	 * into account selection of a best or preferred media resource from a list provided in
	 * an {@link HTMLSourceElement} (which represents a <code>source</code>
	 * element).</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A string object containing the URL of a media resource to use in the
	 * element; this property reflects the value of the HTML element's <code>src</code>
	 * attribute.</p>
	 */
	public String getSrc() {
		return Wrap.String(JS.get(peer, "src"));
	}

	/**
	 * <p>The <strong><code>HTMLMediaElement.src</code></strong> property reflects the value of
	 * the HTML media element's <code>src</code> attribute, which indicates the URL of a media
	 * resource to use in the element.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> The best way to know the URL of the media resource currently
	 * in active use in this element is to look at the value of the
	 * {@link HTMLMediaElement.currentSrc} attribute, which also takes
	 * into account selection of a best or preferred media resource from a list provided in
	 * an {@link HTMLSourceElement} (which represents a <code>source</code>
	 * element).</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A string object containing the URL of a media resource to use in the
	 * element; this property reflects the value of the HTML element's <code>src</code>
	 * attribute.</p>
	 */
	public void setSrc(String value) {
		JS.set(peer, "src", value);
	}

	/**
	 * <p>The <strong><code>srcObject</code></strong> property of the
	 * {@link HTMLMediaElement} interface sets or returns the object which serves as
	 * the source of the media associated with the {@link HTMLMediaElement}.
	 * The object can be a {@link MediaStream}, a {@link MediaSource}, a
	 * {@link Blob}, or a {@link File} (which inherits from <code>Blob</code>).</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> As of March 2020, only Safari supports setting objects other
	 * than <code>MediaStream</code>. Until other browsers catch up, for
	 * <code>MediaSource</code>, <code>Blob</code> and <code>File</code>, consider falling
	 * back to creating a URL with {@link URL.createObjectURL()} and assign it to
	 * {@link HTMLMediaElement.src}. See below for an example.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A {@link MediaStream}, {@link MediaSource}, {@link Blob}, or
	 * {@link File} object (though see the compatibility table for what is actually
	 * supported).</p>
	 */
	public MediaStream getSrcObject() {
		return new MediaStream(JS.get(peer, "srcObject"));
	}

	/**
	 * <p>The <strong><code>srcObject</code></strong> property of the
	 * {@link HTMLMediaElement} interface sets or returns the object which serves as
	 * the source of the media associated with the {@link HTMLMediaElement}.
	 * The object can be a {@link MediaStream}, a {@link MediaSource}, a
	 * {@link Blob}, or a {@link File} (which inherits from <code>Blob</code>).</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> As of March 2020, only Safari supports setting objects other
	 * than <code>MediaStream</code>. Until other browsers catch up, for
	 * <code>MediaSource</code>, <code>Blob</code> and <code>File</code>, consider falling
	 * back to creating a URL with {@link URL.createObjectURL()} and assign it to
	 * {@link HTMLMediaElement.src}. See below for an example.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A {@link MediaStream}, {@link MediaSource}, {@link Blob}, or
	 * {@link File} object (though see the compatibility table for what is actually
	 * supported).</p>
	 */
	public void setSrcObject(MediaStream value) {
		JS.set(peer, "srcObject", value);
	}

	/**
	 * <p>The read-only <strong><code>textTracks</code></strong>
	 * property on <code>HTMLMediaElement</code> objects returns a
	 * <code>TextTrackList</code> object listing all of the <code>TextTrack</code>
	 * objects representing the media element's text tracks, in the same order as in
	 * the list of text tracks.
	 * You can detect when tracks are added to and removed from an
	 * audio or
	 * video element
	 * using the <code>addtrack</code> and <code>removetrack</code> events. However, these
	 * events aren't sent directly to the media element itself. Instead, they're sent to the
	 * track list object of the {@link HTMLMediaElement}
	 * that corresponds to the type of track that was added to the element
	 * The returned list is <em>live</em>; that is, as tracks are added to and removed from
	 * the media element, the list's contents change dynamically. Once you have a reference to
	 * the list, you can monitor it for changes to detect when new text tracks are added or
	 * existing ones removed.
	 * See <code>/en-US/docs/Web/API/TextTrackList&quot;, &quot;Event handlers</code> to learn
	 * more about watching for changes to a media element's track list.</p>
	 * <h2>Value</h2>
	 * <p>A <code>TextTrackList</code> object representing the list of text tracks included in the media element. The list of tracks can be accessed using <code>textTracks{@link TextTrackList/getTrackById} method. Each track is represented by a</code>TextTrack` object which provides
	 * information about the track.</p>
	 */
	public TextTrackList textTracks() {
		return new TextTrackList(JS.get(peer, "textTracks"));
	}

	/**
	 * <p>The read-only <strong><code>videoTracks</code></strong>
	 * property on <code>HTMLMediaElement</code> objects returns a
	 * <code>VideoTrackList</code> object listing all of the <code>VideoTrack</code>
	 * objects representing the media element's video tracks.
	 * The returned list is <em>live</em>; that is, as tracks are added to and removed from
	 * the media element, the list's contents change dynamically. Once you have a reference to
	 * the list, you can monitor it for changes to detect when new video tracks are added or
	 * existing ones removed. See {{SectionOnPage(&quot;/en-US/docs/Web/API/VideoTrackList&quot;, &quot;Event
	 * handlers&quot;)}} to learn more about watching for changes to a media element's track list.</p>
	 * <h2>Value</h2>
	 * <p>A <code>VideoTrackList</code> object representing the list of video tracks included
	 * in the media element. The list of tracks can be accessed using array notation, or using
	 * the object's {@link VideoTrackList.getTrackById} method.
	 * Each track is represented by a <code>VideoTrack</code> object which provides
	 * information about the track.</p>
	 */
	public VideoTrackList videoTracks() {
		return new VideoTrackList(JS.get(peer, "videoTracks"));
	}

	/**
	 * <p>The <strong><code>HTMLMediaElement.volume</code></strong> property sets the volume at
	 * which the media will be played.</p>
	 * <h2>Value</h2>
	 * <p>A double values must fall between 0 and 1, where 0 is effectively muted and 1 is the
	 * loudest possible value.</p>
	 */
	public double getVolume() {
		return JS.getDouble(peer, "volume");
	}

	/**
	 * <p>The <strong><code>HTMLMediaElement.volume</code></strong> property sets the volume at
	 * which the media will be played.</p>
	 * <h2>Value</h2>
	 * <p>A double values must fall between 0 and 1, where 0 is effectively muted and 1 is the
	 * loudest possible value.</p>
	 */
	public void setVolume(double value) {
		JS.set(peer, "volume", value);
	}

	//=========================
	// Methods
	//=========================

	/**
	 * <p>The {@link HTMLMediaElement} method <strong><code>canPlayType()</code></strong> reports how likely it is that the current browser will be able to play media of a given MIME type.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> This feature is not available in {@link Web_Workers_API}.</p>
	 * </blockquote>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>type</code>
	 * <ul>
	 * <li>: A string specifying the MIME type of the media and (optionally) a <code>codecs</code> parameter containing a comma-separated list of the supported codecs.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>A string indicating how likely it is that the media can be played.
	 * The string will be one of the following values:</p>
	 * <ul>
	 * <li><code>&quot;&quot;</code> (empty string)
	 * <ul>
	 * <li>: The media cannot be played on the current device.</li>
	 * </ul>
	 * </li>
	 * <li><code>probably</code>
	 * <ul>
	 * <li>: The media is probably playable on this device.</li>
	 * </ul>
	 * </li>
	 * <li><code>maybe</code>
	 * <ul>
	 * <li>: There is not enough information to determine whether the media can play (until playback is actually attempted).</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public String canPlayType(String type) {
		return Wrap.String(JS.invoke(peer, "canPlayType", JS.param(type)));
	}

	/**
	 * <p>The <strong><code>captureStream()</code></strong> property of the
	 * {@link HTMLMediaElement} interface returns a {@link MediaStream} object
	 * which is streaming a real-time capture of the content being rendered in the media
	 * element.
	 * This can be used, for example, as a source for a {@link WebRTC_API} {@link RTCPeerConnection}.</p>
	 * <h3>Parameters</h3>
	 * <p>None.</p>
	 * <h3>Return value</h3>
	 * <p>A {@link MediaStream} object which can be used as a source for audio and/or
	 * video data by other media processing code, or as a source for WebRTC.</p>
	 */
	public MediaStream captureStream() {
		return new MediaStream(JS.invoke(peer, "captureStream"));
	}

	/**
	 * <p>The <strong><code>HTMLMediaElement.fastSeek()</code></strong> method quickly seeks the
	 * media to the new time with precision tradeoff.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> If you need to seek with precision, you should set {@link HTMLMediaElement/currentTime}
	 * instead.</p>
	 * </blockquote>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>time</code>
	 * <ul>
	 * <li>: A double.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>None (<code>undefined</code>).</p>
	 */
	public void fastSeek(double time) {
		JS.invoke(peer, "fastSeek");
	}

	/**
	 * <p>The {@link HTMLMediaElement} method
	 * <strong><code>load()</code></strong> resets the media element to its initial state and
	 * begins the process of selecting a media source and loading the media in preparation
	 * for playback to begin at the beginning.
	 * The amount of media data that is
	 * prefetched is determined by the value of the element's {{htmlattrxref(&quot;preload&quot;,
	 * &quot;video&quot;)}} attribute.
	 * This method is generally only useful when you've made dynamic changes to the set of
	 * sources available for the media element, either by changing the element's
	 * <code>src</code> attribute or by adding or removing
	 * <code>source</code> elements nested within the media element itself.
	 * <code>load()</code> will reset the element and rescan the available sources, thereby
	 * causing the changes to take effect.</p>
	 * <h3>Parameters</h3>
	 * <p>None.</p>
	 * <h3>Return value</h3>
	 * <p>None (<code>undefined</code>).</p>
	 */
	public void load() {
		JS.invoke(peer, "load");
	}

	// msInsertAudioEffect NON-STANDARD

	// msSetMediaProtectionManager NON-STANDARD

	/**
	 * <p>The <strong><code>HTMLMediaElement.pause()</code></strong> method will pause playback
	 * of the media, if the media is already in a paused state this method will have no effect.</p>
	 * <h3>Parameters</h3>
	 * <p>None.</p>
	 * <h3>Return value</h3>
	 * <p>None (<code>undefined</code>).</p>
	 */
	public void pause() {
		JS.invoke(peer, "pause");
	}

	/**
	 * <p>The {@link HTMLMediaElement}
	 * <strong><code>play()</code></strong> method attempts to begin playback of the media.
	 * It returns a <code>Promise</code> which is resolved when playback has been
	 * successfully started.
	 * Failure to begin playback for any reason, such as
	 * permission issues, result in the promise being rejected.</p>
	 * <h3>Parameters</h3>
	 * <p>None.</p>
	 * <h3>Return value</h3>
	 * <p>A <code>Promise</code> which is resolved when playback has been started, or is
	 * rejected if for any reason playback cannot be started.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> Browsers released before 2019 may not return a value from
	 * <code>play()</code>.</p>
	 * </blockquote>
	 */
	//TODO Promisse return
	public void play() {
		JS.invoke(peer, "play");
	}

	// seekToNextFrame DEPRECATED

	/**
	 * <p>The <strong><code>setMediaKeys()</code></strong> property of the
	 * {@link HTMLMediaElement} interface returns a <code>Promise</code> that resolves
	 * to the passed {@link MediaKeys}, which are those used to decrypt media during
	 * playback.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>mediaKeys</code>
	 * <ul>
	 * <li>: A reference to a {@link MediaKeys} object that the
	 * {@link HTMLMediaElement} can use for decryption of media data during playback.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>A <code>Promise</code> that resolves to the passed instance of <code>MediaKeys</code>.</p>
	 */
	public void setMediaKeys(MediaKeys mediaKeys) {
		JS.invoke(peer, "setMediaKeys", mediaKeys.peer);
	}

	/**
	 * <p>The <strong><code>HTMLMediaElement.setSinkId()</code></strong> method sets the ID of
	 * the audio device to use for output and returns a <code>Promise</code>.
	 * This only works when the application is authorized to use the specified device.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>sinkId</code>
	 * <ul>
	 * <li>: The {@link MediaDeviceInfo.deviceId} of the audio output device.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>A <code>Promise</code> that resolves to <code>undefined</code>.</p>
	 */
	public void setSinkId(String sinkId) {
		JS.invoke(peer, "setSinkId", JS.param(sinkId));
	}

}
