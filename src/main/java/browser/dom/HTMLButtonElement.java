package browser.dom;

import browser.javascript.JS;

/**
 * <p>The <strong><code>HTMLButtonElement</code></strong> interface provides properties and methods (beyond the regular {@link HTMLElement} interface it also has available to it by inheritance) for manipulating <code>button</code> elements.</p>
 */
@SuppressWarnings({"unused", "JavadocReference"})
public class HTMLButtonElement extends HTMLElement {

	HTMLButtonElement(Object peer) {
		super(peer);
	}

	public String getType() {
		return Wrap.String(JS.get(peer, "type"));
	}

	public void setType(String type) {
		JS.set(peer, "type", type);
	}

	//=========================
	// Properties
	//=========================

	/**
	 * <p>The <strong><code>HTMLButtonElement.disabled</code></strong> property indicates whether the control is disabled, meaning that it does not accept any clicks.</p>
	 * <h2>Value</h2>
	 * <p>A boolean value.</p>
	 */
	public boolean isDisabled() {
		return JS.getBoolean(peer, "disabled");
	}

	/**
	 * <p>The <strong><code>HTMLButtonElement.disabled</code></strong> property indicates whether the control is disabled, meaning that it does not accept any clicks.</p>
	 * <h2>Value</h2>
	 * <p>A boolean value.</p>
	 */
	public void setDisabled(boolean value) {
		JS.set(peer, "disabled", value);
	}

	/**
	 * <p>The <strong><code>HTMLButtonElement.labels</code></strong> read-only property returns a
	 * {@link NodeList} of the <code>label</code> elements associated with the
	 * <code>button</code> element.</p>
	 * <h2>Value</h2>
	 * <p>A {@link NodeList} containing the <code>&lt;label&gt;</code> elements associated
	 * with the <code>&lt;button&gt;</code> element.</p>
	 */
	public NodeList labels() {
		return new NodeList(JS.get(peer, "labels"));
	}

	//=========================
	// Methods
	//=========================

}
