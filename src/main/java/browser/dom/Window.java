/*
 * Copyright 2019 - 2022 Volker Berlin (i-net software)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package browser.dom;

import browser.javascript.Browser;
import browser.javascript.JS;

/**
 * <a href="https://developer.mozilla.org/en-US/docs/Web/API/Window">https://developer.mozilla.org/en-US/docs/Web/API/Window</a>
 *
 * @author Volker Berlin
 */
public class Window extends EventTarget {

	/**
	 * Create a Java instance as wrapper of the JavaScript object.
	 */
	Window(Object peer) {
		super(peer);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/API/Window/document">https://developer.mozilla.org/en-US/docs/Web/API/Window/document</a>
	 *
	 * @return the document
	 */
	public Document document() {
		return Browser.document;
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/API/Window/navigator">https://developer.mozilla.org/en-US/docs/Web/API/Window/navigator</a>
	 *
	 * @return the navigator
	 */
	public Navigator navigator() {
		return new Navigator(JS.get(peer, "navigator"));
	}

	public void alert(Object msg) {
		JS.invoke(peer, "alert", JS.param(String.valueOf(msg)));
	}

}
