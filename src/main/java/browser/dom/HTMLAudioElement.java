package browser.dom;

/**
 * <p>The <strong><code>HTMLAudioElement</code></strong> interface provides access to the properties of <code>audio</code> elements, as well as methods to manipulate them.
 * This element is based on, and inherits properties and methods from, the {@link HTMLMediaElement} interface.</p>
 */
public class HTMLAudioElement extends HTMLMediaElement {

	HTMLAudioElement(Object peer) {
		super(peer);
	}

	//=========================
	// Properties
	//=========================

	// msAudioCategory NON-STANDARD

	// msAudioDeviceType NON-STANDARD

	//=========================
	// Methods
	//=========================

}
