package browser.dom;

import browser.javascript.Browser;
import browser.javascript.JS;
import browser.javascript.JSObject;

/**
 * <p>The <code>DOM</code> <strong><code>Node</code></strong> interface is an abstract base
 * class upon which many other DOM API objects are based, thus letting those object types
 * to be used similarly and often interchangeably. As an abstract class, there is
 * no such thing as a plain <code>Node</code> object. All objects that implement
 * <code>Node</code> functionality are based on one of its subclasses. Most notable are
 * {@link Document}, {@link Element}, and {@link DocumentFragment}.
 * In addition, every kind of DOM node is represented by an interface based on
 * <code>Node</code>. These include <code>Attr</code>, <code>CharacterData</code>
 * (which <code>Text</code>, <code>Comment</code>, <code>CDATASection</code> and
 * <code>ProcessingInstruction</code> are all based on), and <code>DocumentType</code>.
 * In some cases, a particular feature of the base <code>Node</code> interface may not
 * apply to one of its child interfaces; in that case, the inheriting node may
 * return <code>null</code> or throw an exception, depending on circumstances. For example,
 * attempting to add children to a node type that cannot have children will throw an
 * exception.</p>
 */
@SuppressWarnings("unused")
public class Node extends EventTarget {

	/**
	 * An Element node like &lt;p&gt; or &lt;div&gt;
	 */
	public static final int ELEMENT_NODE = 1;

	/**
	 * An Attribute of an Element.
	 */
	public static final int ATTRIBUTE_NODE = 2;

	/**
	 * The actual Text inside an Element or Attr.
	 */
	public static final int TEXT_NODE = 3;

	/**
	 * A CDATASection, such as &lt;!CDATA[[ … ]]&lt;.
	 */
	public static final int CDATA_SECTION_NODE = 4;

	/**
	 * A ProcessingInstruction of an XML document, such as &lt;?xml-stylesheet … ?&lt;.
	 */
	public static final int PROCESSING_INSTRUCTION_NODE = 7;

	/**
	 * A Comment node, such as &lt;!-- … --&lt;.
	 */
	public static final int COMMENT_NODE = 8;

	/**
	 * A Document node.
	 */
	public static final int DOCUMENT_NODE = 9;

	/**
	 * A DocumentType node, such as &lt;!DOCTYPE html&lt;.
	 */
	public static final int DOCUMENT_TYPE_NODE = 10;

	/**
	 * A DocumentFragment node.
	 */
	public static final int DOCUMENT_FRAGMENT_NODE = 11;

	public static final int DOCUMENT_POSITION_DISCONNECTED = 1;
	public static final int DOCUMENT_POSITION_PRECEDING = 2;
	public static final int DOCUMENT_POSITION_FOLLOWING = 4;
	public static final int DOCUMENT_POSITION_CONTAINS = 8;
	public static final int DOCUMENT_POSITION_CONTAINED_BY = 16;
	public static final int DOCUMENT_POSITION_IMPLEMENTATION_SPECIFIC = 32;

	protected Node(Object peer) {
		super(peer);
	}

	//=========================
	// Properties
	//=========================

	/**
	 * <p>The read-only <strong><code>baseURI</code></strong> property of the {@link Node} interface
	 * returns the absolute base URL of the document containing the node.
	 * The base URL is used to resolve relative URLs when the browser needs to obtain an
	 * absolute URL, for example when processing the HTML <code>img</code> element's
	 * <code>src</code> attribute or the <code>xlink:href</code> or <code>href</code> attributes in SVG.
	 * Although this property is read-only, its value is determined by an algorithm each time
	 * the property is accessed, and may change if the conditions changed.
	 * The base URL is determined as follows:</p>
	 * <ol>
	 * <li>By default, the base URL is the location of the document
	 * (as determined by {@link Window#getLocation()}).</li>
	 * <li>If it is an HTML Document and there is a <code>Base</code> element in the document,
	 * the <code>href</code> value of the <em>first</em> <code>Base</code> element with such an attribute is used instead.</li>
	 * </ol>
	 * <h2>Value</h2>
	 * <p>A string representing the base URL of the {@link Node}.</p>
	 */
	public String getBaseURI() {
		return Wrap.String(JS.get(peer, "baseURI"));
	}

	/**
	 * <p>The read-only <strong><code>childNodes</code></strong> property of the {@link Node} interface returns a live
	 * {@link NodeList&quot;)}} of child {{domxref(&quot;Node} of the given element where
	 * the first child node is assigned index <code>0</code>. Child nodes include elements, text and
	 * comments.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> The {@link NodeList} being live means that its content is changed each time
	 * new children are added or removed.
	 * The items in the collection of nodes are objects, not strings. To get data from node
	 * objects, use their properties. For example, to get the name of the first
	 * childNode, you can use <code>elementNodeReference.childNodes[0].nodeName</code>.
	 * The {@link Browser#document} object itself has two children: the Doctype declaration and the
	 * root element, typically referred to as <code>documentElement</code>. In HTML
	 * documents the latter is the <code>html</code> element.
	 * It is important to keep in mind that <code>childNodes</code> includes <em>all</em> child nodes,
	 * including non-element nodes like text and comment.
	 * To get a collection containing only elements, use {@link Element#getChildren()} instead.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A live {@link NodeList} containing the children of the node.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> Several calls to <code>childNodes</code> return the <em>same</em> {@link NodeList}.</p>
	 * </blockquote>
	 */
	public NodeList getChildNodes() {
		return Wrap.NodeList(JS.get(peer, "childNodes"));
	}

	/**
	 * <p>The read-only <strong><code>firstChild</code></strong> property of the {@link Node} interface
	 * returns the node's first child in the tree,
	 * or <code>null</code> if the node has no children.
	 * If the node is a {@link Document},
	 * this property returns the first node in the list of its direct children.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> This property returns any type of node that is the first child of this one.
	 * It may be a {@link Text} or a {@link Comment} node.
	 * If you want to get the first {@link Element} that is a child of another element,
	 * consider using {@link Element#getFirstElementChild()}.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A {@link Node}, or <code>null</code> if there are none.</p>
	 */
	public Node getFirstChild() {
		return Wrap.Node(JS.get(peer, "firstChild"));
	}

	/**
	 * <p>The read-only <strong><code>isConnected</code></strong> property of the {@link Node} interface
	 * returns a boolean indicating whether the node is connected
	 * (directly or indirectly) to the context object,
	 * for example the {@link Document} object in the case of the normal DOM,
	 * or the {@link ShadowRoot} in the case of a shadow DOM.</p>
	 * <h2>Value</h2>
	 * <p>A boolean value that is <code>true</code> if the node is connected to its relevant context object,
	 * and <code>false</code> if not.</p>
	 */
	public boolean isConnected() {
		return JS.getBoolean(peer, "isConnected");
	}

	/**
	 * <p>The read-only <strong><code>lastChild</code></strong> property of the {@link Node} interface
	 * returns the last child of the node.
	 * If its parent is an element, then the child is generally an element node, a text node, or a comment node.
	 * It returns <code>null</code> if there are no child nodes.</p>
	 * <h2>Value</h2>
	 * <p>A {@link Node} that is the last child of the node, or <code>null</code> is there are no child nodes.</p>
	 */
	public Node getLastChild() {
		return Wrap.Node(JS.get(peer, "lastChild"));
	}

	/**
	 * <p>The read-only <strong><code>nextSibling</code></strong> property of the {@link Node} interface
	 * returns the node immediately following the specified one in their
	 * parent's {@link #getChildNodes()}, or returns <code>null</code>
	 * if the specified node is the last child in the parent element.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> Browsers insert {@link Text} nodes into a document to represent whitespace in the source markup.
	 * Therefore a node obtained, for example, using {@link Node#getFirstChild()}
	 * or {@link Node#getPreviousSibling()}
	 * may refer to a whitespace text node rather than the actual element the author
	 * intended to get.</p>
	 * <p>The article {@link Document_Object_Model/Whitespace}
	 * contains more information about this behavior.</p>
	 * <p>You can use {@link Element#getNextElementSibling()} to obtain the next element
	 * skipping any whitespace nodes, other between-element text, or comments.</p>
	 * <p>To navigate the opposite way through the child nodes list use {@link #getPreviousSibling()}.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A {@link Node} representing the next sibling of the current node,
	 * or <code>null</code> if there are none.</p>
	 */
	public Node getNextSibling() {
		return Wrap.Node(JS.get(peer, "nextSibling"));
	}

	/**
	 * <p>The read-only <strong><code>nodeName</code></strong> property of {@link Node} returns the name of the current node as a string.</p>
	 * <h2>Value</h2>
	 * <p>A string. Values for the different types of nodes are:</p>
	 * <ul>
	 * <li>{@link Attr}
	 * <ul>
	 * <li>: The value of {@link Attr.name}, that is the <em>qualified name</em> of the attribute.</li>
	 * </ul>
	 * </li>
	 * <li>{@link CDATASection}
	 * <ul>
	 * <li>: The string <code>&quot;#cdata-section&quot;</code>.</li>
	 * </ul>
	 * </li>
	 * <li>{@link Comment}
	 * <ul>
	 * <li>: The string <code>&quot;#comment&quot;</code>.</li>
	 * </ul>
	 * </li>
	 * <li>{@link Document}
	 * <ul>
	 * <li>: The string <code>&quot;#document&quot;</code>.</li>
	 * </ul>
	 * </li>
	 * <li>{@link DocumentFragment}
	 * <ul>
	 * <li>: The string <code>&quot;#document-fragment&quot;</code>.</li>
	 * </ul>
	 * </li>
	 * <li>{@link DocumentType}
	 * <ul>
	 * <li>: The value of {@link DocumentType#getName()}</li>
	 * </ul>
	 * </li>
	 * <li>{@link Element}
	 * <ul>
	 * <li>: The value of {@link Element#getTagName()}, that is the <em>uppercase</em> name of the element tag if an HTML element,
	 * or the <em>lowercase</em> element tag if an XML element (like a SVG or MATHML element).</li>
	 * </ul>
	 * </li>
	 * <li>{@link ProcessingInstruction}
	 * <ul>
	 * <li>: The value of {@link ProcessingInstruction#getTarget()}</li>
	 * </ul>
	 * </li>
	 * <li>{@link Text}
	 * <ul>
	 * <li>: The string <code>&quot;#text&quot;</code>.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public String getNodeName() {
		return Wrap.String(JS.get(peer, "nodeName"));
	}

	/**
	 * <p>The read-only <strong><code>nodeType</code></strong> property of a {@link Node} interface is an integer
	 * that identifies what the node is. It distinguishes different kind of nodes from each other,
	 * such as {@link Element}, {@link Text} and {@link Comment}.</p>
	 * <h2>Value</h2>
	 * <p>An integer which specifies the type of the node. Possible values are:</p>
	 * <ul>
	 * <li><code>Node.ELEMENT_NODE</code> (<code>1</code>)
	 * <ul>
	 * <li>: An {@link Element} node like <code>p</code> or <code>div</code>.</li>
	 * </ul>
	 * </li>
	 * <li><code>Node.ATTRIBUTE_NODE</code> (<code>2</code>)
	 * <ul>
	 * <li>: An {@link Attr} of an {@link Element}.</li>
	 * </ul>
	 * </li>
	 * <li><code>Node.TEXT_NODE</code> (<code>3</code>)
	 * <ul>
	 * <li>: The actual {@link Text} inside an {@link Element} or {@link Attr}.</li>
	 * </ul>
	 * </li>
	 * <li><code>Node.CDATA_SECTION_NODE</code>(<code>4</code>)
	 * <ul>
	 * <li>: A {@link CDATASection}, such as <code>&lt;!CDATA[[ … ]]&gt;</code></li>
	 * </ul>
	 * </li>
	 * <li><code>Node.PROCESSING_INSTRUCTION_NODE</code> (<code>7</code>)
	 * <ul>
	 * <li>: A {@link ProcessingInstruction} of an XML document, such as <code>&lt;?xml-stylesheet … ?&gt;</code>.</li>
	 * </ul>
	 * </li>
	 * <li><code>Node.COMMENT_NODE</code> (<code>8</code>)
	 * <ul>
	 * <li>: A {@link Comment} node, such as <code>&lt;!-- … --&gt;</code>.</li>
	 * </ul>
	 * </li>
	 * <li><code>Node.DOCUMENT_NODE</code> (<code>9</code>)
	 * <ul>
	 * <li>: A {@link Document} node.</li>
	 * </ul>
	 * </li>
	 * <li><code>Node.DOCUMENT_TYPE_NODE</code> (<code>10</code>)
	 * <ul>
	 * <li>: A {@link DocumentType} node, such as <code>&lt;!DOCTYPE html&gt;</code>.</li>
	 * </ul>
	 * </li>
	 * <li><code>Node.DOCUMENT_FRAGMENT_NODE</code> (<code>11</code>)
	 * <ul>
	 * <li>: A {@link DocumentFragment} node.
	 * The following constants have been deprecated and are not in use anymore: <code>Node.ENTITY_REFERENCE_NODE</code> (<code>5</code>),
	 * <code>Node.ENTITY_NODE</code> (<code>6</code>), and <code>Node.NOTATION_NODE</code> (<code>12</code>).</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public int getNodeType() {
		return JS.getInt(peer, "nodeType");
	}

	/**
	 * <p>The <strong><code>nodeValue</code></strong> property of the {@link Node} interface returns or sets the value of the current node.</p>
	 * <h2>Value</h2>
	 * <p>A string containing the value of the current node, if any.
	 * For the document itself, <code>nodeValue</code> returns <code>null</code>.
	 * For text, comment, and CDATA nodes, <code>nodeValue</code> returns the content of the node.
	 * For attribute nodes, the value of the attribute is returned.
	 * The following table shows the return values for different types of nodes.
	 * | Node                                 | Value of nodeValue                  |
	 * | ------------------------------------ | ----------------------------------- |
	 * | {@link CDATASection}          | Content of the CDATA section        |
	 * | {@link Comment}               | Content of the comment              |
	 * | {@link Document}              | <code>null</code>                              |
	 * | {@link DocumentFragment}      | <code>null</code>                              |
	 * | {@link DocumentType}          | <code>null</code>                              |
	 * | {@link Element}               | <code>null</code>                              |
	 * | {@link NamedNodeMap}          | <code>null</code>                              |
	 * | {@link ProcessingInstruction} | Entire content excluding the target |
	 * | {@link Text}                  | Content of the text node            |</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> When <code>nodeValue</code> is defined to be <code>null</code>, setting it has no effect.</p>
	 * </blockquote>
	 */
	public String getNodeValue() {
		return Wrap.String(JS.get(peer, "nodeValue"));
	}

	/**
	 * <p>The <strong><code>nodeValue</code></strong> property of the {@link Node} interface returns or sets the value of the current node.</p>
	 * <h2>Value</h2>
	 * <p>A string containing the value of the current node, if any.
	 * For the document itself, <code>nodeValue</code> returns <code>null</code>.
	 * For text, comment, and CDATA nodes, <code>nodeValue</code> returns the content of the node.
	 * For attribute nodes, the value of the attribute is returned.
	 * The following table shows the return values for different types of nodes.
	 * | Node                                 | Value of nodeValue                  |
	 * | ------------------------------------ | ----------------------------------- |
	 * | {@link CDATASection}          | Content of the CDATA section        |
	 * | {@link Comment}               | Content of the comment              |
	 * | {@link Document}              | <code>null</code>                              |
	 * | {@link DocumentFragment}      | <code>null</code>                              |
	 * | {@link DocumentType}          | <code>null</code>                              |
	 * | {@link Element}               | <code>null</code>                              |
	 * | {@link NamedNodeMap}          | <code>null</code>                              |
	 * | {@link ProcessingInstruction} | Entire content excluding the target |
	 * | {@link Text}                  | Content of the text node            |</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> When <code>nodeValue</code> is defined to be <code>null</code>, setting it has no effect.</p>
	 * </blockquote>
	 */
	public void setNodeValue(String value) {
		JS.set(peer, "nodeValue", value);
	}

	/**
	 * <p>The read-only <strong><code>ownerDocument</code></strong> property of the {@link Node} interface
	 * returns the top-level document object of the node.</p>
	 * <h2>Value</h2>
	 * <p>A {@link Document} that is the top-level object in which all the
	 * child nodes are created.
	 * If this property is used on a node that is itself a document, the value is <code>null</code>.</p>
	 */
	public Document getOwnerDocument() {
		return Wrap.Document(JS.get(peer, "ownerDocument"));
	}

	/**
	 * <p>The read-only <strong><code>parentElement</code></strong> property of {@link Node} interface
	 * returns the DOM node's parent <code>Element</code>, or <code>null</code> if the node either has no
	 * parent, or its parent isn't a DOM <code>Element</code>.</p>
	 * <h2>Value</h2>
	 * <p>An {@link Element} that is the parent element of the current node,
	 * or <code>null</code> if there isn't one.</p>
	 */
	public Element getParentElement() {
		return Wrap.Element(JS.get(peer, "parentElement"));
	}

	/**
	 * <p>The read-only <strong><code>parentNode</code></strong> property of the {@link Node} interface
	 * returns the parent of the specified node in the DOM tree.
	 * <code>Document</code> and <code>DocumentFragment</code> {@link Node/nodeType} can never have a parent, so
	 * <code>parentNode</code> will always return <code>null</code>.
	 * It also returns <code>null</code> if the node has just been created
	 * and is not yet attached to the tree.</p>
	 * <h2>Value</h2>
	 * <p>A {@link Node} that is the parent of the current node. The parent of an element is
	 * an <code>Element</code> node, a <code>Document</code> node, or a <code>DocumentFragment</code> node.</p>
	 */
	public Node getParentNode() {
		return Wrap.Node(JS.get(peer, "parentNode"));
	}

	/**
	 * <p>The read-only <strong><code>previousSibling</code></strong> property of the {@link Node} interface
	 * returns the node immediately preceding the specified one in its parent's
	 * {@link #getChildNodes()} list,
	 * or <code>null</code> if the specified node is the first in that list.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> Browsers insert text nodes into a document to represent whitespace in the source markup.
	 * Therefore a node obtained, for example, using {@link Node/firstChild}
	 * or <code>Node.previousSibling</code>
	 * may refer to a whitespace text node rather than the actual element the author intended to get.</p>
	 * <p>See {@link Document_Object_Model/Whitespace} for more information.</p>
	 * <p>You can use {@link Element#getPreviousElementSibling()}
	 * to get the previous element node (skipping text nodes and any other non-element nodes).</p>
	 * <p>To navigate the opposite way through the child nodes list use {@link Node#getNextSibling()}.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A {@link Node} representing the previous sibling of the current node,
	 * or <code>null</code> if there are none.</p>
	 */
	public Node getPreviousSibling() {
		return Wrap.Node(JS.get(peer, "previousSibling"));
	}

	/**
	 * <p>The <strong><code>textContent</code></strong> property of the <code>Node</code>
	 * interface represents the text content of the node and its descendants.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> <code>textContent</code> and {@link HTMLElement#getInnerText()} are easily confused,
	 * but the two properties are <a href="#differences_from_innertext">different in important ways</a>.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A string, or <code>null</code>. Its value depends on the situation:</p>
	 * <ul>
	 * <li>If the node is a <code>document</code> or a <code>doctype</code>,
	 * <code>textContent</code> returns <code>null</code>.
	 * <blockquote>
	 * <p><strong>Note:</strong> To get <em>all</em> of the text and {@link CDATASection} for the whole
	 * document, use <code>document.documentElement.textContent</code>.</p>
	 * </blockquote>
	 * </li>
	 * <li>If the node is a {@link CDATASection},
	 * a comment, a {@link ProcessingInstruction},
	 * or a {@link Text},
	 * <code>textContent</code> returns, or sets, the text inside the node,
	 * i.e., the {@link #getNodeValue()}.</li>
	 * <li>For other node types, <code>textContent</code> returns the concatenation of the
	 * <code>textContent</code> of every child node, excluding comments and processing
	 * instructions. (This is an empty string if the node has no children.)</li>
	 * </ul>
	 * <blockquote>
	 * <p><strong>Warning:</strong> Setting <code>textContent</code> on a node removes <em>all</em> of the node's children
	 * and replaces them with a single text node with the given string value.</p>
	 * </blockquote>
	 */
	public String getTextContent() {
		return Wrap.String(JS.get(peer, "textContent"));
	}

	/**
	 * <p>The <strong><code>textContent</code></strong> property of the <code>Node</code>
	 * interface represents the text content of the node and its descendants.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> <code>textContent</code> and {@link HTMLElement#getInnerText()} are easily confused,
	 * but the two properties are <a href="#differences_from_innertext">different in important ways</a>.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A string, or <code>null</code>. Its value depends on the situation:</p>
	 * <ul>
	 * <li>If the node is a <code>document</code> or a <code>doctype</code>,
	 * <code>textContent</code> returns <code>null</code>.
	 * <blockquote>
	 * <p><strong>Note:</strong> To get <em>all</em> of the text and {@link CDATASection} for the whole
	 * document, use <code>document.documentElement.textContent</code>.</p>
	 * </blockquote>
	 * </li>
	 * <li>If the node is a {@link CDATASection},
	 * a comment, a {@link ProcessingInstruction},
	 * or a {@link Text},
	 * <code>textContent</code> returns, or sets, the text inside the node,
	 * i.e., the {@link Node#getNodeValue()}.</li>
	 * <li>For other node types, <code>textContent</code> returns the concatenation of the
	 * <code>textContent</code> of every child node, excluding comments and processing
	 * instructions. (This is an empty string if the node has no children.)</li>
	 * </ul>
	 * <blockquote>
	 * <p><strong>Warning:</strong> Setting <code>textContent</code> on a node removes <em>all</em> of the node's children
	 * and replaces them with a single text node with the given string value.</p>
	 * </blockquote>
	 */
	public void setTextContent(String value) {
		JS.set(peer, "textContent", JS.param(value));
	}

	//=========================
	// Methods
	//=========================

	/**
	 * <p>The <strong><code>appendChild()</code></strong> method of the {@link Node} interface adds a node to the end of the list of children of a specified parent node. If the given child is a reference to an existing node in the document, <code>appendChild()</code> moves it from its current position to the new position.
	 * If the given child is a {@link DocumentFragment}, the entire contents of the {@link DocumentFragment} are moved into the child list of the specified parent node.
	 * <code>appendChild()</code> returns the newly appended node, or if the child is a {@link DocumentFragment}, the emptied fragment.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> Unlike this method, the {@link Element#append()} method supports multiple arguments and appending strings. You can prefer using it if your node is an element.</p>
	 * </blockquote>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>aChild</code>
	 * <ul>
	 * <li>: The node to append to the given parent node (commonly an element).</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>A {@link Node} that is the appended child (<code>aChild</code>), except when <code>aChild</code> is a {@link DocumentFragment}, in which case the empty {@link DocumentFragment} is returned.</p>
	 */
	public Node appendChild(Node child) {
		return Wrap.Node(JS.invoke(peer, "appendChild"));
	}

	/**
	 * <p>The <strong><code>cloneNode()</code></strong> method of the {@link Node} interface
	 * returns a duplicate of the node on which this method was called.
	 * Its parameter controls if the subtree contained in a node is also cloned or not.
	 * Cloning a node copies all of its attributes and their values,
	 * including intrinsic (inline) listeners. It does <em>not</em> copy event listeners added
	 * using {@link EventTarget/addEventListener} or
	 * those assigned to element properties (e.g., <code>node.onclick = someFunction</code>).
	 * Additionally, for a <code>canvas</code> element, the painted image is not copied.</p>
	 * <blockquote>
	 * <p><strong>Warning:</strong> <code>cloneNode()</code> may lead to duplicate element IDs in a document!</p>
	 * <p>If the original node has an <code>id</code> attribute, and the clone
	 * will be placed in the same document, then you should modify the clone's ID to be
	 * unique.</p>
	 * <p>Also, <code>name</code> attributes may need to be modified,
	 * depending on whether duplicate names are expected.
	 * To clone a node to insert into a <em>different</em> document, use
	 * {@link Document#importNode()} instead.</p>
	 * </blockquote>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>deep</code> optional_inline
	 * <ul>
	 * <li>: If <code>true</code>, then the node and its whole subtree,
	 * including text that may be in child {@link Text} nodes,
	 * is also copied.
	 * If <code>false</code>, only the node will be cloned.
	 * The subtree, including any text that the node contains, is not cloned.
	 * Note that <code>deep</code> has no effect on empty elements,
	 * such as the <code>img</code> and <code>input</code> elements.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>The new {@link Node} cloned.
	 * The cloned node has no parent and is not part of the document,
	 * <em>until</em> it is added to another node that is part of the document,
	 * using {@link #appendChild(Node)} or a similar method.</p>
	 */
	public Node cloneNode() {
		return Wrap.Node(JS.invoke(peer, "cloneNode"));
	}

	/**
	 * <p>The <strong><code>compareDocumentPosition()</code></strong> method of the {@link Node} interface
	 * reports the position of its argument node relative to the node on which it is called.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>otherNode</code>
	 * <ul>
	 * <li>: The {@link Node} for which position should be reported, relative to the node.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>An integer value representing <code>otherNode</code>'s position relative to <code>node</code>
	 * as a <a href="https://en.wikipedia.org/wiki/Mask_(computing)">bitmask</a> combining the
	 * following constant properties of {@link Node}:</p>
	 * <ul>
	 * <li><code>Node.DOCUMENT_POSITION_DISCONNECTED</code> (<code>1</code>)
	 * <ul>
	 * <li>: Both nodes are in different documents or different trees in the same document.</li>
	 * </ul>
	 * </li>
	 * <li><code>Node.DOCUMENT_POSITION_PRECEDING</code> (<code>2</code>)
	 * <ul>
	 * <li>: <code>otherNode</code> precedes the node in either a <a href="https://en.wikipedia.org/wiki/Tree_traversal#Pre-order,_NLR">pre-order depth-first traversal</a> of a tree containing both (e.g., as an ancestor or previous sibling or a descendant of a previous sibling or previous sibling of an ancestor) or (if they are disconnected) in an arbitrary but consistent ordering.</li>
	 * </ul>
	 * </li>
	 * <li><code>Node.DOCUMENT_POSITION_FOLLOWING</code> (<code>4</code>)
	 * <ul>
	 * <li>: <code>otherNode</code> follows the node in either a <a href="https://en.wikipedia.org/wiki/Tree_traversal#Pre-order,_NLR">pre-order depth-first traversal</a> of a tree containing both (e.g., as a descendant or following sibling or a descendant of a following sibling or following sibling of an ancestor) or (if they are disconnected) in an arbitrary but consistent ordering.</li>
	 * </ul>
	 * </li>
	 * <li><code>Node.DOCUMENT_POSITION_CONTAINS</code> (<code>8</code>)
	 * <ul>
	 * <li>: <code>otherNode</code> is an ancestor of the node.</li>
	 * </ul>
	 * </li>
	 * <li><code>Node.DOCUMENT_POSITION_CONTAINED_BY</code> (<code>16</code>)
	 * <ul>
	 * <li>: <code>otherNode</code> is a descendant of the node.</li>
	 * </ul>
	 * </li>
	 * <li><code>Node.DOCUMENT_POSITION_IMPLEMENTATION_SPECIFIC</code> (<code>32</code>)
	 * <ul>
	 * <li>: The result relies upon arbitrary and/or implementation-specific behavior and is not guaranteed to be portable.
	 * More than one bit is set if multiple scenarios apply. For example, if
	 * <code>otherNode</code> is located earlier in the document <strong><em>and</em></strong>
	 * contains the node on which <code>compareDocumentPosition()</code> was
	 * called, then both the <code>DOCUMENT_POSITION_CONTAINS</code> and
	 * <code>DOCUMENT_POSITION_PRECEDING</code> bits would be set, producing a value of <code>10</code> (<code>0x0A</code>).</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public int compareDocumentPosition(Node otherNode) {
		return JS.invoke(peer, "compareDocumentPosition", JS.param(otherNode));
	}

	/**
	 * <p>The <strong><code>contains()</code></strong> method of the {@link Node} interface
	 * returns a boolean value indicating
	 * whether a node is a descendant of a given node, that is the node itself,
	 * one of its direct children ({@link Node#getChildNodes()}),
	 * one of the children's direct children, and so on.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> A node is <em>contained</em> inside itself.</p>
	 * </blockquote>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>otherNode</code>
	 * <ul>
	 * <li>: The {@link Node} to test with.
	 * <blockquote>
	 * <p><strong>Note:</strong> <code>otherNode</code> is not optional, but can be set to <code>null</code>.</p>
	 * </blockquote>
	 * </li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>A boolean value that is <code>true</code> if <code>otherNode</code> is contained in the node,
	 * <code>false</code> if not.
	 * If the <code>otherNode</code> parameter is <code>null</code>,
	 * <code>contains()</code> always returns <code>false</code>.</p>
	 */
	public boolean contains(Node otherNode) {
		return JS.invoke(peer, "contains", JS.param(otherNode));
	}

	/**
	 * <p>The <strong><code>getRootNode()</code></strong> method of the {@link Node} interface
	 * returns the context object's root,
	 * which optionally includes the shadow root if it is available.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>options</code> optional_inline
	 * <ul>
	 * <li>: An object that sets options for getting the root node. The available options are:
	 * <ul>
	 * <li><code>composed</code>: A boolean value that indicates whether the shadow
	 * root should be returned (<code>false</code>, the default), or a root node beyond
	 * shadow root (<code>true</code>).</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>An object inheriting from {@link Node}. This will differ in exact form depending
	 * on where you called <code>getRootNode()</code>; for example:</p>
	 * <ul>
	 * <li>Calling it on an element inside a standard web page will return an
	 * {@link HTMLDocument} object representing the entire page.</li>
	 * <li>Calling it on an element inside a shadow DOM will return the associated
	 * {@link ShadowRoot}.</li>
	 * </ul>
	 */
	public Node getRootNode(JSObject options) {
		return Wrap.Node(JS.invoke(peer, "getRootNode", JS.param(options)));
	}

	/**
	 * See: {@link #getRootNode(JSObject)}
	 */
	public Node getRootNode() {
		return Wrap.Node(JS.invoke(peer, "getRootNode"));
	}

	/**
	 * <p>The <strong><code>hasChildNodes()</code></strong> method of the {@link Node} interface
	 * returns a boolean value indicating
	 * whether the given {@link Node} has {@link Node/childNodes} or not.</p>
	 * <h3>Parameters</h3>
	 * <p>None.</p>
	 * <h3>Return value</h3>
	 * <p>A boolean value that is <code>true</code> if the node has child nodes, and
	 * <code>false</code> otherwise.</p>
	 */
	public boolean hasChildNodes() {
		return JS.invoke(peer, "hasChildNodes");
	}

	/**
	 * <p>The <strong><code>insertBefore()</code></strong> method of the {@link Node} interface
	 * inserts a node before a <em>reference node</em> as a child of a specified <em>parent node</em>.
	 * If the given node already exists in the document,
	 * <code>insertBefore()</code> moves it from its current position to the new position.
	 * (That is, it will automatically be removed from its existing parent
	 * before appending it to the specified new parent.)
	 * This means that a node cannot be in two locations of the document simultaneously.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> The {@link #cloneNode()} can be used to make a copy
	 * of the node before appending it under the new parent. Note that the copies made with
	 * <code>cloneNode()</code> will not be automatically kept in sync.
	 * If the given child is a {@link DocumentFragment}, the entire contents of the
	 * <code>DocumentFragment</code> are moved into the child list of the specified parent
	 * node.</p>
	 * </blockquote>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>newNode</code>
	 * <ul>
	 * <li>: The node to be inserted.</li>
	 * </ul>
	 * </li>
	 * <li><code>referenceNode</code>
	 * <ul>
	 * <li>: The node before which <code>newNode</code> is inserted. If this is
	 * <code>null</code>, then <code>newNode</code> is inserted at the end of
	 * node's child nodes.
	 * <blockquote>
	 * <p><strong>Note:</strong> <code>referenceNode</code> is <strong>not</strong> an optional parameter.
	 * You must explicitly pass a {@link Node} or <code>null</code>.
	 * Failing to provide it or passing invalid values may <a href="https://bugs.chromium.org/p/chromium/issues/detail?id=419780">behave</a> <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=119489">differently</a> in different browser versions.</p>
	 * </blockquote>
	 * </li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>Returns the added child (unless <code>newNode</code> is a {@link DocumentFragment},
	 * in which case the empty {@link DocumentFragment} is returned).</p>
	 */
	public Node insertBefore(Node newNode, Node referenceNode) {
		return Wrap.Node(JS.invoke(peer, "insertBefore", JS.param(newNode), JS.param(referenceNode)));
	}

	/**
	 * <p>The <strong><code>isDefaultNamespace()</code></strong> method of the {@link Node} interface
	 * accepts a namespace URI as an argument.
	 * It returns a boolean value that is <code>true</code>
	 * if the namespace is the default namespace on the given node
	 * and <code>false</code> if not.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> The default namespace of an HTML element is always <code>&quot;&quot;</code>. For a SVG element, it is set by the <code>xmlns</code> attribute.</p>
	 * </blockquote>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>namespaceURI</code>
	 * <ul>
	 * <li>: A string representing the namespace against which the element will be checked.
	 * <blockquote>
	 * <p><strong>Note:</strong> <code>namespaceURI</code> is not an optional parameter, but can be <code>null</code>.</p>
	 * </blockquote>
	 * </li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>A boolean value that holds the return value <code>true</code> or <code>false</code>,
	 * indicating if the parameter is the default namespace, or not.</p>
	 */
	public boolean isDefaultNamespace(String namespaceURI) {
		return JS.invoke(peer, "isDefaultNamespace", JS.param(namespaceURI));
	}

	/**
	 * <p>The <strong><code>isEqualNode()</code></strong> method of the {@link Node} interface tests whether two nodes are equal.
	 * Two nodes are equal when they have the same type, defining characteristics (for
	 * elements, this would be their ID, number of children, and so forth), its attributes
	 * match, and so on. The specific set of data points that must match varies depending on
	 * the types of the nodes.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>otherNode</code>
	 * <ul>
	 * <li>: The {@link Node} to compare equality with.
	 * <blockquote>
	 * <p><strong>Note:</strong> This parameter is not optional, but can be set to <code>null</code>.</p>
	 * </blockquote>
	 * </li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>A boolean value that is <code>true</code> if the two nodes are equals, or <code>false</code> if not.
	 * If <code>otherNode</code> is <code>null</code>, <code>isEqualNode()</code> always return false.</p>
	 */
	public boolean isEqualNode(Node otherNode) {
		return JS.invoke(peer, "isEqualNode", JS.param(otherNode));
	}

	/**
	 * <p>The <strong><code>isSameNode()</code></strong> method of the {@link Node} interface
	 * is a legacy alias the for the <code>===</code> strict equality operator.
	 * That is, it tests whether two nodes are the same
	 * (in other words, whether they reference the same object).</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> There is no need to use <code>isSameNode()</code>; instead use the <code>===</code> strict equality operator.</p>
	 * </blockquote>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>otherNode</code>
	 * <ul>
	 * <li>: The {@link Node} to test against.
	 * <blockquote>
	 * <p><strong>Note:</strong> This parameter is not optional, but can be set to <code>null</code>.</p>
	 * </blockquote>
	 * </li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>A boolean value that is <code>true</code> if both nodes are strictly equal, <code>false</code> if not.</p>
	 */
	public boolean isSameNode(Node otherNode) {
		return JS.invoke(peer, "isSameNode", JS.param(otherNode));
	}

	// isSupported DEPRECATED

	/**
	 * <p>The <strong><code>lookupNamespaceURI()</code></strong> method of the {@link Node} interface
	 * takes a prefix as parameter and returns the namespace URI associated with it on the given node if found (and
	 * <code>null</code> if not).</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>prefix</code>
	 * <ul>
	 * <li>: The prefix to look for.
	 * <blockquote>
	 * <p><strong>Note:</strong> This parameter is not optional, but can be set to <code>null</code>.</p>
	 * </blockquote>
	 * </li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>A string containing the namespace URI corresponding to the prefix.
	 * If the prefix is not found, it returns <code>null</code>.
	 * If the requested <code>prefix</code> is <code>null</code>, it returns the default namespace URI.</p>
	 */
	public String lookupNamespaceURI(String prefix) {
		return Wrap.String(JS.invoke(peer, "lookupNamespaceURI", JS.param(prefix)));
	}

	/**
	 * <p>The <strong><code>lookupPrefix()</code></strong> method of the {@link Node} interface
	 * returns a string containing the prefix for a given namespace URI, if present,
	 * and <code>null</code> if not.
	 * When multiple prefixes are possible, the first prefix is returned.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>namespace</code>
	 * <ul>
	 * <li>: A string containing the namespace to look the prefix up.
	 * <blockquote>
	 * <p><strong>Note:</strong> This parameter is not optional but can be set to <code>null</code>.</p>
	 * </blockquote>
	 * </li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>A string containing the corresponding prefix, or <code>null</code> if none has been found.
	 * If <code>namespace</code> is null, or the empty string, <code>lookupPrefix()</code> returns <code>null</code>.
	 * If the node is a {@link DocumentType} or a {@link DocumentFragment},
	 * <code>lookupPrefix()</code> always returns <code>null</code>.</p>
	 */
	public String lookupPrefix(String namespace) {
		return Wrap.String(JS.invoke(peer, "lookupPrefix", JS.param(namespace)));
	}

	/**
	 * <p>The <strong><code>normalize()</code></strong> method of the {@link Node} interface puts the specified node
	 * and all of its sub-tree into a <em>normalized</em> form.
	 * In a normalized sub-tree, no text nodes in the sub-tree are empty and there are no adjacent text nodes.</p>
	 * <h3>Parameters</h3>
	 * <p>None.</p>
	 * <h3>Return value</h3>
	 * <p>None.</p>
	 */
	public void normalize() {
		JS.invoke(peer, "normalize");
	}

	/**
	 * <p>The <strong><code>removeChild()</code></strong> method of the {@link Node} interface
	 * removes a child node from the DOM and returns the removed node.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> As long as a reference is kept on the removed child,
	 * it still exists in memory, but is no longer part of the DOM.
	 * It can still be reused later in the code.</p>
	 * <p>If the return value of <code>removeChild()</code> is not stored, and no other reference is kept,
	 * it will be automatically deleted from memory after a short time.
	 * Unlike {@link Node#cloneNode()} the return value preserves the {@link EventListener} objects associated with it.</p>
	 * </blockquote>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>child</code>
	 * <ul>
	 * <li>: A {@link Node} that is the child node to be removed from the DOM.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public void removeChild(Node child) {
		JS.invoke(peer, "removeChild", JS.param(child));
	}

	/**
	 * <p>The <strong><code>replaceChild()</code></strong> method of the {@link Node} element
	 * replaces a child node within the given (parent) node.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>newChild</code>
	 * <ul>
	 * <li>: The new node to replace <code>oldChild</code>.
	 * <blockquote>
	 * <p><strong>Warning:</strong> If the new node is already present somewhere else in the DOM, it is first removed from that position.</p>
	 * </blockquote>
	 * </li>
	 * </ul>
	 * </li>
	 * <li><code>oldChild</code>
	 * <ul>
	 * <li>: The child to be replaced.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <blockquote>
	 * <p><strong>Note:</strong> The parameter order, <em>new</em> before <em>old</em>, is unusual.
	 * {@link Element/replaceWith}, applying only to nodes that are elements,
	 * may be easier to read and use.</p>
	 * </blockquote>
	 * <h3>Return value</h3>
	 * <p>The replaced {@link Node}. This is the same node as <code>oldChild</code>.</p>
	 */
	public Node replaceChild(Node newChild, Node oldChild) {
		return Wrap.Node(JS.invoke(peer, "replaceChild", JS.param(newChild), JS.param(oldChild)));
	}

}
