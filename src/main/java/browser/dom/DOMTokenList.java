package browser.dom;

import browser.javascript.JS;
import browser.javascript.Peered;

/**
 * <p>The <strong><code>DOMTokenList</code></strong> interface represents a set of space-separated tokens. Such a set is returned by {@link Element#classList} or {@link HTMLLinkElement#relList}, and many others.
 * A <code>DOMTokenList</code> is indexed beginning with <code>0</code> as with JavaScript <code>Array</code> objects. <code>DOMTokenList</code> is always case-sensitive.</p>
 */
@SuppressWarnings({"unused", "DanglingJavadoc"})
public class DOMTokenList extends Peered {

	protected DOMTokenList(Object peer) {
		super(peer);
	}

	//=========================
	// Properties
	//=========================

	/**
	 * <p>The read-only <strong><code>length</code></strong> property of the {@link DOMTokenList} interface is an <code>integer</code> representing the number
	 * of objects stored in the object.</p>
	 * <h2>Value</h2>
	 * <p>An positive integer, or <code>0</code> if the list is empty.</p>
	 */
	public int getLength() {
		return JS.getInt(peer, "length");
	}

	/**
	 * <p>The <strong><code>value</code></strong> property of the {@link DOMTokenList}
	 * interface is a <code>stringifier</code> that returns the value of the list serialized as a
	 * string, or clears and sets the list to the given value.</p>
	 * <h2>Value</h2>
	 * <p>A string representing the serialized content of the list.
	 * Each item is separated by a space.</p>
	 */
	public String getValue() {
		return Wrap.String(JS.get(peer, "value"));
	}

	/**
	 * <p>The <strong><code>value</code></strong> property of the {@link DOMTokenList}
	 * interface is a <code>stringifier</code> that returns the value of the list serialized as a
	 * string, or clears and sets the list to the given value.</p>
	 * <h2>Value</h2>
	 * <p>A string representing the serialized content of the list.
	 * Each item is separated by a space.</p>
	 */
	public void setValue(String value) {
		JS.set(peer, "value", JS.param(value));
	}

	//=========================
	// Methods
	//=========================

	/**
	 * <p>The <strong><code>add()</code></strong> method of the {@link DOMTokenList} interface adds the given tokens to the list, omitting any that are already present.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>tokenN</code>
	 * <ul>
	 * <li>: A string representing a token (or tokens) to add to the <code>DOMTokenList</code>.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>None.</p>
	 */
	public void add(String token) {
		JS.invoke(peer, "add", JS.param(token));
	}

	/**
	 * <p>The <strong><code>contains()</code></strong> method of the {@link DOMTokenList} interface
	 * returns a boolean value — <code>true</code> if the underlying list contains the given token,
	 * otherwise <code>false</code>.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>token</code>
	 * <ul>
	 * <li>: A string representing the token
	 * you want to check for the existence of in the list.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>A boolean value, which is <code>true</code> if the calling list contains
	 * <code>token</code>, otherwise <code>false</code>.</p>
	 */
	public boolean contains(String token) {
		return JS.invoke(peer, "contains", JS.param(token));
	}

	/**
	 * <p>The <strong><code>entries()</code></strong> method of the {@link DOMTokenList} interface
	 * returns an <code>Iteration_protocols&quot;,'iterator</code> allowing you
	 * to go through all key/value pairs contained in this object. The values are
	 * <code>Array</code>s which have [key, value] pairs, each representing a single token.</p>
	 * <h3>Return value</h3>
	 * <p>Returns an <code>Iteration_protocols&quot;,&quot;iterator</code>.</p>
	 */
	//TODO check it
//	public void entries() {
//		JS.invoke(peer, "entries");
//	}

	/**
	 * <p>The <strong><code>forEach()</code></strong> method of the {@link DOMTokenList} interface
	 * calls the callback given in parameter once for each value pair in the list, in
	 * insertion order.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>callback</code>
	 * <ul>
	 * <li>: The function to execute for each element, eventually taking three arguments:
	 * <ul>
	 * <li><code>currentValue</code>
	 * <ul>
	 * <li>: The current element being processed in the array.</li>
	 * </ul>
	 * </li>
	 * <li><code>currentIndex</code>
	 * <ul>
	 * <li>: The index of the current element being processed in the array.</li>
	 * </ul>
	 * </li>
	 * <li><code>listObj</code>
	 * <ul>
	 * <li>: The array that <code>forEach()</code> is being applied to.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * </li>
	 * </ul>
	 * </li>
	 * <li><code>thisArg</code> Optional_inline
	 * <ul>
	 * <li>: The value to use as <code>Operators/this&quot;, &quot;this</code> when executing <code>callback</code>.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>None.</p>
	 */
	//TODO check it
//	public void forEach() {
//		JS.invoke(peer, "forEach");
//	}

	/**
	 * <p>The <strong><code>item()</code></strong> method of the {@link DOMTokenList} interface returns an item in the list,
	 * determined by its position in the list, its index.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> This method is equivalent as the operator <code>[]</code>.
	 * So <code>aList.item(i)</code> is the same as <code>aListi]</code>, like the [operator[] of an <code>Array</code>.</p>
	 * </blockquote>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>index</code>
	 * <ul>
	 * <li>: A number representing the index of the item you want to return. If it isn't an integer, only the integer part is considered.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>A string representing the returned item,
	 * or <code>null</code> if the number is greater than or equal to the <code>length</code> of the list.</p>
	 */
	//TODO check it
	public String item(int index) {
		return Wrap.String(JS.invoke(peer, "item", index));
	}

	/**
	 * <p>The <strong><code>keys()</code></strong> method of the {@link DOMTokenList} interface
	 * returns an jsxref(&quot;Iteration_protocols&quot;,'iterator',&quot;&quot;,1) allowing to go through all keys contained in this object.
	 * The keys are unsigned integers.</p>
	 * <h3>Parameters</h3>
	 * <p>None.</p>
	 * <h3>Return value</h3>
	 * <p>Returns an jsxref(&quot;Iteration_protocols&quot;,&quot;iterator&quot;,&quot;&quot;,1).</p>
	 */
	//TODO check it
//	public void keys() {
//		JS.invoke(peer, "keys");
//	}

	/**
	 * <p>The <strong><code>remove()</code></strong> method of the {@link DOMTokenList} interface
	 * removes the specified <em>tokens</em> from the list.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>token1</code>, …, <code>tokenN</code>
	 * <ul>
	 * <li>: A string representing the token you want to remove from the list.
	 * If the string is not in the list, no error is thrown, and nothing happens.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>None (<code>undefined</code>).</p>
	 */
	//TODO check it
//	public void remove() {
//		JS.invoke(peer, "remove");
//	}

	/**
	 * <p>The <strong><code>replace()</code></strong> method of the {@link DOMTokenList} interface
	 * replaces an existing token with a new token.
	 * If the first token doesn't exist, <code>replace()</code> returns <code>false</code> immediately,
	 * without adding the new token to the token list.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>oldToken</code>
	 * <ul>
	 * <li>: A string representing the token you want to replace.</li>
	 * </ul>
	 * </li>
	 * <li><code>newToken</code>
	 * <ul>
	 * <li>: A string representing the token you want to replace <code>oldToken</code> with.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>A boolean value, which is <code>true</code> if <code>oldToken</code> was
	 * successfully replaced, or <code>false</code> if not.</p>
	 */
	public boolean replace(String oldToken, String newToken) {
		return JS.invoke(peer, "replace", JS.param(oldToken), JS.param(newToken));
	}

	/**
	 * <p>The <strong><code>supports()</code></strong> method of the {@link DOMTokenList} interface
	 * returns <code>true</code> if a given <code>token</code> is in the associated attribute's supported tokens.
	 * This method is intended to support feature detection.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>token</code>
	 * <ul>
	 * <li>: A string containing the token to query for.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public boolean supports(String token) {
		return JS.invoke(peer, "supports", JS.param(token));
	}

	/**
	 * <p>The <strong><code>toggle()</code></strong> method of the {@link DOMTokenList} interface
	 * removes an existing token from the list and returns <code>false</code>.
	 * If the token doesn't exist it's added and the function returns <code>true</code>.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>token</code>
	 * <ul>
	 * <li>: A string representing the token you want to toggle.</li>
	 * </ul>
	 * </li>
	 * <li><code>force</code> optional_inline
	 * <ul>
	 * <li>: If included, turns the toggle into a one way-only operation.
	 * If set to <code>false</code>, then <code>token</code> will <em>only</em> be removed, but not added.
	 * If set to <code>true</code>, then <code>token</code> will <em>only</em> be added, but not removed.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>A boolean value, <code>true</code> or <code>false</code>, indicating whether <code>token</code> is in the
	 * list after the call or not.</p>
	 */
	public boolean toggle(String token, boolean force) {
		return JS.invoke(peer, "toggle", JS.param(token), force);
	}

	/**
	 * See: {@link #toggle(String, boolean)}
	 */
	public boolean toggle(String token) {
		return JS.invoke(peer, "toggle", JS.param(token));
	}

	/**
	 * <p>The <strong><code>values()</code></strong> method of the {@link DOMTokenList} interface
	 * returns an <code>Iteration_protocols&quot;,'iterator</code>
	 * allowing the caller to go through all values contained in the <code>DOMTokenList</code>.
	 * The individual values are strings.</p>
	 * <h3>Parameters</h3>
	 * <p>None.</p>
	 * <h3>Return value</h3>
	 * <p>Returns an <code>Iteration_protocols&quot;,&quot;iterator</code>.</p>
	 */
	//TODO check it
//	public void values() {
//		JS.invoke(peer, "values");
//	}

}
