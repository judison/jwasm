package browser.dom;

/**
 * <p>The <strong><code>CDATASection</code></strong> interface represents a CDATA section
 * that can be used within XML to include extended portions of unescaped text.
 * When inside a CDATA section, the symbols <code>&lt;</code> and <code>&amp;</code> don't need escaping
 * as they normally do.
 * In XML, a CDATA section looks like:</p>
 * <pre><code class="language-xml">&lt;![CDATA[ … ]]&gt;
 * </code></pre>
 * <p>For example:</p>
 * <pre><code class="language-html">&lt;foo&gt;
 *   Here is a CDATA section: &lt;![CDATA[ &lt; &gt; &amp; ]]&gt; with all kinds of unescaped text.
 * &lt;/foo&gt;
 * </code></pre>
 * <p>The only sequence which is not allowed within a CDATA section is the closing sequence
 * of a CDATA section itself, <code>]]&gt;</code>.</p>
 * <blockquote>
 * <p><strong>Note:</strong> CDATA sections should not be used within HTML they are considered as comments and not displayed.</p>
 * </blockquote>
 */
@SuppressWarnings("unused")
public class CDATASection extends Node {

	protected CDATASection(Object peer) {
		super(peer);
	}

	//=========================
	// Properties
	//=========================

	//=========================
	// Methods
	//=========================

}
