package browser.dom;

/**
 * <p>The <strong><code>Comment</code></strong> interface represents textual notations within markup; although it is generally not visually shown, such comments are available to be read in the source view.
 * Comments are represented in HTML and XML as content between '<code>&lt;!--</code>' and '<code>--&gt;</code>'. In XML, like inside SVG or MathML markup, the character sequence '<code>--</code>' cannot be used within a comment.</p>
 */
@SuppressWarnings("unused")
public class Comment extends CharacterData {

	protected Comment(Object peer) {
		super(peer);
	}

	//=========================
	// Properties
	//=========================

	//=========================
	// Methods
	//=========================

}
