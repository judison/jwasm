package browser.dom;

import browser.javascript.JS;
import browser.javascript.JSObject;

/**
 * <p><strong><code>Element</code></strong> is the most general base class from which all element objects (i.e. objects that represent elements) in a <code>Document</code> inherit. It only has methods and properties common to all kinds of elements. More specific classes inherit from <code>Element</code>.
 * For example, the <code>HTMLElement</code> interface is the base interface for HTML elements, while the <code>SVGElement</code> interface is the basis for all SVG elements. Most functionality is specified further down the class hierarchy.
 * Languages outside the realm of the Web platform, like XUL through the <code>XULElement</code> interface, also implement <code>Element</code>.</p>
 */
@SuppressWarnings({"unused", "DanglingJavadoc"})
public class Element extends Node {

	protected Element(Object peer) {
		super(peer);
	}

	//=========================
	// Properties
	//=========================

	/**
	 * <p>The <strong><code>ariaAtomic</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-atomic</code> attribute, which indicates whether assistive technologies will present all, or only parts of, the changed region based on the change notifications defined by the {@link aria-relevant} attribute.</p>
	 * <h2>Value</h2>
	 * <p>A string with one of the following values:</p>
	 * <ul>
	 * <li><code>&quot;false&quot;</code>
	 * <ul>
	 * <li>: Assistive technologies will present only the changed node or nodes.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;true&quot;</code>
	 * <ul>
	 * <li>: Assistive technologies will present the entire changed region as a whole, including the author-defined label if one exists.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public String getAriaAtomic() {
		return Wrap.String(JS.get(peer, "ariaAtomic"));
	}

	/**
	 * <p>The <strong><code>ariaAtomic</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-atomic</code> attribute, which indicates whether assistive technologies will present all, or only parts of, the changed region based on the change notifications defined by the {@link aria-relevant} attribute.</p>
	 * <h2>Value</h2>
	 * <p>A string with one of the following values:</p>
	 * <ul>
	 * <li><code>&quot;false&quot;</code>
	 * <ul>
	 * <li>: Assistive technologies will present only the changed node or nodes.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;true&quot;</code>
	 * <ul>
	 * <li>: Assistive technologies will present the entire changed region as a whole, including the author-defined label if one exists.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public void setAriaAtomic(String value) {
		JS.set(peer, "ariaAtomic", JS.param(value));
	}

	/**
	 * <p>The <strong><code>ariaAutoComplete</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-autocomplete</code> attribute, which indicates whether inputting text could trigger display of one or more predictions of the user's intended value for a combobox, searchbox, or textbox and specifies how predictions would be presented if they were made.</p>
	 * <h2>Value</h2>
	 * <p>A string with one of the following values:</p>
	 * <ul>
	 * <li><code>&quot;inline&quot;</code>
	 * <ul>
	 * <li>: When a user is providing input, text suggesting one way to complete the provided input may be dynamically inserted after the caret.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;list&quot;</code>
	 * <ul>
	 * <li>: When a user is providing input, an element containing a collection of values that could complete the provided input may be displayed.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;both&quot;</code>
	 * <ul>
	 * <li>: When a user is providing input, an element containing a collection of values that could complete the provided input may be displayed. If displayed, one value in the collection is automatically selected, and the text needed to complete the automatically selected value appears after the caret in the input.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;none&quot;</code>
	 * <ul>
	 * <li>: When a user is providing input, there is no display of an automatic suggestion that attempts to predict how the user intends to complete the input.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public String getAriaAutoComplete() {
		return Wrap.String(JS.get(peer, "ariaAutoComplete"));
	}

	/**
	 * <p>The <strong><code>ariaAutoComplete</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-autocomplete</code> attribute, which indicates whether inputting text could trigger display of one or more predictions of the user's intended value for a combobox, searchbox, or textbox and specifies how predictions would be presented if they were made.</p>
	 * <h2>Value</h2>
	 * <p>A string with one of the following values:</p>
	 * <ul>
	 * <li><code>&quot;inline&quot;</code>
	 * <ul>
	 * <li>: When a user is providing input, text suggesting one way to complete the provided input may be dynamically inserted after the caret.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;list&quot;</code>
	 * <ul>
	 * <li>: When a user is providing input, an element containing a collection of values that could complete the provided input may be displayed.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;both&quot;</code>
	 * <ul>
	 * <li>: When a user is providing input, an element containing a collection of values that could complete the provided input may be displayed. If displayed, one value in the collection is automatically selected, and the text needed to complete the automatically selected value appears after the caret in the input.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;none&quot;</code>
	 * <ul>
	 * <li>: When a user is providing input, there is no display of an automatic suggestion that attempts to predict how the user intends to complete the input.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public void setAriaAutoComplete(String value) {
		JS.set(peer, "ariaAutoComplete", JS.param(value));
	}

	/**
	 * <p>The <strong><code>ariaBusy</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-busy</code> attribute, which indicates whether an element is being modified, as assistive technologies may want to wait until the modifications are complete before exposing them to the user.</p>
	 * <h2>Value</h2>
	 * <p>A string with one of the following values:</p>
	 * <ul>
	 * <li><code>&quot;true&quot;</code>
	 * <ul>
	 * <li>: The element is being updated.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;false&quot;</code>
	 * <ul>
	 * <li>: There are no expected updates for the element.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public String getAriaBusy() {
		return Wrap.String(JS.get(peer, "ariaBusy"));
	}

	/**
	 * <p>The <strong><code>ariaBusy</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-busy</code> attribute, which indicates whether an element is being modified, as assistive technologies may want to wait until the modifications are complete before exposing them to the user.</p>
	 * <h2>Value</h2>
	 * <p>A string with one of the following values:</p>
	 * <ul>
	 * <li><code>&quot;true&quot;</code>
	 * <ul>
	 * <li>: The element is being updated.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;false&quot;</code>
	 * <ul>
	 * <li>: There are no expected updates for the element.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public void setAriaBusy(String value) {
		JS.set(peer, "ariaBusy", JS.param(value));
	}

	/**
	 * <p>The <strong><code>ariaChecked</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-checked</code> attribute, which indicates the current &quot;checked&quot; state of checkboxes, radio buttons, and other widgets that have a checked state.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> Where possible use an HTML <code>input</code> element with <code>type=&quot;checkbox&quot;</code> as this element has built in semantics and does not require ARIA attributes.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A string with one of the following values:</p>
	 * <ul>
	 * <li><code>&quot;true&quot;</code>
	 * <ul>
	 * <li>: The element is checked.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;mixed&quot;</code>
	 * <ul>
	 * <li>: Indicates a mixed mode value for a tri-state checkbox or menuitemcheckbox.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;false&quot;</code>
	 * <ul>
	 * <li>: The element supports being checked but is not currently checked.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;undefined&quot;</code>
	 * <ul>
	 * <li>: The element does not support being checked.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public String getAriaChecked() {
		return Wrap.String(JS.get(peer, "ariaChecked"));
	}

	/**
	 * <p>The <strong><code>ariaChecked</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-checked</code> attribute, which indicates the current &quot;checked&quot; state of checkboxes, radio buttons, and other widgets that have a checked state.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> Where possible use an HTML <code>input</code> element with <code>type=&quot;checkbox&quot;</code> as this element has built in semantics and does not require ARIA attributes.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A string with one of the following values:</p>
	 * <ul>
	 * <li><code>&quot;true&quot;</code>
	 * <ul>
	 * <li>: The element is checked.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;mixed&quot;</code>
	 * <ul>
	 * <li>: Indicates a mixed mode value for a tri-state checkbox or menuitemcheckbox.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;false&quot;</code>
	 * <ul>
	 * <li>: The element supports being checked but is not currently checked.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;undefined&quot;</code>
	 * <ul>
	 * <li>: The element does not support being checked.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public void setAriaChecked(String value) {
		JS.set(peer, "ariaChecked", JS.param(value));
	}

	/**
	 * <p>The <strong><code>ariaColCount</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-colcount</code> attribute, which defines the number of columns in a table, grid, or treegrid.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public String getAriaColCount() {
		return Wrap.String(JS.get(peer, "ariaColCount"));
	}

	/**
	 * <p>The <strong><code>ariaColCount</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-colcount</code> attribute, which defines the number of columns in a table, grid, or treegrid.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public void setAriaColCount(String value) {
		JS.set(peer, "ariaColCount", JS.param(value));
	}

	/**
	 * <p>The <strong><code>ariaColIndex</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-colindex</code> attribute, which defines an element's column index or position with respect to the total number of columns within a table, grid, or treegrid.</p>
	 * <h2>Value</h2>
	 * <p>A string which contains an integer.</p>
	 */
	public String getAriaColIndex() {
		return Wrap.String(JS.get(peer, "ariaColIndex"));
	}

	/**
	 * <p>The <strong><code>ariaColIndex</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-colindex</code> attribute, which defines an element's column index or position with respect to the total number of columns within a table, grid, or treegrid.</p>
	 * <h2>Value</h2>
	 * <p>A string which contains an integer.</p>
	 */
	public void setAriaColIndex(String value) {
		JS.set(peer, "ariaColIndex", JS.param(value));
	}

	/**
	 * <p>The <strong><code>ariaColIndexText</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-colindextext</code> attribute, which defines a human readable text alternative of aria-colindex.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public String getAriaColIndexText() {
		return Wrap.String(JS.get(peer, "ariaColIndexText"));
	}

	/**
	 * <p>The <strong><code>ariaColIndexText</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-colindextext</code> attribute, which defines a human readable text alternative of aria-colindex.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public void setAriaColIndexText(String value) {
		JS.set(peer, "ariaColIndexText", JS.param(value));
	}

	/**
	 * <p>The <strong><code>ariaColSpan</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-colspan</code> attribute, which defines the number of columns spanned by a cell or gridcell within a table, grid, or treegrid.</p>
	 * <h2>Value</h2>
	 * <p>A string which contains an integer.</p>
	 */
	public String getAriaColSpan() {
		return Wrap.String(JS.get(peer, "ariaColSpan"));
	}

	/**
	 * <p>The <strong><code>ariaColSpan</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-colspan</code> attribute, which defines the number of columns spanned by a cell or gridcell within a table, grid, or treegrid.</p>
	 * <h2>Value</h2>
	 * <p>A string which contains an integer.</p>
	 */
	public void setAriaColSpan(String value) {
		JS.set(peer, "ariaColSpan", JS.param(value));
	}

	/**
	 * <p>The <strong><code>ariaCurrent</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-current</code> attribute, which indicates the element that represents the current item within a container or set of related elements.</p>
	 * <h2>Value</h2>
	 * <p>A string with one of the following values:</p>
	 * <ul>
	 * <li><code>&quot;page&quot;</code>
	 * <ul>
	 * <li>: Represents the current page within a set of pages.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;step&quot;</code>
	 * <ul>
	 * <li>: Represents the current step within a process.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;location&quot;</code>
	 * <ul>
	 * <li>: Represents the current location, for example the current page in a breadcrumbs hierarchy.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;date&quot;</code>
	 * <ul>
	 * <li>: Represents the current date within a collection of dates.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;time&quot;</code>
	 * <ul>
	 * <li>: Represents the current time within a set of times.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;true&quot;</code>
	 * <ul>
	 * <li>: Represents the current item within a set.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;false&quot;</code>
	 * <ul>
	 * <li>: Does not represent the current item within a set.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public String getAriaCurrent() {
		return Wrap.String(JS.get(peer, "ariaCurrent"));
	}

	/**
	 * <p>The <strong><code>ariaCurrent</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-current</code> attribute, which indicates the element that represents the current item within a container or set of related elements.</p>
	 * <h2>Value</h2>
	 * <p>A string with one of the following values:</p>
	 * <ul>
	 * <li><code>&quot;page&quot;</code>
	 * <ul>
	 * <li>: Represents the current page within a set of pages.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;step&quot;</code>
	 * <ul>
	 * <li>: Represents the current step within a process.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;location&quot;</code>
	 * <ul>
	 * <li>: Represents the current location, for example the current page in a breadcrumbs hierarchy.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;date&quot;</code>
	 * <ul>
	 * <li>: Represents the current date within a collection of dates.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;time&quot;</code>
	 * <ul>
	 * <li>: Represents the current time within a set of times.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;true&quot;</code>
	 * <ul>
	 * <li>: Represents the current item within a set.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;false&quot;</code>
	 * <ul>
	 * <li>: Does not represent the current item within a set.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public void setAriaCurrent(String value) {
		JS.set(peer, "ariaCurrent", JS.param(value));
	}

	/**
	 * <p>The <strong><code>ariaDescription</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-description</code> attribute, which defines a string value that describes or annotates the current element.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public String getAriaDescription() {
		return Wrap.String(JS.get(peer, "ariaDescription"));
	}

	/**
	 * <p>The <strong><code>ariaDescription</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-description</code> attribute, which defines a string value that describes or annotates the current element.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public void setAriaDescription(String value) {
		JS.set(peer, "ariaDescription", JS.param(value));
	}

	/**
	 * <p>The <strong><code>ariaDisabled</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-disabled</code> attribute, which indicates that the element is perceivable but disabled, so it is not editable or otherwise operable.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> Where possible, use the <code>input</code> element with <code>type=&quot;button&quot;</code> or the <code>button</code> element — because those elements have built in semantics and do not require ARIA attributes.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A string with one of the following values:</p>
	 * <ul>
	 * <li><code>&quot;true&quot;</code>
	 * <ul>
	 * <li>: The element and all focusable descendants are disabled, but perceivable, and their values cannot be changed by the user.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;false&quot;</code>
	 * <ul>
	 * <li>: The element is enabled.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public String getAriaDisabled() {
		return Wrap.String(JS.get(peer, "ariaDisabled"));
	}

	/**
	 * <p>The <strong><code>ariaDisabled</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-disabled</code> attribute, which indicates that the element is perceivable but disabled, so it is not editable or otherwise operable.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> Where possible, use the <code>input</code> element with <code>type=&quot;button&quot;</code> or the <code>button</code> element — because those elements have built in semantics and do not require ARIA attributes.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A string with one of the following values:</p>
	 * <ul>
	 * <li><code>&quot;true&quot;</code>
	 * <ul>
	 * <li>: The element and all focusable descendants are disabled, but perceivable, and their values cannot be changed by the user.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;false&quot;</code>
	 * <ul>
	 * <li>: The element is enabled.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public void setAriaDisabled(String value) {
		JS.set(peer, "ariaDisabled", JS.param(value));
	}

	/**
	 * <p>The <strong><code>ariaExpanded</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-expanded</code> attribute, which indicates whether a grouping element owned or controlled by this element is expanded or collapsed.</p>
	 * <h2>Value</h2>
	 * <p>A string with one of the following values:</p>
	 * <ul>
	 * <li><code>&quot;true&quot;</code>
	 * <ul>
	 * <li>: The grouping element this element owns or controls is expanded.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;false&quot;</code>
	 * <ul>
	 * <li>: The grouping element this element owns or controls is collapsed.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;undefined&quot;</code>
	 * <ul>
	 * <li>: The element does not own or control a grouping element that is expandable.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public String getAriaExpanded() {
		return Wrap.String(JS.get(peer, "ariaExpanded"));
	}

	/**
	 * <p>The <strong><code>ariaExpanded</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-expanded</code> attribute, which indicates whether a grouping element owned or controlled by this element is expanded or collapsed.</p>
	 * <h2>Value</h2>
	 * <p>A string with one of the following values:</p>
	 * <ul>
	 * <li><code>&quot;true&quot;</code>
	 * <ul>
	 * <li>: The grouping element this element owns or controls is expanded.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;false&quot;</code>
	 * <ul>
	 * <li>: The grouping element this element owns or controls is collapsed.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;undefined&quot;</code>
	 * <ul>
	 * <li>: The element does not own or control a grouping element that is expandable.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public void setAriaExpanded(String value) {
		JS.set(peer, "ariaExpanded", JS.param(value));
	}

	/**
	 * <p>The <strong><code>ariaHasPopup</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-haspopup</code> attribute, which indicates the availability and type of interactive popup element, such as menu or dialog, that can be triggered by an element.</p>
	 * <h2>Value</h2>
	 * <p>A string with one of the following values:</p>
	 * <ul>
	 * <li><code>&quot;false&quot;</code>
	 * <ul>
	 * <li>: The element does not have a popup.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;true&quot;</code>
	 * <ul>
	 * <li>: The element has a popup that is a menu.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;menu&quot;</code>
	 * <ul>
	 * <li>: The element has a popup that is a menu.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;listbox&quot;</code>
	 * <ul>
	 * <li>: The element has a popup that is a listbox.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;tree&quot;</code>
	 * <ul>
	 * <li>: The element has a popup that is a tree.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;grid&quot;</code>
	 * <ul>
	 * <li>: The element has a popup that is a grid.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;dialog&quot;</code>
	 * <ul>
	 * <li>: The element has a popup that is a dialog.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <blockquote>
	 * <p><strong>Warning:</strong> Be aware that support for the different <code>aria-haspopup</code> values can vary depending on the element to which the attribute is specified. Ensure that when using <code>aria-haspopup</code>, it is done in accordance to the ARIA specification, and that it behaves as expected when testing with necessary browsers and assistive technologies.</p>
	 * </blockquote>
	 */
	public String getAriaHasPopup() {
		return Wrap.String(JS.get(peer, "ariaHasPopup"));
	}

	/**
	 * <p>The <strong><code>ariaHasPopup</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-haspopup</code> attribute, which indicates the availability and type of interactive popup element, such as menu or dialog, that can be triggered by an element.</p>
	 * <h2>Value</h2>
	 * <p>A string with one of the following values:</p>
	 * <ul>
	 * <li><code>&quot;false&quot;</code>
	 * <ul>
	 * <li>: The element does not have a popup.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;true&quot;</code>
	 * <ul>
	 * <li>: The element has a popup that is a menu.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;menu&quot;</code>
	 * <ul>
	 * <li>: The element has a popup that is a menu.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;listbox&quot;</code>
	 * <ul>
	 * <li>: The element has a popup that is a listbox.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;tree&quot;</code>
	 * <ul>
	 * <li>: The element has a popup that is a tree.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;grid&quot;</code>
	 * <ul>
	 * <li>: The element has a popup that is a grid.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;dialog&quot;</code>
	 * <ul>
	 * <li>: The element has a popup that is a dialog.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <blockquote>
	 * <p><strong>Warning:</strong> Be aware that support for the different <code>aria-haspopup</code> values can vary depending on the element to which the attribute is specified. Ensure that when using <code>aria-haspopup</code>, it is done in accordance to the ARIA specification, and that it behaves as expected when testing with necessary browsers and assistive technologies.</p>
	 * </blockquote>
	 */
	public void setAriaHasPopup(String value) {
		JS.set(peer, "ariaHasPopup", JS.param(value));
	}

	/**
	 * <p>The <strong><code>ariaHidden</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-hidden</code>) attribute, which indicates whether the element is exposed to an accessibility API.</p>
	 * <h2>Value</h2>
	 * <p>A string with one of the following values:</p>
	 * <ul>
	 * <li><code>&quot;true&quot;</code>
	 * <ul>
	 * <li>: The element is hidden from the accessibility API.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;false&quot;</code>
	 * <ul>
	 * <li>: The element is exposed to the accessibility API as if it were rendered.</li>
	 * </ul>
	 * </li>
	 * <li><code>undefined</code>
	 * <ul>
	 * <li>: The element's hidden state is determined by the user agent based on whether it is rendered.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public String getAriaHidden() {
		return Wrap.String(JS.get(peer, "ariaHidden"));
	}

	/**
	 * <p>The <strong><code>ariaHidden</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-hidden</code>) attribute, which indicates whether the element is exposed to an accessibility API.</p>
	 * <h2>Value</h2>
	 * <p>A string with one of the following values:</p>
	 * <ul>
	 * <li><code>&quot;true&quot;</code>
	 * <ul>
	 * <li>: The element is hidden from the accessibility API.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;false&quot;</code>
	 * <ul>
	 * <li>: The element is exposed to the accessibility API as if it were rendered.</li>
	 * </ul>
	 * </li>
	 * <li><code>undefined</code>
	 * <ul>
	 * <li>: The element's hidden state is determined by the user agent based on whether it is rendered.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public void setAriaHidden(String value) {
		JS.set(peer, "ariaHidden", JS.param(value));
	}

	/**
	 * <p>The <strong><code>ariaKeyShortcuts</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-keyshortcuts</code> attribute, which indicates keyboard shortcuts that an author has implemented to activate or give focus to an element.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public String getAriaKeyShortcuts() {
		return Wrap.String(JS.get(peer, "ariaKeyShortcuts"));
	}

	/**
	 * <p>The <strong><code>ariaKeyShortcuts</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-keyshortcuts</code> attribute, which indicates keyboard shortcuts that an author has implemented to activate or give focus to an element.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public void setAriaKeyShortcuts(String value) {
		JS.set(peer, "ariaKeyShortcuts", JS.param(value));
	}

	/**
	 * <p>The <strong><code>ariaLabel</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-label</code> attribute, which defines a string value that labels the current element.</p>
	 * <h2>Value</h2>
	 * <p>A string or <code>null</code>.</p>
	 */
	public String getAriaLabel() {
		return Wrap.String(JS.get(peer, "ariaLabel"));
	}

	/**
	 * <p>The <strong><code>ariaLabel</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-label</code> attribute, which defines a string value that labels the current element.</p>
	 * <h2>Value</h2>
	 * <p>A string or <code>null</code>.</p>
	 */
	public void setAriaLabel(String value) {
		JS.set(peer, "ariaLabel", JS.param(value));
	}

	/**
	 * <p>The <strong><code>ariaLevel</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-level</code> attribute, which defines the hierarchical level of an element within a structure.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> Where possible use an HTML <code>h1</code> or other correct heading level as these have built in semantics and do not require ARIA attributes.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A string containing an integer.</p>
	 */
	public String getAriaLevel() {
		return Wrap.String(JS.get(peer, "ariaLevel"));
	}

	/**
	 * <p>The <strong><code>ariaLevel</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-level</code> attribute, which defines the hierarchical level of an element within a structure.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> Where possible use an HTML <code>h1</code> or other correct heading level as these have built in semantics and do not require ARIA attributes.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A string containing an integer.</p>
	 */
	public void setAriaLevel(String value) {
		JS.set(peer, "ariaLevel", JS.param(value));
	}

	/**
	 * <p>The <strong><code>ariaLive</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-live</code> attribute, which indicates that an element will be updated, and describes the types of updates the user agents, assistive technologies, and user can expect from the live region.</p>
	 * <h2>Value</h2>
	 * <p>A string with one of the following values:</p>
	 * <ul>
	 * <li><code>&quot;assertive&quot;</code>
	 * <ul>
	 * <li>: Indicates that updates to the region have the highest priority and should be presented the user immediately.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;off&quot;</code>
	 * <ul>
	 * <li>: Indicates that updates to the region should not be presented to the user unless the user is currently focused on that region.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;polite&quot;</code>
	 * <ul>
	 * <li>: Indicates that updates to the region should be presented at the next graceful opportunity, such as at the end of speaking the current sentence or when the user pauses typing.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public String getAriaLive() {
		return Wrap.String(JS.get(peer, "ariaLive"));
	}

	/**
	 * <p>The <strong><code>ariaLive</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-live</code> attribute, which indicates that an element will be updated, and describes the types of updates the user agents, assistive technologies, and user can expect from the live region.</p>
	 * <h2>Value</h2>
	 * <p>A string with one of the following values:</p>
	 * <ul>
	 * <li><code>&quot;assertive&quot;</code>
	 * <ul>
	 * <li>: Indicates that updates to the region have the highest priority and should be presented the user immediately.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;off&quot;</code>
	 * <ul>
	 * <li>: Indicates that updates to the region should not be presented to the user unless the user is currently focused on that region.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;polite&quot;</code>
	 * <ul>
	 * <li>: Indicates that updates to the region should be presented at the next graceful opportunity, such as at the end of speaking the current sentence or when the user pauses typing.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public void setAriaLive(String value) {
		JS.set(peer, "ariaLive", JS.param(value));
	}

	/**
	 * <p>The <strong><code>ariaModal</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-modal</code> attribute, which indicates whether an element is modal when displayed. Applying the <code>aria-modal</code> property to an element with <code>role=&quot;dialog&quot;</code> replaces the technique of using aria-hidden on the background for informing assistive technologies that content outside a dialog is inert.</p>
	 * <h2>Value</h2>
	 * <p>A string with one of the following values:</p>
	 * <ul>
	 * <li><code>&quot;true&quot;</code>
	 * <ul>
	 * <li>: The element is modal.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;false&quot;</code>
	 * <ul>
	 * <li>: The element is not modal.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public String getAriaModal() {
		return Wrap.String(JS.get(peer, "ariaModal"));
	}

	/**
	 * <p>The <strong><code>ariaModal</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-modal</code> attribute, which indicates whether an element is modal when displayed. Applying the <code>aria-modal</code> property to an element with <code>role=&quot;dialog&quot;</code> replaces the technique of using aria-hidden on the background for informing assistive technologies that content outside a dialog is inert.</p>
	 * <h2>Value</h2>
	 * <p>A string with one of the following values:</p>
	 * <ul>
	 * <li><code>&quot;true&quot;</code>
	 * <ul>
	 * <li>: The element is modal.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;false&quot;</code>
	 * <ul>
	 * <li>: The element is not modal.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public void setAriaModal(String value) {
		JS.set(peer, "ariaModal", JS.param(value));
	}

	/**
	 * <p>The <strong><code>ariaMultiLine</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-multiline</code> attribute, which indicates whether a text box accepts multiple lines of input or only a single line.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> Where possible use an HTML <code>input</code> element with <code>type=&quot;text&quot;</code> or a <code>textarea</code> as these have built in semantics and do not require ARIA attributes.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A string with one of the following values:</p>
	 * <ul>
	 * <li><code>&quot;true&quot;</code>
	 * <ul>
	 * <li>: This is a multi-line text box.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;false&quot;</code>
	 * <ul>
	 * <li>: This is a single-line text box.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public String getAriaMultiLine() {
		return Wrap.String(JS.get(peer, "ariaMultiLine"));
	}

	/**
	 * <p>The <strong><code>ariaMultiLine</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-multiline</code> attribute, which indicates whether a text box accepts multiple lines of input or only a single line.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> Where possible use an HTML <code>input</code> element with <code>type=&quot;text&quot;</code> or a <code>textarea</code> as these have built in semantics and do not require ARIA attributes.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A string with one of the following values:</p>
	 * <ul>
	 * <li><code>&quot;true&quot;</code>
	 * <ul>
	 * <li>: This is a multi-line text box.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;false&quot;</code>
	 * <ul>
	 * <li>: This is a single-line text box.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public void setAriaMultiLine(String value) {
		JS.set(peer, "ariaMultiLine", JS.param(value));
	}

	/**
	 * <p>The <strong><code>ariaMultiSelectable</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-multiselectable</code> attribute, which indicates that the user may select more than one item from the current selectable descendants.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> Where possible use an HTML <code>select</code> element as this has built in semantics and does not require ARIA attributes.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A string with one of the following values:</p>
	 * <ul>
	 * <li><code>&quot;true&quot;</code>
	 * <ul>
	 * <li>: More than one item may be selected at a time.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;false&quot;</code>
	 * <ul>
	 * <li>: Only one item may be selected.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public String getAriaMultiSelectable() {
		return Wrap.String(JS.get(peer, "ariaMultiSelectable"));
	}

	/**
	 * <p>The <strong><code>ariaMultiSelectable</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-multiselectable</code> attribute, which indicates that the user may select more than one item from the current selectable descendants.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> Where possible use an HTML <code>select</code> element as this has built in semantics and does not require ARIA attributes.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A string with one of the following values:</p>
	 * <ul>
	 * <li><code>&quot;true&quot;</code>
	 * <ul>
	 * <li>: More than one item may be selected at a time.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;false&quot;</code>
	 * <ul>
	 * <li>: Only one item may be selected.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public void setAriaMultiSelectable(String value) {
		JS.set(peer, "ariaMultiSelectable", JS.param(value));
	}

	/**
	 * <p>The <strong><code>ariaOrientation</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-orientation</code> attribute, which indicates whether the element's orientation is horizontal, vertical, or unknown/ambiguous.</p>
	 * <h2>Value</h2>
	 * <p>A string with one of the following values:</p>
	 * <ul>
	 * <li><code>&quot;horizontal&quot;</code>
	 * <ul>
	 * <li>: The element is horizontal.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;vertical&quot;</code>
	 * <ul>
	 * <li>: The element is vertical.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;undefined&quot;</code>
	 * <ul>
	 * <li>: The element's orientation is unknown.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public String getAriaOrientation() {
		return Wrap.String(JS.get(peer, "ariaOrientation"));
	}

	/**
	 * <p>The <strong><code>ariaOrientation</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-orientation</code> attribute, which indicates whether the element's orientation is horizontal, vertical, or unknown/ambiguous.</p>
	 * <h2>Value</h2>
	 * <p>A string with one of the following values:</p>
	 * <ul>
	 * <li><code>&quot;horizontal&quot;</code>
	 * <ul>
	 * <li>: The element is horizontal.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;vertical&quot;</code>
	 * <ul>
	 * <li>: The element is vertical.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;undefined&quot;</code>
	 * <ul>
	 * <li>: The element's orientation is unknown.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public void setAriaOrientation(String value) {
		JS.set(peer, "ariaOrientation", JS.param(value));
	}

	/**
	 * <p>The <strong><code>ariaPlaceholder</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-placeholder</code> attribute, which defines a short hint intended to aid the user with data entry when the control has no value.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> Where possible use an HTML <code>input</code> element with <code>type=&quot;text&quot;</code> or a <code>textarea</code> as these have built in semantics and do not require ARIA attributes.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public String getAriaPlaceholder() {
		return Wrap.String(JS.get(peer, "ariaPlaceholder"));
	}

	/**
	 * <p>The <strong><code>ariaPlaceholder</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-placeholder</code> attribute, which defines a short hint intended to aid the user with data entry when the control has no value.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> Where possible use an HTML <code>input</code> element with <code>type=&quot;text&quot;</code> or a <code>textarea</code> as these have built in semantics and do not require ARIA attributes.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public void setAriaPlaceholder(String value) {
		JS.set(peer, "ariaPlaceholder", JS.param(value));
	}

	/**
	 * <p>The <strong><code>ariaPosInSet</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-posinset</code> attribute, which defines an element's number or position in the current set of listitems or treeitems.</p>
	 * <h2>Value</h2>
	 * <p>A string containing an integer.</p>
	 */
	public String getAriaPosInSet() {
		return Wrap.String(JS.get(peer, "ariaPosInSet"));
	}

	/**
	 * <p>The <strong><code>ariaPosInSet</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-posinset</code> attribute, which defines an element's number or position in the current set of listitems or treeitems.</p>
	 * <h2>Value</h2>
	 * <p>A string containing an integer.</p>
	 */
	public void setAriaPosInSet(String value) {
		JS.set(peer, "ariaPosInSet", JS.param(value));
	}

	/**
	 * <p>The <strong><code>ariaPressed</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-pressed</code> attribute, which indicates the current &quot;pressed&quot; state of toggle buttons.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> Where possible use an HTML <code>input</code> element with <code>type=&quot;button&quot;</code> or the <code>button</code> element as these have built in semantics and do not require ARIA attributes.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A string with one of the following values:</p>
	 * <ul>
	 * <li><code>&quot;true&quot;</code>
	 * <ul>
	 * <li>: The element is pressed.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;false&quot;</code>
	 * <ul>
	 * <li>: The element supports being pressed but is not currently pressed.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;mixed&quot;</code>
	 * <ul>
	 * <li>: Indicates a mixed mode value for a tri-state toggle button.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;undefined&quot;</code>
	 * <ul>
	 * <li>: The element does not support being pressed.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public String getAriaPressed() {
		return Wrap.String(JS.get(peer, "ariaPressed"));
	}

	/**
	 * <p>The <strong><code>ariaPressed</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-pressed</code> attribute, which indicates the current &quot;pressed&quot; state of toggle buttons.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> Where possible use an HTML <code>input</code> element with <code>type=&quot;button&quot;</code> or the <code>button</code> element as these have built in semantics and do not require ARIA attributes.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A string with one of the following values:</p>
	 * <ul>
	 * <li><code>&quot;true&quot;</code>
	 * <ul>
	 * <li>: The element is pressed.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;false&quot;</code>
	 * <ul>
	 * <li>: The element supports being pressed but is not currently pressed.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;mixed&quot;</code>
	 * <ul>
	 * <li>: Indicates a mixed mode value for a tri-state toggle button.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;undefined&quot;</code>
	 * <ul>
	 * <li>: The element does not support being pressed.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public void setAriaPressed(String value) {
		JS.set(peer, "ariaPressed", JS.param(value));
	}

	/**
	 * <p>The <strong><code>ariaReadOnly</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-readonly</code> attribute, which indicates that the element is not editable, but is otherwise operable.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> Where possible use an HTML <code>input</code> element with <code>type=&quot;text&quot;</code> or a <code>textarea</code> as these have built in semantics and do not require ARIA attributes.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A string with one of the following values:</p>
	 * <ul>
	 * <li><code>&quot;true&quot;</code>
	 * <ul>
	 * <li>: The user cannot change the value of the element.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;false&quot;</code>
	 * <ul>
	 * <li>: The user can set the value of the element.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public String getAriaReadOnly() {
		return Wrap.String(JS.get(peer, "ariaReadOnly"));
	}

	/**
	 * <p>The <strong><code>ariaReadOnly</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-readonly</code> attribute, which indicates that the element is not editable, but is otherwise operable.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> Where possible use an HTML <code>input</code> element with <code>type=&quot;text&quot;</code> or a <code>textarea</code> as these have built in semantics and do not require ARIA attributes.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A string with one of the following values:</p>
	 * <ul>
	 * <li><code>&quot;true&quot;</code>
	 * <ul>
	 * <li>: The user cannot change the value of the element.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;false&quot;</code>
	 * <ul>
	 * <li>: The user can set the value of the element.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public void setAriaReadOnly(String value) {
		JS.set(peer, "ariaReadOnly", JS.param(value));
	}

	// ariaRelevant NON-STANDARD

	/**
	 * <p>The <strong><code>ariaRequired</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-required</code> attribute, which indicates that user input is required on the element before a form may be submitted.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> Where possible use an HTML <code>input</code> element with <code>type=&quot;text&quot;</code> or a <code>textarea</code> as these have built in semantics and do not require ARIA attributes.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A string with one of the following values:</p>
	 * <ul>
	 * <li><code>&quot;true&quot;</code>
	 * <ul>
	 * <li>: Users need to provide input on an element before a form is submitted.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;false&quot;</code>
	 * <ul>
	 * <li>: User input is not necessary to submit the form.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public String getAriaRequired() {
		return Wrap.String(JS.get(peer, "ariaRequired"));
	}

	/**
	 * <p>The <strong><code>ariaRequired</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-required</code> attribute, which indicates that user input is required on the element before a form may be submitted.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> Where possible use an HTML <code>input</code> element with <code>type=&quot;text&quot;</code> or a <code>textarea</code> as these have built in semantics and do not require ARIA attributes.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A string with one of the following values:</p>
	 * <ul>
	 * <li><code>&quot;true&quot;</code>
	 * <ul>
	 * <li>: Users need to provide input on an element before a form is submitted.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;false&quot;</code>
	 * <ul>
	 * <li>: User input is not necessary to submit the form.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public void setAriaRequired(String value) {
		JS.set(peer, "ariaRequired", JS.param(value));
	}

	/**
	 * <p>The <strong><code>ariaRoleDescription</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-roledescription</code> attribute, which defines a human-readable, author-localized description for the role of an element.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public String getAriaRoleDescription() {
		return Wrap.String(JS.get(peer, "ariaRoleDescription"));
	}

	/**
	 * <p>The <strong><code>ariaRoleDescription</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-roledescription</code> attribute, which defines a human-readable, author-localized description for the role of an element.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public void setAriaRoleDescription(String value) {
		JS.set(peer, "ariaRoleDescription", JS.param(value));
	}

	/**
	 * <p>The <strong><code>ariaRowCount</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-rowcount</code> attribute, which defines the total number of rows in a table, grid, or treegrid.</p>
	 * <h2>Value</h2>
	 * <p>A string which contains an integer.</p>
	 */
	public String getAriaRowCount() {
		return Wrap.String(JS.get(peer, "ariaRowCount"));
	}

	/**
	 * <p>The <strong><code>ariaRowCount</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-rowcount</code> attribute, which defines the total number of rows in a table, grid, or treegrid.</p>
	 * <h2>Value</h2>
	 * <p>A string which contains an integer.</p>
	 */
	public void setAriaRowCount(String value) {
		JS.set(peer, "ariaRowCount", JS.param(value));
	}

	/**
	 * <p>The <strong><code>ariaRowIndex</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-rowindex</code> attribute, which defines an element's row index or position with respect to the total number of rows within a table, grid, or treegrid.</p>
	 * <h2>Value</h2>
	 * <p>A string which contains an integer.</p>
	 */
	public String getAriaRowIndex() {
		return Wrap.String(JS.get(peer, "ariaRowIndex"));
	}

	/**
	 * <p>The <strong><code>ariaRowIndex</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-rowindex</code> attribute, which defines an element's row index or position with respect to the total number of rows within a table, grid, or treegrid.</p>
	 * <h2>Value</h2>
	 * <p>A string which contains an integer.</p>
	 */
	public void setAriaRowIndex(String value) {
		JS.set(peer, "ariaRowIndex", JS.param(value));
	}

	/**
	 * <p>The <strong><code>ariaRowIndexText</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-rowindextext</code> attribute, which defines a human readable text alternative of aria-rowindex.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public String getAriaRowIndexText() {
		return Wrap.String(JS.get(peer, "ariaRowIndexText"));
	}

	/**
	 * <p>The <strong><code>ariaRowIndexText</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-rowindextext</code> attribute, which defines a human readable text alternative of aria-rowindex.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public void setAriaRowIndexText(String value) {
		JS.set(peer, "ariaRowIndexText", JS.param(value));
	}

	/**
	 * <p>The <strong><code>ariaRowSpan</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-rowspan</code> attribute, which defines the number of rows spanned by a cell or gridcell within a table, grid, or treegrid.</p>
	 * <h2>Value</h2>
	 * <p>A string which contains an integer.</p>
	 */
	public String getAriaRowSpan() {
		return Wrap.String(JS.get(peer, "ariaRowSpan"));
	}

	/**
	 * <p>The <strong><code>ariaRowSpan</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-rowspan</code> attribute, which defines the number of rows spanned by a cell or gridcell within a table, grid, or treegrid.</p>
	 * <h2>Value</h2>
	 * <p>A string which contains an integer.</p>
	 */
	public void setAriaRowSpan(String value) {
		JS.set(peer, "ariaRowSpan", JS.param(value));
	}

	/**
	 * <p>The <strong><code>ariaSelected</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-selected</code> attribute, which indicates the current &quot;selected&quot; state of elements that have a selected state.</p>
	 * <h2>Value</h2>
	 * <p>A string with one of the following values:</p>
	 * <ul>
	 * <li><code>&quot;true&quot;</code>
	 * <ul>
	 * <li>: The item is selected.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;false&quot;</code>
	 * <ul>
	 * <li>: The item is not selected.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;undefined&quot;</code>
	 * <ul>
	 * <li>: The item is not</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public String getAriaSelected() {
		return Wrap.String(JS.get(peer, "ariaSelected"));
	}

	/**
	 * <p>The <strong><code>ariaSelected</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-selected</code> attribute, which indicates the current &quot;selected&quot; state of elements that have a selected state.</p>
	 * <h2>Value</h2>
	 * <p>A string with one of the following values:</p>
	 * <ul>
	 * <li><code>&quot;true&quot;</code>
	 * <ul>
	 * <li>: The item is selected.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;false&quot;</code>
	 * <ul>
	 * <li>: The item is not selected.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;undefined&quot;</code>
	 * <ul>
	 * <li>: The item is not</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public void setAriaSelected(String value) {
		JS.set(peer, "ariaSelected", JS.param(value));
	}

	/**
	 * <p>The <strong><code>ariaSetSize</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-setsize</code> attribute, which defines the number of items in the current set of listitems or treeitems.</p>
	 * <h2>Value</h2>
	 * <p>A string containing an integer.</p>
	 */
	public String getAriaSetSize() {
		return Wrap.String(JS.get(peer, "ariaSetSize"));
	}

	/**
	 * <p>The <strong><code>ariaSetSize</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-setsize</code> attribute, which defines the number of items in the current set of listitems or treeitems.</p>
	 * <h2>Value</h2>
	 * <p>A string containing an integer.</p>
	 */
	public void setAriaSetSize(String value) {
		JS.set(peer, "ariaSetSize", JS.param(value));
	}

	/**
	 * <p>The <strong><code>ariaSort</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-sort</code> attribute, which indicates if items in a table or grid are sorted in ascending or descending order.</p>
	 * <h2>Value</h2>
	 * <p>A string with one of the following values:</p>
	 * <ul>
	 * <li><code>&quot;ascending&quot;</code>
	 * <ul>
	 * <li>: Items are sorted in ascending order by this column.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;descending&quot;</code>
	 * <ul>
	 * <li>: Items are sorted in descending order by this column.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;none&quot;</code>
	 * <ul>
	 * <li>: There is no defined sort applied to the column.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;other&quot;</code>
	 * <ul>
	 * <li>: A sort algorithm other than ascending or descending has been applied.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public String getAriaSort() {
		return Wrap.String(JS.get(peer, "ariaSort"));
	}

	/**
	 * <p>The <strong><code>ariaSort</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-sort</code> attribute, which indicates if items in a table or grid are sorted in ascending or descending order.</p>
	 * <h2>Value</h2>
	 * <p>A string with one of the following values:</p>
	 * <ul>
	 * <li><code>&quot;ascending&quot;</code>
	 * <ul>
	 * <li>: Items are sorted in ascending order by this column.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;descending&quot;</code>
	 * <ul>
	 * <li>: Items are sorted in descending order by this column.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;none&quot;</code>
	 * <ul>
	 * <li>: There is no defined sort applied to the column.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;other&quot;</code>
	 * <ul>
	 * <li>: A sort algorithm other than ascending or descending has been applied.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public void setAriaSort(String value) {
		JS.set(peer, "ariaSort", JS.param(value));
	}

	/**
	 * <p>The <strong><code>ariaValueMax</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-valuemax</code> attribute, which defines the maximum allowed value for a range widget.</p>
	 * <h2>Value</h2>
	 * <p>A string which contains a number.</p>
	 */
	public String getAriaValueMax() {
		return Wrap.String(JS.get(peer, "ariaValueMax"));
	}

	/**
	 * <p>The <strong><code>ariaValueMax</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-valuemax</code> attribute, which defines the maximum allowed value for a range widget.</p>
	 * <h2>Value</h2>
	 * <p>A string which contains a number.</p>
	 */
	public void setAriaValueMax(String value) {
		JS.set(peer, "ariaValueMax", JS.param(value));
	}

	/**
	 * <p>The <strong><code>ariaValueMin</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-valuemin</code> attribute, which defines the minimum allowed value for a range widget.</p>
	 * <h2>Value</h2>
	 * <p>A string which contains a number.</p>
	 */
	public String getAriaValueMin() {
		return Wrap.String(JS.get(peer, "ariaValueMin"));
	}

	/**
	 * <p>The <strong><code>ariaValueMin</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-valuemin</code> attribute, which defines the minimum allowed value for a range widget.</p>
	 * <h2>Value</h2>
	 * <p>A string which contains a number.</p>
	 */
	public void setAriaValueMin(String value) {
		JS.set(peer, "ariaValueMin", JS.param(value));
	}

	/**
	 * <p>The <strong><code>ariaValueNow</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-valuenow</code> attribute, which defines the current value for a range widget.</p>
	 * <h2>Value</h2>
	 * <p>A string which contains a number.</p>
	 */
	public String getAriaValueNow() {
		return Wrap.String(JS.get(peer, "ariaValueNow"));
	}

	/**
	 * <p>The <strong><code>ariaValueNow</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-valuenow</code> attribute, which defines the current value for a range widget.</p>
	 * <h2>Value</h2>
	 * <p>A string which contains a number.</p>
	 */
	public void setAriaValueNow(String value) {
		JS.set(peer, "ariaValueNow", JS.param(value));
	}

	/**
	 * <p>The <strong><code>ariaValueText</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-valuetext</code> attribute, which defines the human readable text alternative of aria-valuenow for a range widget.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public String getAriaValueText() {
		return Wrap.String(JS.get(peer, "ariaValueText"));
	}

	/**
	 * <p>The <strong><code>ariaValueText</code></strong> property of the {@link Element} interface reflects the value of the <code>aria-valuetext</code> attribute, which defines the human readable text alternative of aria-valuenow for a range widget.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public void setAriaValueText(String value) {
		JS.set(peer, "ariaValueText", JS.param(value));
	}

	/**
	 * <p>The <strong><code>assignedSlot</code></strong> read-only
	 * property of the {@link Element} interface returns an
	 * {@link HTMLSlotElement} representing the <code>slot</code> element the
	 * node is inserted in.</p>
	 * <h2>Value</h2>
	 * <p>An {@link HTMLSlotElement} instance, or <code>null</code> if the element is not
	 * assigned to a slot, or if the associated shadow root was attached with its
	 * {@link ShadowRoot.mode} set to <code>closed</code> (see
	 * {@link Element.attachShadow} for further details).</p>
	 */
	public String getAssignedSlot() {
		return Wrap.String(JS.get(peer, "assignedSlot"));
	}

	/**
	 * <p>The <strong><code>Element.attributes</code></strong> property returns a live collection
	 * of all attribute nodes registered to the specified node. It is a
	 * {@link NamedNodeMap}, not an <code>Array</code>, so it has no <code>Array</code>
	 * methods and the {@link Attr} nodes' indexes may differ among browsers. To be more
	 * specific, <code>attributes</code> is a key/value pair of strings that represents any
	 * information regarding that attribute.</p>
	 * <h2>Value</h2>
	 * <p>A {@link NamedNodeMap} object.</p>
	 */
	public String getAttributes() {
		return Wrap.String(JS.get(peer, "attributes"));
	}

	/**
	 * <p>The <strong><code>Element.attributes</code></strong> property returns a live collection
	 * of all attribute nodes registered to the specified node. It is a
	 * {@link NamedNodeMap}, not an <code>Array</code>, so it has no <code>Array</code>
	 * methods and the {@link Attr} nodes' indexes may differ among browsers. To be more
	 * specific, <code>attributes</code> is a key/value pair of strings that represents any
	 * information regarding that attribute.</p>
	 * <h2>Value</h2>
	 * <p>A {@link NamedNodeMap} object.</p>
	 */
	public void setAttributes(String value) {
		JS.set(peer, "attributes", JS.param(value));
	}

	/**
	 * <p>The <strong><code>Element.childElementCount</code></strong> read-only property
	 * returns the number of child elements of this element.</p>
	 */
	public String getChildElementCount() {
		return Wrap.String(JS.get(peer, "childElementCount"));
	}

	/**
	 * <p>The read-only <strong><code>children</code></strong> property returns a live {@link HTMLCollection}
	 * which contains all of the child {@link Element} of the element upon which it was called.
	 * <code>Element.children</code> includes only element nodes. To get all child nodes, including non-element nodes like text and comment nodes, use {@link Node.childNodes}.</p>
	 * <h2>Value</h2>
	 * <p>An  domxref(&quot;HTMLCollection&quot;)  which is a live, ordered collection of the DOM
	 * elements which are children of <code>node</code>. You can access the
	 * individual child nodes in the collection by using either the
	 * {@link HTMLCollection.item()} method on the collection, or by using
	 * JavaScript array-style notation.
	 * If the element has no element children, then <code>children</code> is an empty list with a
	 * <code>length</code> of <code>0</code>.</p>
	 */
	public HTMLCollection getChildren() {
		return Wrap.HTMLCollection(JS.get(peer, "children"));
	}

	/**
	 * <p>The <strong><code>Element.classList</code></strong> is a read-only property that
	 * returns a live {@link DOMTokenList} collection of the <code>class</code>
	 * attributes of the element. This can then be used to manipulate the class list.
	 * Using <code>classList</code> is a convenient alternative to accessing an element's list
	 * of classes as a space-delimited string via {@link element.className}.</p>
	 * <h2>Value</h2>
	 * <p>A {@link DOMTokenList} representing the contents of the element's
	 * <code>class</code> attribute. If the <code>class</code> attribute is not set or empty,
	 * it returns an empty <code>DOMTokenList</code>, i.e. a <code>DOMTokenList</code> with
	 * the <code>length</code> property equal to <code>0</code>.
	 * Although the <code>classList</code> property itself is read-only, you can modify its associated <code>DOMTokenList</code> using the {@link DOMTokenList/add}, {@link DOMTokenList/remove}, {@link DOMTokenList/replace}, and {@link DOMTokenList/toggle} methods.</p>
	 */
	public DOMTokenList getClassList() {
		return Wrap.DOMTokenList(JS.get(peer, "classList"));
	}

	/**
	 * <p>The <strong><code>className</code></strong> property of the
	 * {@link Element} interface gets and sets the value of the <code>class</code> attribute
	 * of the specified element.</p>
	 * <h2>Value</h2>
	 * <p>A string variable representing the class or space-separated classes of the current element.</p>
	 */
	public String getClassName() {
		return Wrap.String(JS.get(peer, "className"));
	}

	/**
	 * <p>The <strong><code>className</code></strong> property of the
	 * {@link Element} interface gets and sets the value of the <code>class</code> attribute
	 * of the specified element.</p>
	 * <h2>Value</h2>
	 * <p>A string variable representing the class or space-separated classes of the current element.</p>
	 */
	public void setClassName(String value) {
		JS.set(peer, "className", JS.param(value));
	}

	/**
	 * <p>The <strong><code>Element.clientHeight</code></strong> read-only property is zero for
	 * elements with no CSS or inline layout boxes; otherwise, it's the inner height of an
	 * element in pixels. It includes padding but excludes borders, margins, and horizontal
	 * scrollbars (if present).
	 * <code>clientHeight</code> can be calculated as: CSS <code>height</code> + CSS
	 * <code>padding</code> - height of horizontal scrollbar (if present).
	 * When <code>clientHeight</code> is used on the root element (the
	 * <code>&lt;html&gt;</code> element), (or on <code>&lt;body&gt;</code> if the document is
	 * in quirks mode), the viewport's height (excluding any scrollbar) is returned. <a href="https://www.w3.org/TR/2016/WD-cssom-view-1-20160317/#dom-element-clientheight">This is a special case of <code>clientHeight</code></a>.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> This property will round the value to an integer. If you need
	 * a fractional value, use  domxref(&quot;element.getBoundingClientRect()&quot;) .</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A number.</p>
	 */
	public int getClientHeight() {
		return JS.getInt(peer, "clientHeight");
	}

	/**
	 * <p>The width of the left border of an element in pixels. It includes the width of the
	 * vertical scrollbar if the text direction of the element is right-to-left and if there
	 * is an overflow causing a left vertical scrollbar to be rendered.
	 * <code>clientLeft</code> does not include the left margin or the left padding.
	 * <code>clientLeft</code> is read-only.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> This property will round the value to an integer. If you
	 * need a fractional value, use  domxref(&quot;element.getBoundingClientRect()&quot;) .
	 * <strong>Note:</strong> When an element has
	 * <code>display: inline</code>, <code>clientLeft</code> returns <code>0</code>
	 * regardless of the element's border.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A number.</p>
	 */
	public int getClientLeft() {
		return JS.getInt(peer, "clientLeft");
	}

	/**
	 * <p>The width of the top border of an element in pixels. It is a read-only, integer
	 * property of element.
	 * As it happens, all that lies between the two locations (<code>offsetTop</code> and
	 * client area top) is the element's border. This is because the <code>offsetTop</code>
	 * indicates the location of the top of the border (not the margin) while the client area
	 * starts immediately below the border, (client area includes padding.) Therefore, the
	 * <strong>clientTop</strong> value will always equal the integer portion of the
	 * <code>.getComputedStyle()</code> value for &quot;border-top-width&quot;. (Actually might be
	 * Math.round(parseFloat()).) For example, if the computed &quot;border-top-width&quot; is zero,
	 * then <strong><code>clientTop</code></strong> is also zero.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> This property will round the value to an integer. If you
	 * need a fractional value, use  domxref(&quot;element.getBoundingClientRect()&quot;) .</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A number.</p>
	 */
	public int getClientTop() {
		return JS.getInt(peer, "clientTop");
	}

	/**
	 * <p>The <strong><code>Element.clientWidth</code></strong> property is zero for inline
	 * elements and elements with no CSS; otherwise, it's the inner width of an element in
	 * pixels. It includes padding but excludes borders, margins, and vertical scrollbars (if
	 * present).
	 * When <code>clientWidth</code> is used on the root element (the
	 * <code>&lt;html&gt;</code> element), (or on <code>&lt;body&gt;</code> if the document is
	 * in quirks mode), the viewport's width (excluding any scrollbar) is returned. <a href="https://www.w3.org/TR/2016/WD-cssom-view-1-20160317/#dom-element-clientwidth">This is a special case of <code>clientWidth</code></a>.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> This property will round the value to an integer. If you need
	 * a fractional value, use  domxref(&quot;element.getBoundingClientRect()&quot;) .</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A number.</p>
	 */
	public int getClientWidth() {
		return JS.getInt(peer, "clientWidth");
	}

	/**
	 * <p>The <strong><code>Element.firstElementChild</code></strong> read-only property
	 * returns an element's first child {@link Element}, or <code>null</code> if there
	 * are no child elements.
	 * <code>Element.firstElementChild</code> includes only element nodes.
	 * To get all child nodes, including non-element nodes like text and comment nodes, use {@link Node.firstChild}.</p>
	 * <h2>Value</h2>
	 * <p>An {@link Element} object, or <code>null</code>.</p>
	 */
	public Element getFirstElementChild() {
		return Wrap.Element(JS.get(peer, "firstElementChild"));
	}

	/**
	 * <p>ApiRef(&quot;DOM&quot;)
	 * The <strong><code>id</code></strong> property of the {@link Element} interface
	 * represents the element's identifier, reflecting the
	 * <strong><code>id</code></strong>
	 * global attribute.
	 * If the <code>id</code> value is not the empty string, it must be unique in a document.
	 * The <code>id</code> is often used with {{domxref(&quot;Document.getElementById()&quot;,
	 * &quot;getElementById()&quot;)}} to retrieve a particular element. Another common case is to use an
	 * element's ID as a selector when styling
	 * the document with CSS.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> Identifiers are case-sensitive, but you should avoid creating
	 * IDs that differ only in the capitalization.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public String getId() {
		return Wrap.String(JS.get(peer, "id"));
	}

	/**
	 * The <strong><code>id</code></strong> property of the {@link Element} interface
	 * represents the element's identifier, reflecting the
	 * <strong><code>id</code></strong>
	 * global attribute.
	 * If the <code>id</code> value is not the empty string, it must be unique in a document.
	 * The <code>id</code> is often used with {{domxref(&quot;Document.getElementById()&quot;,
	 * &quot;getElementById()&quot;)}} to retrieve a particular element. Another common case is to use an
	 * element's ID as a selector when styling
	 * the document with CSS.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> Identifiers are case-sensitive, but you should avoid creating
	 * IDs that differ only in the capitalization.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public void setId(String value) {
		JS.set(peer, "id", JS.param(value));
	}

	/**
	 * <p>The {@link Element} property
	 * <strong><code>innerHTML</code></strong> gets or sets the HTML or XML markup contained
	 * within the element.
	 * To insert the HTML into the document rather than replace the contents of an element,
	 * use the method {@link Element.insertAdjacentHTML}.</p>
	 * <h2>Value</h2>
	 * <p>A string containing the HTML serialization of the element's
	 * descendants. Setting the value of <code>innerHTML</code> removes all of the element's
	 * descendants and replaces them with nodes constructed by parsing the HTML given in the
	 * string <em>htmlString</em>.</p>
	 */
	public String getInnerHTML() {
		return Wrap.String(JS.get(peer, "innerHTML"));
	}

	/**
	 * <p>The {@link Element} property
	 * <strong><code>innerHTML</code></strong> gets or sets the HTML or XML markup contained
	 * within the element.
	 * To insert the HTML into the document rather than replace the contents of an element,
	 * use the method {@link Element.insertAdjacentHTML}.</p>
	 * <h2>Value</h2>
	 * <p>A string containing the HTML serialization of the element's
	 * descendants. Setting the value of <code>innerHTML</code> removes all of the element's
	 * descendants and replaces them with nodes constructed by parsing the HTML given in the
	 * string <em>htmlString</em>.</p>
	 */
	public void setInnerHTML(String value) {
		JS.set(peer, "innerHTML", JS.param(value));
	}

	/**
	 * <p>The <strong><code>Element.lastElementChild</code></strong> read-only property
	 * returns an element's last child {@link Element}, or <code>null</code> if there
	 * are no child elements.
	 * <code>Element.lastElementChild</code> includes only element nodes.
	 * To get all child nodes, including non-element nodes like text and comment nodes, use {@link Node.lastChild}.</p>
	 * <h2>Value</h2>
	 * <p>A {@link Element} object, or <code>null</code>.</p>
	 */
	public Element getLastElementChild() {
		return Wrap.Element(JS.get(peer, "lastElementChild"));
	}

	/**
	 * <p>The <strong><code>Element.localName</code></strong> read-only property returns the
	 * local part of the qualified name of an element.</p>
	 * <h2>Value</h2>
	 * <p>A string representing the local part of the element's qualified name.</p>
	 */
	public String getLocalName() {
		return Wrap.String(JS.get(peer, "localName"));
	}

	// msRegionOverflow NON-STANDARD

	/**
	 * <p>The <strong><code>Element.namespaceURI</code></strong> read-only property returns the namespace URI of the element, or <code>null</code> if the element is not in a namespace.</p>
	 * <h2>Value</h2>
	 * <p>A string, or <code>null</code>.</p>
	 */
	public String getNamespaceURI() {
		return Wrap.String(JS.get(peer, "namespaceURI"));
	}

	/**
	 * <p>The <strong><code>Element.nextElementSibling</code></strong> read-only
	 * property returns the element immediately following the specified one in its parent's
	 * children list, or <code>null</code> if the specified element is the last one in the list.</p>
	 * <h2>Value</h2>
	 * <p>A {@link Element} object, or <code>null</code>.</p>
	 */
	public Element getNextElementSibling() {
		return Wrap.Element(JS.get(peer, "nextElementSibling"));
	}

	// openOrClosedShadowRoot NON-STANDARD

	/**
	 * <p>The <strong><code>outerHTML</code></strong> attribute of the  domxref(&quot;Element&quot;)
	 * DOM interface gets the serialized HTML fragment describing the element including its
	 * descendants. It can also be set to replace the element with nodes parsed from the given
	 * string.
	 * To only obtain the HTML representation of the contents of an element, or to replace the
	 * contents of an element, use the {@link Element.innerHTML} property
	 * instead.</p>
	 * <h2>Value</h2>
	 * <p>Reading the value of <code>outerHTML</code> returns a string
	 * containing an HTML serialization of the <code>element</code> and its descendants.
	 * Setting the value of <code>outerHTML</code> replaces the element and all of its
	 * descendants with a new DOM tree constructed by parsing the specified
	 * <code>htmlString</code>.</p>
	 */
	public String getOuterHTML() {
		return Wrap.String(JS.get(peer, "outerHTML"));
	}

	/**
	 * <p>The <strong><code>outerHTML</code></strong> attribute of the  domxref(&quot;Element&quot;)
	 * DOM interface gets the serialized HTML fragment describing the element including its
	 * descendants. It can also be set to replace the element with nodes parsed from the given
	 * string.
	 * To only obtain the HTML representation of the contents of an element, or to replace the
	 * contents of an element, use the {@link Element.innerHTML} property
	 * instead.</p>
	 * <h2>Value</h2>
	 * <p>Reading the value of <code>outerHTML</code> returns a string
	 * containing an HTML serialization of the <code>element</code> and its descendants.
	 * Setting the value of <code>outerHTML</code> replaces the element and all of its
	 * descendants with a new DOM tree constructed by parsing the specified
	 * <code>htmlString</code>.</p>
	 */
	public void setOuterHTML(String value) {
		JS.set(peer, "outerHTML", JS.param(value));
	}

	/**
	 * <p>ApiRef(&quot;DOM&quot;)
	 * The <strong><code>part</code></strong> property of the {@link Element} interface
	 * represents the part identifier(s) of the element (i.e. set using the <code>part</code>
	 * attribute), returned as a {@link DOMTokenList}. These can be used to style parts
	 * of a shadow DOM, via the <code>::part</code> pseudo-element.</p>
	 * <h2>Value</h2>
	 * <p>A {@link DOMTokenList} object.</p>
	 */
	public DOMTokenList getPart() {
		return Wrap.DOMTokenList(JS.get(peer, "part"));
	}

	/**
	 * <p>ApiRef(&quot;DOM&quot;)
	 * The <strong><code>part</code></strong> property of the {@link Element} interface
	 * represents the part identifier(s) of the element (i.e. set using the <code>part</code>
	 * attribute), returned as a {@link DOMTokenList}. These can be used to style parts
	 * of a shadow DOM, via the <code>::part</code> pseudo-element.</p>
	 * <h2>Value</h2>
	 * <p>A {@link DOMTokenList} object.</p>
	 */
	public void setPart(DOMTokenList value) {
		JS.set(peer, "part", JS.param(value));
	}

	/**
	 * <p>The <strong><code>Element.prefix</code></strong> read-only property returns the
	 * namespace prefix of the specified element, or <code>null</code> if no prefix is
	 * specified.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public String getPrefix() {
		return Wrap.String(JS.get(peer, "prefix"));
	}

	/**
	 * <p>The <strong><code>Element.previousElementSibling</code></strong>
	 * read-only property returns the {@link Element} immediately prior to the specified
	 * one in its parent's children list, or <code>null</code> if the specified element is the first one in the list.</p>
	 * <h2>Value</h2>
	 * <p>An {@link Element} object, or <code>null</code>.</p>
	 */
	public Element getPreviousElementSibling() {
		return Wrap.Element(JS.get(peer, "previousElementSibling"));
	}

	/**
	 * <p>The <strong><code>Element.scrollHeight</code></strong> read-only property is a
	 * measurement of the height of an element's content, including content not visible on the
	 * screen due to overflow.
	 * <img src="scrollheight.png" alt="" />
	 * The <code>scrollHeight</code> value is equal to the minimum height the element would
	 * require in order to fit all the content in the viewport without using a vertical
	 * scrollbar. The height is measured in the same way as {{domxref(&quot;Element.clientHeight&quot;,
	 * &quot;clientHeight&quot;)}}: it includes the element's padding, but not its border, margin or
	 * horizontal scrollbar (if present). It can also include the height of pseudo-elements
	 * such as <code>::before</code> or <code>::after</code>. If the element's content can
	 * fit without a need for vertical scrollbar, its <code>scrollHeight</code> is equal to
	 * {@link Element.clientHeight}</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> This property will round the value to an integer. If you need a fractional value, use
	 * {@link Element.getBoundingClientRect()}.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>An integer corresponding to the scrollHeight pixel value of the element.</p>
	 */
	public int getScrollHeight() {
		return JS.getInt(peer, "scrollHeight");
	}

	/**
	 * <p>The <strong><code>Element.scrollLeft</code></strong> property gets or sets the number
	 * of pixels that an element's content is scrolled from its left edge.
	 * If the element's <code>direction</code> is <code>rtl</code> (right-to-left), then
	 * <code>scrollLeft</code> is <code>0</code> when the scrollbar is at its rightmost
	 * position (at the start of the scrolled content), and then increasingly negative as you
	 * scroll towards the end of the content.
	 * It can be specified as any integer value. However:</p>
	 * <ul>
	 * <li>If the element can't be scrolled (e.g., it has no overflow), <code>scrollLeft</code>
	 * is set to <code>0</code>.</li>
	 * <li>If specified as a value less than <code>0</code> (greater than <code>0</code> for
	 * right-to-left elements), <code>scrollLeft</code> is set to <code>0</code>.</li>
	 * <li>If specified as a value greater than the maximum that the content can be scrolled,
	 * <code>scrollLeft</code> is set to the maximum.</li>
	 * </ul>
	 * <blockquote>
	 * <p><strong>Warning:</strong> On systems using display scaling, <code>scrollLeft</code> may give you a decimal
	 * value.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A number.</p>
	 */
	public double getScrollLeft() {
		return JS.getDouble(peer, "scrollLeft");
	}

	/**
	 * <p>The <strong><code>Element.scrollLeft</code></strong> property gets or sets the number
	 * of pixels that an element's content is scrolled from its left edge.
	 * If the element's <code>direction</code> is <code>rtl</code> (right-to-left), then
	 * <code>scrollLeft</code> is <code>0</code> when the scrollbar is at its rightmost
	 * position (at the start of the scrolled content), and then increasingly negative as you
	 * scroll towards the end of the content.
	 * It can be specified as any integer value. However:</p>
	 * <ul>
	 * <li>If the element can't be scrolled (e.g., it has no overflow), <code>scrollLeft</code>
	 * is set to <code>0</code>.</li>
	 * <li>If specified as a value less than <code>0</code> (greater than <code>0</code> for
	 * right-to-left elements), <code>scrollLeft</code> is set to <code>0</code>.</li>
	 * <li>If specified as a value greater than the maximum that the content can be scrolled,
	 * <code>scrollLeft</code> is set to the maximum.</li>
	 * </ul>
	 * <blockquote>
	 * <p><strong>Warning:</strong> On systems using display scaling, <code>scrollLeft</code> may give you a decimal
	 * value.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A number.</p>
	 */
	public void setScrollLeft(double value) {
		JS.set(peer, "scrollLeft", value);
	}

	// scrollLeftMax NON-STANDARD

	/**
	 * <p>The <strong><code>Element.scrollTop</code></strong> property gets or sets the number of pixels that an element's content is scrolled vertically.
	 * An element's <code>scrollTop</code> value is a measurement of the distance from the element's top to its topmost <em>visible</em> content. When an element's content does not generate a vertical scrollbar, then its <code>scrollTop</code> value is <code>0</code>.
	 * <code>scrollTop</code> can be set to any integer value, with certain caveats:</p>
	 * <ul>
	 * <li>If the element can't be scrolled (e.g. it has no overflow or if the element has a property of &quot;<strong>non-scrollable</strong>&quot;), <code>scrollTop</code> is <code>0</code>.</li>
	 * <li><code>scrollTop</code> doesn't respond to negative values; instead, it sets itself back to <code>0</code>.</li>
	 * <li>If set to a value greater than the maximum available for the element, <code>scrollTop</code> settles itself to the maximum value.
	 * When <code>scrollTop</code> is used on the root element (the <code>&lt;html&gt;</code> element), the <code>scrollY</code> of the window is returned. <a href="https://www.w3.org/TR/2016/WD-cssom-view-1-20160317/#dom-element-scrolltop">This is a special case of <code>scrollTop</code></a>.</li>
	 * </ul>
	 * <blockquote>
	 * <p><strong>Warning:</strong> On systems using display scaling, <code>scrollTop</code> may give you a decimal value.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A number.</p>
	 */
	public double getScrollTop() {
		return JS.getDouble(peer, "scrollTop");
	}

	/**
	 * <p>The <strong><code>Element.scrollTop</code></strong> property gets or sets the number of pixels that an element's content is scrolled vertically.
	 * An element's <code>scrollTop</code> value is a measurement of the distance from the element's top to its topmost <em>visible</em> content. When an element's content does not generate a vertical scrollbar, then its <code>scrollTop</code> value is <code>0</code>.
	 * <code>scrollTop</code> can be set to any integer value, with certain caveats:</p>
	 * <ul>
	 * <li>If the element can't be scrolled (e.g. it has no overflow or if the element has a property of &quot;<strong>non-scrollable</strong>&quot;), <code>scrollTop</code> is <code>0</code>.</li>
	 * <li><code>scrollTop</code> doesn't respond to negative values; instead, it sets itself back to <code>0</code>.</li>
	 * <li>If set to a value greater than the maximum available for the element, <code>scrollTop</code> settles itself to the maximum value.
	 * When <code>scrollTop</code> is used on the root element (the <code>&lt;html&gt;</code> element), the <code>scrollY</code> of the window is returned. <a href="https://www.w3.org/TR/2016/WD-cssom-view-1-20160317/#dom-element-scrolltop">This is a special case of <code>scrollTop</code></a>.</li>
	 * </ul>
	 * <blockquote>
	 * <p><strong>Warning:</strong> On systems using display scaling, <code>scrollTop</code> may give you a decimal value.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A number.</p>
	 */
	public void setScrollTop(double value) {
		JS.set(peer, "scrollTop", value);
	}

	// scrollTopMax NON-STANDARD

	/**
	 * <p>The <strong><code>Element.scrollWidth</code></strong> read-only property is a
	 * measurement of the width of an element's content, including content not visible on the
	 * screen due to overflow.
	 * The <code>scrollWidth</code> value is equal to the minimum width the element would
	 * require in order to fit all the content in the viewport without using a horizontal
	 * scrollbar. The width is measured in the same way as {{domxref(&quot;Element.clientWidth&quot;,
	 * &quot;clientWidth&quot;)}}: it includes the element's padding, but not its border, margin or
	 * vertical scrollbar (if present). It can also include the width of pseudo-elements such
	 * as <code>::before</code> or <code>::after</code>. If the element's content can fit
	 * without a need for horizontal scrollbar, its <code>scrollWidth</code> is equal to
	 * {@link Element.clientWidth}</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> This property will round the value to an integer. If you need a fractional value,
	 * use  domxref(&quot;element.getBoundingClientRect()&quot;) .</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A number.</p>
	 */
	public double getScrollWidth() {
		return JS.getDouble(peer, "scrollWidth");
	}

	/**
	 * <p>The <code>Element.shadowRoot</code> read-only property
	 * represents the shadow root hosted by the element.
	 * Use <code>Element.attachShadow()</code> to add a shadow root to an existing element.</p>
	 * <h2>Value</h2>
	 * <p>A <code>ShadowRoot</code> object instance, or <code>null</code> if the associated
	 * shadow root was attached with its <code>ShadowRoot.mode&quot;, &quot;mode</code> set to
	 * <code>closed</code>. (See <code>Element.attachShadow()</code> for further details).</p>
	 */
	public ShadowRoot getShadowRoot() {
		return Wrap.ShadowRoot(JS.get(peer, "shadowRoot"));
	}

	/**
	 * <p>The <strong><code>slot</code></strong> property of the {@link Element} interface
	 * returns the name of the shadow DOM slot the element is inserted in.
	 * A slot is a placeholder inside a web component that users can fill with their own markup (see Using templates and slots for more information).</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public String getSlot() {
		return Wrap.String(JS.get(peer, "slot"));
	}

	/**
	 * <p>The <strong><code>slot</code></strong> property of the {@link Element} interface
	 * returns the name of the shadow DOM slot the element is inserted in.
	 * A slot is a placeholder inside a web component that users can fill with their own markup (see Using templates and slots for more information).</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public void setSlot(String value) {
		JS.set(peer, "slot", JS.param(value));
	}

	/**
	 * <p><code>DOM</code>
	 * The <strong><code>tagName</code></strong> read-only property
	 * of the {@link Element} interface returns the tag name of the element on which
	 * it's called.
	 * For example, if the element is an <code>img</code>, its
	 * <code>tagName</code> property is <code>&quot;IMG&quot;</code> (for HTML documents; it may be cased
	 * differently for XML/XHTML documents).</p>
	 * <h2>Value</h2>
	 * <p>A string indicating the element's tag name. This string's capitalization depends on the
	 * document type:</p>
	 * <ul>
	 * <li>For DOM trees which represent HTML documents, the returned tag name is always in the
	 * canonical upper-case form. For example, <code>tagName</code> called on a
	 * <code>div</code> element returns <code>&quot;DIV&quot;</code>.</li>
	 * <li>The tag names of elements in an XML DOM tree are returned in the same case in which
	 * they're written in the original XML file. If an XML document includes a tag
	 * <code>&quot;&lt;SomeTag&gt;&quot;</code>, then the <code>tagName</code> property's value is
	 * <code>&quot;SomeTag&quot;</code>.
	 * For {@link Element} objects, the value of <code>tagName</code> is the same as
	 * the value of the {@link Node.nodeName} property the element object
	 * inherits from {@link Node}.</li>
	 * </ul>
	 */
	public String getTagName() {
		return Wrap.String(JS.get(peer, "tagName"));
	}

	//=========================
	// Methods
	//=========================

	/**
	 * <p>The <strong><code>Element.after()</code></strong> method inserts a set of
	 * {@link Node} or string objects in the children list of the
	 * <code>Element</code>'s parent, just after the <code>Element</code>.
	 * String objects are inserted as equivalent {@link Text} nodes.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>node1</code>, …, <code>nodeN</code>
	 * <ul>
	 * <li>: A set of {@link Node} or string objects to insert.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>None (<code>undefined</code>).</p>
	 */
	//TODO check it
//	public void after() {
//		JS.invoke(peer, "after");
//	}

	/**
	 * <p>The {@link Element} interface's <strong><code>animate()</code></strong> method
	 * is a shortcut method which creates a new {@link Animation}, applies it to the
	 * element, then plays the animation. It returns the created {@link Animation}
	 * object instance.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> Elements can have multiple animations applied to them. You can get a list of the
	 * animations that affect an element by calling {@link Element.getAnimations()}.</p>
	 * </blockquote>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>keyframes</code>
	 * <ul>
	 * <li>: Either an array of keyframe objects, <strong>or</strong> a keyframe object whose
	 * properties are arrays of values to iterate over. See {@link Web_Animations_API/Keyframe_Formats} for more details.</li>
	 * </ul>
	 * </li>
	 * <li><code>options</code>
	 * <ul>
	 * <li>: Either an <strong>integer representing the animation's duration</strong> (in
	 * milliseconds), <strong>or</strong> an Object containing one or more timing properties described in the {@link KeyframeEffect/KeyframeEffect#parameters} and/or the following options:
	 * <ul>
	 * <li><code>id</code> optional_inline
	 * <ul>
	 * <li>: A property unique to <code>animate()</code>: a string
	 * with which to reference the animation.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * </li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>Returns an {@link Animation}.</p>
	 */
	//TODO check it
//	public void animate() {
//		JS.invoke(peer, "animate");
//	}

	/**
	 * <p>The <strong><code>Element.append()</code></strong> method
	 * inserts a set of {@link Node} objects or string objects after
	 * the last child of the <code>Element</code>. String objects
	 * are inserted as equivalent {@link Text} nodes.
	 * Differences from {@link Node.appendChild()}:</p>
	 * <ul>
	 * <li><code>Element.append()</code> allows you to also append string
	 * objects, whereas <code>Node.appendChild()</code> only accepts {@link Node}
	 * objects.</li>
	 * <li><code>Element.append()</code> has no return value, whereas
	 * <code>Node.appendChild()</code> returns the appended {@link Node} object.</li>
	 * <li><code>Element.append()</code> can append several nodes and strings, whereas
	 * <code>Node.appendChild()</code> can only append one node.</li>
	 * </ul>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>param1</code>, …, <code>paramN</code>
	 * <ul>
	 * <li>: A set of {@link Node} or string objects to insert.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>None (<code>undefined</code>).</p>
	 */
	//TODO check it
//	public void append() {
//		JS.invoke(peer, "append");
//	}

	/**
	 * <p>The <strong><code>Element.attachShadow()</code></strong> method attaches a shadow DOM tree to the specified element and returns a reference to its {@link ShadowRoot}.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>options</code>
	 * <ul>
	 * <li>: An object which contains the following fields:
	 * <ul>
	 * <li><code>mode</code>
	 * <ul>
	 * <li>: A string specifying the <em>encapsulation mode</em> for the shadow DOM tree.
	 * This can be one of:
	 * <ul>
	 * <li><code>open</code>
	 * <ul>
	 * <li>: Elements of the shadow root are accessible from JavaScript outside the root,
	 * for example using {@link Element.shadowRoot}:
	 * <pre><code class="language-js">element.shadowRoot; // Returns a ShadowRoot obj
	 * </code></pre>
	 * </li>
	 * </ul>
	 * </li>
	 * <li><code>closed</code>
	 * <ul>
	 * <li>: Denies access to the node(s) of a closed shadow root
	 * from JavaScript outside it:
	 * <pre><code class="language-js">element.shadowRoot; // Returns null
	 * </code></pre>
	 * </li>
	 * </ul>
	 * </li>
	 * </ul>
	 * </li>
	 * </ul>
	 * </li>
	 * <li><code>delegatesFocus</code>
	 * <ul>
	 * <li>: A boolean that, when set to <code>true</code>, specifies behavior that mitigates custom element issues around focusability.
	 * When a non-focusable part of the shadow DOM is clicked, the first focusable part is given focus, and the shadow host is given any available <code>:focus</code> styling.</li>
	 * </ul>
	 * </li>
	 * <li><code>slotAssignment</code>
	 * <ul>
	 * <li>: Either <code>manual</code> or <code>named</code> (default). When set to <code>manual</code>, use <code>HTMLSlotElement.assign()</code> to assign a value to <code>slot</code>.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * </li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>Returns a {@link ShadowRoot} object.</p>
	 */
	//TODO check it
	public ShadowRoot attachShadow(JSObject options) {
		return Wrap.ShadowRoot(JS.invoke(peer, "attachShadow", JS.param(options)));
	}

	/**
	 * <p>The <strong><code>Element.before()</code></strong> method inserts a set of
	 * {@link Node} or string objects in the children list of this
	 * <code>Element</code>'s parent, just before this <code>Element</code>.
	 * String objects are inserted as equivalent {@link Text} nodes.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>param1</code>, …, <code>paramN</code>
	 * <ul>
	 * <li>: A set of {@link Node} or string objects to insert.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>None (<code>undefined</code>).</p>
	 */
	//TODO check it
//	public void before() {
//		JS.invoke(peer, "before");
//	}

	/**
	 * <p>The <strong><code>closest()</code></strong> method of the {@link Element} interface traverses the element and its parents (heading toward the document root) until it finds a node that matches the specified CSS selector.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>selectors</code>
	 * <ul>
	 * <li>: A string of valid CSS selector to match the {@link Element} and its ancestors against.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>The closest ancestor {@link Element} or itself, which matches the <code>selectors</code>. If there are no such element, <code>null</code>.</p>
	 */
	public Element closest(String selectors) {
		return Wrap.Element(JS.invoke(peer, "closest", JS.param(selectors)));
	}

	/**
	 * <p>The <strong><code>computedStyleMap()</code></strong> method of
	 * the {@link Element} interface returns a {@link StylePropertyMapReadOnly}
	 * interface which provides a read-only representation of a CSS declaration block that is
	 * an alternative to {@link CSSStyleDeclaration}.</p>
	 * <h3>Parameters</h3>
	 * <p>None.</p>
	 * <h3>Return value</h3>
	 * <p>A {@link StylePropertyMapReadOnly} interface.</p>
	 */
	//TODO check it
//	public void computedStyleMap() {
//		JS.invoke(peer, "computedStyleMap");
//	}

	// createShadowRoot DEPRECATED

	/**
	 * <p>The <code>getAnimations()</code> method of the {@link Element} interface
	 * (specified on the <code>Animatable</code> mixin) returns an array of all
	 * {@link Animation} objects affecting this element or which are scheduled to do so
	 * in future. It can optionally return {@link Animation} objects for descendant
	 * elements too.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> This array includes {@link Web_Animations_API}.</p>
	 * </blockquote>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>options</code> optional_inline
	 * <ul>
	 * <li>: An options object containing the following property:
	 * <ul>
	 * <li><code>subtree</code>
	 * <ul>
	 * <li>: A boolean value which, if <code>true</code>, causes animations that target
	 * descendants of <em>Element</em> to be returned as well. This includes animations
	 * that target any CSS pseudo-elements attached to
	 * <em>Element</em> or one of its descendants. Defaults to <code>false</code>.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * </li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>An <code>Array</code> of {@link Animation} objects, each representing an
	 * animation currently targeting the {@link Element} on which this method is called,
	 * or one of its descendant elements if <code>{ subtree: true }</code> is specified.</p>
	 */
	//TODO check it
//	public void getAnimations() {
//		JS.invoke(peer, "getAnimations");
//	}

	/**
	 * <p>The <strong><code>getAttribute()</code></strong> method of the
	 * {@link Element} interface returns the value of a specified attribute on the
	 * element.
	 * If the given attribute does not exist, the value returned will
	 * either be <code>null</code> or <code>&quot;&quot;</code> (the empty string); see <a href="#non-existing_attributes">Non-existing attributes</a> for details.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>attributeName</code> is the name of the attribute whose value you want to get.</li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>A string containing the value of <code>attributeName</code>.</p>
	 */
	public String getAttribute(String attributeName) {
		return Wrap.String(JS.invoke(peer, "getAttribute", JS.param(attributeName)));
	}

	/**
	 * <p>The <strong><code>getAttributeNS()</code></strong> method of the {@link Element}
	 * interface returns the string value of the attribute with the specified namespace and
	 * name. If the named attribute does not exist, the value returned will either be
	 * <code>null</code> or <code>&quot;&quot;</code> (the empty string); see <a href="#notes">Notes</a> for
	 * details.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>namespace</code>
	 * <ul>
	 * <li>: The namespace in which to look for the specified attribute.</li>
	 * </ul>
	 * </li>
	 * <li><code>name</code>
	 * <ul>
	 * <li>: The name of the attribute to look for.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>The string value of the specified attribute. If the attribute doesn't exist, the result
	 * is <code>null</code>.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> Earlier versions of the DOM specification had
	 * this method described as returning an empty string for non-existent attributes, but it
	 * was not typically implemented this way since null makes more sense. The DOM4
	 * specification now says this method should return null for non-existent attributes.</p>
	 * </blockquote>
	 */
	public String getAttributeNS(String namespace, String name) {
		return Wrap.String(JS.invoke(peer, "getAttributeNS", JS.param(namespace), JS.param(name)));
	}

	/**
	 * <p>The <strong><code>getAttributeNames()</code></strong> method of the
	 * {@link Element} interface returns the attribute names of the element as an
	 * <code>Array</code> of strings. If the element has no attributes it returns an empty
	 * array.
	 * Using <code>getAttributeNames()</code> along with
	 * {@link Element.getAttribute}, is a memory-efficient and
	 * performant alternative to accessing {@link Element.attributes}.
	 * The names returned by <strong><code>getAttributeNames()</code></strong> are <em>qualified</em> attribute names, meaning that attributes with a namespace prefix have their names returned with that namespace prefix (<em>not</em> the actual namespace), followed by a colon, followed by the attribute name (for example, <strong><code>xlink:href</code></strong>), while any attributes which have no namespace prefix have their names returned as-is (for example, <strong><code>href</code></strong>).</p>
	 * <h3>Parameters</h3>
	 * <p>None.</p>
	 * <h3>Return value</h3>
	 * <p>None (<code>undefined</code>).</p>
	 */
	//TODO check it
//	public void getAttributeNames() {
//		JS.invoke(peer, "getAttributeNames");
//	}

	/**
	 * <p>Returns the specified attribute of the specified element, as an <code>Attr</code> node.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>attrName</code> is a string containing the name of the attribute.</li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>An <code>Attr</code> node for the attribute.</p>
	 */
	public Attr getAttributeNode(String attrName) {
		return Wrap.Attr(JS.invoke(peer, "getAttributeNode", JS.param(attrName)));
	}

	/**
	 * <p>Returns the <code>Attr</code> node for the attribute with the given namespace and name.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>namespace</code> is a string specifying the namespace of the attribute.</li>
	 * <li><code>nodeName</code> is a string specifying the name of the attribute.</li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>The node for specified attribute.</p>
	 */
	public Attr getAttributeNodeNS(String namespace, String name) {
		return Wrap.Attr(JS.invoke(peer, "getAttributeNodeNS", JS.param(namespace), JS.param(name)));
	}

	/**
	 * <p>The <strong><code>Element.getBoundingClientRect()</code></strong> method returns a
	 * {@link DOMRect} object providing information about the size of an element and its
	 * position relative to the viewport.</p>
	 * <h3>Parameters</h3>
	 * <p>None.</p>
	 * <h3>Return value</h3>
	 * <p>The returned value is a {@link DOMRect} object which is the smallest rectangle
	 * which contains the entire element, including its padding and border-width. The
	 * <code>left</code>, <code>top</code>, <code>right</code>, <code>bottom</code>,
	 * <code>x</code>, <code>y</code>, <code>width</code>, and <code>height</code> properties
	 * describe the position and size of the overall rectangle in pixels. Properties other than
	 * <code>width</code> and <code>height</code> are relative to the top-left of the viewport.
	 * <img src="element-box-diagram.png" alt="" />
	 * The <code>width</code> and <code>height</code> properties of the {@link DOMRect}
	 * object returned by the method include the <code>padding</code> and
	 * <code>border-width</code>, not only the content width/height. In the standard box model,
	 * this would be equal to the <code>width</code> or <code>height</code> property of the
	 * element + <code>padding</code> + <code>border-width</code>. But
	 * if <code>box-sizing: border-box</code> is
	 * set for the element this would be directly equal to its <code>width</code> or
	 * <code>height</code>.
	 * The returned value can be thought of as the union of the rectangles returned by
	 * {@link Element.getClientRects} for the element, i.e., the CSS
	 * border-boxes associated with the element.
	 * Empty border-boxes are completely ignored. If all the element's border-boxes are empty,
	 * then a rectangle is returned with a <code>width</code> and <code>height</code> of zero
	 * and where the <code>top</code> and <code>left</code> are the top-left of the border-box
	 * for the first CSS box (in content order) for the element.
	 * The amount of scrolling that has been done of the viewport area (or any other
	 * scrollable element) is taken into account when computing the bounding rectangle. This
	 * means that the rectangle's boundary edges (<code>top</code>, <code>right</code>,
	 * <code>bottom</code>, <code>left</code>) change their values every time the scrolling
	 * position changes (because their values are relative to the viewport and not absolute).
	 * If you need the bounding rectangle relative to the top-left corner of the document,
	 * just add the current scrolling position to the <code>top</code> and <code>left</code>
	 * properties (these can be obtained using {@link window.scrollX} and
	 * {@link window.scrollY}) to get a bounding rectangle which is independent from the
	 * current scrolling position.</p>
	 */
	//TODO check it
//	public void getBoundingClientRect() {
//		JS.invoke(peer, "getBoundingClientRect");
//	}

	/**
	 * <p>The <strong><code>getClientRects()</code></strong> method of the {@link Element}
	 * interface returns a collection of <code>DOMRect</code> objects that indicate the
	 * bounding rectangles for each CSS border box in a client.
	 * Most elements only have one border box each, but a multiline inline element (such as a multiline
	 * <code>span</code> element, by default) has a border box around each line.</p>
	 * <h3>Parameters</h3>
	 * <p>None.</p>
	 * <h3>Return value</h3>
	 * <p>The returned value is a collection of <code>DOMRect</code> objects, one for each CSS
	 * border box associated with the element. Each <code>DOMRect</code> object contains
	 * read-only <code>left</code>, <code>top</code>, <code>right</code> and
	 * <code>bottom</code> properties describing the border box, in pixels, with the top-left
	 * relative to the top-left of the viewport. For tables with captions, the caption is
	 * included even though it's outside the border box of the table. When called on SVG
	 * elements other than an outer-<code>&lt;svg&gt;</code>, the &quot;viewport&quot; that the resulting
	 * rectangles are relative to is the viewport that the element's
	 * outer-<code>&lt;svg&gt;</code> establishes (and to be clear, the rectangles are also
	 * transformed by the outer-<code>&lt;svg&gt;</code>'s <code>viewBox</code> transform, if
	 * any).
	 * Originally, Microsoft intended this method to return a <code>TextRectangle</code>
	 * object for each <em>line</em> of text. However, the CSSOM working draft specifies that
	 * it returns a <code>DOMRect</code> for each <em>border box</em>. For an inline element,
	 * the two definitions are the same. But for a block element, Mozilla will return only a
	 * single rectangle.
	 * {{Fx_MinVersion_Note(3.5, &quot;Firefox 3.5 adds <code>width</code> and <code>height</code>
	 * properties to the <code>TextRectangle</code> object.&quot;)}}
	 * The amount of scrolling that has been done of the viewport area (or any other
	 * scrollable element) is taken into account when computing the rectangles.
	 * The returned rectangles do not include the bounds of any child elements that might
	 * happen to overflow.
	 * For HTML <code>area</code> elements, SVG elements that do not render anything
	 * themselves, <code>display:none</code> elements, and generally any elements that are not
	 * directly rendered, an empty list is returned.
	 * Rectangles are returned even for CSS boxes that have empty border-boxes. The
	 * <code>left</code>, <code>top</code>, <code>right</code>, and <code>bottom</code>
	 * coordinates can still be meaningful.
	 * Fractional pixel offsets are possible.</p>
	 */
	//TODO check it
//	public void getClientRects() {
//		JS.invoke(peer, "getClientRects");
//	}

	/**
	 * <p>The {@link Element} method
	 * <strong><code>getElementsByClassName()</code></strong> returns a live
	 * {@link HTMLCollection} which contains every descendant element which has the
	 * specified class name or names.
	 * The method {@link Document.getElementsByClassName}
	 * on the {@link Document} interface works essentially the same way, except it acts
	 * on the entire document, starting at the document root.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>names</code>
	 * <ul>
	 * <li>: A string containing one or more class names to match on, separated
	 * by whitespace.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>An {@link HTMLCollection} providing a live-updating list of every element which
	 * is a member of every class in <code>names</code>.</p>
	 */
	public HTMLCollection getElementsByClassName(String names) {
		return Wrap.HTMLCollection(JS.invoke(peer, "getElementsByClassName", JS.param(names)));
	}

	/**
	 * <p>The
	 * <strong><code>Element.getElementsByTagName()</code></strong> method returns a live
	 * {@link HTMLCollection} of elements with the given {@link Element/tagName}.
	 * All descendants of the
	 * specified element are searched, but not the element itself. The returned list is
	 * <em>live</em>, which means it updates itself with the DOM tree automatically.
	 * Therefore, there is no need to call <code>Element.getElementsByTagName()</code> with
	 * the same element and arguments repeatedly if the DOM changes in between calls.
	 * When called on an HTML element in an HTML document, <code>getElementsByTagName</code>
	 * lower-cases the argument before searching for it. This is undesirable when trying to
	 * match camel-cased SVG elements (such as
	 * <code>&lt;linearGradient&gt;</code>)
	 * in an HTML document. Instead, use  domxref(&quot;Element.getElementsByTagNameNS()&quot;) ,
	 * which preserves the capitalization of the tag name.
	 * <code>Element.getElementsByTagName</code> is similar to
	 * {@link Document.getElementsByTagName()}, except that it only searches for
	 * elements that are descendants of the specified element.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>tagName</code> is the qualified name to look for. The special string
	 * <code>&quot;*&quot;</code> represents all elements. For compatibility with XHTML, lower-case
	 * should be used.</li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>A <em>live</em> {@link HTMLCollection} of elements with a matching tag name, in the order they appear. If no elements are found, the <code>HTMLCollection</code> is empty.</p>
	 */
	public HTMLCollection getElementsByTagName(String tagName) {
		return Wrap.HTMLCollection(JS.invoke(peer, "getElementsByTagName", JS.param(tagName)));
	}

	/**
	 * <p>The <strong><code>Element.getElementsByTagNameNS()</code></strong> method returns a
	 * live {@link HTMLCollection} of elements with the given tag name belonging to the
	 * given namespace. It is similar to <code>Document.getElementsByTagNameNS</code>, except
	 * that its search is restricted to descendants of the specified element.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>namespaceURI</code> is the namespace URI of elements to look for (see
	 * {@link Element.namespaceURI} and {@link Attr.namespaceURI}). For
	 * example, if you need to look for XHTML elements, use the XHTML namespace URI,
	 * <code>http://www.w3.org/1999/xhtml</code>.</li>
	 * <li><code>localName</code> is either the local name of elements to look for or the
	 * special value <code>&quot;*&quot;</code>, which matches all elements (see
	 * {@link Element.localName} and {@link Attr.localName}).</li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>A live {@link HTMLCollection} of found elements in the order they appear in the tree.</p>
	 */
	//TODO check it
	public HTMLCollection getElementsByTagNameNS(String namespaceURI, String localName) {
		return Wrap.HTMLCollection(JS.invoke(peer, "getElementsByTagNameNS", JS.param(namespaceURI), JS.param(localName)));
	}

	/**
	 * <p>The <strong><code>Element.hasAttribute()</code></strong> method returns a
	 * <strong>Boolean</strong> value indicating whether the specified element has the
	 * specified attribute or not.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>name</code>
	 * <ul>
	 * <li>: is a string representing the name of the attribute.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>A boolean.</p>
	 */
	public boolean hasAttribute(String name) {
		return JS.invoke(peer, "hasAttribute", JS.param(name));
	}

	/**
	 * <p><code>hasAttributeNS</code> returns a boolean value indicating whether the current element has the specified attribute.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>namespace</code> is a string specifying the namespace of the attribute.</li>
	 * <li><code>localName</code> is the name of the attribute.</li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>A boolean.</p>
	 */
	public boolean hasAttributeNS(String namespace, String localName) {
		return JS.invoke(peer, "hasAttributeNS", JS.param(namespace), JS.param(localName));
	}

	/**
	 * <p><code>DOM</code>
	 * The <strong><code>hasAttributes()</code></strong> method of the {@link Element}
	 * interface returns a boolean value indicating whether the current element has any
	 * attributes or not.</p>
	 * <h3>Parameters</h3>
	 * <p>None.</p>
	 * <h3>Return value</h3>
	 * <p>A boolean.</p>
	 */
	public boolean hasAttributes() {
		return JS.invoke(peer, "hasAttributes");
	}

	/**
	 * <p>The <strong><code>hasPointerCapture()</code></strong> method of the
	 * {@link Element} interface checks whether the element on which it is invoked has
	 * pointer capture for the pointer identified by the given pointer ID.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>pointerId</code>
	 * <ul>
	 * <li>: The {@link PointerEvent.pointerId} of a
	 * {@link PointerEvent} object.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>A boolean value — <code>true</code> if the element does have pointer
	 * capture, <code>false</code> if it doesn't.</p>
	 */
	public boolean hasPointerCapture(String pointerId) {
		return JS.invoke(peer, "hasPointerCapture", JS.param(pointerId));
	}

	/**
	 * <p>The <strong><code>insertAdjacentElement()</code></strong> method of the
	 * {@link Element} interface inserts a given element node at a given position
	 * relative to the element it is invoked upon.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>position</code>
	 * <ul>
	 * <li>: A string representing the position relative to the
	 * <code>targetElement</code>; must match (case-insensitively) one of the following
	 * strings:
	 * <ul>
	 * <li><code>'beforebegin'</code>: Before the
	 * <code>targetElement</code> itself.</li>
	 * <li><code>'afterbegin'</code>: Just inside the
	 * <code>targetElement</code>, before its first child.</li>
	 * <li><code>'beforeend'</code>: Just inside the
	 * <code>targetElement</code>, after its last child.</li>
	 * <li><code>'afterend'</code>: After the
	 * <code>targetElement</code> itself.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * </li>
	 * <li><code>element</code>
	 * <ul>
	 * <li>: The element to be inserted into the tree.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>The element that was inserted, or <code>null</code>, if the insertion failed.</p>
	 */
	public Element insertAdjacentElement(String position, Element element) {
		return Wrap.Element(JS.invoke(peer, "insertAdjacentElement", JS.param(position), JS.param(element)));
	}

	/**
	 * <p>The <strong><code>insertAdjacentHTML()</code></strong> method of the
	 * {@link Element} interface parses the specified text as HTML or XML and inserts
	 * the resulting nodes into the DOM tree at a specified position.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>position</code>
	 * <ul>
	 * <li>: A string representing the position relative to the element. Must be one of the following strings:
	 * <ul>
	 * <li><code>&quot;beforebegin&quot;</code>
	 * <ul>
	 * <li>: Before the element. Only valid if the element is in the DOM tree and has a parent element.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;afterbegin&quot;</code>
	 * <ul>
	 * <li>: Just inside the element, before its first child.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;beforeend&quot;</code>
	 * <ul>
	 * <li>: Just inside the element, after its last child.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;afterend&quot;</code>
	 * <ul>
	 * <li>: After the element. Only valid if the element is in the DOM tree and has a parent element.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * </li>
	 * </ul>
	 * </li>
	 * <li><code>text</code>
	 * <ul>
	 * <li>: The string to be parsed as HTML or XML and inserted into the tree.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>None (<code>undefined</code>).</p>
	 */
	public void insertAdjacentHTML(String position, String text) {
		JS.invoke(peer, "insertAdjacentHTML", JS.param(position), JS.param(text));
	}

	/**
	 * <p>The <strong><code>insertAdjacentText()</code></strong> method of the {@link Element} interface, given a relative position and a string, inserts a new text node at the given position relative to the element it is called from.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>where</code>
	 * <ul>
	 * <li>: A string representing the position relative to the element the method is called from; must be one of the following strings:
	 * <ul>
	 * <li><code>'beforebegin'</code>: Before the <code>element</code> itself.</li>
	 * <li><code>'afterbegin'</code>: Just inside the <code>element</code>, before its first child.</li>
	 * <li><code>'beforeend'</code>: Just inside the <code>element</code>, after its last child.</li>
	 * <li><code>'afterend'</code>: After the <code>element</code> itself.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * </li>
	 * <li><code>data</code>
	 * <ul>
	 * <li>: A string from which to create a new text node to insert at the given position <code>where</code> relative to the element the method is called from.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>None (<code>undefined</code>).</p>
	 */
	public void insertAdjacentText(String where, String data) {
		JS.invoke(peer, "insertAdjacentText", JS.param(where), JS.param(data));
	}

	/**
	 * <p>The <strong><code>matches()</code></strong> method of the {@link Element} interface tests whether the element would be selected by the specified CSS selector.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>selectors</code>
	 * <ul>
	 * <li>: A string containing valid CSS selectors to test the {@link Element} against.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p><code>true</code> if the {@link Element} matches the <code>selectors</code>. Otherwise, <code>false</code>.</p>
	 */
	public boolean matches(String selectors) {
		return JS.invoke(peer, "matches", JS.param(selectors));
	}

	// msGetRegionContent NON-STANDARD

	// msZoomTo NON-STANDARD

	/**
	 * <p>The <strong><code>Element.prepend()</code></strong> method inserts a set of
	 * {@link Node} objects or string objects before the first child
	 * of the {@link Element}. String objects are inserted as
	 * equivalent {@link Text} nodes.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>param1</code>, …, <code>paramN</code>
	 * <ul>
	 * <li>: A set of {@link Node} or string objects to insert.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>None (<code>undefined</code>).</p>
	 */
	//TODO check it
//	public void prepend() {
//		JS.invoke(peer, "prepend");
//	}

	/**
	 * <p>The <strong><code>querySelector()</code></strong> method of the {@link Element}
	 * interface returns the first element that is a descendant of the element on which it is
	 * invoked that matches the specified group of selectors.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>selectors</code>
	 * <ul>
	 * <li>: A group of selectors to match
	 * the descendant elements of the {@link Element} <code>baseElement</code>
	 * against; this must be valid CSS syntax, or a <code>SyntaxError</code> exception will
	 * occur. The first element found which matches this group of selectors is returned.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>The first descendant element of <code>baseElement</code> which matches the specified
	 * group of <code>selectors</code>. The entire hierarchy of elements is considered when
	 * matching, including those outside the set of elements including <code>baseElement</code>
	 * and its descendants; in other words, <code>selectors</code> is first applied to the
	 * whole document, not the <code>baseElement</code>, to generate an initial list of
	 * potential elements. The resulting elements are then examined to see if they are
	 * descendants of <code>baseElement</code>. The first match of those remaining elements is
	 * returned by the <code>querySelector()</code> method.
	 * If no matches are found, the returned value is <code>null</code>.</p>
	 */
	public Element querySelector(String selectors) {
		return Wrap.Element(JS.invoke(peer, "querySelector", JS.param(selectors)));
	}

	/**
	 * <p>The {@link Element} method <strong><code>querySelectorAll()</code></strong>
	 * returns a static (not live) {@link NodeList} representing a list of elements
	 * matching the specified group of selectors which are descendants of the element on which
	 * the method was called.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>selectors</code>
	 * <ul>
	 * <li>: A string containing one or more selectors to match against. This
	 * string must be a valid CSS selector
	 * string; if it's not, a <code>SyntaxError</code> exception is thrown. See {@link Document_object_model/Locating_DOM_elements_using_selectors} for more information about using selectors to
	 * identify elements. Multiple selectors may be specified by separating them using
	 * commas.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <blockquote>
	 * <p><strong>Note:</strong> Characters which are not part of standard CSS syntax must be
	 * escaped using a backslash character. Since JavaScript also uses backslash escaping,
	 * special care must be taken when writing string literals using these characters. See
	 * Escaping special characters for more information.</p>
	 * </blockquote>
	 * <h3>Return value</h3>
	 * <p>A non-live {@link NodeList} containing one {@link Element} object for
	 * each descendant node that matches at least one of the specified selectors.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> If the specified <code>selectors</code> include a CSS pseudo-element, the returned list
	 * is always empty.</p>
	 * </blockquote>
	 */
	public NodeList querySelectorAll(String selectors) {
		return Wrap.NodeList(JS.invoke(peer, "querySelectorAll", JS.param(selectors)));
	}

	/**
	 * <p>The <strong><code>releasePointerCapture()</code></strong> method of the
	 * {@link Element} interface releases (stops) <em>pointer capture</em> that was
	 * previously set for a specific ({@link PointerEvent}) <em>pointer</em>.
	 * See the {@link Element.setPointerCapture} method
	 * for a description of <em>pointer capture</em> and how to set it for a particular
	 * element.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>pointerId</code>
	 * <ul>
	 * <li>: The {@link PointerEvent.pointerId} of a
	 * {@link PointerEvent} object.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>None (<code>undefined</code>).</p>
	 */
	public void releasePointerCapture(String pointerId) {
		JS.invoke(peer, "releasePointerCapture", JS.param(pointerId));
	}

	/**
	 * <p>The <strong><code>Element.remove()</code></strong> method removes the element from the DOM.</p>
	 * <h3>Parameters</h3>
	 * <p>None.</p>
	 * <h3>Return value</h3>
	 * <p>None (<code>undefined</code>).</p>
	 */
	public void remove() {
		JS.invoke(peer, "remove");
	}

	/**
	 * <p>The {@link Element} method
	 * <strong><code>removeAttribute()</code></strong> removes the attribute with the
	 * specified name from the element.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>attrName</code>
	 * <ul>
	 * <li>: A string specifying the name of the attribute to remove from the
	 * element. If the specified attribute does not exist, <code>removeAttribute()</code>
	 * returns without generating an error.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>None (<code>undefined</code>).</p>
	 */
	public void removeAttribute(String attrName) {
		JS.invoke(peer, "removeAttribute", JS.param(attrName));
	}

	/**
	 * <p>The <strong><code>removeAttributeNS()</code></strong> method of the
	 * {@link Element} interface removes the specified attribute from an element.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>namespace</code> is a string that contains the namespace of the attribute.</li>
	 * <li><code>attrName</code> is a string that names the attribute to be removed from the
	 * current node.</li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>None (<code>undefined</code>).</p>
	 */
	public void removeAttributeNS(String namespace, String attrName) {
		JS.invoke(peer, "removeAttributeNS", JS.param(namespace), JS.param(attrName));
	}

	/**
	 * <p>The <strong><code>removeAttributeNode()</code></strong> method of the
	 * {@link Element} interface removes the specified attribute from the element.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>attributeNode</code>
	 * <ul>
	 * <li>: The attribute node to remove from the element.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>The attribute node that was removed.</p>
	 */
	public Attr removeAttributeNode(Attr attributeNode) {
		return Wrap.Attr(JS.invoke(peer, "removeAttributeNode", JS.param(attributeNode)));
	}

	/**
	 * <p>The <strong><code>Element.replaceChildren()</code></strong> method replaces the
	 * existing children of a {@link Node} with a specified new set of children. These
	 * can be string or {@link Node} objects.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>param1</code>, …, <code>paramN</code>
	 * <ul>
	 * <li>: A set of {@link Node} or string objects to replace the
	 * <code>Element</code>'s existing children with. If no replacement objects are
	 * specified, then the <code>Element</code> is emptied of all child nodes.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>None (<code>undefined</code>).</p>
	 */
	//TODO check it
//	public void replaceChildren() {
//		JS.invoke(peer, "replaceChildren");
//	}

	/**
	 * <p>The <strong><code>Element.replaceWith()</code></strong> method replaces this
	 * <code>Element</code> in the children list of its parent with a set of
	 * {@link Node} or string objects. String
	 * objects are inserted as equivalent {@link Text} nodes.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>param1</code>, …, <code>paramN</code>
	 * <ul>
	 * <li>: A set of {@link Node} or string objects to replace.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>None (<code>undefined</code>).</p>
	 */
	//TODO check it
//	public void replaceWith() {
//		JS.invoke(peer, "replaceWith");
//	}

	/**
	 * <p>The <strong><code>Element.requestFullscreen()</code></strong>
	 * method issues an asynchronous request to make the element be displayed in fullscreen
	 * mode.
	 * It's not guaranteed that the element will be put into full screen mode. If permission
	 * to enter full screen mode is granted, the returned <code>Promise</code> will resolve
	 * and the element will receive a {@link Element/fullscreenchange_event} event to let it know that
	 * it's now in full screen mode. If permission is denied, the promise is rejected and the
	 * element receives a {@link Element/fullscreenerror_event} event instead. If the element has been
	 * detached from the original document, then the document receives these events instead.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> This method must be called while responding to a user
	 * interaction or a device orientation change; otherwise it will fail.</p>
	 * </blockquote>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>options</code> optional_inline
	 * <ul>
	 * <li>: An object that controls the behavior of the transition to fullscreen mode. The available options are:
	 * <ul>
	 * <li><code>navigationUI</code> optional_inline
	 * <ul>
	 * <li>: Controls whether or not to show navigation UI while the element is in fullscreen mode.
	 * The default value is <code>&quot;auto&quot;</code>, which indicates that the browser should decide what to do.
	 * <ul>
	 * <li><code>&quot;hide&quot;</code>
	 * <ul>
	 * <li>: The browser's navigation interface will be hidden
	 * and the entire dimensions of the screen will be allocated to the display of the element.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;show&quot;</code>
	 * <ul>
	 * <li>: The browser will present page navigation controls and possibly other
	 * user interface; the dimensions of the element (and the perceived size of the screen) will be clamped
	 * to leave room for this user interface.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;auto&quot;</code>
	 * <ul>
	 * <li>: The browser will choose which of the above settings to apply.
	 * This is the default value.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * </li>
	 * </ul>
	 * </li>
	 * </ul>
	 * </li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>A <code>Promise</code> which is resolved with a value of <code>undefined</code> when
	 * the transition to full screen is complete.</p>
	 */
	//TODO check it
//	public void requestFullscreen() {
//		JS.invoke(peer, "requestFullscreen");
//	}

	/**
	 * <p>The <strong><code>Element.requestPointerLock()</code></strong> method lets you
	 * asynchronously ask for the pointer to be locked on the given element.
	 * To track the success or failure of the request, it is necessary to listen for the
	 * {@link Element/pointerlockchange_event} and {@link Element/pointerlockerror_event} events at the
	 * {@link Document} level.</p>
	 * <h3>Parameters</h3>
	 * <p>None.</p>
	 * <h3>Return value</h3>
	 * <p>None (<code>undefined</code>).</p>
	 */
	public void requestPointerLock() {
		JS.invoke(peer, "requestPointerLock");
	}

	/**
	 * <p>The <strong><code>scroll()</code></strong> method of the {@link Element}
	 * interface scrolls the element to a particular set of coordinates inside a given
	 * element.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>x-coord</code>
	 * <ul>
	 * <li>: The pixel along the horizontal axis of the element that you want displayed in the
	 * upper left.</li>
	 * </ul>
	 * </li>
	 * <li><code>y-coord</code>
	 * <ul>
	 * <li>: The pixel along the vertical axis of the element that you want displayed in the
	 * upper left.
	 * - or -</li>
	 * </ul>
	 * </li>
	 * <li><code>options</code>
	 * <ul>
	 * <li>: A dictionary containing the following parameters:
	 * <ul>
	 * <li><code>top</code>
	 * <ul>
	 * <li>: Specifies the number of pixels along the Y axis to scroll the window or element.</li>
	 * </ul>
	 * </li>
	 * <li><code>left</code>
	 * <ul>
	 * <li>: Specifies the number of pixels along the X axis to scroll the window or element.</li>
	 * </ul>
	 * </li>
	 * <li><code>behavior</code>
	 * <ul>
	 * <li>: Specifies whether the scrolling should animate smoothly (<code>smooth</code>), happen instantly in a single jump (<code>instant</code>), or let the browser choose (<code>auto</code>, default).</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * </li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>None (<code>undefined</code>).</p>
	 */
	public void scroll(int xCoord, int yCoord) {
		JS.invoke(peer, "scroll", xCoord, yCoord);
	}

	/**
	 * {@link #scroll(int, int)}
	 */
	public void scroll(JSObject options) {
		JS.invoke(peer, "scroll", JS.param(options));
	}

	/**
	 * <p>The <strong><code>scrollBy()</code></strong> method of the {@link Element}
	 * interface scrolls an element by the given amount.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>x-coord</code> is the horizontal pixel value that you want to
	 * scroll by.</li>
	 * <li><code>y-coord</code> is the vertical pixel value that you want to scroll
	 * by.
	 * - or -</li>
	 * <li><code>options</code>
	 * <ul>
	 * <li>: A dictionary containing the following parameters:
	 * <ul>
	 * <li><code>top</code>
	 * <ul>
	 * <li>: Specifies the number of pixels along the Y axis to scroll the window or element.</li>
	 * </ul>
	 * </li>
	 * <li><code>left</code>
	 * <ul>
	 * <li>: Specifies the number of pixels along the X axis to scroll the window or element.</li>
	 * </ul>
	 * </li>
	 * <li><code>behavior</code>
	 * <ul>
	 * <li>: Specifies whether the scrolling should animate smoothly (<code>smooth</code>), happen instantly in a single jump (<code>instant</code>), or let the browser choose (<code>auto</code>, default).</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * </li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>None (<code>undefined</code>).</p>
	 */
	public void scrollBy(int xCoord, int yCoord) {
		JS.invoke(peer, "scrollBy", xCoord, yCoord);
	}

	/**
	 * {@link #scrollBy(int, int)}
	 */
	public void scrollBy(JSObject options) {
		JS.invoke(peer, "scrollBy", JS.param(options));
	}

	/**
	 * <p>The {@link Element} interface's
	 * <strong><code>scrollIntoView()</code></strong> method scrolls the element's ancestor
	 * containers such that the element on which <code>scrollIntoView()</code> is called is
	 * visible to the user.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>alignToTop</code> optional_inline
	 * <ul>
	 * <li>: A boolean value:
	 * <ul>
	 * <li>If <code>true</code>, the top of the element will be aligned to the top of the
	 * visible area of the scrollable ancestor. Corresponds to
	 * <code>scrollIntoViewOptions: {block: &quot;start&quot;, inline: &quot;nearest&quot;}</code>. This is
	 * the default value.</li>
	 * <li>If <code>false</code>, the bottom of the element will be aligned to the bottom
	 * of the visible area of the scrollable ancestor. Corresponds to
	 * <code>scrollIntoViewOptions: {block: &quot;end&quot;, inline: &quot;nearest&quot;}</code>.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * </li>
	 * <li><code>scrollIntoViewOptions</code> optional_inline
	 * experimental_inline
	 * <ul>
	 * <li>: An Object with the following properties:
	 * <ul>
	 * <li><code>behavior</code> optional_inline
	 * <ul>
	 * <li>: Defines the transition animation.
	 * One of <code>auto</code> or <code>smooth</code>. Defaults to <code>auto</code>.</li>
	 * </ul>
	 * </li>
	 * <li><code>block</code> optional_inline
	 * <ul>
	 * <li>: Defines vertical alignment.
	 * One of <code>start</code>, <code>center</code>, <code>end</code>, or
	 * <code>nearest</code>. Defaults to <code>start</code>.</li>
	 * </ul>
	 * </li>
	 * <li><code>inline</code> optional_inline
	 * <ul>
	 * <li>: Defines horizontal alignment.
	 * One of <code>start</code>, <code>center</code>, <code>end</code>, or
	 * <code>nearest</code>. Defaults to <code>nearest</code>.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * </li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>None (<code>undefined</code>).</p>
	 */
	//TODO check it
//	public void scrollIntoView() {
//		JS.invoke(peer, "scrollIntoView");
//	}

	// scrollIntoViewIfNeeded NON-STANDARD

	/**
	 * <p>The <strong><code>scrollTo()</code></strong> method of the {@link Element}
	 * interface scrolls to a particular set of coordinates inside a given element.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>x-coord</code> is the pixel along the horizontal axis of the
	 * element that you want displayed in the upper left.</li>
	 * <li><code>y-coord</code> is the pixel along the vertical axis of the element
	 * that you want displayed in the upper left.
	 * - or -</li>
	 * <li><code>options</code>
	 * <ul>
	 * <li>: A dictionary containing the following parameters:
	 * <ul>
	 * <li><code>top</code>
	 * <ul>
	 * <li>: Specifies the number of pixels along the Y axis to scroll the window or element.</li>
	 * </ul>
	 * </li>
	 * <li><code>left</code>
	 * <ul>
	 * <li>: Specifies the number of pixels along the X axis to scroll the window or element.</li>
	 * </ul>
	 * </li>
	 * <li><code>behavior</code>
	 * <ul>
	 * <li>: Specifies whether the scrolling should animate smoothly (<code>smooth</code>), happen instantly in a single jump (<code>instant</code>), or let the browser choose (<code>auto</code>, default).</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * </li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>None (<code>undefined</code>).</p>
	 */
	public void scrollTo(int xCoord, int yCoord) {
		JS.invoke(peer, "scrollTo", xCoord, yCoord);
	}

	/**
	 * {@link #scrollTo(int, int)}
	 */
	public void scrollTo(JSObject options) {
		JS.invoke(peer, "scrollTo", JS.param(options));
	}

	/**
	 * <p>Sets the value of an attribute on the specified element. If
	 * the attribute already exists, the value is updated; otherwise a new attribute is added
	 * with the specified name and value.
	 * To get the current value of an attribute, use {{domxref(&quot;Element.getAttribute&quot;,
	 * &quot;getAttribute()&quot;)}}; to remove an attribute, call {{domxref(&quot;Element.removeAttribute&quot;,
	 * &quot;removeAttribute()&quot;)}}.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>name</code>
	 * <ul>
	 * <li>: A string specifying the name of the attribute whose value is to be
	 * set. The attribute name is automatically converted to all lower-case when
	 * <code>setAttribute()</code> is called on an HTML element in an HTML document.</li>
	 * </ul>
	 * </li>
	 * <li><code>value</code>
	 * <ul>
	 * <li>: A string containing the value to assign to the attribute. Any
	 * non-string value specified is converted automatically into a string.
	 * Boolean attributes are considered to be <code>true</code> if they're present on the
	 * element at all. You should set <code>value</code> to the empty string (<code>&quot;&quot;</code>)
	 * or the attribute's name, with no leading or trailing whitespace. See the <a href="#examples">example</a> below for a practical demonstration.
	 * Since the specified <code>value</code> gets converted into a string, specifying
	 * <code>null</code> doesn't necessarily do what you expect. Instead of removing the
	 * attribute or setting its value to be <code>null</code>, it instead sets the attribute's
	 * value to the string <code>&quot;null&quot;</code>. If you wish to remove an attribute, call
	 * {@link Element.removeAttribute}.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>None (<code>undefined</code>).</p>
	 */
	public void setAttribute(String name, String value) {
		JS.invoke(peer, "setAttribute", JS.param(name), JS.param(value));
	}

	/**
	 * <p><code>setAttributeNS</code> adds a new attribute or changes the value of an attribute
	 * with the given namespace and name.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>namespace</code> is a string specifying the namespace of the attribute.</li>
	 * <li><code>name</code> is a string identifying the attribute by its qualified name;
	 * that is, a namespace prefix followed by a colon followed by a local name.</li>
	 * <li><code>value</code> is the desired string value of the new attribute.</li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>None (<code>undefined</code>).</p>
	 */
	public void setAttributeNS(String namespace, String name, String value) {
		JS.invoke(peer, "setAttributeNS", JS.param(namespace), JS.param(name), JS.param(value));
	}

	/**
	 * <p>The <strong><code>setAttributeNode()</code></strong> method adds a new
	 * <code>Attr</code> node to the specified element.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>attribute</code> is the <code>Attr</code> node to set on the element.</li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>The replaced attribute node, if any, returned by this function.</p>
	 */
	public Attr setAttributeNode(Attr attribute) {
		return Wrap.Attr(JS.invoke(peer, "setAttributeNode", JS.param(attribute)));
	}

	/**
	 * <p><code>setAttributeNodeNS</code> adds a new namespaced attribute node to an element.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>attributeNode</code> is an <code>Attr</code> node.</li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>The replaced attribute node, if any, returned by this function.</p>
	 */
	public Attr setAttributeNodeNS(Attr attribute) {
		return Wrap.Attr(JS.invoke(peer, "setAttributeNodeNS", JS.param(attribute)));
	}

	// setCapture DEPRECATED

	/**
	 * <p>The <strong><code>setHTML()</code></strong> method of the {@link Element} interface is used to parse and sanitize a string of HTML and then insert it into the DOM as a subtree of the element.
	 * It should be used instead of {@link Element.innerHTML} for inserting untrusted strings of HTML into an element.
	 * The parsing process drops any elements in the HTML string that are invalid in the context of the current element, while sanitizing removes any unsafe or otherwise unwanted elements, attributes or comments.
	 * The default <code>Sanitizer()</code> configuration strips out XSS-relevant input by default, including <code>script</code> tags, custom elements, and comments.
	 * The sanitizer configuration may be customized using {@link Sanitizer.Sanitizer} constructor options.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> Use {@link Sanitizer.sanitizeFor()} instead of this method if the string must be inserted into the DOM at a later point, for example if the target element is not yet available.</p>
	 * </blockquote>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>input</code>
	 * <ul>
	 * <li>: A string defining HTML to be sanitized.</li>
	 * </ul>
	 * </li>
	 * <li><code>options</code> optional_inline
	 * <ul>
	 * <li>: A options object with the following optional parameters:
	 * <ul>
	 * <li><code>sanitizer</code>
	 * <ul>
	 * <li>: A {@link Sanitizer} object which defines what elements of the input will be sanitized.
	 * If not specified, the default {@link Sanitizer} object is used.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * </li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>None (<code>undefined</code>).</p>
	 */
	public void setHTML(String input) {
		JS.invoke(peer, "setHTML", JS.param(input));
	}

	/**
	 * <p>The <strong><code>setPointerCapture()</code></strong> method of the
	 * {@link Element} interface is used to designate a specific element as the
	 * <em>capture target</em> of future pointer events. Subsequent events for the pointer will
	 * be targeted at the capture element until capture is released (via
	 * {@link Element.releasePointerCapture()} or the
	 * {@link HTMLElement/pointerup_event} event is fired).</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> When pointer capture is set,
	 * {@link HTMLElement/pointerover_event},
	 * {@link HTMLElement/pointerout_event},
	 * {@link HTMLElement/pointerenter_event}, and
	 * {@link HTMLElement/pointerleave_event} events are only generated
	 * when crossing the boundary of the capture target. This has the effect of suppressing
	 * these events on all other elements.</p>
	 * </blockquote>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>pointerId</code>
	 * <ul>
	 * <li>: The {@link PointerEvent.pointerId} of a
	 * {@link PointerEvent} object.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>None (<code>undefined</code>).</p>
	 */
	public void setPointerCapture(String pointerId) {
		JS.invoke(peer, "setPointerCapture", JS.param(pointerId));
	}

	/**
	 * <p>The <strong><code>toggleAttribute()</code></strong> method of the
	 * {@link Element} interface toggles a Boolean attribute (removing it if it is
	 * present and adding it if it is not present) on the given element.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>name</code>
	 * <ul>
	 * <li>: A string specifying the name of the attribute to be toggled. The
	 * attribute name is automatically converted to all lower-case when
	 * <code>toggleAttribute()</code> is called on an HTML element in an HTML document.</li>
	 * </ul>
	 * </li>
	 * <li><code>force</code> optional_inline
	 * <ul>
	 * <li>: A boolean value which has the following effects:
	 * <ul>
	 * <li>if not specified at all, the <code>toggleAttribute</code> method &quot;toggles&quot; the attribute named <code>name</code> — removing it if it is present, or else adding it if it is not present</li>
	 * <li>if true, the <code>toggleAttribute</code> method adds an attribute named <code>name</code></li>
	 * <li>if false, the <code>toggleAttribute</code> method removes the attribute named <code>name</code></li>
	 * </ul>
	 * </li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p><code>true</code> if attribute <strong><code>name</code></strong> is eventually
	 * present, and <code>false</code> otherwise.</p>
	 */
	public boolean toggleAttribute(String name) {
		return JS.invoke(peer, "toggleAttribute", JS.param(name));
	}

}
