package browser.dom;

public interface EventListener {

	void handleEvent(Event evt);

}
