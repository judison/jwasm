package browser.dom;

import browser.javascript.JS;

/**
 * <p>The <strong><code>AudioTrackList</code></strong> interface is used to represent a list of the audio tracks contained within a given HTML media element, with each track represented by a separate {@link AudioTrack} object in the list.
 * Retrieve an instance of this object with {@link HTMLMediaElement#audioTracks}. The individual tracks can be accessed using array syntax.</p>
 */
@SuppressWarnings("unused")
public class AudioTrackList extends EventTarget {

	protected AudioTrackList(Object peer) {
		super(peer);
	}

	//=========================
	// Properties
	//=========================

	/**
	 * <p>The read-only <strong>{@link AudioTrackList}</strong>
	 * property <strong><code>length</code></strong> returns the number of entries in the
	 * <code>AudioTrackList</code>, each of which is an {@link AudioTrack}
	 * representing one audio track in the media element. A value of 0 indicates that
	 * there are no audio tracks in the media.</p>
	 * <h2>Value</h2>
	 * <p>A number indicating how many audio tracks are included in the
	 * <code>AudioTrackList</code>. Each track can be accessed by treating the
	 * <code>AudioTrackList</code> as an array of objects of type {@link AudioTrack}.</p>
	 */
	public double getLength() {
		return JS.getDouble(peer, "length");
	}

	//=========================
	// Methods
	//=========================

	/**
	 * <p>The <strong>{@link AudioTrackList}</strong> method
	 * <strong><code>getTrackById()</code></strong> returns the first
	 * {@link AudioTrack} object from the track list whose {{domxref(&quot;AudioTrack.id&quot;,
	 * &quot;id&quot;)}} matches the specified string. This lets you find a specified track if
	 * you know its ID string.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>id</code>
	 * <ul>
	 * <li>: A string indicating the ID of the track to locate within the track
	 * list.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>An {@link AudioTrack} object indicating the first track found within the
	 * <code>AudioTrackList</code> whose <code>id</code> matches the specified string. If no
	 * match is found, this method returns <code>null</code>.
	 * The tracks are searched in their natural order; that is, in the order defined by the
	 * media resource itself, or, if the resource doesn't define an order, the relative order
	 * in which the tracks are declared by the media resource.</p>
	 */
	public AudioTrack getTrackById(String id) {
		return Wrap.AudioTrack(JS.invoke(peer, "getTrackById", JS.param(id)));
	}

}
