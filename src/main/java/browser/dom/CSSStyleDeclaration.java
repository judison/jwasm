/*
 * Copyright 2022 Volker Berlin (i-net software)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package browser.dom;

import javax.annotation.Nonnull;

import browser.javascript.JS;
import browser.javascript.Peered;

/**
 * <a href="https://developer.mozilla.org/en-US/docs/Web/API/CSSStyleDeclaration">https://developer.mozilla.org/en-US/docs/Web/API/CSSStyleDeclaration</a>
 *
 * @author Volker Berlin
 */
public class CSSStyleDeclaration extends Peered {

	/**
	 * @param peer the native JavaScript object
	 */
	public CSSStyleDeclaration(Object peer) {
		super(peer);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/API/CSSStyleDeclaration/cssText">https://developer.mozilla.org/en-US/docs/Web/API/CSSStyleDeclaration/cssText</a>
	 *
	 * @param value the value
	 */
	public void setCssText(String value) {
		JS.set(peer, "cssText", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/API/CSSStyleDeclaration/cssText">https://developer.mozilla.org/en-US/docs/Web/API/CSSStyleDeclaration/cssText</a>
	 *
	 * @return the current value
	 */
	public String getCssText() {
		return Wrap.String(JS.get(peer, "cssText"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/API/CSSStyleDeclaration/length">https://developer.mozilla.org/en-US/docs/Web/API/CSSStyleDeclaration/length</a>
	 *
	 * @return the current value
	 */
	public int getLength() {
		return JS.getInt(peer, "length");
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/API/CSSStyleDeclaration/setProperty">https://developer.mozilla.org/en-US/docs/Web/API/CSSStyleDeclaration/setProperty</a>
	 *
	 * @param propertyName the property name
	 * @param value        the value
	 */
	public void setProperty(@Nonnull String propertyName, String value) {
		JS.set(peer, propertyName, value);
	}

	/* =====================================================
	 * The CSS properties in alphabetical order
	 * https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Properties_Reference
	 * https://developer.mozilla.org/en-US/docs/Web/CSS/Reference
	 * =====================================================*/

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/background">https://developer.mozilla.org/en-US/docs/Web/CSS/background</a>
	 *
	 * @param value the value
	 */
	public void setBackground(String value) {
		JS.set(peer, "background", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/background">https://developer.mozilla.org/en-US/docs/Web/CSS/background</a>
	 *
	 * @return the current value
	 */
	public String getBackground() {
		return Wrap.String(JS.get(peer, "background"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/background-attachment">https://developer.mozilla.org/en-US/docs/Web/CSS/background-attachment</a>
	 *
	 * @param value the value
	 */
	public void setBackgroundAttachment(String value) {
		JS.set(peer, "background-attachment", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/background-attachment">https://developer.mozilla.org/en-US/docs/Web/CSS/background-attachment</a>
	 *
	 * @return the current value
	 */
	public String getBackgroundAttachment() {
		return Wrap.String(JS.get(peer, "background-attachment"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/background-color">https://developer.mozilla.org/en-US/docs/Web/CSS/background-color</a>
	 *
	 * @param value the value
	 */
	public void setBackgroundColor(String value) {
		JS.set(peer, "background-color", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/background-color">https://developer.mozilla.org/en-US/docs/Web/CSS/background-color</a>
	 *
	 * @return the current value
	 */
	public String getBackgroundColor() {
		return Wrap.String(JS.get(peer, "background-color"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/background-image">https://developer.mozilla.org/en-US/docs/Web/CSS/background-image</a>
	 *
	 * @param value the value
	 */
	public void setBackgroundImage(String value) {
		JS.set(peer, "background-image", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/background-image">https://developer.mozilla.org/en-US/docs/Web/CSS/background-image</a>
	 *
	 * @return the current value
	 */
	public String getBackgroundImage() {
		return Wrap.String(JS.get(peer, "background-image"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/background-position">https://developer.mozilla.org/en-US/docs/Web/CSS/background-position</a>
	 *
	 * @param value the value
	 */
	public void setBackgroundPosition(String value) {
		JS.set(peer, "background-position", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/background-position">https://developer.mozilla.org/en-US/docs/Web/CSS/background-position</a>
	 *
	 * @return the current value
	 */
	public String getBackgroundPosition() {
		return Wrap.String(JS.get(peer, "background-position"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/background-repeat">https://developer.mozilla.org/en-US/docs/Web/CSS/background-repeat</a>
	 *
	 * @param value the value
	 */
	public void setBackgroundRepeat(String value) {
		JS.set(peer, "background-repeat", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/background-repeat">https://developer.mozilla.org/en-US/docs/Web/CSS/background-repeat</a>
	 *
	 * @return the current value
	 */
	public String getBackgroundRepeat() {
		return Wrap.String(JS.get(peer, "background-repeat"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border">https://developer.mozilla.org/en-US/docs/Web/CSS/border</a>
	 *
	 * @param value the value
	 */
	public void setBorder(String value) {
		JS.set(peer, "border", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border">https://developer.mozilla.org/en-US/docs/Web/CSS/border</a>
	 *
	 * @return the current value
	 */
	public String getBorder() {
		return Wrap.String(JS.get(peer, "border"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-bottom">https://developer.mozilla.org/en-US/docs/Web/CSS/border-bottom</a>
	 *
	 * @param value the value
	 */
	public void setBorderBottom(String value) {
		JS.set(peer, "border-bottom", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-bottom">https://developer.mozilla.org/en-US/docs/Web/CSS/border-bottom</a>
	 *
	 * @return the current value
	 */
	public String getBorderBottom() {
		return Wrap.String(JS.get(peer, "border-bottom"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-bottom-color">https://developer.mozilla.org/en-US/docs/Web/CSS/border-bottom-color</a>
	 *
	 * @param value the value
	 */
	public void setBorderBottomColor(String value) {
		JS.set(peer, "border-bottom-color", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-bottom-color">https://developer.mozilla.org/en-US/docs/Web/CSS/border-bottom-color</a>
	 *
	 * @return the current value
	 */
	public String getBorderBottomColor() {
		return Wrap.String(JS.get(peer, "border-bottom-color"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-bottom-style">https://developer.mozilla.org/en-US/docs/Web/CSS/border-bottom-style</a>
	 *
	 * @param value the value
	 */
	public void setBorderBottomStyle(String value) {
		JS.set(peer, "border-bottom-style", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-bottom-style">https://developer.mozilla.org/en-US/docs/Web/CSS/border-bottom-style</a>
	 *
	 * @return the current value
	 */
	public String getBorderBottomStyle() {
		return Wrap.String(JS.get(peer, "border-bottom-style"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-bottom-width">https://developer.mozilla.org/en-US/docs/Web/CSS/border-bottom-width</a>
	 *
	 * @param value the value
	 */
	public void setBorderBottomWidth(String value) {
		JS.set(peer, "border-bottom-width", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-bottom-width">https://developer.mozilla.org/en-US/docs/Web/CSS/border-bottom-width</a>
	 *
	 * @return the current value
	 */
	public String getBorderBottomWidth() {
		return Wrap.String(JS.get(peer, "border-bottom-width"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-color">https://developer.mozilla.org/en-US/docs/Web/CSS/border-color</a>
	 *
	 * @param value the value
	 */
	public void setBorderColor(String value) {
		JS.set(peer, "border-color", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-color">https://developer.mozilla.org/en-US/docs/Web/CSS/border-color</a>
	 *
	 * @return the current value
	 */
	public String getBorderColor() {
		return Wrap.String(JS.get(peer, "border-color"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-left">https://developer.mozilla.org/en-US/docs/Web/CSS/border-left</a>
	 *
	 * @param value the value
	 */
	public void setBorderLeft(String value) {
		JS.set(peer, "border-left", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-left">https://developer.mozilla.org/en-US/docs/Web/CSS/border-left</a>
	 *
	 * @return the current value
	 */
	public String getBorderLeft() {
		return Wrap.String(JS.get(peer, "border-left"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-left-color">https://developer.mozilla.org/en-US/docs/Web/CSS/border-left-color</a>
	 *
	 * @param value the value
	 */
	public void setBorderLeftColor(String value) {
		JS.set(peer, "border-left-color", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-left-color">https://developer.mozilla.org/en-US/docs/Web/CSS/border-left-color</a>
	 *
	 * @return the current value
	 */
	public String getBorderLeftColor() {
		return Wrap.String(JS.get(peer, "border-left-color"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-left-style">https://developer.mozilla.org/en-US/docs/Web/CSS/border-left-style</a>
	 *
	 * @param value the value
	 */
	public void setBorderLeftStyle(String value) {
		JS.set(peer, "border-left-style", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-left-style">https://developer.mozilla.org/en-US/docs/Web/CSS/border-left-style</a>
	 *
	 * @return the current value
	 */
	public String getBorderLeftStyle() {
		return Wrap.String(JS.get(peer, "border-left-style"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-left-width">https://developer.mozilla.org/en-US/docs/Web/CSS/border-left-width</a>
	 *
	 * @param value the value
	 */
	public void setBorderLeftWidth(String value) {
		JS.set(peer, "border-left-width", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-left-width">https://developer.mozilla.org/en-US/docs/Web/CSS/border-left-width</a>
	 *
	 * @return the current value
	 */
	public String getBorderLeftWidth() {
		return Wrap.String(JS.get(peer, "border-left-width"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-right">https://developer.mozilla.org/en-US/docs/Web/CSS/border-right</a>
	 *
	 * @param value the value
	 */
	public void setBorderRight(String value) {
		JS.set(peer, "border-right", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-right">https://developer.mozilla.org/en-US/docs/Web/CSS/border-right</a>
	 *
	 * @return the current value
	 */
	public String getBorderRight() {
		return Wrap.String(JS.get(peer, "border-right"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-right-color">https://developer.mozilla.org/en-US/docs/Web/CSS/border-right-color</a>
	 *
	 * @param value the value
	 */
	public void setBorderRightColor(String value) {
		JS.set(peer, "border-right-color", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-right-color">https://developer.mozilla.org/en-US/docs/Web/CSS/border-right-color</a>
	 *
	 * @return the current value
	 */
	public String getBorderRightColor() {
		return Wrap.String(JS.get(peer, "border-right-color"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-right-style">https://developer.mozilla.org/en-US/docs/Web/CSS/border-right-style</a>
	 *
	 * @param value the value
	 */
	public void setBorderRightStyle(String value) {
		JS.set(peer, "border-right-style", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-right-style">https://developer.mozilla.org/en-US/docs/Web/CSS/border-right-style</a>
	 *
	 * @return the current value
	 */
	public String getBorderRightStyle() {
		return Wrap.String(JS.get(peer, "border-right-style"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-right-width">https://developer.mozilla.org/en-US/docs/Web/CSS/border-right-width</a>
	 *
	 * @param value the value
	 */
	public void setBorderRightWidth(String value) {
		JS.set(peer, "border-right-width", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-right-width">https://developer.mozilla.org/en-US/docs/Web/CSS/border-right-width</a>
	 *
	 * @return the current value
	 */
	public String getBorderRightWidth() {
		return Wrap.String(JS.get(peer, "border-right-width"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-style">https://developer.mozilla.org/en-US/docs/Web/CSS/border-style</a>
	 *
	 * @param value the value
	 */
	public void setBorderStyle(String value) {
		JS.set(peer, "border-style", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-style">https://developer.mozilla.org/en-US/docs/Web/CSS/border-style</a>
	 *
	 * @return the current value
	 */
	public String getBorderStyle() {
		return Wrap.String(JS.get(peer, "border-style"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-top">https://developer.mozilla.org/en-US/docs/Web/CSS/border-top</a>
	 *
	 * @param value the value
	 */
	public void setBorderTop(String value) {
		JS.set(peer, "border-top", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-top">https://developer.mozilla.org/en-US/docs/Web/CSS/border-top</a>
	 *
	 * @return the current value
	 */
	public String getBorderTop() {
		return Wrap.String(JS.get(peer, "border-top"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-top-color">https://developer.mozilla.org/en-US/docs/Web/CSS/border-top-color</a>
	 *
	 * @param value the value
	 */
	public void setBorderTopColor(String value) {
		JS.set(peer, "border-top-color", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-top-color">https://developer.mozilla.org/en-US/docs/Web/CSS/border-top-color</a>
	 *
	 * @return the current value
	 */
	public String getBorderTopColor() {
		return Wrap.String(JS.get(peer, "border-top-color"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-top-style">https://developer.mozilla.org/en-US/docs/Web/CSS/border-top-style</a>
	 *
	 * @param value the value
	 */
	public void setBorderTopStyle(String value) {
		JS.set(peer, "border-top-style", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-top-style">https://developer.mozilla.org/en-US/docs/Web/CSS/border-top-style</a>
	 *
	 * @return the current value
	 */
	public String getBorderTopStyle() {
		return Wrap.String(JS.get(peer, "border-top-style"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-top-width">https://developer.mozilla.org/en-US/docs/Web/CSS/border-top-width</a>
	 *
	 * @param value the value
	 */
	public void setBorderTopWidth(String value) {
		JS.set(peer, "border-top-width", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-top-width">https://developer.mozilla.org/en-US/docs/Web/CSS/border-top-width</a>
	 *
	 * @return the current value
	 */
	public String getBorderTopWidth() {
		return Wrap.String(JS.get(peer, "border-top-width"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-width">https://developer.mozilla.org/en-US/docs/Web/CSS/border-width</a>
	 *
	 * @param value the value
	 */
	public void setBorderWidth(String value) {
		JS.set(peer, "border-width", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/border-width">https://developer.mozilla.org/en-US/docs/Web/CSS/border-width</a>
	 *
	 * @return the current value
	 */
	public String getBorderWidth() {
		return Wrap.String(JS.get(peer, "border-width"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/clear">https://developer.mozilla.org/en-US/docs/Web/CSS/clear</a>
	 *
	 * @param value the value
	 */
	public void setClear(String value) {
		JS.set(peer, "clear", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/clear">https://developer.mozilla.org/en-US/docs/Web/CSS/clear</a>
	 *
	 * @return the current value
	 */
	public String getClear() {
		return Wrap.String(JS.get(peer, "clear"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/clip">https://developer.mozilla.org/en-US/docs/Web/CSS/clip</a>
	 *
	 * @param value the value
	 */
	public void setClip(String value) {
		JS.set(peer, "clip", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/clip">https://developer.mozilla.org/en-US/docs/Web/CSS/clip</a>
	 *
	 * @return the current value
	 */
	public String getClip() {
		return Wrap.String(JS.get(peer, "clip"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/color">https://developer.mozilla.org/en-US/docs/Web/CSS/color</a>
	 *
	 * @param value the value
	 */
	public void setColor(String value) {
		JS.set(peer, "color", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/color">https://developer.mozilla.org/en-US/docs/Web/CSS/color</a>
	 *
	 * @return the current value
	 */
	public String getColor() {
		return Wrap.String(JS.get(peer, "color"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/cursor">https://developer.mozilla.org/en-US/docs/Web/CSS/cursor</a>
	 *
	 * @param value the value
	 */
	public void setCursor(String value) {
		JS.set(peer, "cursor", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/cursor">https://developer.mozilla.org/en-US/docs/Web/CSS/cursor</a>
	 *
	 * @return the current value
	 */
	public String getCursor() {
		return Wrap.String(JS.get(peer, "cursor"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/display">https://developer.mozilla.org/en-US/docs/Web/CSS/display</a>
	 *
	 * @param value the value
	 */
	public void setDisplay(String value) {
		JS.set(peer, "display", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/display">https://developer.mozilla.org/en-US/docs/Web/CSS/display</a>
	 *
	 * @return the current value
	 */
	public String getDisplay() {
		return Wrap.String(JS.get(peer, "display"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/filter">https://developer.mozilla.org/en-US/docs/Web/CSS/filter</a>
	 *
	 * @param value the value
	 */
	public void setFilter(String value) {
		JS.set(peer, "filter", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/filter">https://developer.mozilla.org/en-US/docs/Web/CSS/filter</a>
	 *
	 * @return the current value
	 */
	public String getFilter() {
		return Wrap.String(JS.get(peer, "filter"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/flex">https://developer.mozilla.org/en-US/docs/Web/CSS/flex</a>
	 *
	 * @param value the value
	 */
	public void setFlex(String value) {
		JS.set(peer, "flex", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/flex">https://developer.mozilla.org/en-US/docs/Web/CSS/flex</a>
	 *
	 * @return the current value
	 */
	public String getFlex() {
		return Wrap.String(JS.get(peer, "flex"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/flex-direction">https://developer.mozilla.org/en-US/docs/Web/CSS/flex-direction</a>
	 *
	 * @param value the value
	 */
	public void setFlexDirection(String value) {
		JS.set(peer, "flex-direction", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/flex-direction">https://developer.mozilla.org/en-US/docs/Web/CSS/flex-direction</a>
	 *
	 * @return the current value
	 */
	public String getFlexDirection() {
		return Wrap.String(JS.get(peer, "flex-direction"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/flex-wrap">https://developer.mozilla.org/en-US/docs/Web/CSS/flex-wrap</a>
	 *
	 * @param value the value
	 */
	public void setFlexWrap(String value) {
		JS.set(peer, "flex-wrap", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/flex-wrap">https://developer.mozilla.org/en-US/docs/Web/CSS/flex-wrap</a>
	 *
	 * @return the current value
	 */
	public String getFlexWrap() {
		return Wrap.String(JS.get(peer, "flex-wrap"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/float">https://developer.mozilla.org/en-US/docs/Web/CSS/float</a>
	 *
	 * @param value the value
	 */
	public void setFloat(String value) {
		JS.set(peer, "float", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/float">https://developer.mozilla.org/en-US/docs/Web/CSS/float</a>
	 *
	 * @return the current value
	 */
	public String getFloat() {
		return Wrap.String(JS.get(peer, "float"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/font">https://developer.mozilla.org/en-US/docs/Web/CSS/font</a>
	 *
	 * @param value the value
	 */
	public void setFont(String value) {
		JS.set(peer, "font", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/font">https://developer.mozilla.org/en-US/docs/Web/CSS/font</a>
	 *
	 * @return the current value
	 */
	public String getFont() {
		return Wrap.String(JS.get(peer, "font"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/font-family">https://developer.mozilla.org/en-US/docs/Web/CSS/font-family</a>
	 *
	 * @param value the value
	 */
	public void setFontFamily(String value) {
		JS.set(peer, "font-family", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/font-family">https://developer.mozilla.org/en-US/docs/Web/CSS/font-family</a>
	 *
	 * @return the current value
	 */
	public String getFontFamily() {
		return Wrap.String(JS.get(peer, "font-family"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/font-size">https://developer.mozilla.org/en-US/docs/Web/CSS/font-size</a>
	 *
	 * @param value the value
	 */
	public void setFontSize(String value) {
		JS.set(peer, "font-size", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/font-size">https://developer.mozilla.org/en-US/docs/Web/CSS/font-size</a>
	 *
	 * @return the current value
	 */
	public String getFontSize() {
		return Wrap.String(JS.get(peer, "font-size"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/font-variant">https://developer.mozilla.org/en-US/docs/Web/CSS/font-variant</a>
	 *
	 * @param value the value
	 */
	public void setFontVariant(String value) {
		JS.set(peer, "font-variant", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/font-variant">https://developer.mozilla.org/en-US/docs/Web/CSS/font-variant</a>
	 *
	 * @return the current value
	 */
	public String getFontVariant() {
		return Wrap.String(JS.get(peer, "font-variant"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/font-weight">https://developer.mozilla.org/en-US/docs/Web/CSS/font-weight</a>
	 *
	 * @param value the value
	 */
	public void setFontWeight(String value) {
		JS.set(peer, "font-weight", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/font-weight">https://developer.mozilla.org/en-US/docs/Web/CSS/font-weight</a>
	 *
	 * @return the current value
	 */
	public String getFontWeight() {
		return Wrap.String(JS.get(peer, "font-weight"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/height">https://developer.mozilla.org/en-US/docs/Web/CSS/height</a>
	 *
	 * @param value the value
	 */
	public void setHeight(String value) {
		JS.set(peer, "height", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/height">https://developer.mozilla.org/en-US/docs/Web/CSS/height</a>
	 *
	 * @return the current value
	 */
	public String getHeight() {
		return Wrap.String(JS.get(peer, "height"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/left">https://developer.mozilla.org/en-US/docs/Web/CSS/left</a>
	 *
	 * @param value the value
	 */
	public void setLeft(String value) {
		JS.set(peer, "left", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/left">https://developer.mozilla.org/en-US/docs/Web/CSS/left</a>
	 *
	 * @return the current value
	 */
	public String getLeft() {
		return Wrap.String(JS.get(peer, "left"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/letter-spacing">https://developer.mozilla.org/en-US/docs/Web/CSS/letter-spacing</a>
	 *
	 * @param value the value
	 */
	public void setLetterSpacing(String value) {
		JS.set(peer, "letter-spacing", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/letter-spacing">https://developer.mozilla.org/en-US/docs/Web/CSS/letter-spacing</a>
	 *
	 * @return the current value
	 */
	public String getLetterSpacing() {
		return Wrap.String(JS.get(peer, "letter-spacing"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/line-height">https://developer.mozilla.org/en-US/docs/Web/CSS/line-height</a>
	 *
	 * @param value the value
	 */
	public void setLineHeight(String value) {
		JS.set(peer, "line-height", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/line-height">https://developer.mozilla.org/en-US/docs/Web/CSS/line-height</a>
	 *
	 * @return the current value
	 */
	public String getLineHeight() {
		return Wrap.String(JS.get(peer, "line-height"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/list-style">https://developer.mozilla.org/en-US/docs/Web/CSS/list-style</a>
	 *
	 * @param value the value
	 */
	public void setListStyle(String value) {
		JS.set(peer, "list-style", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/list-style">https://developer.mozilla.org/en-US/docs/Web/CSS/list-style</a>
	 *
	 * @return the current value
	 */
	public String getListStyle() {
		return Wrap.String(JS.get(peer, "list-style"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/list-style-image">https://developer.mozilla.org/en-US/docs/Web/CSS/list-style-image</a>
	 *
	 * @param value the value
	 */
	public void setListStyleImage(String value) {
		JS.set(peer, "list-style-image", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/list-style-image">https://developer.mozilla.org/en-US/docs/Web/CSS/list-style-image</a>
	 *
	 * @return the current value
	 */
	public String getListStyleImage() {
		return Wrap.String(JS.get(peer, "list-style-image"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/list-style-position">https://developer.mozilla.org/en-US/docs/Web/CSS/list-style-position</a>
	 *
	 * @param value the value
	 */
	public void setListStylePosition(String value) {
		JS.set(peer, "list-style-position", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/list-style-position">https://developer.mozilla.org/en-US/docs/Web/CSS/list-style-position</a>
	 *
	 * @return the current value
	 */
	public String getListStylePosition() {
		return Wrap.String(JS.get(peer, "list-style-position"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/list-style-type">https://developer.mozilla.org/en-US/docs/Web/CSS/list-style-type</a>
	 *
	 * @param value the value
	 */
	public void setListStyleType(String value) {
		JS.set(peer, "list-style-type", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/list-style-type">https://developer.mozilla.org/en-US/docs/Web/CSS/list-style-type</a>
	 *
	 * @return the current value
	 */
	public String getListStyleType() {
		return Wrap.String(JS.get(peer, "list-style-type"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/margin">https://developer.mozilla.org/en-US/docs/Web/CSS/margin</a>
	 *
	 * @param value the value
	 */
	public void setMargin(String value) {
		JS.set(peer, "margin", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/margin">https://developer.mozilla.org/en-US/docs/Web/CSS/margin</a>
	 *
	 * @return the current value
	 */
	public String getMargin() {
		return Wrap.String(JS.get(peer, "margin"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/margin-bottom">https://developer.mozilla.org/en-US/docs/Web/CSS/margin-bottom</a>
	 *
	 * @param value the value
	 */
	public void setMarginBottom(String value) {
		JS.set(peer, "margin-bottom", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/margin-bottom">https://developer.mozilla.org/en-US/docs/Web/CSS/margin-bottom</a>
	 *
	 * @return the current value
	 */
	public String getMarginBottom() {
		return Wrap.String(JS.get(peer, "margin-bottom"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/margin-left">https://developer.mozilla.org/en-US/docs/Web/CSS/margin-left</a>
	 *
	 * @param value the value
	 */
	public void setMarginLeft(String value) {
		JS.set(peer, "margin-left", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/margin-left">https://developer.mozilla.org/en-US/docs/Web/CSS/margin-left</a>
	 *
	 * @return the current value
	 */
	public String getMarginLeft() {
		return Wrap.String(JS.get(peer, "margin-left"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/margin-right">https://developer.mozilla.org/en-US/docs/Web/CSS/margin-right</a>
	 *
	 * @param value the value
	 */
	public void setMarginRight(String value) {
		JS.set(peer, "margin-right", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/margin-right">https://developer.mozilla.org/en-US/docs/Web/CSS/margin-right</a>
	 *
	 * @return the current value
	 */
	public String getMarginRight() {
		return Wrap.String(JS.get(peer, "margin-right"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/margin-top">https://developer.mozilla.org/en-US/docs/Web/CSS/margin-top</a>
	 *
	 * @param value the value
	 */
	public void setMarginTop(String value) {
		JS.set(peer, "margin-top", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/margin-top">https://developer.mozilla.org/en-US/docs/Web/CSS/margin-top</a>
	 *
	 * @return the current value
	 */
	public String getMarginTop() {
		return Wrap.String(JS.get(peer, "margin-top"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/overflow">https://developer.mozilla.org/en-US/docs/Web/CSS/overflow</a>
	 *
	 * @param value the value
	 */
	public void setOverflow(String value) {
		JS.set(peer, "overflow", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/overflow">https://developer.mozilla.org/en-US/docs/Web/CSS/overflow</a>
	 *
	 * @return the current value
	 */
	public String getOverflow() {
		return Wrap.String(JS.get(peer, "overflow"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/padding">https://developer.mozilla.org/en-US/docs/Web/CSS/padding</a>
	 *
	 * @param value the value
	 */
	public void setPadding(String value) {
		JS.set(peer, "padding", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/padding">https://developer.mozilla.org/en-US/docs/Web/CSS/padding</a>
	 *
	 * @return the current value
	 */
	public String getPadding() {
		return Wrap.String(JS.get(peer, "padding"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/padding-bottom">https://developer.mozilla.org/en-US/docs/Web/CSS/padding-bottom</a>
	 *
	 * @param value the value
	 */
	public void setPaddingBottom(String value) {
		JS.set(peer, "padding-bottom", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/padding-bottom">https://developer.mozilla.org/en-US/docs/Web/CSS/padding-bottom</a>
	 *
	 * @return the current value
	 */
	public String getPaddingBottom() {
		return Wrap.String(JS.get(peer, "padding-bottom"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/padding-left">https://developer.mozilla.org/en-US/docs/Web/CSS/padding-left</a>
	 *
	 * @param value the value
	 */
	public void setPaddingLeft(String value) {
		JS.set(peer, "padding-left", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/padding-left">https://developer.mozilla.org/en-US/docs/Web/CSS/padding-left</a>
	 *
	 * @return the current value
	 */
	public String getPaddingLeft() {
		return Wrap.String(JS.get(peer, "padding-left"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/padding-right">https://developer.mozilla.org/en-US/docs/Web/CSS/padding-right</a>
	 *
	 * @param value the value
	 */
	public void setPaddingRight(String value) {
		JS.set(peer, "padding-right", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/padding-right">https://developer.mozilla.org/en-US/docs/Web/CSS/padding-right</a>
	 *
	 * @return the current value
	 */
	public String getPaddingRight() {
		return Wrap.String(JS.get(peer, "padding-right"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/padding-top">https://developer.mozilla.org/en-US/docs/Web/CSS/padding-top</a>
	 *
	 * @param value the value
	 */
	public void setPaddingTop(String value) {
		JS.set(peer, "padding-top", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/padding-top">https://developer.mozilla.org/en-US/docs/Web/CSS/padding-top</a>
	 *
	 * @return the current value
	 */
	public String getPaddingTop() {
		return Wrap.String(JS.get(peer, "padding-top"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/page-break-after">https://developer.mozilla.org/en-US/docs/Web/CSS/page-break-after</a>
	 *
	 * @param value the value
	 */
	public void setPageBreakAfter(String value) {
		JS.set(peer, "page-break-after", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/page-break-after">https://developer.mozilla.org/en-US/docs/Web/CSS/page-break-after</a>
	 *
	 * @return the current value
	 */
	public String getPageBreakAfter() {
		return Wrap.String(JS.get(peer, "page-break-after"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/page-break-before">https://developer.mozilla.org/en-US/docs/Web/CSS/page-break-before</a>
	 *
	 * @param value the value
	 */
	public void setPageBreakBefore(String value) {
		JS.set(peer, "page-break-before", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/page-break-before">https://developer.mozilla.org/en-US/docs/Web/CSS/page-break-before</a>
	 *
	 * @return the current value
	 */
	public String getPageBreakBefore() {
		return Wrap.String(JS.get(peer, "page-break-before"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/position">https://developer.mozilla.org/en-US/docs/Web/CSS/position</a>
	 *
	 * @param value the value
	 */
	public void setPosition(String value) {
		JS.set(peer, "position", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/position">https://developer.mozilla.org/en-US/docs/Web/CSS/position</a>
	 *
	 * @return the current value
	 */
	public String getPosition() {
		return Wrap.String(JS.get(peer, "position"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/stroke-dasharray">https://developer.mozilla.org/en-US/docs/Web/CSS/stroke-dasharray</a>
	 *
	 * @param value the value
	 */
	public void setStrokeDasharray(String value) {
		JS.set(peer, "stroke-dasharray", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/stroke-dasharray">https://developer.mozilla.org/en-US/docs/Web/CSS/stroke-dasharray</a>
	 *
	 * @return the current value
	 */
	public String getStrokeDasharray() {
		return Wrap.String(JS.get(peer, "stroke-dasharray"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/stroke-dashoffset">https://developer.mozilla.org/en-US/docs/Web/CSS/stroke-dashoffset</a>
	 *
	 * @param value the value
	 */
	public void setStrokeDasoffset(String value) {
		JS.set(peer, "stroke-dashoffset", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/stroke-dashoffset">https://developer.mozilla.org/en-US/docs/Web/CSS/stroke-dashoffset</a>
	 *
	 * @return the current value
	 */
	public String getStrokeDashoffset() {
		return Wrap.String(JS.get(peer, "stroke-dashoffset"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/text-align">https://developer.mozilla.org/en-US/docs/Web/CSS/text-align</a>
	 *
	 * @param value the value
	 */
	public void setTextAlign(String value) {
		JS.set(peer, "text-align", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/text-align">https://developer.mozilla.org/en-US/docs/Web/CSS/text-align</a>
	 *
	 * @return the current value
	 */
	public String getTextAlign() {
		return Wrap.String(JS.get(peer, "text-align"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/text-decoration">https://developer.mozilla.org/en-US/docs/Web/CSS/text-decoration</a>
	 *
	 * @param value the value
	 */
	public void setTextDecoration(String value) {
		JS.set(peer, "text-decoration", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/text-decoration">https://developer.mozilla.org/en-US/docs/Web/CSS/text-decoration</a>
	 *
	 * @return the current value
	 */
	public String getTextDecoration() {
		return Wrap.String(JS.get(peer, "text-decoration"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/text-indent">https://developer.mozilla.org/en-US/docs/Web/CSS/text-indent</a>
	 *
	 * @param value the value
	 */
	public void setTextIndent(String value) {
		JS.set(peer, "text-indent", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/text-indent">https://developer.mozilla.org/en-US/docs/Web/CSS/text-indent</a>
	 *
	 * @return the current value
	 */
	public String getTextIndent() {
		return Wrap.String(JS.get(peer, "text-indent"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/text-transform">https://developer.mozilla.org/en-US/docs/Web/CSS/text-transform</a>
	 *
	 * @param value the value
	 */
	public void setTextTransform(String value) {
		JS.set(peer, "text-transform", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/text-transform">https://developer.mozilla.org/en-US/docs/Web/CSS/text-transform</a>
	 *
	 * @return the current value
	 */
	public String getTextTransform() {
		return Wrap.String(JS.get(peer, "text-transform"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/top">https://developer.mozilla.org/en-US/docs/Web/CSS/top</a>
	 *
	 * @param value the value
	 */
	public void setTop(String value) {
		JS.set(peer, "top", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/top">https://developer.mozilla.org/en-US/docs/Web/CSS/top</a>
	 *
	 * @return the current value
	 */
	public String getTop() {
		return Wrap.String(JS.get(peer, "top"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/vertical-align">https://developer.mozilla.org/en-US/docs/Web/CSS/vertical-align</a>
	 *
	 * @param value the value
	 */
	public void setVerticalAlign(String value) {
		JS.set(peer, "vertical-align", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/vertical-align">https://developer.mozilla.org/en-US/docs/Web/CSS/vertical-align</a>
	 *
	 * @return the current value
	 */
	public String getVerticalAlign() {
		return Wrap.String(JS.get(peer, "vertical-align"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/visibility">https://developer.mozilla.org/en-US/docs/Web/CSS/visibility</a>
	 *
	 * @param value the value
	 */
	public void setVisibility(String value) {
		JS.set(peer, "visibility", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/visibility">https://developer.mozilla.org/en-US/docs/Web/CSS/visibility</a>
	 *
	 * @return the current value
	 */
	public String getVisibility() {
		return Wrap.String(JS.get(peer, "visibility"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/width">https://developer.mozilla.org/en-US/docs/Web/CSS/width</a>
	 *
	 * @param value the value
	 */
	public void setWidth(String value) {
		JS.set(peer, "width", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/width">https://developer.mozilla.org/en-US/docs/Web/CSS/width</a>
	 *
	 * @return the current value
	 */
	public String getWidth() {
		return Wrap.String(JS.get(peer, "width"));
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/z-index">https://developer.mozilla.org/en-US/docs/Web/CSS/z-index</a>
	 *
	 * @param value the value
	 */
	public void setZIndex(String value) {
		JS.set(peer, "z-index", value);
	}

	/**
	 * <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/z-index">https://developer.mozilla.org/en-US/docs/Web/CSS/z-index</a>
	 *
	 * @return the current value
	 */
	public String getZIndex() {
		return Wrap.String(JS.get(peer, "z-index"));
	}

}
