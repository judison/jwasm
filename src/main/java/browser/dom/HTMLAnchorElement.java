package browser.dom;

import browser.javascript.JS;

/**
 * <p>The <strong><code>HTMLAnchorElement</code></strong> interface represents hyperlink elements and provides special properties and methods (beyond those of the regular {@link HTMLElement} object interface that they inherit from) for manipulating the layout and presentation of such elements. This interface corresponds to {@link HTMLLinkElement})</p>
 */
@SuppressWarnings({"unused", "JavadocLinkAsPlainText"})
public class HTMLAnchorElement extends HTMLElement {

	protected HTMLAnchorElement(Object peer) {
		super(peer);
	}

	//=========================
	// Properties
	//=========================

	/**
	 * <p>The <strong><code>HTMLAnchorElement.download</code></strong> property is a
	 * string indicating that the linked resource is intended to be
	 * downloaded rather than displayed in the browser. The value, if any, specifies the
	 * default file name for use in labeling the resource in a local file system. If the name
	 * is not a valid file name in the underlying OS, the browser will adjust it.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> This value might not be used for download. This value cannot
	 * be used to determine whether the download will occur.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public String getDownload() {
		return Wrap.String(JS.get(peer, "download"));
	}

	/**
	 * <p>The <strong><code>HTMLAnchorElement.download</code></strong> property is a
	 * string indicating that the linked resource is intended to be
	 * downloaded rather than displayed in the browser. The value, if any, specifies the
	 * default file name for use in labeling the resource in a local file system. If the name
	 * is not a valid file name in the underlying OS, the browser will adjust it.</p>
	 * <blockquote>
	 * <p><strong>Note:</strong> This value might not be used for download. This value cannot
	 * be used to determine whether the download will occur.</p>
	 * </blockquote>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public void setDownload(String value) {
		JS.set(peer, "download", value);
	}

	/**
	 * <p>The
	 * <strong><code>HTMLAnchorElement.hash</code></strong> property returns a
	 * string containing a <code>'#'</code> followed by the fragment
	 * identifier of the URL.
	 * The fragment is not percent-decoded. If the URL does not
	 * have a fragment identifier, this property contains an empty string, <code>&quot;&quot;</code>.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public String getHash() {
		return Wrap.String(JS.get(peer, "hash"));
	}

	/**
	 * <p>The
	 * <strong><code>HTMLAnchorElement.hash</code></strong> property returns a
	 * string containing a <code>'#'</code> followed by the fragment
	 * identifier of the URL.
	 * The fragment is not percent-decoded. If the URL does not
	 * have a fragment identifier, this property contains an empty string, <code>&quot;&quot;</code>.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public void setHash(String value) {
		JS.set(peer, "hash", value);
	}

	/**
	 * <p>The <strong><code>HTMLAnchorElement.host</code></strong> property is a
	 * string containing the host, that is the <em>hostname</em>, and then,
	 * if the <em>port</em> of the URL is nonempty, a <code>':'</code>, and the <em>port</em>
	 * of the URL.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public String getHost() {
		return Wrap.String(JS.get(peer, "host"));
	}

	/**
	 * <p>The <strong><code>HTMLAnchorElement.host</code></strong> property is a
	 * string containing the host, that is the <em>hostname</em>, and then,
	 * if the <em>port</em> of the URL is nonempty, a <code>':'</code>, and the <em>port</em>
	 * of the URL.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public void setHost(String value) {
		JS.set(peer, "host", value);
	}

	/**
	 * <p>The <strong><code>HTMLAnchorElement.hostname</code></strong> property is a
	 * string containing the domain of the URL.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public String getHostname() {
		return Wrap.String(JS.get(peer, "hostname"));
	}

	/**
	 * <p>The <strong><code>HTMLAnchorElement.hostname</code></strong> property is a
	 * string containing the domain of the URL.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public void setHostname(String value) {
		JS.set(peer, "hostname", value);
	}

	/**
	 * <p>The <strong><code>HTMLAnchorElement.href</code></strong> property is a
	 * that returns a string containing the whole URL, and allows
	 * the href to be updated.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public String getHref() {
		return Wrap.String(JS.get(peer, "href"));
	}

	/**
	 * <p>The <strong><code>HTMLAnchorElement.href</code></strong> property is a
	 * that returns a string containing the whole URL, and allows
	 * the href to be updated.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public void setHref(String value) {
		JS.set(peer, "href", value);
	}

	/**
	 * <p>The
	 * <strong><code>HTMLAnchorElement.origin</code></strong> read-only property is a
	 * string containing the Unicode serialization of the origin of the
	 * represented URL.
	 * That is:</p>
	 * <ul>
	 * <li>for URL using the <code>http</code> or <code>https</code>, the scheme followed by
	 * <code>'://'</code>, followed by the domain, followed by <code>':'</code>, followed by
	 * the port (the default port, <code>80</code> and <code>443</code> respectively, if
	 * explicitly specified);</li>
	 * <li>for URL using <code>file:</code> scheme, the value is browser dependent;</li>
	 * <li>for URL using the <code>blob:</code> scheme, the origin of the URL following
	 * <code>blob:</code>. E.g <code>&quot;blob:https://mozilla.org&quot;</code> will have
	 * <code>&quot;https://mozilla.org&quot;.</code></li>
	 * </ul>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public String getOrigin() {
		return Wrap.String(JS.get(peer, "origin"));
	}

	/**
	 * <p>The <strong><code>HTMLAnchorElement.password</code></strong> property is a
	 * string containing the password specified before the domain name.
	 * If it is set without first setting the
	 * {@link HTMLAnchorElement/username}
	 * property, it silently fails.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public String getPassword() {
		return Wrap.String(JS.get(peer, "password"));
	}

	/**
	 * <p>The <strong><code>HTMLAnchorElement.password</code></strong> property is a
	 * string containing the password specified before the domain name.
	 * If it is set without first setting the
	 * {@link HTMLAnchorElement/username}
	 * property, it silently fails.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public void setPassword(String value) {
		JS.set(peer, "password", value);
	}

	/**
	 * <p>The <strong><code>HTMLAnchorElement.pathname</code></strong> property is a
	 * string containing an initial <code>'/'</code> followed by the path of
	 * the URL not including the query string or fragment (or the empty string if there is no
	 * path).</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public String getPathname() {
		return Wrap.String(JS.get(peer, "pathname"));
	}

	/**
	 * <p>The <strong><code>HTMLAnchorElement.pathname</code></strong> property is a
	 * string containing an initial <code>'/'</code> followed by the path of
	 * the URL not including the query string or fragment (or the empty string if there is no
	 * path).</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public void setPathname(String value) {
		JS.set(peer, "pathname", value);
	}

	/**
	 * <p>The <strong><code>HTMLAnchorElement.port</code></strong> property is a
	 * string containing the port number of the URL. If the URL does not
	 * contain an explicit port number, it will be set to <code>''</code>.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public String getPort() {
		return Wrap.String(JS.get(peer, "port"));
	}

	/**
	 * <p>The <strong><code>HTMLAnchorElement.port</code></strong> property is a
	 * string containing the port number of the URL. If the URL does not
	 * contain an explicit port number, it will be set to <code>''</code>.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public void setPort(String value) {
		JS.set(peer, "port", value);
	}

	/**
	 * <p>The
	 * <strong><code>HTMLAnchorElement.protocol</code></strong>
	 * property is a string representing the protocol scheme of the URL,
	 * including the final <code>':'</code>.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public String getProtocol() {
		return Wrap.String(JS.get(peer, "protocol"));
	}

	/**
	 * <p>The
	 * <strong><code>HTMLAnchorElement.protocol</code></strong>
	 * property is a string representing the protocol scheme of the URL,
	 * including the final <code>':'</code>.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public void setProtocol(String value) {
		JS.set(peer, "protocol", value);
	}

	/**
	 * <p>The
	 * <strong><code>HTMLAnchorElement.referrerPolicy</code></strong>
	 * property reflect the HTML  attribute of the
	 * element defining which referrer is sent when fetching the resource.</p>
	 * <h2>Value</h2>
	 * <p>A string; one of the following:</p>
	 * <ul>
	 * <li><code>no-referrer</code>
	 * <ul>
	 * <li>: The  header will be omitted entirely. No referrer
	 * information is sent along with requests.</li>
	 * </ul>
	 * </li>
	 * <li><code>no-referrer-when-downgrade</code>
	 * <ul>
	 * <li>: The URL is sent
	 * as a referrer when the protocol security level stays the same (e.g.HTTP→HTTP,
	 * HTTPS→HTTPS), but isn't sent to a less secure destination (e.g. HTTPS→HTTP).</li>
	 * </ul>
	 * </li>
	 * <li><code>origin</code>
	 * <ul>
	 * <li>: Only send the origin of the document as the referrer in all cases.
	 * The document <code>https://example.com/page.html</code> will send the referrer
	 * <code>https://example.com/</code>.</li>
	 * </ul>
	 * </li>
	 * <li><code>origin-when-cross-origin</code>
	 * <ul>
	 * <li>: Send a full URL when performing a same-origin request, but only send the origin of
	 * the document for other cases.</li>
	 * </ul>
	 * </li>
	 * <li><code>same-origin</code>
	 * <ul>
	 * <li>: A referrer will be sent for same-site origins, but
	 * cross-origin requests will contain no referrer information.</li>
	 * </ul>
	 * </li>
	 * <li><code>strict-origin</code>
	 * <ul>
	 * <li>: Only send the origin of the document as the referrer when the protocol security
	 * level stays the same (e.g. HTTPS→HTTPS), but don't send it to a less secure
	 * destination (e.g. HTTPS→HTTP).</li>
	 * </ul>
	 * </li>
	 * <li><code>strict-origin-when-cross-origin</code> (default)
	 * <ul>
	 * <li>: This is the user agent's default behavior if no policy is specified. Send a full URL when performing a same-origin request, only send the origin when the
	 * protocol security level stays the same (e.g. HTTPS→HTTPS), and send no header to a
	 * less secure destination (e.g. HTTPS→HTTP).</li>
	 * </ul>
	 * </li>
	 * <li><code>unsafe-url</code>
	 * <ul>
	 * <li>: Send a full URL when performing a same-origin or cross-origin request. This policy
	 * will leak origins and paths from TLS-protected resources to insecure origins.
	 * Carefully consider the impact of this setting.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public String getReferrerPolicy() {
		return Wrap.String(JS.get(peer, "referrerPolicy"));
	}

	/**
	 * <p>The
	 * <strong><code>HTMLAnchorElement.referrerPolicy</code></strong>
	 * property reflect the HTML  attribute of the
	 * element defining which referrer is sent when fetching the resource.</p>
	 * <h2>Value</h2>
	 * <p>A string; one of the following:</p>
	 * <ul>
	 * <li><code>no-referrer</code>
	 * <ul>
	 * <li>: The  header will be omitted entirely. No referrer
	 * information is sent along with requests.</li>
	 * </ul>
	 * </li>
	 * <li><code>no-referrer-when-downgrade</code>
	 * <ul>
	 * <li>: The URL is sent
	 * as a referrer when the protocol security level stays the same (e.g.HTTP→HTTP,
	 * HTTPS→HTTPS), but isn't sent to a less secure destination (e.g. HTTPS→HTTP).</li>
	 * </ul>
	 * </li>
	 * <li><code>origin</code>
	 * <ul>
	 * <li>: Only send the origin of the document as the referrer in all cases.
	 * The document <code>https://example.com/page.html</code> will send the referrer
	 * <code>https://example.com/</code>.</li>
	 * </ul>
	 * </li>
	 * <li><code>origin-when-cross-origin</code>
	 * <ul>
	 * <li>: Send a full URL when performing a same-origin request, but only send the origin of
	 * the document for other cases.</li>
	 * </ul>
	 * </li>
	 * <li><code>same-origin</code>
	 * <ul>
	 * <li>: A referrer will be sent for same-site origins, but
	 * cross-origin requests will contain no referrer information.</li>
	 * </ul>
	 * </li>
	 * <li><code>strict-origin</code>
	 * <ul>
	 * <li>: Only send the origin of the document as the referrer when the protocol security
	 * level stays the same (e.g. HTTPS→HTTPS), but don't send it to a less secure
	 * destination (e.g. HTTPS→HTTP).</li>
	 * </ul>
	 * </li>
	 * <li><code>strict-origin-when-cross-origin</code> (default)
	 * <ul>
	 * <li>: This is the user agent's default behavior if no policy is specified. Send a full URL when performing a same-origin request, only send the origin when the
	 * protocol security level stays the same (e.g. HTTPS→HTTPS), and send no header to a
	 * less secure destination (e.g. HTTPS→HTTP).</li>
	 * </ul>
	 * </li>
	 * <li><code>unsafe-url</code>
	 * <ul>
	 * <li>: Send a full URL when performing a same-origin or cross-origin request. This policy
	 * will leak origins and paths from TLS-protected resources to insecure origins.
	 * Carefully consider the impact of this setting.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	public void setReferrerPolicy(String value) {
		JS.set(peer, "referrerPolicy", value);
	}

	/**
	 * <p>The <strong><code>HTMLAnchorElement.rel</code></strong> property reflects the
	 * attribute. It is a string containing a
	 * space-separated list of link types
	 * indicating the relationship between the resource represented by the
	 * element and the current document.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public String getRel() {
		return Wrap.String(JS.get(peer, "rel"));
	}

	/**
	 * <p>The <strong><code>HTMLAnchorElement.rel</code></strong> property reflects the
	 * attribute. It is a string containing a
	 * space-separated list of link types
	 * indicating the relationship between the resource represented by the
	 * element and the current document.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public void setRel(String value) {
		JS.set(peer, "rel", value);
	}

	/**
	 * <p>The <strong><code>HTMLAnchorElement.relList</code></strong> read-only property reflects
	 * the  attribute. It is a live {@link DOMTokenList}
	 * containing the set of link types
	 * indicating the relationship between the resource represented by the
	 * element and the current document.
	 * The property itself is read-only, meaning you can't substitute the
	 * {@link DOMTokenList} with another one, but its contents can still be changed.</p>
	 * <h2>Value</h2>
	 * <p>A live {@link DOMTokenList} of strings.</p>
	 */
	public DOMTokenList getRelList() {
		return Wrap.DOMTokenList(JS.get(peer, "relList"));
	}

	/**
	 * <p>The <strong><code>HTMLAnchorElement.search</code></strong> property is a search
	 * string, also called a <em>query string</em>, that is a string containing
	 * a <code>'?'</code> followed by the parameters of the URL.
	 * Modern browsers provide
	 * {@link URLSearchParams/get#examples}
	 * and
	 * {@link URL/searchParams#examples}
	 * to make it easy to parse out the parameters from the querystring.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	@SuppressWarnings("JavadocReference")
	public String getSearch() {
		return Wrap.String(JS.get(peer, "search"));
	}

	/**
	 * <p>The <strong><code>HTMLAnchorElement.search</code></strong> property is a search
	 * string, also called a <em>query string</em>, that is a string containing
	 * a <code>'?'</code> followed by the parameters of the URL.
	 * Modern browsers provide
	 * {@link URLSearchParams/get#examples}
	 * and
	 * {@link URL/searchParams#examples}
	 * to make it easy to parse out the parameters from the querystring.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	@SuppressWarnings("JavadocReference")
	public void setSearch(String value) {
		JS.set(peer, "search", value);
	}

	/**
	 * <p>The <strong><code>HTMLAnchorElement.username</code></strong> property is a
	 * string containing the username specified before the domain name.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public String getUsername() {
		return Wrap.String(JS.get(peer, "username"));
	}

	/**
	 * <p>The <strong><code>HTMLAnchorElement.username</code></strong> property is a
	 * string containing the username specified before the domain name.</p>
	 * <h2>Value</h2>
	 * <p>A string.</p>
	 */
	public void setUsername(String value) {
		JS.set(peer, "username", value);
	}

	//=========================
	// Methods
	//=========================


}
