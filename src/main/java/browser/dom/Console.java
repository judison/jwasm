package browser.dom;

import browser.javascript.JS;
import browser.javascript.Peered;

public class Console extends Peered {

	Console(Object peer) {
		super(JS.console());
	}

	public void log(Object obj) {
		JS.invoke(peer, "log", obj);
	}

	public void log(String str) {
		JS.invoke(peer, "log", JS.param(str));
	}

}
