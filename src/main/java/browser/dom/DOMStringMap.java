package browser.dom;

import browser.javascript.JS;
import browser.javascript.Peered;

public class DOMStringMap extends Peered {

	public DOMStringMap(Object peer) {
		super(peer);
	}

	public String get(String name) {
		return Wrap.String(JS.get(peer, name));
	}

	public void set(String name, String value) {
		JS.set(peer, name, value);
	}

	public boolean has(String name) {
		return JS.in(peer, name);
	}

	public void remove(String name) {
		JS.delete(peer, name);
	}

}
