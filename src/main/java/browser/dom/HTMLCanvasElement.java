package browser.dom;

import browser.javascript.Callback;
import browser.javascript.JS;
import browser.javascript.JSObject;

/**
 * <p>The <strong><code>HTMLCanvasElement</code></strong> interface provides properties and methods for manipulating the layout and presentation of <code>canvas</code> elements. The <code>HTMLCanvasElement</code> interface also inherits the properties and methods of the {@link HTMLElement} interface.</p>
 */
@SuppressWarnings({"unused", "JavadocReference"})
public class HTMLCanvasElement extends HTMLElement {

	HTMLCanvasElement(Object peer) {
		super(peer);
	}

	//=========================
	// Properties
	//=========================

	/**
	 * <p>The <strong><code>HTMLCanvasElement.height</code></strong> property is a
	 * positive <code>integer</code> reflecting the <code>height</code> HTML
	 * attribute of the <code>canvas</code> element interpreted in CSS pixels. When the
	 * attribute is not specified, or if it is set to an invalid value, like a negative, the
	 * default value of <code>150</code> is used.
	 * This is one of the two properties, the other being
	 * {@link #getWidth()}, that controls the size of the canvas.</p>
	 * <h2>Value</h2>
	 * <p>A number.</p>
	 */
	public double getHeight() {
		return JS.getDouble(peer, "height");
	}

	/**
	 * <p>The <strong><code>HTMLCanvasElement.height</code></strong> property is a
	 * positive <code>integer</code> reflecting the <code>height</code> HTML
	 * attribute of the <code>canvas</code> element interpreted in CSS pixels. When the
	 * attribute is not specified, or if it is set to an invalid value, like a negative, the
	 * default value of <code>150</code> is used.
	 * This is one of the two properties, the other being
	 * {@link #getWidth()}, that controls the size of the canvas.</p>
	 * <h2>Value</h2>
	 * <p>A number.</p>
	 */
	public void setHeight(double value) {
		JS.set(peer, "height", value);
	}

	// mozOpaque DEPRECATED

	/**
	 * <p>The <strong><code>HTMLCanvasElement.width</code></strong> property is a
	 * positive <code>integer</code> reflecting the <code>width</code> HTML
	 * attribute of the <code>canvas</code> element interpreted in CSS pixels. When the
	 * attribute is not specified, or if it is set to an invalid value, like a negative, the
	 * default value of <code>300</code> is used.
	 * This is one of the two properties, the other being
	 * {@link HTMLCanvasElement.height}, that controls the size of the canvas.</p>
	 * <h2>Value</h2>
	 * <p>A number.</p>
	 */
	public double getWidth() {
		return JS.getDouble(peer, "width");
	}

	/**
	 * <p>The <strong><code>HTMLCanvasElement.width</code></strong> property is a
	 * positive <code>integer</code> reflecting the <code>width</code> HTML
	 * attribute of the <code>canvas</code> element interpreted in CSS pixels. When the
	 * attribute is not specified, or if it is set to an invalid value, like a negative, the
	 * default value of <code>300</code> is used.
	 * This is one of the two properties, the other being
	 * {@link HTMLCanvasElement.height}, that controls the size of the canvas.</p>
	 * <h2>Value</h2>
	 * <p>A number.</p>
	 */
	public void setWidth(double value) {
		JS.set(peer, "width", value);
	}

	//=========================
	// Methods
	//=========================

	/**
	 * <p>The {@link HTMLCanvasElement} <strong><code>captureStream()</code></strong> method returns a {@link MediaStream}
	 * which includes a {@link CanvasCaptureMediaStreamTrack} containing a real-time video capture of the canvas's contents.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>frameRate</code> optional_inline
	 * <ul>
	 * <li>: A double-precision floating-point value that indicates the rate of capture of each
	 * frame. If not set, a new frame will be captured each time the canvas changes; if set
	 * to <code>0</code>, frames will not be captured automatically; instead, they will only
	 * be captured when the returned track's
	 * {@link CanvasCaptureMediaStreamTrack.requestFrame} method is
	 * called.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>A reference to a {@link MediaStream} object, which has a single
	 * {@link CanvasCaptureMediaStreamTrack} in it.</p>
	 */
	public JSObject captureStream() {
		return Wrap.JSObject(JS.invoke(peer, "captureStream"));
	}

	/**
	 * <p>The
	 * <strong><code>HTMLCanvasElement.getContext()</code></strong> method returns a drawing
	 * context on the canvas, or <code>null</code> if the context identifier is not
	 * supported, or the canvas has already been set to a different context mode.
	 * Later calls to this method on the same canvas element, with the same
	 * <code>contextType</code> argument, will always return the same drawing context instance
	 * as was returned the first time the method was invoked. It is not possible to get a
	 * different drawing context object on a given canvas element.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>contextType</code>
	 * <ul>
	 * <li>: A string containing the context identifier defining the drawing
	 * context associated to the canvas. Possible values are:
	 * <ul>
	 * <li><code>&quot;2d&quot;</code>, leading to the creation of a
	 * {@link CanvasRenderingContext2D} object representing a two-dimensional
	 * rendering context.</li>
	 * <li><code>&quot;webgl&quot;</code> (or <code>&quot;experimental-webgl&quot;</code>) which will create a
	 * {@link WebGLRenderingContext} object representing a three-dimensional
	 * rendering context. This context is only available on browsers that implement {@link WebGL_API} version 1 (OpenGL ES 2.0).</li>
	 * <li><code>&quot;webgl2&quot;</code> which will create a {@link WebGL2RenderingContext}
	 * object representing a three-dimensional rendering context. This context is only
	 * available on browsers that implement {@link WebGL_API}
	 * version 2 (OpenGL ES 3.0). experimental_inline</li>
	 * <li><code>&quot;bitmaprenderer&quot;</code> which will create an
	 * {@link ImageBitmapRenderingContext} which only provides functionality to
	 * replace the content of the canvas with a given {@link ImageBitmap}.</li>
	 * </ul>
	 * <blockquote>
	 * <p><strong>Note:</strong> The identifier <code>&quot;experimental-webgl&quot;</code> is used
	 * in new implementations of WebGL. These implementations have either not reached
	 * test suite conformance, or the graphics drivers on the platform are not yet
	 * stable. The <a href="https://www.khronos.org/">Khronos Group</a> certifies WebGL
	 * implementations under certain <a href="https://www.khronos.org/registry/webgl/sdk/tests/CONFORMANCE_RULES.txt">conformance &gt; rules</a>.</p>
	 * </blockquote>
	 * </li>
	 * </ul>
	 * </li>
	 * <li><code>contextAttributes</code> optional_inline
	 * <ul>
	 * <li>: You can use several context attributes when creating your rendering context, for
	 * example:
	 * <pre><code class="language-js">const gl = canvas.getContext('webgl', {
	 *   antialias: false,
	 *   depth: false
	 * });
	 * </code></pre>
	 * <p>2d context attributes:</p>
	 * <ul>
	 * <li><code>alpha</code>
	 * <ul>
	 * <li>: A boolean value that indicates if the canvas
	 * contains an alpha channel. If set to <code>false</code>, the browser now knows
	 * that the backdrop is always opaque, which can speed up drawing of transparent
	 * content and images.</li>
	 * </ul>
	 * </li>
	 * <li><code>colorSpace</code> optional_inline
	 * <ul>
	 * <li>: Specifies the color space of the rendering context. Possible values are:
	 * <ul>
	 * <li><code>&quot;srgb&quot;</code> selects the <a href="https://en.wikipedia.org/wiki/SRGB">sRGB color space</a>. This is the default value.</li>
	 * <li><code>&quot;display-p3&quot;</code> selects the <a href="https://en.wikipedia.org/wiki/DCI-P3">display-p3 color space</a>.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * </li>
	 * <li><code>desynchronized</code>
	 * <ul>
	 * <li>: A boolean value that hints the user agent
	 * to reduce the latency by desynchronizing the canvas paint cycle from the event
	 * loop</li>
	 * </ul>
	 * </li>
	 * <li><code>willReadFrequently</code>
	 * <ul>
	 * <li>: A boolean value that indicates whether
	 * or not a lot of read-back operations are planned. This will force the use of a
	 * software (instead of hardware accelerated) 2D canvas and can save memory when
	 * calling {@link CanvasRenderingContext2D.getImageData}
	 * frequently.
	 * WebGL context attributes:</li>
	 * </ul>
	 * </li>
	 * <li><code>alpha</code>
	 * <ul>
	 * <li>: A boolean value that indicates if the canvas
	 * contains an alpha buffer.</li>
	 * </ul>
	 * </li>
	 * <li><code>depth</code>
	 * <ul>
	 * <li>: A boolean value that indicates that the drawing
	 * buffer is requested to have a depth buffer of at least 16 bits.</li>
	 * </ul>
	 * </li>
	 * <li><code>stencil</code>
	 * <ul>
	 * <li>: A boolean value that indicates that the drawing
	 * buffer is requested to have a stencil buffer of at least 8 bits.</li>
	 * </ul>
	 * </li>
	 * <li><code>desynchronized</code>
	 * <ul>
	 * <li>: A boolean value that hints the user agent
	 * to reduce the latency by desynchronizing the canvas paint cycle from the event
	 * loop</li>
	 * </ul>
	 * </li>
	 * <li><code>antialias</code>
	 * <ul>
	 * <li>: A boolean value that indicates whether or not
	 * to perform anti-aliasing if possible.</li>
	 * </ul>
	 * </li>
	 * <li><code>failIfMajorPerformanceCaveat</code>
	 * <ul>
	 * <li>: A boolean value that
	 * indicates if a context will be created if the system performance is low or if no
	 * hardware GPU is available.</li>
	 * </ul>
	 * </li>
	 * <li><code>powerPreference</code>
	 * <ul>
	 * <li>: A hint to the user agent
	 * indicating what configuration of GPU is suitable for the WebGL context. Possible
	 * values are:
	 * <ul>
	 * <li><code>&quot;default&quot;</code>
	 * <ul>
	 * <li>: Let the user agent decide which GPU configuration is
	 * most suitable. This is the default value.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;high-performance&quot;</code>
	 * <ul>
	 * <li>: Prioritizes rendering performance over
	 * power consumption.</li>
	 * </ul>
	 * </li>
	 * <li><code>&quot;low-power&quot;</code>
	 * <ul>
	 * <li>: Prioritizes power saving over rendering
	 * performance.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * </li>
	 * </ul>
	 * </li>
	 * <li><code>premultipliedAlpha</code>
	 * <ul>
	 * <li>: A boolean value that indicates that
	 * the page compositor will assume the drawing buffer contains colors with
	 * pre-multiplied alpha.</li>
	 * </ul>
	 * </li>
	 * <li><code>preserveDrawingBuffer</code>
	 * <ul>
	 * <li>: If the value is true the
	 * buffers will not be cleared and will preserve their values until cleared or
	 * overwritten by the author.</li>
	 * </ul>
	 * </li>
	 * <li><code>xrCompatible</code>
	 * <ul>
	 * <li>: A boolean value that hints to the user agent
	 * to use a compatible graphics adapter for an
	 * {@link WebXR_Device_API}. Setting this
	 * synchronous flag at context creation is discouraged; rather call the asynchronous
	 * {@link WebGLRenderingContext.makeXRCompatible()} method the moment you
	 * intend to start an XR session.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * </li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>A rendering context which is either a</p>
	 * <ul>
	 * <li>{@link CanvasRenderingContext2D} for <code>&quot;2d&quot;</code>,</li>
	 * <li>{@link WebGLRenderingContext} for <code>&quot;webgl&quot;</code> and
	 * <code>&quot;experimental-webgl&quot;</code>,</li>
	 * <li>{@link WebGL2RenderingContext} for <code>&quot;webgl2&quot;</code> or</li>
	 * <li>{@link ImageBitmapRenderingContext} for <code>&quot;bitmaprenderer&quot;</code>.
	 * If the <code>contextType</code> doesn't match a possible drawing context, or differs
	 * from the first <code>contextType</code> requested, <code>null</code> is returned.</li>
	 * </ul>
	 */
	public JSObject getContext(String contextType, JSObject contextAttributes) {
		return Wrap.JSObject(JS.invoke(peer, "getContext", JS.param(contextType), JS.param(contextAttributes)));
	}

	// mozFetchAsStream DEPRECATED

	/**
	 * <p>The <strong><code>HTMLCanvasElement.toBlob()</code></strong> method creates a {@link Blob} object representing the image contained in the canvas.
	 * This file may be cached on the disk or stored in memory at the discretion of the user agent.
	 * The desired file format and image quality may be specified.
	 * If the file format is not specified, or if the given format is not supported, then the data will be exported as <code>image/png</code>.
	 * Browsers are required to support <code>image/png</code>; many will support additional formats including <code>image/jpeg</code> and <code>image/webp</code>.
	 * The created image will have a resolution of 96dpi for file formats that support encoding resolution metadata.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>callback</code>
	 * <ul>
	 * <li>: A callback function with the resulting {@link Blob} object as a single argument.
	 * <code>null</code> may be passed if the image cannot be created for any reason.</li>
	 * </ul>
	 * </li>
	 * <li><code>type</code> optional_inline
	 * <ul>
	 * <li>: A string indicating the image format.
	 * The default type is <code>image/png</code>; that type is also used if the given type isn't supported.</li>
	 * </ul>
	 * </li>
	 * <li><code>quality</code> optional_inline
	 * <ul>
	 * <li>: A <code>Number</code> between <code>0</code> and <code>1</code> indicating the image quality to be used when creating images using file formats that support lossy compression (such as <code>image/jpeg</code> or <code>image/webp</code>).
	 * A user agent will use its default quality value if this option is not specified, or if the number is outside the allowed range.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>None (<code>undefined</code>).</p>
	 */
	public void toBlob(Callback<?> callback, String type, double quality) {
		JS.invoke(peer, "toBlob", JS.param(callback), JS.param(type), quality);
	}

	/**
	 * See: {@link #toBlob(Callback, String, double)}
	 */
	public void toBlob(Callback<?> callback, String type) {
		JS.invoke(peer, "toBlob", JS.param(callback), JS.param(type));
	}

	/**
	 * See: {@link #toBlob(Callback, String, double)}
	 */
	public void toBlob(Callback<?> callback) {
		JS.invoke(peer, "toBlob", JS.param(callback));
	}

	/**
	 * <p>The <strong><code>HTMLCanvasElement.toDataURL()</code></strong> method returns a data URL containing a representation of the image in the format specified by the <code>type</code> parameter.
	 * The desired file format and image quality may be specified.
	 * If the file format is not specified, or if the given format is not supported, then the data will be exported as <code>image/png</code>.
	 * In other words, if the returned value starts with <code>data:image/png</code> for any other requested <code>type</code>, then that format is not supported.
	 * Browsers are required to support <code>image/png</code>; many will support additional formats including <code>image/jpeg</code> and <code>image/webp</code>.
	 * The created image data will have a resolution of 96dpi for file formats that support encoding resolution metadata.</p>
	 * <h3>Parameters</h3>
	 * <ul>
	 * <li><code>type</code> optional_inline
	 * <ul>
	 * <li>: A string indicating the image format.
	 * The default type is <code>image/png</code>; this image format will be also used if the specified type is not supported.</li>
	 * </ul>
	 * </li>
	 * <li><code>encoderOptions</code> optional_inline
	 * <ul>
	 * <li>: A <code>Number</code> between <code>0</code> and <code>1</code> indicating the image quality to be used when creating images using file formats that support lossy compression (such as <code>image/jpeg</code> or <code>image/webp</code>).
	 * A user agent will use its default quality value if this option is not specified, or if the number is outside the allowed range.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * <h3>Return value</h3>
	 * <p>A string containing the requested data URL.
	 * If the height or width of the canvas is <code>0</code> or larger than the canvas#maximum_canvas_size, the string <code>&quot;data:,&quot;</code> is returned.</p>
	 */
	public String toDataURL(String type, double encoderOptions) {
		return Wrap.String(JS.invoke(peer, "toDataURL", JS.param(type), encoderOptions));
	}

	/**
	 * See: {@link #toDataURL(String, double)}
	 */
	public String toDataURL(String type) {
		return Wrap.String(JS.invoke(peer, "toDataURL", JS.param(type)));
	}

	/**
	 * See: {@link #toDataURL(String, double)}
	 */
	public String toDataURL() {
		return Wrap.String(JS.invoke(peer, "toDataURL"));
	}

	/**
	 * <p>The
	 * <strong><code>HTMLCanvasElement.transferControlToOffscreen()</code></strong>
	 * method transfers control to an {@link OffscreenCanvas} object, either on the main
	 * thread or on a worker.</p>
	 * <h3>Parameters</h3>
	 * <p>None.</p>
	 * <h3>Return value</h3>
	 * <p>An {@link OffscreenCanvas} object.</p>
	 */
	public JSObject transferControlToOffscreen() {
		return Wrap.JSObject(JS.invoke(peer, "transferControlToOffscreen"));
	}

}
