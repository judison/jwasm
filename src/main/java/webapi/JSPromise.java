package webapi;

public class JSPromise<T> extends WrappedObject {

	public JSPromise() {
		super(JS.newObject());
	}

	public JSPromise(Object peer) {
		super(peer);
	}

	public static <T> JSPromise<T> wrap(Class<T> cls, Object peer) {
		return peer == null ? null : new JSPromise<T>(peer);
	}
	//TODO
}
