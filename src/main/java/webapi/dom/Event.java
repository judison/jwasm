// Generated file; DO NOT edit
package webapi.dom;

import webapi.JS;
import webapi.JSSequence;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class Event extends WrappedObject {
    protected Event(Object peer) {
        super(peer);
    }

    public static Event wrap(Object peer) {
        return peer == null ? null : new Event(peer);
    }

    public String getType() {
        return JS.string(JS.get(peer, "type"));
    }

    public EventTarget getTarget() {
        return EventTarget.wrap(JS.get(peer, "target"));
    }

    public EventTarget getSrcElement() {
        return EventTarget.wrap(JS.get(peer, "srcElement"));
    }

    public EventTarget getCurrentTarget() {
        return EventTarget.wrap(JS.get(peer, "currentTarget"));
    }

    public int getEventPhase() {
        return JS.getInt(peer, "eventPhase");
    }

    public boolean isCancelBubble() {
        return JS.getBoolean(peer, "cancelBubble");
    }

    public void setCancelBubble(boolean value) {
        JS.set(peer, "cancelBubble", value);
    }

    public boolean isBubbles() {
        return JS.getBoolean(peer, "bubbles");
    }

    public boolean isCancelable() {
        return JS.getBoolean(peer, "cancelable");
    }

    public boolean isReturnValue() {
        return JS.getBoolean(peer, "returnValue");
    }

    public void setReturnValue(boolean value) {
        JS.set(peer, "returnValue", value);
    }

    public boolean isDefaultPrevented() {
        return JS.getBoolean(peer, "defaultPrevented");
    }

    public boolean isComposed() {
        return JS.getBoolean(peer, "composed");
    }

    public boolean isIsTrusted() {
        return JS.getBoolean(peer, "isTrusted");
    }

    public WrappedObject getTimeStamp() {
        return WrappedObject.wrap(JS.get(peer, "timeStamp"));
    }

    public JSSequence<EventTarget> composedPath() {
        return JSSequence.wrap(EventTarget.class, JS.invoke(peer, "composedPath"));
    }

    public void stopPropagation() {
        JS.invoke(peer, "stopPropagation");
    }

    public void stopImmediatePropagation() {
        JS.invoke(peer, "stopImmediatePropagation");
    }

    public void preventDefault() {
        JS.invoke(peer, "preventDefault");
    }

    public void initEvent(String type, boolean bubbles, boolean cancelable) {
        JS.invoke(peer, "initEvent", JS.param(type), bubbles, cancelable);
    }

    public void initEvent(String type, boolean bubbles) {
        JS.invoke(peer, "initEvent", JS.param(type), bubbles);
    }

    public void initEvent(String type) {
        JS.invoke(peer, "initEvent", JS.param(type));
    }
}
