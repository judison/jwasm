// Generated file; DO NOT edit
package webapi.dom;

import webapi.JS;

@SuppressWarnings({"unused"})
public class Range extends AbstractRange {
    protected Range(Object peer) {
        super(peer);
    }

    public static Range wrap(Object peer) {
        return peer == null ? null : new Range(peer);
    }

    public Node getCommonAncestorContainer() {
        return Node.wrap(JS.get(peer, "commonAncestorContainer"));
    }

    public void setStart(Node node, int offset) {
        JS.invoke(peer, "setStart", JS.param(node), offset);
    }

    public void setEnd(Node node, int offset) {
        JS.invoke(peer, "setEnd", JS.param(node), offset);
    }

    public void setStartBefore(Node node) {
        JS.invoke(peer, "setStartBefore", JS.param(node));
    }

    public void setStartAfter(Node node) {
        JS.invoke(peer, "setStartAfter", JS.param(node));
    }

    public void setEndBefore(Node node) {
        JS.invoke(peer, "setEndBefore", JS.param(node));
    }

    public void setEndAfter(Node node) {
        JS.invoke(peer, "setEndAfter", JS.param(node));
    }

    public void collapse(boolean toStart) {
        JS.invoke(peer, "collapse", toStart);
    }

    public void collapse() {
        JS.invoke(peer, "collapse");
    }

    public void selectNode(Node node) {
        JS.invoke(peer, "selectNode", JS.param(node));
    }

    public void selectNodeContents(Node node) {
        JS.invoke(peer, "selectNodeContents", JS.param(node));
    }

    public int compareBoundaryPoints(int how, Range sourceRange) {
        return int.wrap(JS.invoke(peer, "compareBoundaryPoints", JS.param(how), JS.param(sourceRange)));
    }

    public void deleteContents() {
        JS.invoke(peer, "deleteContents");
    }

    public DocumentFragment extractContents() {
        return DocumentFragment.wrap(JS.invoke(peer, "extractContents"));
    }

    public DocumentFragment cloneContents() {
        return DocumentFragment.wrap(JS.invoke(peer, "cloneContents"));
    }

    public void insertNode(Node node) {
        JS.invoke(peer, "insertNode", JS.param(node));
    }

    public void surroundContents(Node newParent) {
        JS.invoke(peer, "surroundContents", JS.param(newParent));
    }

    public Range cloneRange() {
        return Range.wrap(JS.invoke(peer, "cloneRange"));
    }

    public void detach() {
        JS.invoke(peer, "detach");
    }

    public boolean isPointInRange(Node node, int offset) {
        return JS.invoke(peer, "isPointInRange", JS.param(node), offset);
    }

    public int comparePoint(Node node, int offset) {
        return int.wrap(JS.invoke(peer, "comparePoint", JS.param(node), offset));
    }

    public boolean intersectsNode(Node node) {
        return JS.invoke(peer, "intersectsNode", JS.param(node));
    }
}
