// Generated file; DO NOT edit
package webapi.dom;

import webapi.JS;
import webapi.WrappedObject;
import webapi.css.StyleSheetList;
import webapi.html.EventHandler;

@SuppressWarnings({"unused"})
public class ShadowRoot extends DocumentFragment {
    protected ShadowRoot(Object peer) {
        super(peer);
    }

    public static ShadowRoot wrap(Object peer) {
        return peer == null ? null : new ShadowRoot(peer);
    }

    public WrappedObject getMode() {
        return WrappedObject.wrap(JS.get(peer, "mode"));
    }

    public boolean isDelegatesFocus() {
        return JS.getBoolean(peer, "delegatesFocus");
    }

    public WrappedObject getSlotAssignment() {
        return WrappedObject.wrap(JS.get(peer, "slotAssignment"));
    }

    public Element getHost() {
        return Element.wrap(JS.get(peer, "host"));
    }

    public EventHandler getOnslotchange() {
        return EventHandler.wrap(JS.get(peer, "onslotchange"));
    }

    public void setOnslotchange(EventHandler value) {
        JS.set(peer, "onslotchange", value.peer);
    }

    public Element getActiveElement() {
        return Element.wrap(JS.get(peer, "activeElement"));
    }

    public StyleSheetList getStyleSheets() {
        return StyleSheetList.wrap(JS.get(peer, "styleSheets"));
    }

    public WrappedObject getAdoptedStyleSheets() {
        return WrappedObject.wrap(JS.get(peer, "adoptedStyleSheets"));
    }

    public void setAdoptedStyleSheets(WrappedObject value) {
        JS.set(peer, "adoptedStyleSheets", value.peer);
    }
}
