// Generated file; DO NOT edit
package webapi.dom;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class AbstractRange extends WrappedObject {
    protected AbstractRange(Object peer) {
        super(peer);
    }

    public static AbstractRange wrap(Object peer) {
        return peer == null ? null : new AbstractRange(peer);
    }

    public Node getStartContainer() {
        return Node.wrap(JS.get(peer, "startContainer"));
    }

    public int getStartOffset() {
        return JS.getInt(peer, "startOffset");
    }

    public Node getEndContainer() {
        return Node.wrap(JS.get(peer, "endContainer"));
    }

    public int getEndOffset() {
        return JS.getInt(peer, "endOffset");
    }

    public boolean isCollapsed() {
        return JS.getBoolean(peer, "collapsed");
    }
}
