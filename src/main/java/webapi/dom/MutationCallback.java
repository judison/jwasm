// Generated file; DO NOT edit
package webapi.dom;

import webapi.JSCallback;
import webapi.JSSequence;

@SuppressWarnings({"unused"})
public class MutationCallback extends JSCallback {
    public MutationCallback(Object peer) {
        super(peer);
    }

    public MutationCallback(Callback javaCallback) {
        super(javaCallback);
    }

    public static MutationCallback wrap(Object peer) {
        return peer == null ? null : new MutationCallback(peer);
    }

    public interface Callback extends JSCallback.BaseCallback {
        void handle(JSSequence<MutationRecord> mutations, MutationObserver observer);
    }
}
