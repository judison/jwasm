// Generated file; DO NOT edit
package webapi.dom;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class AbortController extends WrappedObject {
    protected AbortController(Object peer) {
        super(peer);
    }

    public static AbortController wrap(Object peer) {
        return peer == null ? null : new AbortController(peer);
    }

    public AbortSignal getSignal() {
        return AbortSignal.wrap(JS.get(peer, "signal"));
    }

    public void abort(WrappedObject reason) {
        JS.invoke(peer, "abort", JS.param(reason));
    }

    public void abort() {
        JS.invoke(peer, "abort");
    }
}
