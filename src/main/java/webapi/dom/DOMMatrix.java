// Generated file; DO NOT edit
// Ignored static operation fromMatrix
// Ignored static operation fromFloat32Array
// Ignored static operation fromFloat64Array
package webapi.dom;

import webapi.JS;
import webapi.JSObject;

@SuppressWarnings({"unused"})
public class DOMMatrix extends DOMMatrixReadOnly {
    protected DOMMatrix(Object peer) {
        super(peer);
    }

    public static DOMMatrix wrap(Object peer) {
        return peer == null ? null : new DOMMatrix(peer);
    }

    public double getA() {
        return JS.getDouble(peer, "a");
    }

    public void setA(double value) {
        JS.set(peer, "a", value);
    }

    public double getB() {
        return JS.getDouble(peer, "b");
    }

    public void setB(double value) {
        JS.set(peer, "b", value);
    }

    public double getC() {
        return JS.getDouble(peer, "c");
    }

    public void setC(double value) {
        JS.set(peer, "c", value);
    }

    public double getD() {
        return JS.getDouble(peer, "d");
    }

    public void setD(double value) {
        JS.set(peer, "d", value);
    }

    public double getE() {
        return JS.getDouble(peer, "e");
    }

    public void setE(double value) {
        JS.set(peer, "e", value);
    }

    public double getF() {
        return JS.getDouble(peer, "f");
    }

    public void setF(double value) {
        JS.set(peer, "f", value);
    }

    public double getM11() {
        return JS.getDouble(peer, "m11");
    }

    public void setM11(double value) {
        JS.set(peer, "m11", value);
    }

    public double getM12() {
        return JS.getDouble(peer, "m12");
    }

    public void setM12(double value) {
        JS.set(peer, "m12", value);
    }

    public double getM13() {
        return JS.getDouble(peer, "m13");
    }

    public void setM13(double value) {
        JS.set(peer, "m13", value);
    }

    public double getM14() {
        return JS.getDouble(peer, "m14");
    }

    public void setM14(double value) {
        JS.set(peer, "m14", value);
    }

    public double getM21() {
        return JS.getDouble(peer, "m21");
    }

    public void setM21(double value) {
        JS.set(peer, "m21", value);
    }

    public double getM22() {
        return JS.getDouble(peer, "m22");
    }

    public void setM22(double value) {
        JS.set(peer, "m22", value);
    }

    public double getM23() {
        return JS.getDouble(peer, "m23");
    }

    public void setM23(double value) {
        JS.set(peer, "m23", value);
    }

    public double getM24() {
        return JS.getDouble(peer, "m24");
    }

    public void setM24(double value) {
        JS.set(peer, "m24", value);
    }

    public double getM31() {
        return JS.getDouble(peer, "m31");
    }

    public void setM31(double value) {
        JS.set(peer, "m31", value);
    }

    public double getM32() {
        return JS.getDouble(peer, "m32");
    }

    public void setM32(double value) {
        JS.set(peer, "m32", value);
    }

    public double getM33() {
        return JS.getDouble(peer, "m33");
    }

    public void setM33(double value) {
        JS.set(peer, "m33", value);
    }

    public double getM34() {
        return JS.getDouble(peer, "m34");
    }

    public void setM34(double value) {
        JS.set(peer, "m34", value);
    }

    public double getM41() {
        return JS.getDouble(peer, "m41");
    }

    public void setM41(double value) {
        JS.set(peer, "m41", value);
    }

    public double getM42() {
        return JS.getDouble(peer, "m42");
    }

    public void setM42(double value) {
        JS.set(peer, "m42", value);
    }

    public double getM43() {
        return JS.getDouble(peer, "m43");
    }

    public void setM43(double value) {
        JS.set(peer, "m43", value);
    }

    public double getM44() {
        return JS.getDouble(peer, "m44");
    }

    public void setM44(double value) {
        JS.set(peer, "m44", value);
    }

    public DOMMatrix multiplySelf(JSObject other) {
        return DOMMatrix.wrap(JS.invoke(peer, "multiplySelf", JS.param(other)));
    }

    public DOMMatrix multiplySelf() {
        return DOMMatrix.wrap(JS.invoke(peer, "multiplySelf"));
    }

    public DOMMatrix preMultiplySelf(JSObject other) {
        return DOMMatrix.wrap(JS.invoke(peer, "preMultiplySelf", JS.param(other)));
    }

    public DOMMatrix preMultiplySelf() {
        return DOMMatrix.wrap(JS.invoke(peer, "preMultiplySelf"));
    }

    public DOMMatrix translateSelf(double tx, double ty, double tz) {
        return DOMMatrix.wrap(JS.invoke(peer, "translateSelf", tx, ty, tz));
    }

    public DOMMatrix translateSelf(double tx, double ty) {
        return DOMMatrix.wrap(JS.invoke(peer, "translateSelf", tx, ty));
    }

    public DOMMatrix translateSelf(double tx) {
        return DOMMatrix.wrap(JS.invoke(peer, "translateSelf", tx));
    }

    public DOMMatrix translateSelf() {
        return DOMMatrix.wrap(JS.invoke(peer, "translateSelf"));
    }

    public DOMMatrix scaleSelf(double scaleX, double scaleY, double scaleZ, double originX,
            double originY, double originZ) {
        return DOMMatrix.wrap(JS.invoke(peer, "scaleSelf", scaleX, scaleY, scaleZ, originX, originY, originZ));
    }

    public DOMMatrix scaleSelf(double scaleX, double scaleY, double scaleZ, double originX,
            double originY) {
        return DOMMatrix.wrap(JS.invoke(peer, "scaleSelf", scaleX, scaleY, scaleZ, originX, originY));
    }

    public DOMMatrix scaleSelf(double scaleX, double scaleY, double scaleZ, double originX) {
        return DOMMatrix.wrap(JS.invoke(peer, "scaleSelf", scaleX, scaleY, scaleZ, originX));
    }

    public DOMMatrix scaleSelf(double scaleX, double scaleY, double scaleZ) {
        return DOMMatrix.wrap(JS.invoke(peer, "scaleSelf", scaleX, scaleY, scaleZ));
    }

    public DOMMatrix scaleSelf(double scaleX, double scaleY) {
        return DOMMatrix.wrap(JS.invoke(peer, "scaleSelf", scaleX, scaleY));
    }

    public DOMMatrix scaleSelf(double scaleX) {
        return DOMMatrix.wrap(JS.invoke(peer, "scaleSelf", scaleX));
    }

    public DOMMatrix scaleSelf() {
        return DOMMatrix.wrap(JS.invoke(peer, "scaleSelf"));
    }

    public DOMMatrix scale3dSelf(double scale, double originX, double originY, double originZ) {
        return DOMMatrix.wrap(JS.invoke(peer, "scale3dSelf", scale, originX, originY, originZ));
    }

    public DOMMatrix scale3dSelf(double scale, double originX, double originY) {
        return DOMMatrix.wrap(JS.invoke(peer, "scale3dSelf", scale, originX, originY));
    }

    public DOMMatrix scale3dSelf(double scale, double originX) {
        return DOMMatrix.wrap(JS.invoke(peer, "scale3dSelf", scale, originX));
    }

    public DOMMatrix scale3dSelf(double scale) {
        return DOMMatrix.wrap(JS.invoke(peer, "scale3dSelf", scale));
    }

    public DOMMatrix scale3dSelf() {
        return DOMMatrix.wrap(JS.invoke(peer, "scale3dSelf"));
    }

    public DOMMatrix rotateSelf(double rotX, double rotY, double rotZ) {
        return DOMMatrix.wrap(JS.invoke(peer, "rotateSelf", rotX, rotY, rotZ));
    }

    public DOMMatrix rotateSelf(double rotX, double rotY) {
        return DOMMatrix.wrap(JS.invoke(peer, "rotateSelf", rotX, rotY));
    }

    public DOMMatrix rotateSelf(double rotX) {
        return DOMMatrix.wrap(JS.invoke(peer, "rotateSelf", rotX));
    }

    public DOMMatrix rotateSelf() {
        return DOMMatrix.wrap(JS.invoke(peer, "rotateSelf"));
    }

    public DOMMatrix rotateFromVectorSelf(double x, double y) {
        return DOMMatrix.wrap(JS.invoke(peer, "rotateFromVectorSelf", x, y));
    }

    public DOMMatrix rotateFromVectorSelf(double x) {
        return DOMMatrix.wrap(JS.invoke(peer, "rotateFromVectorSelf", x));
    }

    public DOMMatrix rotateFromVectorSelf() {
        return DOMMatrix.wrap(JS.invoke(peer, "rotateFromVectorSelf"));
    }

    public DOMMatrix rotateAxisAngleSelf(double x, double y, double z, double angle) {
        return DOMMatrix.wrap(JS.invoke(peer, "rotateAxisAngleSelf", x, y, z, angle));
    }

    public DOMMatrix rotateAxisAngleSelf(double x, double y, double z) {
        return DOMMatrix.wrap(JS.invoke(peer, "rotateAxisAngleSelf", x, y, z));
    }

    public DOMMatrix rotateAxisAngleSelf(double x, double y) {
        return DOMMatrix.wrap(JS.invoke(peer, "rotateAxisAngleSelf", x, y));
    }

    public DOMMatrix rotateAxisAngleSelf(double x) {
        return DOMMatrix.wrap(JS.invoke(peer, "rotateAxisAngleSelf", x));
    }

    public DOMMatrix rotateAxisAngleSelf() {
        return DOMMatrix.wrap(JS.invoke(peer, "rotateAxisAngleSelf"));
    }

    public DOMMatrix skewXSelf(double sx) {
        return DOMMatrix.wrap(JS.invoke(peer, "skewXSelf", sx));
    }

    public DOMMatrix skewXSelf() {
        return DOMMatrix.wrap(JS.invoke(peer, "skewXSelf"));
    }

    public DOMMatrix skewYSelf(double sy) {
        return DOMMatrix.wrap(JS.invoke(peer, "skewYSelf", sy));
    }

    public DOMMatrix skewYSelf() {
        return DOMMatrix.wrap(JS.invoke(peer, "skewYSelf"));
    }

    public DOMMatrix invertSelf() {
        return DOMMatrix.wrap(JS.invoke(peer, "invertSelf"));
    }

    public DOMMatrix setMatrixValue(String transformList) {
        return DOMMatrix.wrap(JS.invoke(peer, "setMatrixValue", JS.param(transformList)));
    }
}
