// Generated file; DO NOT edit
package webapi.dom;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class DOMRectList extends WrappedObject {
    protected DOMRectList(Object peer) {
        super(peer);
    }

    public static DOMRectList wrap(Object peer) {
        return peer == null ? null : new DOMRectList(peer);
    }

    public int getLength() {
        return JS.getInt(peer, "length");
    }
}
