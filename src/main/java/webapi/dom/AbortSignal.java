// Generated file; DO NOT edit
// Ignored static operation abort
// Ignored static operation timeout
package webapi.dom;

import webapi.JS;
import webapi.WrappedObject;
import webapi.html.EventHandler;

@SuppressWarnings({"unused"})
public class AbortSignal extends EventTarget {
    protected AbortSignal(Object peer) {
        super(peer);
    }

    public static AbortSignal wrap(Object peer) {
        return peer == null ? null : new AbortSignal(peer);
    }

    public boolean isAborted() {
        return JS.getBoolean(peer, "aborted");
    }

    public WrappedObject getReason() {
        return WrappedObject.wrap(JS.get(peer, "reason"));
    }

    public EventHandler getOnabort() {
        return EventHandler.wrap(JS.get(peer, "onabort"));
    }

    public void setOnabort(EventHandler value) {
        JS.set(peer, "onabort", value.peer);
    }

    public void throwIfAborted() {
        JS.invoke(peer, "throwIfAborted");
    }
}
