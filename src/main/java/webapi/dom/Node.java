// Generated file; DO NOT edit
package webapi.dom;

import webapi.JS;
import webapi.JSObject;

@SuppressWarnings({"unused"})
public class Node extends EventTarget {
    protected Node(Object peer) {
        super(peer);
    }

    public static Node wrap(Object peer) {
        return peer == null ? null : new Node(peer);
    }

    public int getNodeType() {
        return JS.getInt(peer, "nodeType");
    }

    public String getNodeName() {
        return JS.string(JS.get(peer, "nodeName"));
    }

    public String getBaseURI() {
        return JS.string(JS.get(peer, "baseURI"));
    }

    public boolean isIsConnected() {
        return JS.getBoolean(peer, "isConnected");
    }

    public Document getOwnerDocument() {
        return Document.wrap(JS.get(peer, "ownerDocument"));
    }

    public Node getParentNode() {
        return Node.wrap(JS.get(peer, "parentNode"));
    }

    public Element getParentElement() {
        return Element.wrap(JS.get(peer, "parentElement"));
    }

    public NodeList getChildNodes() {
        return NodeList.wrap(JS.get(peer, "childNodes"));
    }

    public Node getFirstChild() {
        return Node.wrap(JS.get(peer, "firstChild"));
    }

    public Node getLastChild() {
        return Node.wrap(JS.get(peer, "lastChild"));
    }

    public Node getPreviousSibling() {
        return Node.wrap(JS.get(peer, "previousSibling"));
    }

    public Node getNextSibling() {
        return Node.wrap(JS.get(peer, "nextSibling"));
    }

    public String getNodeValue() {
        return JS.string(JS.get(peer, "nodeValue"));
    }

    public void setNodeValue(String value) {
        JS.set(peer, "nodeValue", JS.param(value));
    }

    public String getTextContent() {
        return JS.string(JS.get(peer, "textContent"));
    }

    public void setTextContent(String value) {
        JS.set(peer, "textContent", JS.param(value));
    }

    public Node getRootNode(JSObject options) {
        return Node.wrap(JS.invoke(peer, "getRootNode", JS.param(options)));
    }

    public Node getRootNode() {
        return Node.wrap(JS.invoke(peer, "getRootNode"));
    }

    public boolean hasChildNodes() {
        return JS.invoke(peer, "hasChildNodes");
    }

    public void normalize() {
        JS.invoke(peer, "normalize");
    }

    public Node cloneNode(boolean deep) {
        return Node.wrap(JS.invoke(peer, "cloneNode", deep));
    }

    public Node cloneNode() {
        return Node.wrap(JS.invoke(peer, "cloneNode"));
    }

    public boolean isEqualNode(Node otherNode) {
        return JS.invoke(peer, "isEqualNode", JS.param(otherNode));
    }

    public boolean isSameNode(Node otherNode) {
        return JS.invoke(peer, "isSameNode", JS.param(otherNode));
    }

    public int compareDocumentPosition(Node other) {
        return int.wrap(JS.invoke(peer, "compareDocumentPosition", JS.param(other)));
    }

    public boolean contains(Node other) {
        return JS.invoke(peer, "contains", JS.param(other));
    }

    public String lookupPrefix(String namespace) {
        return JS.string(JS.invoke(peer, "lookupPrefix", JS.param(namespace)));
    }

    public String lookupNamespaceURI(String prefix) {
        return JS.string(JS.invoke(peer, "lookupNamespaceURI", JS.param(prefix)));
    }

    public boolean isDefaultNamespace(String namespace) {
        return JS.invoke(peer, "isDefaultNamespace", JS.param(namespace));
    }

    public Node insertBefore(Node node, Node child) {
        return Node.wrap(JS.invoke(peer, "insertBefore", JS.param(node), JS.param(child)));
    }

    public Node appendChild(Node node) {
        return Node.wrap(JS.invoke(peer, "appendChild", JS.param(node)));
    }

    public Node replaceChild(Node node, Node child) {
        return Node.wrap(JS.invoke(peer, "replaceChild", JS.param(node), JS.param(child)));
    }

    public Node removeChild(Node child) {
        return Node.wrap(JS.invoke(peer, "removeChild", JS.param(child)));
    }
}
