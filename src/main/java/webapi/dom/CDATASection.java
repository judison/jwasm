// Generated file; DO NOT edit
package webapi.dom;

@SuppressWarnings({"unused"})
public class CDATASection extends Text {
    protected CDATASection(Object peer) {
        super(peer);
    }

    public static CDATASection wrap(Object peer) {
        return peer == null ? null : new CDATASection(peer);
    }
}
