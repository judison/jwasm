// Generated file; DO NOT edit
package webapi.dom;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class XPathEvaluator extends WrappedObject {
    protected XPathEvaluator(Object peer) {
        super(peer);
    }

    public static XPathEvaluator wrap(Object peer) {
        return peer == null ? null : new XPathEvaluator(peer);
    }

    public XPathExpression createExpression(String expression, XPathNSResolver resolver) {
        return XPathExpression.wrap(JS.invoke(peer, "createExpression", JS.param(expression), JS.param(resolver)));
    }

    public XPathExpression createExpression(String expression) {
        return XPathExpression.wrap(JS.invoke(peer, "createExpression", JS.param(expression)));
    }

    public XPathNSResolver createNSResolver(Node nodeResolver) {
        return XPathNSResolver.wrap(JS.invoke(peer, "createNSResolver", JS.param(nodeResolver)));
    }

    public XPathResult evaluate(String expression, Node contextNode, XPathNSResolver resolver,
            int type, XPathResult result) {
        return XPathResult.wrap(JS.invoke(peer, "evaluate", JS.param(expression), JS.param(contextNode), JS.param(resolver), JS.param(type), JS.param(result)));
    }

    public XPathResult evaluate(String expression, Node contextNode, XPathNSResolver resolver,
            int type) {
        return XPathResult.wrap(JS.invoke(peer, "evaluate", JS.param(expression), JS.param(contextNode), JS.param(resolver), JS.param(type)));
    }

    public XPathResult evaluate(String expression, Node contextNode, XPathNSResolver resolver) {
        return XPathResult.wrap(JS.invoke(peer, "evaluate", JS.param(expression), JS.param(contextNode), JS.param(resolver)));
    }

    public XPathResult evaluate(String expression, Node contextNode) {
        return XPathResult.wrap(JS.invoke(peer, "evaluate", JS.param(expression), JS.param(contextNode)));
    }
}
