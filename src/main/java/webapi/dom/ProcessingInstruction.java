// Generated file; DO NOT edit
package webapi.dom;

import webapi.JS;
import webapi.css.CSSStyleSheet;

@SuppressWarnings({"unused"})
public class ProcessingInstruction extends CharacterData {
    protected ProcessingInstruction(Object peer) {
        super(peer);
    }

    public static ProcessingInstruction wrap(Object peer) {
        return peer == null ? null : new ProcessingInstruction(peer);
    }

    public String getTarget() {
        return JS.string(JS.get(peer, "target"));
    }

    public CSSStyleSheet getSheet() {
        return CSSStyleSheet.wrap(JS.get(peer, "sheet"));
    }
}
