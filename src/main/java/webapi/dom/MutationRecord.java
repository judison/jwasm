// Generated file; DO NOT edit
package webapi.dom;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class MutationRecord extends WrappedObject {
    protected MutationRecord(Object peer) {
        super(peer);
    }

    public static MutationRecord wrap(Object peer) {
        return peer == null ? null : new MutationRecord(peer);
    }

    public String getType() {
        return JS.string(JS.get(peer, "type"));
    }

    public Node getTarget() {
        return Node.wrap(JS.get(peer, "target"));
    }

    public NodeList getAddedNodes() {
        return NodeList.wrap(JS.get(peer, "addedNodes"));
    }

    public NodeList getRemovedNodes() {
        return NodeList.wrap(JS.get(peer, "removedNodes"));
    }

    public Node getPreviousSibling() {
        return Node.wrap(JS.get(peer, "previousSibling"));
    }

    public Node getNextSibling() {
        return Node.wrap(JS.get(peer, "nextSibling"));
    }

    public String getAttributeName() {
        return JS.string(JS.get(peer, "attributeName"));
    }

    public String getAttributeNamespace() {
        return JS.string(JS.get(peer, "attributeNamespace"));
    }

    public String getOldValue() {
        return JS.string(JS.get(peer, "oldValue"));
    }
}
