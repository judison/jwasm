// Generated file; DO NOT edit
// Ignored static operation fromRect
package webapi.dom;

import webapi.JS;

@SuppressWarnings({"unused"})
public class DOMRect extends DOMRectReadOnly {
    protected DOMRect(Object peer) {
        super(peer);
    }

    public static DOMRect wrap(Object peer) {
        return peer == null ? null : new DOMRect(peer);
    }

    public double getX() {
        return JS.getDouble(peer, "x");
    }

    public void setX(double value) {
        JS.set(peer, "x", value);
    }

    public double getY() {
        return JS.getDouble(peer, "y");
    }

    public void setY(double value) {
        JS.set(peer, "y", value);
    }

    public double getWidth() {
        return JS.getDouble(peer, "width");
    }

    public void setWidth(double value) {
        JS.set(peer, "width", value);
    }

    public double getHeight() {
        return JS.getDouble(peer, "height");
    }

    public void setHeight(double value) {
        JS.set(peer, "height", value);
    }
}
