// Generated file; DO NOT edit
package webapi.dom;

import webapi.JS;

@SuppressWarnings({"unused"})
public class CharacterData extends Node {
    protected CharacterData(Object peer) {
        super(peer);
    }

    public static CharacterData wrap(Object peer) {
        return peer == null ? null : new CharacterData(peer);
    }

    public String getData() {
        return JS.string(JS.get(peer, "data"));
    }

    public void setData(String value) {
        JS.set(peer, "data", JS.param(value));
    }

    public int getLength() {
        return JS.getInt(peer, "length");
    }

    public Element getPreviousElementSibling() {
        return Element.wrap(JS.get(peer, "previousElementSibling"));
    }

    public Element getNextElementSibling() {
        return Element.wrap(JS.get(peer, "nextElementSibling"));
    }

    public String substringData(int offset, int count) {
        return JS.string(JS.invoke(peer, "substringData", offset, count));
    }

    public void appendData(String data) {
        JS.invoke(peer, "appendData", JS.param(data));
    }

    public void insertData(int offset, String data) {
        JS.invoke(peer, "insertData", offset, JS.param(data));
    }

    public void deleteData(int offset, int count) {
        JS.invoke(peer, "deleteData", offset, count);
    }

    public void replaceData(int offset, int count, String data) {
        JS.invoke(peer, "replaceData", offset, count, JS.param(data));
    }

    public void before(Node nodes) {
        JS.invoke(peer, "before", JS.param(nodes));
    }

    public void before(String nodes) {
        JS.invoke(peer, "before", JS.param(nodes));
    }

    public void after(Node nodes) {
        JS.invoke(peer, "after", JS.param(nodes));
    }

    public void after(String nodes) {
        JS.invoke(peer, "after", JS.param(nodes));
    }

    public void replaceWith(Node nodes) {
        JS.invoke(peer, "replaceWith", JS.param(nodes));
    }

    public void replaceWith(String nodes) {
        JS.invoke(peer, "replaceWith", JS.param(nodes));
    }

    public void remove() {
        JS.invoke(peer, "remove");
    }
}
