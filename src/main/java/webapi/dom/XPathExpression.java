// Generated file; DO NOT edit
package webapi.dom;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class XPathExpression extends WrappedObject {
    protected XPathExpression(Object peer) {
        super(peer);
    }

    public static XPathExpression wrap(Object peer) {
        return peer == null ? null : new XPathExpression(peer);
    }

    public XPathResult evaluate(Node contextNode, int type, XPathResult result) {
        return XPathResult.wrap(JS.invoke(peer, "evaluate", JS.param(contextNode), JS.param(type), JS.param(result)));
    }

    public XPathResult evaluate(Node contextNode, int type) {
        return XPathResult.wrap(JS.invoke(peer, "evaluate", JS.param(contextNode), JS.param(type)));
    }

    public XPathResult evaluate(Node contextNode) {
        return XPathResult.wrap(JS.invoke(peer, "evaluate", JS.param(contextNode)));
    }
}
