// Generated file; DO NOT edit
package webapi.dom;

@SuppressWarnings({"unused"})
public class Comment extends CharacterData {
    protected Comment(Object peer) {
        super(peer);
    }

    public static Comment wrap(Object peer) {
        return peer == null ? null : new Comment(peer);
    }
}
