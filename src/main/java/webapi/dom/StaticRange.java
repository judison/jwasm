// Generated file; DO NOT edit
package webapi.dom;

@SuppressWarnings({"unused"})
public class StaticRange extends AbstractRange {
    protected StaticRange(Object peer) {
        super(peer);
    }

    public static StaticRange wrap(Object peer) {
        return peer == null ? null : new StaticRange(peer);
    }
}
