// Generated file; DO NOT edit
package webapi.dom;

import webapi.JS;

@SuppressWarnings({"unused"})
public class Attr extends Node {
    protected Attr(Object peer) {
        super(peer);
    }

    public static Attr wrap(Object peer) {
        return peer == null ? null : new Attr(peer);
    }

    public String getNamespaceURI() {
        return JS.string(JS.get(peer, "namespaceURI"));
    }

    public String getPrefix() {
        return JS.string(JS.get(peer, "prefix"));
    }

    public String getLocalName() {
        return JS.string(JS.get(peer, "localName"));
    }

    public String getName() {
        return JS.string(JS.get(peer, "name"));
    }

    public String getValue() {
        return JS.string(JS.get(peer, "value"));
    }

    public void setValue(String value) {
        JS.set(peer, "value", JS.param(value));
    }

    public Element getOwnerElement() {
        return Element.wrap(JS.get(peer, "ownerElement"));
    }

    public boolean isSpecified() {
        return JS.getBoolean(peer, "specified");
    }
}
