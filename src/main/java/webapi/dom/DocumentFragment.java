// Generated file; DO NOT edit
package webapi.dom;

import webapi.JS;

@SuppressWarnings({"unused"})
public class DocumentFragment extends Node {
    protected DocumentFragment(Object peer) {
        super(peer);
    }

    public static DocumentFragment wrap(Object peer) {
        return peer == null ? null : new DocumentFragment(peer);
    }

    public HTMLCollection getChildren() {
        return HTMLCollection.wrap(JS.get(peer, "children"));
    }

    public Element getFirstElementChild() {
        return Element.wrap(JS.get(peer, "firstElementChild"));
    }

    public Element getLastElementChild() {
        return Element.wrap(JS.get(peer, "lastElementChild"));
    }

    public int getChildElementCount() {
        return JS.getInt(peer, "childElementCount");
    }

    public Element getElementById(String elementId) {
        return Element.wrap(JS.invoke(peer, "getElementById", JS.param(elementId)));
    }

    public void prepend(Node nodes) {
        JS.invoke(peer, "prepend", JS.param(nodes));
    }

    public void prepend(String nodes) {
        JS.invoke(peer, "prepend", JS.param(nodes));
    }

    public void append(Node nodes) {
        JS.invoke(peer, "append", JS.param(nodes));
    }

    public void append(String nodes) {
        JS.invoke(peer, "append", JS.param(nodes));
    }

    public void replaceChildren(Node nodes) {
        JS.invoke(peer, "replaceChildren", JS.param(nodes));
    }

    public void replaceChildren(String nodes) {
        JS.invoke(peer, "replaceChildren", JS.param(nodes));
    }

    public Element querySelector(String selectors) {
        return Element.wrap(JS.invoke(peer, "querySelector", JS.param(selectors)));
    }

    public NodeList querySelectorAll(String selectors) {
        return NodeList.wrap(JS.invoke(peer, "querySelectorAll", JS.param(selectors)));
    }
}
