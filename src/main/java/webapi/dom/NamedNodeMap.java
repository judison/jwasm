// Generated file; DO NOT edit
package webapi.dom;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class NamedNodeMap extends WrappedObject {
    protected NamedNodeMap(Object peer) {
        super(peer);
    }

    public static NamedNodeMap wrap(Object peer) {
        return peer == null ? null : new NamedNodeMap(peer);
    }

    public int getLength() {
        return JS.getInt(peer, "length");
    }

    public Attr getNamedItemNS(String namespace, String localName) {
        return Attr.wrap(JS.invoke(peer, "getNamedItemNS", JS.param(namespace), JS.param(localName)));
    }

    public Attr setNamedItem(Attr attr) {
        return Attr.wrap(JS.invoke(peer, "setNamedItem", JS.param(attr)));
    }

    public Attr setNamedItemNS(Attr attr) {
        return Attr.wrap(JS.invoke(peer, "setNamedItemNS", JS.param(attr)));
    }

    public Attr removeNamedItem(String qualifiedName) {
        return Attr.wrap(JS.invoke(peer, "removeNamedItem", JS.param(qualifiedName)));
    }

    public Attr removeNamedItemNS(String namespace, String localName) {
        return Attr.wrap(JS.invoke(peer, "removeNamedItemNS", JS.param(namespace), JS.param(localName)));
    }
}
