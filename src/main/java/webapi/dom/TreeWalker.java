// Generated file; DO NOT edit
package webapi.dom;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class TreeWalker extends WrappedObject {
    protected TreeWalker(Object peer) {
        super(peer);
    }

    public static TreeWalker wrap(Object peer) {
        return peer == null ? null : new TreeWalker(peer);
    }

    public Node getRoot() {
        return Node.wrap(JS.get(peer, "root"));
    }

    public int getWhatToShow() {
        return JS.getInt(peer, "whatToShow");
    }

    public NodeFilter getFilter() {
        return NodeFilter.wrap(JS.get(peer, "filter"));
    }

    public Node getCurrentNode() {
        return Node.wrap(JS.get(peer, "currentNode"));
    }

    public void setCurrentNode(Node value) {
        JS.set(peer, "currentNode", value.peer);
    }

    public Node parentNode() {
        return Node.wrap(JS.invoke(peer, "parentNode"));
    }

    public Node firstChild() {
        return Node.wrap(JS.invoke(peer, "firstChild"));
    }

    public Node lastChild() {
        return Node.wrap(JS.invoke(peer, "lastChild"));
    }

    public Node previousSibling() {
        return Node.wrap(JS.invoke(peer, "previousSibling"));
    }

    public Node nextSibling() {
        return Node.wrap(JS.invoke(peer, "nextSibling"));
    }

    public Node previousNode() {
        return Node.wrap(JS.invoke(peer, "previousNode"));
    }

    public Node nextNode() {
        return Node.wrap(JS.invoke(peer, "nextNode"));
    }
}
