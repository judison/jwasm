// Generated file; DO NOT edit
package webapi.dom;

import webapi.JS;
import webapi.JSObject;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class EventTarget extends WrappedObject {
    protected EventTarget(Object peer) {
        super(peer);
    }

    public static EventTarget wrap(Object peer) {
        return peer == null ? null : new EventTarget(peer);
    }

    public void addEventListener(String type, EventListener callback, JSObject options) {
        JS.invoke(peer, "addEventListener", JS.param(type), JS.param(callback), JS.param(options));
    }

    public void addEventListener(String type, EventListener callback, boolean options) {
        JS.invoke(peer, "addEventListener", JS.param(type), JS.param(callback), options);
    }

    public void addEventListener(String type, EventListener callback) {
        JS.invoke(peer, "addEventListener", JS.param(type), JS.param(callback));
    }

    public void removeEventListener(String type, EventListener callback, JSObject options) {
        JS.invoke(peer, "removeEventListener", JS.param(type), JS.param(callback), JS.param(options));
    }

    public void removeEventListener(String type, EventListener callback, boolean options) {
        JS.invoke(peer, "removeEventListener", JS.param(type), JS.param(callback), options);
    }

    public void removeEventListener(String type, EventListener callback) {
        JS.invoke(peer, "removeEventListener", JS.param(type), JS.param(callback));
    }

    public boolean dispatchEvent(Event event) {
        return JS.invoke(peer, "dispatchEvent", JS.param(event));
    }
}
