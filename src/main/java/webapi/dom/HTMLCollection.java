// Generated file; DO NOT edit
package webapi.dom;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class HTMLCollection extends WrappedObject {
    protected HTMLCollection(Object peer) {
        super(peer);
    }

    public static HTMLCollection wrap(Object peer) {
        return peer == null ? null : new HTMLCollection(peer);
    }

    public int getLength() {
        return JS.getInt(peer, "length");
    }
}
