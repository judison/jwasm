// Generated file; DO NOT edit
package webapi.dom;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class CustomEvent extends Event {
    protected CustomEvent(Object peer) {
        super(peer);
    }

    public static CustomEvent wrap(Object peer) {
        return peer == null ? null : new CustomEvent(peer);
    }

    public WrappedObject getDetail() {
        return WrappedObject.wrap(JS.get(peer, "detail"));
    }

    public void initCustomEvent(String type, boolean bubbles, boolean cancelable,
            WrappedObject detail) {
        JS.invoke(peer, "initCustomEvent", JS.param(type), bubbles, cancelable, JS.param(detail));
    }

    public void initCustomEvent(String type, boolean bubbles, boolean cancelable) {
        JS.invoke(peer, "initCustomEvent", JS.param(type), bubbles, cancelable);
    }

    public void initCustomEvent(String type, boolean bubbles) {
        JS.invoke(peer, "initCustomEvent", JS.param(type), bubbles);
    }

    public void initCustomEvent(String type) {
        JS.invoke(peer, "initCustomEvent", JS.param(type));
    }
}
