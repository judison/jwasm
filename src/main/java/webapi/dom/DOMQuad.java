// Generated file; DO NOT edit
// Ignored static operation fromRect
// Ignored static operation fromQuad
package webapi.dom;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class DOMQuad extends WrappedObject {
    protected DOMQuad(Object peer) {
        super(peer);
    }

    public static DOMQuad wrap(Object peer) {
        return peer == null ? null : new DOMQuad(peer);
    }

    public DOMPoint getP1() {
        return DOMPoint.wrap(JS.get(peer, "p1"));
    }

    public DOMPoint getP2() {
        return DOMPoint.wrap(JS.get(peer, "p2"));
    }

    public DOMPoint getP3() {
        return DOMPoint.wrap(JS.get(peer, "p3"));
    }

    public DOMPoint getP4() {
        return DOMPoint.wrap(JS.get(peer, "p4"));
    }

    public DOMRect getBounds() {
        return DOMRect.wrap(JS.invoke(peer, "getBounds"));
    }

    public WrappedObject toJSON() {
        return WrappedObject.wrap(JS.invoke(peer, "toJSON"));
    }
}
