// Generated file; DO NOT edit
package webapi.dom;

import webapi.JS;

@SuppressWarnings({"unused"})
public class DocumentType extends Node {
    protected DocumentType(Object peer) {
        super(peer);
    }

    public static DocumentType wrap(Object peer) {
        return peer == null ? null : new DocumentType(peer);
    }

    public String getName() {
        return JS.string(JS.get(peer, "name"));
    }

    public String getPublicId() {
        return JS.string(JS.get(peer, "publicId"));
    }

    public String getSystemId() {
        return JS.string(JS.get(peer, "systemId"));
    }

    public void before(Node nodes) {
        JS.invoke(peer, "before", JS.param(nodes));
    }

    public void before(String nodes) {
        JS.invoke(peer, "before", JS.param(nodes));
    }

    public void after(Node nodes) {
        JS.invoke(peer, "after", JS.param(nodes));
    }

    public void after(String nodes) {
        JS.invoke(peer, "after", JS.param(nodes));
    }

    public void replaceWith(Node nodes) {
        JS.invoke(peer, "replaceWith", JS.param(nodes));
    }

    public void replaceWith(String nodes) {
        JS.invoke(peer, "replaceWith", JS.param(nodes));
    }

    public void remove() {
        JS.invoke(peer, "remove");
    }
}
