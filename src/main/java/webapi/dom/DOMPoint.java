// Generated file; DO NOT edit
// Ignored static operation fromPoint
package webapi.dom;

import webapi.JS;

@SuppressWarnings({"unused"})
public class DOMPoint extends DOMPointReadOnly {
    protected DOMPoint(Object peer) {
        super(peer);
    }

    public static DOMPoint wrap(Object peer) {
        return peer == null ? null : new DOMPoint(peer);
    }

    public double getX() {
        return JS.getDouble(peer, "x");
    }

    public void setX(double value) {
        JS.set(peer, "x", value);
    }

    public double getY() {
        return JS.getDouble(peer, "y");
    }

    public void setY(double value) {
        JS.set(peer, "y", value);
    }

    public double getZ() {
        return JS.getDouble(peer, "z");
    }

    public void setZ(double value) {
        JS.set(peer, "z", value);
    }

    public double getW() {
        return JS.getDouble(peer, "w");
    }

    public void setW(double value) {
        JS.set(peer, "w", value);
    }
}
