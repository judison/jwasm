// Generated file; DO NOT edit
// Ignored static operation fromMatrix
// Ignored static operation fromFloat32Array
// Ignored static operation fromFloat64Array
package webapi.dom;

import webapi.JS;
import webapi.JSObject;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class DOMMatrixReadOnly extends WrappedObject {
    protected DOMMatrixReadOnly(Object peer) {
        super(peer);
    }

    public static DOMMatrixReadOnly wrap(Object peer) {
        return peer == null ? null : new DOMMatrixReadOnly(peer);
    }

    public double getA() {
        return JS.getDouble(peer, "a");
    }

    public double getB() {
        return JS.getDouble(peer, "b");
    }

    public double getC() {
        return JS.getDouble(peer, "c");
    }

    public double getD() {
        return JS.getDouble(peer, "d");
    }

    public double getE() {
        return JS.getDouble(peer, "e");
    }

    public double getF() {
        return JS.getDouble(peer, "f");
    }

    public double getM11() {
        return JS.getDouble(peer, "m11");
    }

    public double getM12() {
        return JS.getDouble(peer, "m12");
    }

    public double getM13() {
        return JS.getDouble(peer, "m13");
    }

    public double getM14() {
        return JS.getDouble(peer, "m14");
    }

    public double getM21() {
        return JS.getDouble(peer, "m21");
    }

    public double getM22() {
        return JS.getDouble(peer, "m22");
    }

    public double getM23() {
        return JS.getDouble(peer, "m23");
    }

    public double getM24() {
        return JS.getDouble(peer, "m24");
    }

    public double getM31() {
        return JS.getDouble(peer, "m31");
    }

    public double getM32() {
        return JS.getDouble(peer, "m32");
    }

    public double getM33() {
        return JS.getDouble(peer, "m33");
    }

    public double getM34() {
        return JS.getDouble(peer, "m34");
    }

    public double getM41() {
        return JS.getDouble(peer, "m41");
    }

    public double getM42() {
        return JS.getDouble(peer, "m42");
    }

    public double getM43() {
        return JS.getDouble(peer, "m43");
    }

    public double getM44() {
        return JS.getDouble(peer, "m44");
    }

    public boolean isIs2D() {
        return JS.getBoolean(peer, "is2D");
    }

    public boolean isIsIdentity() {
        return JS.getBoolean(peer, "isIdentity");
    }

    public DOMMatrix translate(double tx, double ty, double tz) {
        return DOMMatrix.wrap(JS.invoke(peer, "translate", tx, ty, tz));
    }

    public DOMMatrix translate(double tx, double ty) {
        return DOMMatrix.wrap(JS.invoke(peer, "translate", tx, ty));
    }

    public DOMMatrix translate(double tx) {
        return DOMMatrix.wrap(JS.invoke(peer, "translate", tx));
    }

    public DOMMatrix translate() {
        return DOMMatrix.wrap(JS.invoke(peer, "translate"));
    }

    public DOMMatrix scale(double scaleX, double scaleY, double scaleZ, double originX,
            double originY, double originZ) {
        return DOMMatrix.wrap(JS.invoke(peer, "scale", scaleX, scaleY, scaleZ, originX, originY, originZ));
    }

    public DOMMatrix scale(double scaleX, double scaleY, double scaleZ, double originX,
            double originY) {
        return DOMMatrix.wrap(JS.invoke(peer, "scale", scaleX, scaleY, scaleZ, originX, originY));
    }

    public DOMMatrix scale(double scaleX, double scaleY, double scaleZ, double originX) {
        return DOMMatrix.wrap(JS.invoke(peer, "scale", scaleX, scaleY, scaleZ, originX));
    }

    public DOMMatrix scale(double scaleX, double scaleY, double scaleZ) {
        return DOMMatrix.wrap(JS.invoke(peer, "scale", scaleX, scaleY, scaleZ));
    }

    public DOMMatrix scale(double scaleX, double scaleY) {
        return DOMMatrix.wrap(JS.invoke(peer, "scale", scaleX, scaleY));
    }

    public DOMMatrix scale(double scaleX) {
        return DOMMatrix.wrap(JS.invoke(peer, "scale", scaleX));
    }

    public DOMMatrix scale() {
        return DOMMatrix.wrap(JS.invoke(peer, "scale"));
    }

    public DOMMatrix scaleNonUniform(double scaleX, double scaleY) {
        return DOMMatrix.wrap(JS.invoke(peer, "scaleNonUniform", scaleX, scaleY));
    }

    public DOMMatrix scaleNonUniform(double scaleX) {
        return DOMMatrix.wrap(JS.invoke(peer, "scaleNonUniform", scaleX));
    }

    public DOMMatrix scaleNonUniform() {
        return DOMMatrix.wrap(JS.invoke(peer, "scaleNonUniform"));
    }

    public DOMMatrix scale3d(double scale, double originX, double originY, double originZ) {
        return DOMMatrix.wrap(JS.invoke(peer, "scale3d", scale, originX, originY, originZ));
    }

    public DOMMatrix scale3d(double scale, double originX, double originY) {
        return DOMMatrix.wrap(JS.invoke(peer, "scale3d", scale, originX, originY));
    }

    public DOMMatrix scale3d(double scale, double originX) {
        return DOMMatrix.wrap(JS.invoke(peer, "scale3d", scale, originX));
    }

    public DOMMatrix scale3d(double scale) {
        return DOMMatrix.wrap(JS.invoke(peer, "scale3d", scale));
    }

    public DOMMatrix scale3d() {
        return DOMMatrix.wrap(JS.invoke(peer, "scale3d"));
    }

    public DOMMatrix rotate(double rotX, double rotY, double rotZ) {
        return DOMMatrix.wrap(JS.invoke(peer, "rotate", rotX, rotY, rotZ));
    }

    public DOMMatrix rotate(double rotX, double rotY) {
        return DOMMatrix.wrap(JS.invoke(peer, "rotate", rotX, rotY));
    }

    public DOMMatrix rotate(double rotX) {
        return DOMMatrix.wrap(JS.invoke(peer, "rotate", rotX));
    }

    public DOMMatrix rotate() {
        return DOMMatrix.wrap(JS.invoke(peer, "rotate"));
    }

    public DOMMatrix rotateFromVector(double x, double y) {
        return DOMMatrix.wrap(JS.invoke(peer, "rotateFromVector", x, y));
    }

    public DOMMatrix rotateFromVector(double x) {
        return DOMMatrix.wrap(JS.invoke(peer, "rotateFromVector", x));
    }

    public DOMMatrix rotateFromVector() {
        return DOMMatrix.wrap(JS.invoke(peer, "rotateFromVector"));
    }

    public DOMMatrix rotateAxisAngle(double x, double y, double z, double angle) {
        return DOMMatrix.wrap(JS.invoke(peer, "rotateAxisAngle", x, y, z, angle));
    }

    public DOMMatrix rotateAxisAngle(double x, double y, double z) {
        return DOMMatrix.wrap(JS.invoke(peer, "rotateAxisAngle", x, y, z));
    }

    public DOMMatrix rotateAxisAngle(double x, double y) {
        return DOMMatrix.wrap(JS.invoke(peer, "rotateAxisAngle", x, y));
    }

    public DOMMatrix rotateAxisAngle(double x) {
        return DOMMatrix.wrap(JS.invoke(peer, "rotateAxisAngle", x));
    }

    public DOMMatrix rotateAxisAngle() {
        return DOMMatrix.wrap(JS.invoke(peer, "rotateAxisAngle"));
    }

    public DOMMatrix skewX(double sx) {
        return DOMMatrix.wrap(JS.invoke(peer, "skewX", sx));
    }

    public DOMMatrix skewX() {
        return DOMMatrix.wrap(JS.invoke(peer, "skewX"));
    }

    public DOMMatrix skewY(double sy) {
        return DOMMatrix.wrap(JS.invoke(peer, "skewY", sy));
    }

    public DOMMatrix skewY() {
        return DOMMatrix.wrap(JS.invoke(peer, "skewY"));
    }

    public DOMMatrix multiply(JSObject other) {
        return DOMMatrix.wrap(JS.invoke(peer, "multiply", JS.param(other)));
    }

    public DOMMatrix multiply() {
        return DOMMatrix.wrap(JS.invoke(peer, "multiply"));
    }

    public DOMMatrix flipX() {
        return DOMMatrix.wrap(JS.invoke(peer, "flipX"));
    }

    public DOMMatrix flipY() {
        return DOMMatrix.wrap(JS.invoke(peer, "flipY"));
    }

    public DOMMatrix inverse() {
        return DOMMatrix.wrap(JS.invoke(peer, "inverse"));
    }

    public DOMPoint transformPoint(JSObject point) {
        return DOMPoint.wrap(JS.invoke(peer, "transformPoint", JS.param(point)));
    }

    public DOMPoint transformPoint() {
        return DOMPoint.wrap(JS.invoke(peer, "transformPoint"));
    }

    public WrappedObject toFloat32Array() {
        return WrappedObject.wrap(JS.invoke(peer, "toFloat32Array"));
    }

    public WrappedObject toFloat64Array() {
        return WrappedObject.wrap(JS.invoke(peer, "toFloat64Array"));
    }

    public WrappedObject toJSON() {
        return WrappedObject.wrap(JS.invoke(peer, "toJSON"));
    }
}
