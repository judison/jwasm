// Generated file; DO NOT edit
// Ignored static operation fromRect
package webapi.dom;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class DOMRectReadOnly extends WrappedObject {
    protected DOMRectReadOnly(Object peer) {
        super(peer);
    }

    public static DOMRectReadOnly wrap(Object peer) {
        return peer == null ? null : new DOMRectReadOnly(peer);
    }

    public double getX() {
        return JS.getDouble(peer, "x");
    }

    public double getY() {
        return JS.getDouble(peer, "y");
    }

    public double getWidth() {
        return JS.getDouble(peer, "width");
    }

    public double getHeight() {
        return JS.getDouble(peer, "height");
    }

    public double getTop() {
        return JS.getDouble(peer, "top");
    }

    public double getRight() {
        return JS.getDouble(peer, "right");
    }

    public double getBottom() {
        return JS.getDouble(peer, "bottom");
    }

    public double getLeft() {
        return JS.getDouble(peer, "left");
    }

    public WrappedObject toJSON() {
        return WrappedObject.wrap(JS.invoke(peer, "toJSON"));
    }
}
