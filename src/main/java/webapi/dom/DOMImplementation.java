// Generated file; DO NOT edit
package webapi.dom;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class DOMImplementation extends WrappedObject {
    protected DOMImplementation(Object peer) {
        super(peer);
    }

    public static DOMImplementation wrap(Object peer) {
        return peer == null ? null : new DOMImplementation(peer);
    }

    public DocumentType createDocumentType(String qualifiedName, String publicId, String systemId) {
        return DocumentType.wrap(JS.invoke(peer, "createDocumentType", JS.param(qualifiedName), JS.param(publicId), JS.param(systemId)));
    }

    public XMLDocument createDocument(String namespace, String qualifiedName,
            DocumentType doctype) {
        return XMLDocument.wrap(JS.invoke(peer, "createDocument", JS.param(namespace), JS.param(qualifiedName), JS.param(doctype)));
    }

    public XMLDocument createDocument(String namespace, String qualifiedName) {
        return XMLDocument.wrap(JS.invoke(peer, "createDocument", JS.param(namespace), JS.param(qualifiedName)));
    }

    public Document createHTMLDocument(String title) {
        return Document.wrap(JS.invoke(peer, "createHTMLDocument", JS.param(title)));
    }

    public Document createHTMLDocument() {
        return Document.wrap(JS.invoke(peer, "createHTMLDocument"));
    }

    public boolean hasFeature() {
        return JS.invoke(peer, "hasFeature");
    }
}
