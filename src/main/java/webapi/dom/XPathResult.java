// Generated file; DO NOT edit
package webapi.dom;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class XPathResult extends WrappedObject {
    protected XPathResult(Object peer) {
        super(peer);
    }

    public static XPathResult wrap(Object peer) {
        return peer == null ? null : new XPathResult(peer);
    }

    public int getResultType() {
        return JS.getInt(peer, "resultType");
    }

    public double getNumberValue() {
        return JS.getDouble(peer, "numberValue");
    }

    public String getStringValue() {
        return JS.string(JS.get(peer, "stringValue"));
    }

    public boolean isBooleanValue() {
        return JS.getBoolean(peer, "booleanValue");
    }

    public Node getSingleNodeValue() {
        return Node.wrap(JS.get(peer, "singleNodeValue"));
    }

    public boolean isInvalidIteratorState() {
        return JS.getBoolean(peer, "invalidIteratorState");
    }

    public int getSnapshotLength() {
        return JS.getInt(peer, "snapshotLength");
    }

    public Node iterateNext() {
        return Node.wrap(JS.invoke(peer, "iterateNext"));
    }

    public Node snapshotItem(int index) {
        return Node.wrap(JS.invoke(peer, "snapshotItem", index));
    }
}
