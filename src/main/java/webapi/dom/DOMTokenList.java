// Generated file; DO NOT edit
package webapi.dom;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class DOMTokenList extends WrappedObject {
    protected DOMTokenList(Object peer) {
        super(peer);
    }

    public static DOMTokenList wrap(Object peer) {
        return peer == null ? null : new DOMTokenList(peer);
    }

    public int getLength() {
        return JS.getInt(peer, "length");
    }

    public String getValue() {
        return JS.string(JS.get(peer, "value"));
    }

    public void setValue(String value) {
        JS.set(peer, "value", JS.param(value));
    }

    public boolean contains(String token) {
        return JS.invoke(peer, "contains", JS.param(token));
    }

    public void add(String tokens) {
        JS.invoke(peer, "add", JS.param(tokens));
    }

    public void remove(String tokens) {
        JS.invoke(peer, "remove", JS.param(tokens));
    }

    public boolean toggle(String token, boolean force) {
        return JS.invoke(peer, "toggle", JS.param(token), force);
    }

    public boolean toggle(String token) {
        return JS.invoke(peer, "toggle", JS.param(token));
    }

    public boolean replace(String token, String newToken) {
        return JS.invoke(peer, "replace", JS.param(token), JS.param(newToken));
    }

    public boolean supports(String token) {
        return JS.invoke(peer, "supports", JS.param(token));
    }
}
