// Generated file; DO NOT edit
package webapi.dom;

import webapi.JS;
import webapi.JSObject;
import webapi.JSSequence;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class MutationObserver extends WrappedObject {
    protected MutationObserver(Object peer) {
        super(peer);
    }

    public static MutationObserver wrap(Object peer) {
        return peer == null ? null : new MutationObserver(peer);
    }

    public void observe(Node target, JSObject options) {
        JS.invoke(peer, "observe", JS.param(target), JS.param(options));
    }

    public void observe(Node target) {
        JS.invoke(peer, "observe", JS.param(target));
    }

    public void disconnect() {
        JS.invoke(peer, "disconnect");
    }

    public JSSequence<MutationRecord> takeRecords() {
        return JSSequence.wrap(MutationRecord.class, JS.invoke(peer, "takeRecords"));
    }
}
