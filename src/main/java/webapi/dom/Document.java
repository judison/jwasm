// Generated file; DO NOT edit
package webapi.dom;

import webapi.JS;
import webapi.JSObject;
import webapi.WrappedObject;
import webapi.css.StyleSheetList;
import webapi.html.EventHandler;
import webapi.html.HTMLAllCollection;
import webapi.html.HTMLElement;
import webapi.html.HTMLHeadElement;
import webapi.html.Location;
import webapi.html.WindowProxy;

@SuppressWarnings({"unused"})
public class Document extends Node {
    protected Document(Object peer) {
        super(peer);
    }

    public static Document wrap(Object peer) {
        return peer == null ? null : new Document(peer);
    }

    public DOMImplementation getImplementation() {
        return DOMImplementation.wrap(JS.get(peer, "implementation"));
    }

    public String getURL() {
        return JS.string(JS.get(peer, "URL"));
    }

    public String getDocumentURI() {
        return JS.string(JS.get(peer, "documentURI"));
    }

    public String getCompatMode() {
        return JS.string(JS.get(peer, "compatMode"));
    }

    public String getCharacterSet() {
        return JS.string(JS.get(peer, "characterSet"));
    }

    public String getCharset() {
        return JS.string(JS.get(peer, "charset"));
    }

    public String getInputEncoding() {
        return JS.string(JS.get(peer, "inputEncoding"));
    }

    public String getContentType() {
        return JS.string(JS.get(peer, "contentType"));
    }

    public DocumentType getDoctype() {
        return DocumentType.wrap(JS.get(peer, "doctype"));
    }

    public Element getDocumentElement() {
        return Element.wrap(JS.get(peer, "documentElement"));
    }

    public Location getLocation() {
        return Location.wrap(JS.get(peer, "location"));
    }

    public String getDomain() {
        return JS.string(JS.get(peer, "domain"));
    }

    public void setDomain(String value) {
        JS.set(peer, "domain", JS.param(value));
    }

    public String getReferrer() {
        return JS.string(JS.get(peer, "referrer"));
    }

    public String getCookie() {
        return JS.string(JS.get(peer, "cookie"));
    }

    public void setCookie(String value) {
        JS.set(peer, "cookie", JS.param(value));
    }

    public String getLastModified() {
        return JS.string(JS.get(peer, "lastModified"));
    }

    public WrappedObject getReadyState() {
        return WrappedObject.wrap(JS.get(peer, "readyState"));
    }

    public String getTitle() {
        return JS.string(JS.get(peer, "title"));
    }

    public void setTitle(String value) {
        JS.set(peer, "title", JS.param(value));
    }

    public String getDir() {
        return JS.string(JS.get(peer, "dir"));
    }

    public void setDir(String value) {
        JS.set(peer, "dir", JS.param(value));
    }

    public HTMLElement getBody() {
        return HTMLElement.wrap(JS.get(peer, "body"));
    }

    public void setBody(HTMLElement value) {
        JS.set(peer, "body", value.peer);
    }

    public HTMLHeadElement getHead() {
        return HTMLHeadElement.wrap(JS.get(peer, "head"));
    }

    public HTMLCollection getImages() {
        return HTMLCollection.wrap(JS.get(peer, "images"));
    }

    public HTMLCollection getEmbeds() {
        return HTMLCollection.wrap(JS.get(peer, "embeds"));
    }

    public HTMLCollection getPlugins() {
        return HTMLCollection.wrap(JS.get(peer, "plugins"));
    }

    public HTMLCollection getLinks() {
        return HTMLCollection.wrap(JS.get(peer, "links"));
    }

    public HTMLCollection getForms() {
        return HTMLCollection.wrap(JS.get(peer, "forms"));
    }

    public HTMLCollection getScripts() {
        return HTMLCollection.wrap(JS.get(peer, "scripts"));
    }

    public WrappedObject getCurrentScript() {
        return WrappedObject.wrap(JS.get(peer, "currentScript"));
    }

    public WindowProxy getDefaultView() {
        return WindowProxy.wrap(JS.get(peer, "defaultView"));
    }

    public String getDesignMode() {
        return JS.string(JS.get(peer, "designMode"));
    }

    public void setDesignMode(String value) {
        JS.set(peer, "designMode", JS.param(value));
    }

    public boolean isHidden() {
        return JS.getBoolean(peer, "hidden");
    }

    public WrappedObject getVisibilityState() {
        return WrappedObject.wrap(JS.get(peer, "visibilityState"));
    }

    public EventHandler getOnreadystatechange() {
        return EventHandler.wrap(JS.get(peer, "onreadystatechange"));
    }

    public void setOnreadystatechange(EventHandler value) {
        JS.set(peer, "onreadystatechange", value.peer);
    }

    public EventHandler getOnvisibilitychange() {
        return EventHandler.wrap(JS.get(peer, "onvisibilitychange"));
    }

    public void setOnvisibilitychange(EventHandler value) {
        JS.set(peer, "onvisibilitychange", value.peer);
    }

    public String getFgColor() {
        return JS.string(JS.get(peer, "fgColor"));
    }

    public void setFgColor(String value) {
        JS.set(peer, "fgColor", JS.param(value));
    }

    public String getLinkColor() {
        return JS.string(JS.get(peer, "linkColor"));
    }

    public void setLinkColor(String value) {
        JS.set(peer, "linkColor", JS.param(value));
    }

    public String getVlinkColor() {
        return JS.string(JS.get(peer, "vlinkColor"));
    }

    public void setVlinkColor(String value) {
        JS.set(peer, "vlinkColor", JS.param(value));
    }

    public String getAlinkColor() {
        return JS.string(JS.get(peer, "alinkColor"));
    }

    public void setAlinkColor(String value) {
        JS.set(peer, "alinkColor", JS.param(value));
    }

    public String getBgColor() {
        return JS.string(JS.get(peer, "bgColor"));
    }

    public void setBgColor(String value) {
        JS.set(peer, "bgColor", JS.param(value));
    }

    public HTMLCollection getAnchors() {
        return HTMLCollection.wrap(JS.get(peer, "anchors"));
    }

    public HTMLCollection getApplets() {
        return HTMLCollection.wrap(JS.get(peer, "applets"));
    }

    public HTMLAllCollection getAll() {
        return HTMLAllCollection.wrap(JS.get(peer, "all"));
    }

    public Element getActiveElement() {
        return Element.wrap(JS.get(peer, "activeElement"));
    }

    public StyleSheetList getStyleSheets() {
        return StyleSheetList.wrap(JS.get(peer, "styleSheets"));
    }

    public WrappedObject getAdoptedStyleSheets() {
        return WrappedObject.wrap(JS.get(peer, "adoptedStyleSheets"));
    }

    public void setAdoptedStyleSheets(WrappedObject value) {
        JS.set(peer, "adoptedStyleSheets", value.peer);
    }

    public HTMLCollection getChildren() {
        return HTMLCollection.wrap(JS.get(peer, "children"));
    }

    public Element getFirstElementChild() {
        return Element.wrap(JS.get(peer, "firstElementChild"));
    }

    public Element getLastElementChild() {
        return Element.wrap(JS.get(peer, "lastElementChild"));
    }

    public int getChildElementCount() {
        return JS.getInt(peer, "childElementCount");
    }

    public EventHandler getOnabort() {
        return EventHandler.wrap(JS.get(peer, "onabort"));
    }

    public void setOnabort(EventHandler value) {
        JS.set(peer, "onabort", value.peer);
    }

    public EventHandler getOnauxclick() {
        return EventHandler.wrap(JS.get(peer, "onauxclick"));
    }

    public void setOnauxclick(EventHandler value) {
        JS.set(peer, "onauxclick", value.peer);
    }

    public EventHandler getOnbeforeinput() {
        return EventHandler.wrap(JS.get(peer, "onbeforeinput"));
    }

    public void setOnbeforeinput(EventHandler value) {
        JS.set(peer, "onbeforeinput", value.peer);
    }

    public EventHandler getOnbeforematch() {
        return EventHandler.wrap(JS.get(peer, "onbeforematch"));
    }

    public void setOnbeforematch(EventHandler value) {
        JS.set(peer, "onbeforematch", value.peer);
    }

    public EventHandler getOnblur() {
        return EventHandler.wrap(JS.get(peer, "onblur"));
    }

    public void setOnblur(EventHandler value) {
        JS.set(peer, "onblur", value.peer);
    }

    public EventHandler getOncancel() {
        return EventHandler.wrap(JS.get(peer, "oncancel"));
    }

    public void setOncancel(EventHandler value) {
        JS.set(peer, "oncancel", value.peer);
    }

    public EventHandler getOncanplay() {
        return EventHandler.wrap(JS.get(peer, "oncanplay"));
    }

    public void setOncanplay(EventHandler value) {
        JS.set(peer, "oncanplay", value.peer);
    }

    public EventHandler getOncanplaythrough() {
        return EventHandler.wrap(JS.get(peer, "oncanplaythrough"));
    }

    public void setOncanplaythrough(EventHandler value) {
        JS.set(peer, "oncanplaythrough", value.peer);
    }

    public EventHandler getOnchange() {
        return EventHandler.wrap(JS.get(peer, "onchange"));
    }

    public void setOnchange(EventHandler value) {
        JS.set(peer, "onchange", value.peer);
    }

    public EventHandler getOnclick() {
        return EventHandler.wrap(JS.get(peer, "onclick"));
    }

    public void setOnclick(EventHandler value) {
        JS.set(peer, "onclick", value.peer);
    }

    public EventHandler getOnclose() {
        return EventHandler.wrap(JS.get(peer, "onclose"));
    }

    public void setOnclose(EventHandler value) {
        JS.set(peer, "onclose", value.peer);
    }

    public EventHandler getOncontextlost() {
        return EventHandler.wrap(JS.get(peer, "oncontextlost"));
    }

    public void setOncontextlost(EventHandler value) {
        JS.set(peer, "oncontextlost", value.peer);
    }

    public EventHandler getOncontextmenu() {
        return EventHandler.wrap(JS.get(peer, "oncontextmenu"));
    }

    public void setOncontextmenu(EventHandler value) {
        JS.set(peer, "oncontextmenu", value.peer);
    }

    public EventHandler getOncontextrestored() {
        return EventHandler.wrap(JS.get(peer, "oncontextrestored"));
    }

    public void setOncontextrestored(EventHandler value) {
        JS.set(peer, "oncontextrestored", value.peer);
    }

    public EventHandler getOncuechange() {
        return EventHandler.wrap(JS.get(peer, "oncuechange"));
    }

    public void setOncuechange(EventHandler value) {
        JS.set(peer, "oncuechange", value.peer);
    }

    public EventHandler getOndblclick() {
        return EventHandler.wrap(JS.get(peer, "ondblclick"));
    }

    public void setOndblclick(EventHandler value) {
        JS.set(peer, "ondblclick", value.peer);
    }

    public EventHandler getOndrag() {
        return EventHandler.wrap(JS.get(peer, "ondrag"));
    }

    public void setOndrag(EventHandler value) {
        JS.set(peer, "ondrag", value.peer);
    }

    public EventHandler getOndragend() {
        return EventHandler.wrap(JS.get(peer, "ondragend"));
    }

    public void setOndragend(EventHandler value) {
        JS.set(peer, "ondragend", value.peer);
    }

    public EventHandler getOndragenter() {
        return EventHandler.wrap(JS.get(peer, "ondragenter"));
    }

    public void setOndragenter(EventHandler value) {
        JS.set(peer, "ondragenter", value.peer);
    }

    public EventHandler getOndragleave() {
        return EventHandler.wrap(JS.get(peer, "ondragleave"));
    }

    public void setOndragleave(EventHandler value) {
        JS.set(peer, "ondragleave", value.peer);
    }

    public EventHandler getOndragover() {
        return EventHandler.wrap(JS.get(peer, "ondragover"));
    }

    public void setOndragover(EventHandler value) {
        JS.set(peer, "ondragover", value.peer);
    }

    public EventHandler getOndragstart() {
        return EventHandler.wrap(JS.get(peer, "ondragstart"));
    }

    public void setOndragstart(EventHandler value) {
        JS.set(peer, "ondragstart", value.peer);
    }

    public EventHandler getOndrop() {
        return EventHandler.wrap(JS.get(peer, "ondrop"));
    }

    public void setOndrop(EventHandler value) {
        JS.set(peer, "ondrop", value.peer);
    }

    public EventHandler getOndurationchange() {
        return EventHandler.wrap(JS.get(peer, "ondurationchange"));
    }

    public void setOndurationchange(EventHandler value) {
        JS.set(peer, "ondurationchange", value.peer);
    }

    public EventHandler getOnemptied() {
        return EventHandler.wrap(JS.get(peer, "onemptied"));
    }

    public void setOnemptied(EventHandler value) {
        JS.set(peer, "onemptied", value.peer);
    }

    public EventHandler getOnended() {
        return EventHandler.wrap(JS.get(peer, "onended"));
    }

    public void setOnended(EventHandler value) {
        JS.set(peer, "onended", value.peer);
    }

    public WrappedObject getOnerror() {
        return WrappedObject.wrap(JS.get(peer, "onerror"));
    }

    public void setOnerror(WrappedObject value) {
        JS.set(peer, "onerror", value.peer);
    }

    public EventHandler getOnfocus() {
        return EventHandler.wrap(JS.get(peer, "onfocus"));
    }

    public void setOnfocus(EventHandler value) {
        JS.set(peer, "onfocus", value.peer);
    }

    public EventHandler getOnformdata() {
        return EventHandler.wrap(JS.get(peer, "onformdata"));
    }

    public void setOnformdata(EventHandler value) {
        JS.set(peer, "onformdata", value.peer);
    }

    public EventHandler getOninput() {
        return EventHandler.wrap(JS.get(peer, "oninput"));
    }

    public void setOninput(EventHandler value) {
        JS.set(peer, "oninput", value.peer);
    }

    public EventHandler getOninvalid() {
        return EventHandler.wrap(JS.get(peer, "oninvalid"));
    }

    public void setOninvalid(EventHandler value) {
        JS.set(peer, "oninvalid", value.peer);
    }

    public EventHandler getOnkeydown() {
        return EventHandler.wrap(JS.get(peer, "onkeydown"));
    }

    public void setOnkeydown(EventHandler value) {
        JS.set(peer, "onkeydown", value.peer);
    }

    public EventHandler getOnkeypress() {
        return EventHandler.wrap(JS.get(peer, "onkeypress"));
    }

    public void setOnkeypress(EventHandler value) {
        JS.set(peer, "onkeypress", value.peer);
    }

    public EventHandler getOnkeyup() {
        return EventHandler.wrap(JS.get(peer, "onkeyup"));
    }

    public void setOnkeyup(EventHandler value) {
        JS.set(peer, "onkeyup", value.peer);
    }

    public EventHandler getOnload() {
        return EventHandler.wrap(JS.get(peer, "onload"));
    }

    public void setOnload(EventHandler value) {
        JS.set(peer, "onload", value.peer);
    }

    public EventHandler getOnloadeddata() {
        return EventHandler.wrap(JS.get(peer, "onloadeddata"));
    }

    public void setOnloadeddata(EventHandler value) {
        JS.set(peer, "onloadeddata", value.peer);
    }

    public EventHandler getOnloadedmetadata() {
        return EventHandler.wrap(JS.get(peer, "onloadedmetadata"));
    }

    public void setOnloadedmetadata(EventHandler value) {
        JS.set(peer, "onloadedmetadata", value.peer);
    }

    public EventHandler getOnloadstart() {
        return EventHandler.wrap(JS.get(peer, "onloadstart"));
    }

    public void setOnloadstart(EventHandler value) {
        JS.set(peer, "onloadstart", value.peer);
    }

    public EventHandler getOnmousedown() {
        return EventHandler.wrap(JS.get(peer, "onmousedown"));
    }

    public void setOnmousedown(EventHandler value) {
        JS.set(peer, "onmousedown", value.peer);
    }

    public EventHandler getOnmouseenter() {
        return EventHandler.wrap(JS.get(peer, "onmouseenter"));
    }

    public void setOnmouseenter(EventHandler value) {
        JS.set(peer, "onmouseenter", value.peer);
    }

    public EventHandler getOnmouseleave() {
        return EventHandler.wrap(JS.get(peer, "onmouseleave"));
    }

    public void setOnmouseleave(EventHandler value) {
        JS.set(peer, "onmouseleave", value.peer);
    }

    public EventHandler getOnmousemove() {
        return EventHandler.wrap(JS.get(peer, "onmousemove"));
    }

    public void setOnmousemove(EventHandler value) {
        JS.set(peer, "onmousemove", value.peer);
    }

    public EventHandler getOnmouseout() {
        return EventHandler.wrap(JS.get(peer, "onmouseout"));
    }

    public void setOnmouseout(EventHandler value) {
        JS.set(peer, "onmouseout", value.peer);
    }

    public EventHandler getOnmouseover() {
        return EventHandler.wrap(JS.get(peer, "onmouseover"));
    }

    public void setOnmouseover(EventHandler value) {
        JS.set(peer, "onmouseover", value.peer);
    }

    public EventHandler getOnmouseup() {
        return EventHandler.wrap(JS.get(peer, "onmouseup"));
    }

    public void setOnmouseup(EventHandler value) {
        JS.set(peer, "onmouseup", value.peer);
    }

    public EventHandler getOnpause() {
        return EventHandler.wrap(JS.get(peer, "onpause"));
    }

    public void setOnpause(EventHandler value) {
        JS.set(peer, "onpause", value.peer);
    }

    public EventHandler getOnplay() {
        return EventHandler.wrap(JS.get(peer, "onplay"));
    }

    public void setOnplay(EventHandler value) {
        JS.set(peer, "onplay", value.peer);
    }

    public EventHandler getOnplaying() {
        return EventHandler.wrap(JS.get(peer, "onplaying"));
    }

    public void setOnplaying(EventHandler value) {
        JS.set(peer, "onplaying", value.peer);
    }

    public EventHandler getOnprogress() {
        return EventHandler.wrap(JS.get(peer, "onprogress"));
    }

    public void setOnprogress(EventHandler value) {
        JS.set(peer, "onprogress", value.peer);
    }

    public EventHandler getOnratechange() {
        return EventHandler.wrap(JS.get(peer, "onratechange"));
    }

    public void setOnratechange(EventHandler value) {
        JS.set(peer, "onratechange", value.peer);
    }

    public EventHandler getOnreset() {
        return EventHandler.wrap(JS.get(peer, "onreset"));
    }

    public void setOnreset(EventHandler value) {
        JS.set(peer, "onreset", value.peer);
    }

    public EventHandler getOnresize() {
        return EventHandler.wrap(JS.get(peer, "onresize"));
    }

    public void setOnresize(EventHandler value) {
        JS.set(peer, "onresize", value.peer);
    }

    public EventHandler getOnscroll() {
        return EventHandler.wrap(JS.get(peer, "onscroll"));
    }

    public void setOnscroll(EventHandler value) {
        JS.set(peer, "onscroll", value.peer);
    }

    public EventHandler getOnsecuritypolicyviolation() {
        return EventHandler.wrap(JS.get(peer, "onsecuritypolicyviolation"));
    }

    public void setOnsecuritypolicyviolation(EventHandler value) {
        JS.set(peer, "onsecuritypolicyviolation", value.peer);
    }

    public EventHandler getOnseeked() {
        return EventHandler.wrap(JS.get(peer, "onseeked"));
    }

    public void setOnseeked(EventHandler value) {
        JS.set(peer, "onseeked", value.peer);
    }

    public EventHandler getOnseeking() {
        return EventHandler.wrap(JS.get(peer, "onseeking"));
    }

    public void setOnseeking(EventHandler value) {
        JS.set(peer, "onseeking", value.peer);
    }

    public EventHandler getOnselect() {
        return EventHandler.wrap(JS.get(peer, "onselect"));
    }

    public void setOnselect(EventHandler value) {
        JS.set(peer, "onselect", value.peer);
    }

    public EventHandler getOnslotchange() {
        return EventHandler.wrap(JS.get(peer, "onslotchange"));
    }

    public void setOnslotchange(EventHandler value) {
        JS.set(peer, "onslotchange", value.peer);
    }

    public EventHandler getOnstalled() {
        return EventHandler.wrap(JS.get(peer, "onstalled"));
    }

    public void setOnstalled(EventHandler value) {
        JS.set(peer, "onstalled", value.peer);
    }

    public EventHandler getOnsubmit() {
        return EventHandler.wrap(JS.get(peer, "onsubmit"));
    }

    public void setOnsubmit(EventHandler value) {
        JS.set(peer, "onsubmit", value.peer);
    }

    public EventHandler getOnsuspend() {
        return EventHandler.wrap(JS.get(peer, "onsuspend"));
    }

    public void setOnsuspend(EventHandler value) {
        JS.set(peer, "onsuspend", value.peer);
    }

    public EventHandler getOntimeupdate() {
        return EventHandler.wrap(JS.get(peer, "ontimeupdate"));
    }

    public void setOntimeupdate(EventHandler value) {
        JS.set(peer, "ontimeupdate", value.peer);
    }

    public EventHandler getOntoggle() {
        return EventHandler.wrap(JS.get(peer, "ontoggle"));
    }

    public void setOntoggle(EventHandler value) {
        JS.set(peer, "ontoggle", value.peer);
    }

    public EventHandler getOnvolumechange() {
        return EventHandler.wrap(JS.get(peer, "onvolumechange"));
    }

    public void setOnvolumechange(EventHandler value) {
        JS.set(peer, "onvolumechange", value.peer);
    }

    public EventHandler getOnwaiting() {
        return EventHandler.wrap(JS.get(peer, "onwaiting"));
    }

    public void setOnwaiting(EventHandler value) {
        JS.set(peer, "onwaiting", value.peer);
    }

    public EventHandler getOnwebkitanimationend() {
        return EventHandler.wrap(JS.get(peer, "onwebkitanimationend"));
    }

    public void setOnwebkitanimationend(EventHandler value) {
        JS.set(peer, "onwebkitanimationend", value.peer);
    }

    public EventHandler getOnwebkitanimationiteration() {
        return EventHandler.wrap(JS.get(peer, "onwebkitanimationiteration"));
    }

    public void setOnwebkitanimationiteration(EventHandler value) {
        JS.set(peer, "onwebkitanimationiteration", value.peer);
    }

    public EventHandler getOnwebkitanimationstart() {
        return EventHandler.wrap(JS.get(peer, "onwebkitanimationstart"));
    }

    public void setOnwebkitanimationstart(EventHandler value) {
        JS.set(peer, "onwebkitanimationstart", value.peer);
    }

    public EventHandler getOnwebkittransitionend() {
        return EventHandler.wrap(JS.get(peer, "onwebkittransitionend"));
    }

    public void setOnwebkittransitionend(EventHandler value) {
        JS.set(peer, "onwebkittransitionend", value.peer);
    }

    public EventHandler getOnwheel() {
        return EventHandler.wrap(JS.get(peer, "onwheel"));
    }

    public void setOnwheel(EventHandler value) {
        JS.set(peer, "onwheel", value.peer);
    }

    public EventHandler getOncopy() {
        return EventHandler.wrap(JS.get(peer, "oncopy"));
    }

    public void setOncopy(EventHandler value) {
        JS.set(peer, "oncopy", value.peer);
    }

    public EventHandler getOncut() {
        return EventHandler.wrap(JS.get(peer, "oncut"));
    }

    public void setOncut(EventHandler value) {
        JS.set(peer, "oncut", value.peer);
    }

    public EventHandler getOnpaste() {
        return EventHandler.wrap(JS.get(peer, "onpaste"));
    }

    public void setOnpaste(EventHandler value) {
        JS.set(peer, "onpaste", value.peer);
    }

    public HTMLCollection getElementsByTagName(String qualifiedName) {
        return HTMLCollection.wrap(JS.invoke(peer, "getElementsByTagName", JS.param(qualifiedName)));
    }

    public HTMLCollection getElementsByTagNameNS(String namespace, String localName) {
        return HTMLCollection.wrap(JS.invoke(peer, "getElementsByTagNameNS", JS.param(namespace), JS.param(localName)));
    }

    public HTMLCollection getElementsByClassName(String classNames) {
        return HTMLCollection.wrap(JS.invoke(peer, "getElementsByClassName", JS.param(classNames)));
    }

    public Element createElement(String localName, String options) {
        return Element.wrap(JS.invoke(peer, "createElement", JS.param(localName), JS.param(options)));
    }

    public Element createElement(String localName, JSObject options) {
        return Element.wrap(JS.invoke(peer, "createElement", JS.param(localName), JS.param(options)));
    }

    public Element createElement(String localName) {
        return Element.wrap(JS.invoke(peer, "createElement", JS.param(localName)));
    }

    public Element createElementNS(String namespace, String qualifiedName, String options) {
        return Element.wrap(JS.invoke(peer, "createElementNS", JS.param(namespace), JS.param(qualifiedName), JS.param(options)));
    }

    public Element createElementNS(String namespace, String qualifiedName, JSObject options) {
        return Element.wrap(JS.invoke(peer, "createElementNS", JS.param(namespace), JS.param(qualifiedName), JS.param(options)));
    }

    public Element createElementNS(String namespace, String qualifiedName) {
        return Element.wrap(JS.invoke(peer, "createElementNS", JS.param(namespace), JS.param(qualifiedName)));
    }

    public DocumentFragment createDocumentFragment() {
        return DocumentFragment.wrap(JS.invoke(peer, "createDocumentFragment"));
    }

    public Text createTextNode(String data) {
        return Text.wrap(JS.invoke(peer, "createTextNode", JS.param(data)));
    }

    public CDATASection createCDATASection(String data) {
        return CDATASection.wrap(JS.invoke(peer, "createCDATASection", JS.param(data)));
    }

    public Comment createComment(String data) {
        return Comment.wrap(JS.invoke(peer, "createComment", JS.param(data)));
    }

    public ProcessingInstruction createProcessingInstruction(String target, String data) {
        return ProcessingInstruction.wrap(JS.invoke(peer, "createProcessingInstruction", JS.param(target), JS.param(data)));
    }

    public Node importNode(Node node, boolean deep) {
        return Node.wrap(JS.invoke(peer, "importNode", JS.param(node), deep));
    }

    public Node importNode(Node node) {
        return Node.wrap(JS.invoke(peer, "importNode", JS.param(node)));
    }

    public Node adoptNode(Node node) {
        return Node.wrap(JS.invoke(peer, "adoptNode", JS.param(node)));
    }

    public Attr createAttribute(String localName) {
        return Attr.wrap(JS.invoke(peer, "createAttribute", JS.param(localName)));
    }

    public Attr createAttributeNS(String namespace, String qualifiedName) {
        return Attr.wrap(JS.invoke(peer, "createAttributeNS", JS.param(namespace), JS.param(qualifiedName)));
    }

    public Event createEvent(String interface_) {
        return Event.wrap(JS.invoke(peer, "createEvent", JS.param(interface_)));
    }

    public Range createRange() {
        return Range.wrap(JS.invoke(peer, "createRange"));
    }

    public NodeIterator createNodeIterator(Node root, int whatToShow, NodeFilter filter) {
        return NodeIterator.wrap(JS.invoke(peer, "createNodeIterator", JS.param(root), whatToShow, JS.param(filter)));
    }

    public NodeIterator createNodeIterator(Node root, int whatToShow) {
        return NodeIterator.wrap(JS.invoke(peer, "createNodeIterator", JS.param(root), whatToShow));
    }

    public NodeIterator createNodeIterator(Node root) {
        return NodeIterator.wrap(JS.invoke(peer, "createNodeIterator", JS.param(root)));
    }

    public TreeWalker createTreeWalker(Node root, int whatToShow, NodeFilter filter) {
        return TreeWalker.wrap(JS.invoke(peer, "createTreeWalker", JS.param(root), whatToShow, JS.param(filter)));
    }

    public TreeWalker createTreeWalker(Node root, int whatToShow) {
        return TreeWalker.wrap(JS.invoke(peer, "createTreeWalker", JS.param(root), whatToShow));
    }

    public TreeWalker createTreeWalker(Node root) {
        return TreeWalker.wrap(JS.invoke(peer, "createTreeWalker", JS.param(root)));
    }

    public NodeList getElementsByName(String elementName) {
        return NodeList.wrap(JS.invoke(peer, "getElementsByName", JS.param(elementName)));
    }

    public Document open(String unused1, String unused2) {
        return Document.wrap(JS.invoke(peer, "open", JS.param(unused1), JS.param(unused2)));
    }

    public Document open(String unused1) {
        return Document.wrap(JS.invoke(peer, "open", JS.param(unused1)));
    }

    public Document open() {
        return Document.wrap(JS.invoke(peer, "open"));
    }

    public WindowProxy open(String url, String name, String features) {
        return WindowProxy.wrap(JS.invoke(peer, "open", JS.param(url), JS.param(name), JS.param(features)));
    }

    public void close() {
        JS.invoke(peer, "close");
    }

    public void write(String text) {
        JS.invoke(peer, "write", JS.param(text));
    }

    public void writeln(String text) {
        JS.invoke(peer, "writeln", JS.param(text));
    }

    public boolean hasFocus() {
        return JS.invoke(peer, "hasFocus");
    }

    public boolean execCommand(String commandId, boolean showUI, String value) {
        return JS.invoke(peer, "execCommand", JS.param(commandId), showUI, JS.param(value));
    }

    public boolean execCommand(String commandId, boolean showUI) {
        return JS.invoke(peer, "execCommand", JS.param(commandId), showUI);
    }

    public boolean execCommand(String commandId) {
        return JS.invoke(peer, "execCommand", JS.param(commandId));
    }

    public boolean queryCommandEnabled(String commandId) {
        return JS.invoke(peer, "queryCommandEnabled", JS.param(commandId));
    }

    public boolean queryCommandIndeterm(String commandId) {
        return JS.invoke(peer, "queryCommandIndeterm", JS.param(commandId));
    }

    public boolean queryCommandState(String commandId) {
        return JS.invoke(peer, "queryCommandState", JS.param(commandId));
    }

    public boolean queryCommandSupported(String commandId) {
        return JS.invoke(peer, "queryCommandSupported", JS.param(commandId));
    }

    public String queryCommandValue(String commandId) {
        return JS.string(JS.invoke(peer, "queryCommandValue", JS.param(commandId)));
    }

    public void clear() {
        JS.invoke(peer, "clear");
    }

    public void captureEvents() {
        JS.invoke(peer, "captureEvents");
    }

    public void releaseEvents() {
        JS.invoke(peer, "releaseEvents");
    }

    public Element getElementById(String elementId) {
        return Element.wrap(JS.invoke(peer, "getElementById", JS.param(elementId)));
    }

    public void prepend(Node nodes) {
        JS.invoke(peer, "prepend", JS.param(nodes));
    }

    public void prepend(String nodes) {
        JS.invoke(peer, "prepend", JS.param(nodes));
    }

    public void append(Node nodes) {
        JS.invoke(peer, "append", JS.param(nodes));
    }

    public void append(String nodes) {
        JS.invoke(peer, "append", JS.param(nodes));
    }

    public void replaceChildren(Node nodes) {
        JS.invoke(peer, "replaceChildren", JS.param(nodes));
    }

    public void replaceChildren(String nodes) {
        JS.invoke(peer, "replaceChildren", JS.param(nodes));
    }

    public Element querySelector(String selectors) {
        return Element.wrap(JS.invoke(peer, "querySelector", JS.param(selectors)));
    }

    public NodeList querySelectorAll(String selectors) {
        return NodeList.wrap(JS.invoke(peer, "querySelectorAll", JS.param(selectors)));
    }

    public XPathExpression createExpression(String expression, XPathNSResolver resolver) {
        return XPathExpression.wrap(JS.invoke(peer, "createExpression", JS.param(expression), JS.param(resolver)));
    }

    public XPathExpression createExpression(String expression) {
        return XPathExpression.wrap(JS.invoke(peer, "createExpression", JS.param(expression)));
    }

    public XPathNSResolver createNSResolver(Node nodeResolver) {
        return XPathNSResolver.wrap(JS.invoke(peer, "createNSResolver", JS.param(nodeResolver)));
    }

    public XPathResult evaluate(String expression, Node contextNode, XPathNSResolver resolver,
            int type, XPathResult result) {
        return XPathResult.wrap(JS.invoke(peer, "evaluate", JS.param(expression), JS.param(contextNode), JS.param(resolver), JS.param(type), JS.param(result)));
    }

    public XPathResult evaluate(String expression, Node contextNode, XPathNSResolver resolver,
            int type) {
        return XPathResult.wrap(JS.invoke(peer, "evaluate", JS.param(expression), JS.param(contextNode), JS.param(resolver), JS.param(type)));
    }

    public XPathResult evaluate(String expression, Node contextNode, XPathNSResolver resolver) {
        return XPathResult.wrap(JS.invoke(peer, "evaluate", JS.param(expression), JS.param(contextNode), JS.param(resolver)));
    }

    public XPathResult evaluate(String expression, Node contextNode) {
        return XPathResult.wrap(JS.invoke(peer, "evaluate", JS.param(expression), JS.param(contextNode)));
    }
}
