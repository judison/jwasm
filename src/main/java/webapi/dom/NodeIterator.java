// Generated file; DO NOT edit
package webapi.dom;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class NodeIterator extends WrappedObject {
    protected NodeIterator(Object peer) {
        super(peer);
    }

    public static NodeIterator wrap(Object peer) {
        return peer == null ? null : new NodeIterator(peer);
    }

    public Node getRoot() {
        return Node.wrap(JS.get(peer, "root"));
    }

    public Node getReferenceNode() {
        return Node.wrap(JS.get(peer, "referenceNode"));
    }

    public boolean isPointerBeforeReferenceNode() {
        return JS.getBoolean(peer, "pointerBeforeReferenceNode");
    }

    public int getWhatToShow() {
        return JS.getInt(peer, "whatToShow");
    }

    public NodeFilter getFilter() {
        return NodeFilter.wrap(JS.get(peer, "filter"));
    }

    public Node nextNode() {
        return Node.wrap(JS.invoke(peer, "nextNode"));
    }

    public Node previousNode() {
        return Node.wrap(JS.invoke(peer, "previousNode"));
    }

    public void detach() {
        JS.invoke(peer, "detach");
    }
}
