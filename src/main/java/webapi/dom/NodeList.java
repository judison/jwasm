// Generated file; DO NOT edit
package webapi.dom;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class NodeList extends WrappedObject {
    protected NodeList(Object peer) {
        super(peer);
    }

    public static NodeList wrap(Object peer) {
        return peer == null ? null : new NodeList(peer);
    }

    public int getLength() {
        return JS.getInt(peer, "length");
    }
}
