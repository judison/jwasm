// Generated file; DO NOT edit
package webapi.dom;

@SuppressWarnings({"unused"})
public class XMLDocument extends Document {
    protected XMLDocument(Object peer) {
        super(peer);
    }

    public static XMLDocument wrap(Object peer) {
        return peer == null ? null : new XMLDocument(peer);
    }
}
