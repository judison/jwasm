// Generated file; DO NOT edit
package webapi.dom;

import webapi.JS;
import webapi.html.HTMLSlotElement;

@SuppressWarnings({"unused"})
public class Text extends CharacterData {
    protected Text(Object peer) {
        super(peer);
    }

    public static Text wrap(Object peer) {
        return peer == null ? null : new Text(peer);
    }

    public String getWholeText() {
        return JS.string(JS.get(peer, "wholeText"));
    }

    public HTMLSlotElement getAssignedSlot() {
        return HTMLSlotElement.wrap(JS.get(peer, "assignedSlot"));
    }

    public Text splitText(int offset) {
        return Text.wrap(JS.invoke(peer, "splitText", offset));
    }
}
