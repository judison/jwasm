// Generated file; DO NOT edit
package webapi.dom;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class XSLTProcessor extends WrappedObject {
    protected XSLTProcessor(Object peer) {
        super(peer);
    }

    public static XSLTProcessor wrap(Object peer) {
        return peer == null ? null : new XSLTProcessor(peer);
    }

    public void importStylesheet(Node style) {
        JS.invoke(peer, "importStylesheet", JS.param(style));
    }

    public DocumentFragment transformToFragment(Node source, Document output) {
        return DocumentFragment.wrap(JS.invoke(peer, "transformToFragment", JS.param(source), JS.param(output)));
    }

    public Document transformToDocument(Node source) {
        return Document.wrap(JS.invoke(peer, "transformToDocument", JS.param(source)));
    }

    public void setParameter(String namespaceURI, String localName, WrappedObject value) {
        JS.invoke(peer, "setParameter", JS.param(namespaceURI), JS.param(localName), JS.param(value));
    }

    public WrappedObject getParameter(String namespaceURI, String localName) {
        return WrappedObject.wrap(JS.invoke(peer, "getParameter", JS.param(namespaceURI), JS.param(localName)));
    }

    public void removeParameter(String namespaceURI, String localName) {
        JS.invoke(peer, "removeParameter", JS.param(namespaceURI), JS.param(localName));
    }

    public void clearParameters() {
        JS.invoke(peer, "clearParameters");
    }

    public void reset() {
        JS.invoke(peer, "reset");
    }
}
