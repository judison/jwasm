// Generated file; DO NOT edit
// Ignored static operation fromPoint
package webapi.dom;

import webapi.JS;
import webapi.JSObject;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class DOMPointReadOnly extends WrappedObject {
    protected DOMPointReadOnly(Object peer) {
        super(peer);
    }

    public static DOMPointReadOnly wrap(Object peer) {
        return peer == null ? null : new DOMPointReadOnly(peer);
    }

    public double getX() {
        return JS.getDouble(peer, "x");
    }

    public double getY() {
        return JS.getDouble(peer, "y");
    }

    public double getZ() {
        return JS.getDouble(peer, "z");
    }

    public double getW() {
        return JS.getDouble(peer, "w");
    }

    public DOMPoint matrixTransform(JSObject matrix) {
        return DOMPoint.wrap(JS.invoke(peer, "matrixTransform", JS.param(matrix)));
    }

    public DOMPoint matrixTransform() {
        return DOMPoint.wrap(JS.invoke(peer, "matrixTransform"));
    }

    public WrappedObject toJSON() {
        return WrappedObject.wrap(JS.invoke(peer, "toJSON"));
    }
}
