package webapi;

import browser.javascript.JS;

public class WrappedObject {

	public final Object peer;

	protected WrappedObject(Object peer) {
		this.peer = peer;
	}

	public static WrappedObject wrap(Object peer) {
		return peer == null ? null : new WrappedObject(peer);
	}

	@SuppressWarnings("unchecked")
	protected <T> T _get(String name) {
		return (T)JS.get(peer, name);
	}

}
