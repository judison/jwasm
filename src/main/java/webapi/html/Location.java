// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class Location extends WrappedObject {
    protected Location(Object peer) {
        super(peer);
    }

    public static Location wrap(Object peer) {
        return peer == null ? null : new Location(peer);
    }

    public String getHref() {
        return JS.string(JS.get(peer, "href"));
    }

    public void setHref(String value) {
        JS.set(peer, "href", JS.param(value));
    }

    public String getOrigin() {
        return JS.string(JS.get(peer, "origin"));
    }

    public String getProtocol() {
        return JS.string(JS.get(peer, "protocol"));
    }

    public void setProtocol(String value) {
        JS.set(peer, "protocol", JS.param(value));
    }

    public String getHost() {
        return JS.string(JS.get(peer, "host"));
    }

    public void setHost(String value) {
        JS.set(peer, "host", JS.param(value));
    }

    public String getHostname() {
        return JS.string(JS.get(peer, "hostname"));
    }

    public void setHostname(String value) {
        JS.set(peer, "hostname", JS.param(value));
    }

    public String getPort() {
        return JS.string(JS.get(peer, "port"));
    }

    public void setPort(String value) {
        JS.set(peer, "port", JS.param(value));
    }

    public String getPathname() {
        return JS.string(JS.get(peer, "pathname"));
    }

    public void setPathname(String value) {
        JS.set(peer, "pathname", JS.param(value));
    }

    public String getSearch() {
        return JS.string(JS.get(peer, "search"));
    }

    public void setSearch(String value) {
        JS.set(peer, "search", JS.param(value));
    }

    public String getHash() {
        return JS.string(JS.get(peer, "hash"));
    }

    public void setHash(String value) {
        JS.set(peer, "hash", JS.param(value));
    }

    public DOMStringList getAncestorOrigins() {
        return DOMStringList.wrap(JS.get(peer, "ancestorOrigins"));
    }

    public void assign(String url) {
        JS.invoke(peer, "assign", JS.param(url));
    }

    public void replace(String url) {
        JS.invoke(peer, "replace", JS.param(url));
    }

    public void reload() {
        JS.invoke(peer, "reload");
    }
}
