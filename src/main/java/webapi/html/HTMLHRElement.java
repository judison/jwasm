// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;

@SuppressWarnings({"unused"})
public class HTMLHRElement extends HTMLElement {
    protected HTMLHRElement(Object peer) {
        super(peer);
    }

    public static HTMLHRElement wrap(Object peer) {
        return peer == null ? null : new HTMLHRElement(peer);
    }

    public String getAlign() {
        return JS.string(JS.get(peer, "align"));
    }

    public void setAlign(String value) {
        JS.set(peer, "align", JS.param(value));
    }

    public String getColor() {
        return JS.string(JS.get(peer, "color"));
    }

    public void setColor(String value) {
        JS.set(peer, "color", JS.param(value));
    }

    public boolean isNoShade() {
        return JS.getBoolean(peer, "noShade");
    }

    public void setNoShade(boolean value) {
        JS.set(peer, "noShade", value);
    }

    public String getSize() {
        return JS.string(JS.get(peer, "size"));
    }

    public void setSize(String value) {
        JS.set(peer, "size", JS.param(value));
    }

    public String getWidth() {
        return JS.string(JS.get(peer, "width"));
    }

    public void setWidth(String value) {
        JS.set(peer, "width", JS.param(value));
    }
}
