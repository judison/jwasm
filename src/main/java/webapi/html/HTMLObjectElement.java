// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.dom.Document;

@SuppressWarnings({"unused"})
public class HTMLObjectElement extends HTMLElement {
    protected HTMLObjectElement(Object peer) {
        super(peer);
    }

    public static HTMLObjectElement wrap(Object peer) {
        return peer == null ? null : new HTMLObjectElement(peer);
    }

    public String getData() {
        return JS.string(JS.get(peer, "data"));
    }

    public void setData(String value) {
        JS.set(peer, "data", JS.param(value));
    }

    public String getType() {
        return JS.string(JS.get(peer, "type"));
    }

    public void setType(String value) {
        JS.set(peer, "type", JS.param(value));
    }

    public String getName() {
        return JS.string(JS.get(peer, "name"));
    }

    public void setName(String value) {
        JS.set(peer, "name", JS.param(value));
    }

    public HTMLFormElement getForm() {
        return HTMLFormElement.wrap(JS.get(peer, "form"));
    }

    public String getWidth() {
        return JS.string(JS.get(peer, "width"));
    }

    public void setWidth(String value) {
        JS.set(peer, "width", JS.param(value));
    }

    public String getHeight() {
        return JS.string(JS.get(peer, "height"));
    }

    public void setHeight(String value) {
        JS.set(peer, "height", JS.param(value));
    }

    public Document getContentDocument() {
        return Document.wrap(JS.get(peer, "contentDocument"));
    }

    public WindowProxy getContentWindow() {
        return WindowProxy.wrap(JS.get(peer, "contentWindow"));
    }

    public boolean isWillValidate() {
        return JS.getBoolean(peer, "willValidate");
    }

    public ValidityState getValidity() {
        return ValidityState.wrap(JS.get(peer, "validity"));
    }

    public String getValidationMessage() {
        return JS.string(JS.get(peer, "validationMessage"));
    }

    public String getAlign() {
        return JS.string(JS.get(peer, "align"));
    }

    public void setAlign(String value) {
        JS.set(peer, "align", JS.param(value));
    }

    public String getArchive() {
        return JS.string(JS.get(peer, "archive"));
    }

    public void setArchive(String value) {
        JS.set(peer, "archive", JS.param(value));
    }

    public String getCode() {
        return JS.string(JS.get(peer, "code"));
    }

    public void setCode(String value) {
        JS.set(peer, "code", JS.param(value));
    }

    public boolean isDeclare() {
        return JS.getBoolean(peer, "declare");
    }

    public void setDeclare(boolean value) {
        JS.set(peer, "declare", value);
    }

    public int getHspace() {
        return JS.getInt(peer, "hspace");
    }

    public void setHspace(int value) {
        JS.set(peer, "hspace", value);
    }

    public String getStandby() {
        return JS.string(JS.get(peer, "standby"));
    }

    public void setStandby(String value) {
        JS.set(peer, "standby", JS.param(value));
    }

    public int getVspace() {
        return JS.getInt(peer, "vspace");
    }

    public void setVspace(int value) {
        JS.set(peer, "vspace", value);
    }

    public String getCodeBase() {
        return JS.string(JS.get(peer, "codeBase"));
    }

    public void setCodeBase(String value) {
        JS.set(peer, "codeBase", JS.param(value));
    }

    public String getCodeType() {
        return JS.string(JS.get(peer, "codeType"));
    }

    public void setCodeType(String value) {
        JS.set(peer, "codeType", JS.param(value));
    }

    public String getUseMap() {
        return JS.string(JS.get(peer, "useMap"));
    }

    public void setUseMap(String value) {
        JS.set(peer, "useMap", JS.param(value));
    }

    public String getBorder() {
        return JS.string(JS.get(peer, "border"));
    }

    public void setBorder(String value) {
        JS.set(peer, "border", JS.param(value));
    }

    public Document getSVGDocument() {
        return Document.wrap(JS.invoke(peer, "getSVGDocument"));
    }

    public boolean checkValidity() {
        return JS.invoke(peer, "checkValidity");
    }

    public boolean reportValidity() {
        return JS.invoke(peer, "reportValidity");
    }

    public void setCustomValidity(String error) {
        JS.invoke(peer, "setCustomValidity", JS.param(error));
    }
}
