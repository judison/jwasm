// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.dom.EventTarget;

@SuppressWarnings({"unused"})
public class EventSource extends EventTarget {
    protected EventSource(Object peer) {
        super(peer);
    }

    public static EventSource wrap(Object peer) {
        return peer == null ? null : new EventSource(peer);
    }

    public String getUrl() {
        return JS.string(JS.get(peer, "url"));
    }

    public boolean isWithCredentials() {
        return JS.getBoolean(peer, "withCredentials");
    }

    public int getReadyState() {
        return JS.getInt(peer, "readyState");
    }

    public EventHandler getOnopen() {
        return EventHandler.wrap(JS.get(peer, "onopen"));
    }

    public void setOnopen(EventHandler value) {
        JS.set(peer, "onopen", value.peer);
    }

    public EventHandler getOnmessage() {
        return EventHandler.wrap(JS.get(peer, "onmessage"));
    }

    public void setOnmessage(EventHandler value) {
        JS.set(peer, "onmessage", value.peer);
    }

    public EventHandler getOnerror() {
        return EventHandler.wrap(JS.get(peer, "onerror"));
    }

    public void setOnerror(EventHandler value) {
        JS.set(peer, "onerror", value.peer);
    }

    public void close() {
        JS.invoke(peer, "close");
    }
}
