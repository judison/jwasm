// Generated file; DO NOT edit
package webapi.html;

@SuppressWarnings({"unused"})
public class HTMLHeadElement extends HTMLElement {
    protected HTMLHeadElement(Object peer) {
        super(peer);
    }

    public static HTMLHeadElement wrap(Object peer) {
        return peer == null ? null : new HTMLHeadElement(peer);
    }
}
