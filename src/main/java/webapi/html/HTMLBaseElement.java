// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;

@SuppressWarnings({"unused"})
public class HTMLBaseElement extends HTMLElement {
    protected HTMLBaseElement(Object peer) {
        super(peer);
    }

    public static HTMLBaseElement wrap(Object peer) {
        return peer == null ? null : new HTMLBaseElement(peer);
    }

    public String getHref() {
        return JS.string(JS.get(peer, "href"));
    }

    public void setHref(String value) {
        JS.set(peer, "href", JS.param(value));
    }

    public String getTarget() {
        return JS.string(JS.get(peer, "target"));
    }

    public void setTarget(String value) {
        JS.set(peer, "target", JS.param(value));
    }
}
