// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.dom.HTMLCollection;

@SuppressWarnings({"unused"})
public class HTMLDataListElement extends HTMLElement {
    protected HTMLDataListElement(Object peer) {
        super(peer);
    }

    public static HTMLDataListElement wrap(Object peer) {
        return peer == null ? null : new HTMLDataListElement(peer);
    }

    public HTMLCollection getOptions() {
        return HTMLCollection.wrap(JS.get(peer, "options"));
    }
}
