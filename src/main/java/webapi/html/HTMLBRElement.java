// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;

@SuppressWarnings({"unused"})
public class HTMLBRElement extends HTMLElement {
    protected HTMLBRElement(Object peer) {
        super(peer);
    }

    public static HTMLBRElement wrap(Object peer) {
        return peer == null ? null : new HTMLBRElement(peer);
    }

    public String getClear() {
        return JS.string(JS.get(peer, "clear"));
    }

    public void setClear(String value) {
        JS.set(peer, "clear", JS.param(value));
    }
}
