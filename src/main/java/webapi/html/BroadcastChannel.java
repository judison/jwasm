// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;
import webapi.dom.EventTarget;

@SuppressWarnings({"unused"})
public class BroadcastChannel extends EventTarget {
    protected BroadcastChannel(Object peer) {
        super(peer);
    }

    public static BroadcastChannel wrap(Object peer) {
        return peer == null ? null : new BroadcastChannel(peer);
    }

    public String getName() {
        return JS.string(JS.get(peer, "name"));
    }

    public EventHandler getOnmessage() {
        return EventHandler.wrap(JS.get(peer, "onmessage"));
    }

    public void setOnmessage(EventHandler value) {
        JS.set(peer, "onmessage", value.peer);
    }

    public EventHandler getOnmessageerror() {
        return EventHandler.wrap(JS.get(peer, "onmessageerror"));
    }

    public void setOnmessageerror(EventHandler value) {
        JS.set(peer, "onmessageerror", value.peer);
    }

    public void postMessage(WrappedObject message) {
        JS.invoke(peer, "postMessage", JS.param(message));
    }

    public void close() {
        JS.invoke(peer, "close");
    }
}
