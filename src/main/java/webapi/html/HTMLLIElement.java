// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;

@SuppressWarnings({"unused"})
public class HTMLLIElement extends HTMLElement {
    protected HTMLLIElement(Object peer) {
        super(peer);
    }

    public static HTMLLIElement wrap(Object peer) {
        return peer == null ? null : new HTMLLIElement(peer);
    }

    public long getValue() {
        return (long)JS.get(peer, "value");
    }

    public void setValue(long value) {
        JS.set(peer, "value", value);
    }

    public String getType() {
        return JS.string(JS.get(peer, "type"));
    }

    public void setType(String value) {
        JS.set(peer, "type", JS.param(value));
    }
}
