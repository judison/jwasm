// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;

@SuppressWarnings({"unused"})
public class HTMLOptionElement extends HTMLElement {
    protected HTMLOptionElement(Object peer) {
        super(peer);
    }

    public static HTMLOptionElement wrap(Object peer) {
        return peer == null ? null : new HTMLOptionElement(peer);
    }

    public boolean isDisabled() {
        return JS.getBoolean(peer, "disabled");
    }

    public void setDisabled(boolean value) {
        JS.set(peer, "disabled", value);
    }

    public HTMLFormElement getForm() {
        return HTMLFormElement.wrap(JS.get(peer, "form"));
    }

    public String getLabel() {
        return JS.string(JS.get(peer, "label"));
    }

    public void setLabel(String value) {
        JS.set(peer, "label", JS.param(value));
    }

    public boolean isDefaultSelected() {
        return JS.getBoolean(peer, "defaultSelected");
    }

    public void setDefaultSelected(boolean value) {
        JS.set(peer, "defaultSelected", value);
    }

    public boolean isSelected() {
        return JS.getBoolean(peer, "selected");
    }

    public void setSelected(boolean value) {
        JS.set(peer, "selected", value);
    }

    public String getValue() {
        return JS.string(JS.get(peer, "value"));
    }

    public void setValue(String value) {
        JS.set(peer, "value", JS.param(value));
    }

    public String getText() {
        return JS.string(JS.get(peer, "text"));
    }

    public void setText(String value) {
        JS.set(peer, "text", JS.param(value));
    }

    public long getIndex() {
        return (long)JS.get(peer, "index");
    }
}
