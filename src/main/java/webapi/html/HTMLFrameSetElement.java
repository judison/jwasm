// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class HTMLFrameSetElement extends HTMLElement {
    protected HTMLFrameSetElement(Object peer) {
        super(peer);
    }

    public static HTMLFrameSetElement wrap(Object peer) {
        return peer == null ? null : new HTMLFrameSetElement(peer);
    }

    public String getCols() {
        return JS.string(JS.get(peer, "cols"));
    }

    public void setCols(String value) {
        JS.set(peer, "cols", JS.param(value));
    }

    public String getRows() {
        return JS.string(JS.get(peer, "rows"));
    }

    public void setRows(String value) {
        JS.set(peer, "rows", JS.param(value));
    }

    public EventHandler getOnafterprint() {
        return EventHandler.wrap(JS.get(peer, "onafterprint"));
    }

    public void setOnafterprint(EventHandler value) {
        JS.set(peer, "onafterprint", value.peer);
    }

    public EventHandler getOnbeforeprint() {
        return EventHandler.wrap(JS.get(peer, "onbeforeprint"));
    }

    public void setOnbeforeprint(EventHandler value) {
        JS.set(peer, "onbeforeprint", value.peer);
    }

    public WrappedObject getOnbeforeunload() {
        return WrappedObject.wrap(JS.get(peer, "onbeforeunload"));
    }

    public void setOnbeforeunload(WrappedObject value) {
        JS.set(peer, "onbeforeunload", value.peer);
    }

    public EventHandler getOnhashchange() {
        return EventHandler.wrap(JS.get(peer, "onhashchange"));
    }

    public void setOnhashchange(EventHandler value) {
        JS.set(peer, "onhashchange", value.peer);
    }

    public EventHandler getOnlanguagechange() {
        return EventHandler.wrap(JS.get(peer, "onlanguagechange"));
    }

    public void setOnlanguagechange(EventHandler value) {
        JS.set(peer, "onlanguagechange", value.peer);
    }

    public EventHandler getOnmessage() {
        return EventHandler.wrap(JS.get(peer, "onmessage"));
    }

    public void setOnmessage(EventHandler value) {
        JS.set(peer, "onmessage", value.peer);
    }

    public EventHandler getOnmessageerror() {
        return EventHandler.wrap(JS.get(peer, "onmessageerror"));
    }

    public void setOnmessageerror(EventHandler value) {
        JS.set(peer, "onmessageerror", value.peer);
    }

    public EventHandler getOnoffline() {
        return EventHandler.wrap(JS.get(peer, "onoffline"));
    }

    public void setOnoffline(EventHandler value) {
        JS.set(peer, "onoffline", value.peer);
    }

    public EventHandler getOnonline() {
        return EventHandler.wrap(JS.get(peer, "ononline"));
    }

    public void setOnonline(EventHandler value) {
        JS.set(peer, "ononline", value.peer);
    }

    public EventHandler getOnpagehide() {
        return EventHandler.wrap(JS.get(peer, "onpagehide"));
    }

    public void setOnpagehide(EventHandler value) {
        JS.set(peer, "onpagehide", value.peer);
    }

    public EventHandler getOnpageshow() {
        return EventHandler.wrap(JS.get(peer, "onpageshow"));
    }

    public void setOnpageshow(EventHandler value) {
        JS.set(peer, "onpageshow", value.peer);
    }

    public EventHandler getOnpopstate() {
        return EventHandler.wrap(JS.get(peer, "onpopstate"));
    }

    public void setOnpopstate(EventHandler value) {
        JS.set(peer, "onpopstate", value.peer);
    }

    public EventHandler getOnrejectionhandled() {
        return EventHandler.wrap(JS.get(peer, "onrejectionhandled"));
    }

    public void setOnrejectionhandled(EventHandler value) {
        JS.set(peer, "onrejectionhandled", value.peer);
    }

    public EventHandler getOnstorage() {
        return EventHandler.wrap(JS.get(peer, "onstorage"));
    }

    public void setOnstorage(EventHandler value) {
        JS.set(peer, "onstorage", value.peer);
    }

    public EventHandler getOnunhandledrejection() {
        return EventHandler.wrap(JS.get(peer, "onunhandledrejection"));
    }

    public void setOnunhandledrejection(EventHandler value) {
        JS.set(peer, "onunhandledrejection", value.peer);
    }

    public EventHandler getOnunload() {
        return EventHandler.wrap(JS.get(peer, "onunload"));
    }

    public void setOnunload(EventHandler value) {
        JS.set(peer, "onunload", value.peer);
    }
}
