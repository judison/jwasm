// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.dom.DOMTokenList;
import webapi.dom.NodeList;

@SuppressWarnings({"unused"})
public class HTMLOutputElement extends HTMLElement {
    protected HTMLOutputElement(Object peer) {
        super(peer);
    }

    public static HTMLOutputElement wrap(Object peer) {
        return peer == null ? null : new HTMLOutputElement(peer);
    }

    public DOMTokenList getHtmlFor() {
        return DOMTokenList.wrap(JS.get(peer, "htmlFor"));
    }

    public HTMLFormElement getForm() {
        return HTMLFormElement.wrap(JS.get(peer, "form"));
    }

    public String getName() {
        return JS.string(JS.get(peer, "name"));
    }

    public void setName(String value) {
        JS.set(peer, "name", JS.param(value));
    }

    public String getType() {
        return JS.string(JS.get(peer, "type"));
    }

    public String getDefaultValue() {
        return JS.string(JS.get(peer, "defaultValue"));
    }

    public void setDefaultValue(String value) {
        JS.set(peer, "defaultValue", JS.param(value));
    }

    public String getValue() {
        return JS.string(JS.get(peer, "value"));
    }

    public void setValue(String value) {
        JS.set(peer, "value", JS.param(value));
    }

    public boolean isWillValidate() {
        return JS.getBoolean(peer, "willValidate");
    }

    public ValidityState getValidity() {
        return ValidityState.wrap(JS.get(peer, "validity"));
    }

    public String getValidationMessage() {
        return JS.string(JS.get(peer, "validationMessage"));
    }

    public NodeList getLabels() {
        return NodeList.wrap(JS.get(peer, "labels"));
    }

    public boolean checkValidity() {
        return JS.invoke(peer, "checkValidity");
    }

    public boolean reportValidity() {
        return JS.invoke(peer, "reportValidity");
    }

    public void setCustomValidity(String error) {
        JS.invoke(peer, "setCustomValidity", JS.param(error));
    }
}
