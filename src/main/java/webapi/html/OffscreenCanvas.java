// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.JSObject;
import webapi.JSPromise;
import webapi.WrappedObject;
import webapi.dom.EventTarget;

@SuppressWarnings({"unused"})
public class OffscreenCanvas extends EventTarget {
    protected OffscreenCanvas(Object peer) {
        super(peer);
    }

    public static OffscreenCanvas wrap(Object peer) {
        return peer == null ? null : new OffscreenCanvas(peer);
    }

    public long getWidth() {
        return (long)JS.get(peer, "width");
    }

    public void setWidth(long value) {
        JS.set(peer, "width", value);
    }

    public long getHeight() {
        return (long)JS.get(peer, "height");
    }

    public void setHeight(long value) {
        JS.set(peer, "height", value);
    }

    public EventHandler getOncontextlost() {
        return EventHandler.wrap(JS.get(peer, "oncontextlost"));
    }

    public void setOncontextlost(EventHandler value) {
        JS.set(peer, "oncontextlost", value.peer);
    }

    public EventHandler getOncontextrestored() {
        return EventHandler.wrap(JS.get(peer, "oncontextrestored"));
    }

    public void setOncontextrestored(EventHandler value) {
        JS.set(peer, "oncontextrestored", value.peer);
    }

    public WrappedObject getContext(WrappedObject contextId, WrappedObject options) {
        return WrappedObject.wrap(JS.invoke(peer, "getContext", JS.param(contextId), JS.param(options)));
    }

    public WrappedObject getContext(WrappedObject contextId) {
        return WrappedObject.wrap(JS.invoke(peer, "getContext", JS.param(contextId)));
    }

    public ImageBitmap transferToImageBitmap() {
        return ImageBitmap.wrap(JS.invoke(peer, "transferToImageBitmap"));
    }

    public JSPromise<WrappedObject> convertToBlob(JSObject options) {
        return JSPromise.wrap(WrappedObject.class, JS.invoke(peer, "convertToBlob", JS.param(options)));
    }

    public JSPromise<WrappedObject> convertToBlob() {
        return JSPromise.wrap(WrappedObject.class, JS.invoke(peer, "convertToBlob"));
    }
}
