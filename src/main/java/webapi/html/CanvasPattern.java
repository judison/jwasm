// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.JSObject;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class CanvasPattern extends WrappedObject {
    protected CanvasPattern(Object peer) {
        super(peer);
    }

    public static CanvasPattern wrap(Object peer) {
        return peer == null ? null : new CanvasPattern(peer);
    }

    public void setTransform(JSObject transform) {
        JS.invoke(peer, "setTransform", JS.param(transform));
    }

    public void setTransform() {
        JS.invoke(peer, "setTransform");
    }
}
