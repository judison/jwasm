// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.css.CSSStyleSheet;
import webapi.dom.DOMTokenList;

@SuppressWarnings({"unused"})
public class HTMLStyleElement extends HTMLElement {
    protected HTMLStyleElement(Object peer) {
        super(peer);
    }

    public static HTMLStyleElement wrap(Object peer) {
        return peer == null ? null : new HTMLStyleElement(peer);
    }

    public boolean isDisabled() {
        return JS.getBoolean(peer, "disabled");
    }

    public void setDisabled(boolean value) {
        JS.set(peer, "disabled", value);
    }

    public String getMedia() {
        return JS.string(JS.get(peer, "media"));
    }

    public void setMedia(String value) {
        JS.set(peer, "media", JS.param(value));
    }

    public DOMTokenList getBlocking() {
        return DOMTokenList.wrap(JS.get(peer, "blocking"));
    }

    public String getType() {
        return JS.string(JS.get(peer, "type"));
    }

    public void setType(String value) {
        JS.set(peer, "type", JS.param(value));
    }

    public CSSStyleSheet getSheet() {
        return CSSStyleSheet.wrap(JS.get(peer, "sheet"));
    }
}
