// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;

@SuppressWarnings({"unused"})
public class HTMLTableColElement extends HTMLElement {
    protected HTMLTableColElement(Object peer) {
        super(peer);
    }

    public static HTMLTableColElement wrap(Object peer) {
        return peer == null ? null : new HTMLTableColElement(peer);
    }

    public int getSpan() {
        return JS.getInt(peer, "span");
    }

    public void setSpan(int value) {
        JS.set(peer, "span", value);
    }

    public String getAlign() {
        return JS.string(JS.get(peer, "align"));
    }

    public void setAlign(String value) {
        JS.set(peer, "align", JS.param(value));
    }

    public String getCh() {
        return JS.string(JS.get(peer, "ch"));
    }

    public void setCh(String value) {
        JS.set(peer, "ch", JS.param(value));
    }

    public String getChOff() {
        return JS.string(JS.get(peer, "chOff"));
    }

    public void setChOff(String value) {
        JS.set(peer, "chOff", JS.param(value));
    }

    public String getVAlign() {
        return JS.string(JS.get(peer, "vAlign"));
    }

    public void setVAlign(String value) {
        JS.set(peer, "vAlign", JS.param(value));
    }

    public String getWidth() {
        return JS.string(JS.get(peer, "width"));
    }

    public void setWidth(String value) {
        JS.set(peer, "width", JS.param(value));
    }
}
