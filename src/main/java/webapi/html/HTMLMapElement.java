// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.dom.HTMLCollection;

@SuppressWarnings({"unused"})
public class HTMLMapElement extends HTMLElement {
    protected HTMLMapElement(Object peer) {
        super(peer);
    }

    public static HTMLMapElement wrap(Object peer) {
        return peer == null ? null : new HTMLMapElement(peer);
    }

    public String getName() {
        return JS.string(JS.get(peer, "name"));
    }

    public void setName(String value) {
        JS.set(peer, "name", JS.param(value));
    }

    public HTMLCollection getAreas() {
        return HTMLCollection.wrap(JS.get(peer, "areas"));
    }
}
