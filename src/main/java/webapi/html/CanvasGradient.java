// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class CanvasGradient extends WrappedObject {
    protected CanvasGradient(Object peer) {
        super(peer);
    }

    public static CanvasGradient wrap(Object peer) {
        return peer == null ? null : new CanvasGradient(peer);
    }

    public void addColorStop(double offset, String color) {
        JS.invoke(peer, "addColorStop", offset, JS.param(color));
    }
}
