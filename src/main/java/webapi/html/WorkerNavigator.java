// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class WorkerNavigator extends WrappedObject {
    protected WorkerNavigator(Object peer) {
        super(peer);
    }

    public static WorkerNavigator wrap(Object peer) {
        return peer == null ? null : new WorkerNavigator(peer);
    }

    public String getAppCodeName() {
        return JS.string(JS.get(peer, "appCodeName"));
    }

    public String getAppName() {
        return JS.string(JS.get(peer, "appName"));
    }

    public String getAppVersion() {
        return JS.string(JS.get(peer, "appVersion"));
    }

    public String getPlatform() {
        return JS.string(JS.get(peer, "platform"));
    }

    public String getProduct() {
        return JS.string(JS.get(peer, "product"));
    }

    public String getProductSub() {
        return JS.string(JS.get(peer, "productSub"));
    }

    public String getUserAgent() {
        return JS.string(JS.get(peer, "userAgent"));
    }

    public String getVendor() {
        return JS.string(JS.get(peer, "vendor"));
    }

    public String getVendorSub() {
        return JS.string(JS.get(peer, "vendorSub"));
    }

    public String getOscpu() {
        return JS.string(JS.get(peer, "oscpu"));
    }

    public String getLanguage() {
        return JS.string(JS.get(peer, "language"));
    }

    public WrappedObject getLanguages() {
        return WrappedObject.wrap(JS.get(peer, "languages"));
    }

    public boolean isOnLine() {
        return JS.getBoolean(peer, "onLine");
    }

    public long getHardwareConcurrency() {
        return (long)JS.get(peer, "hardwareConcurrency");
    }

    public boolean taintEnabled() {
        return JS.invoke(peer, "taintEnabled");
    }
}
