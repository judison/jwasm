// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class PluginArray extends WrappedObject {
    protected PluginArray(Object peer) {
        super(peer);
    }

    public static PluginArray wrap(Object peer) {
        return peer == null ? null : new PluginArray(peer);
    }

    public int getLength() {
        return JS.getInt(peer, "length");
    }

    public void refresh() {
        JS.invoke(peer, "refresh");
    }
}
