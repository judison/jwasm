// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.dom.DOMTokenList;

@SuppressWarnings({"unused"})
public class HTMLFormElement extends HTMLElement {
    protected HTMLFormElement(Object peer) {
        super(peer);
    }

    public static HTMLFormElement wrap(Object peer) {
        return peer == null ? null : new HTMLFormElement(peer);
    }

    public String getAcceptCharset() {
        return JS.string(JS.get(peer, "acceptCharset"));
    }

    public void setAcceptCharset(String value) {
        JS.set(peer, "acceptCharset", JS.param(value));
    }

    public String getAction() {
        return JS.string(JS.get(peer, "action"));
    }

    public void setAction(String value) {
        JS.set(peer, "action", JS.param(value));
    }

    public String getAutocomplete() {
        return JS.string(JS.get(peer, "autocomplete"));
    }

    public void setAutocomplete(String value) {
        JS.set(peer, "autocomplete", JS.param(value));
    }

    public String getEnctype() {
        return JS.string(JS.get(peer, "enctype"));
    }

    public void setEnctype(String value) {
        JS.set(peer, "enctype", JS.param(value));
    }

    public String getEncoding() {
        return JS.string(JS.get(peer, "encoding"));
    }

    public void setEncoding(String value) {
        JS.set(peer, "encoding", JS.param(value));
    }

    public String getMethod() {
        return JS.string(JS.get(peer, "method"));
    }

    public void setMethod(String value) {
        JS.set(peer, "method", JS.param(value));
    }

    public String getName() {
        return JS.string(JS.get(peer, "name"));
    }

    public void setName(String value) {
        JS.set(peer, "name", JS.param(value));
    }

    public boolean isNoValidate() {
        return JS.getBoolean(peer, "noValidate");
    }

    public void setNoValidate(boolean value) {
        JS.set(peer, "noValidate", value);
    }

    public String getTarget() {
        return JS.string(JS.get(peer, "target"));
    }

    public void setTarget(String value) {
        JS.set(peer, "target", JS.param(value));
    }

    public String getRel() {
        return JS.string(JS.get(peer, "rel"));
    }

    public void setRel(String value) {
        JS.set(peer, "rel", JS.param(value));
    }

    public DOMTokenList getRelList() {
        return DOMTokenList.wrap(JS.get(peer, "relList"));
    }

    public HTMLFormControlsCollection getElements() {
        return HTMLFormControlsCollection.wrap(JS.get(peer, "elements"));
    }

    public int getLength() {
        return JS.getInt(peer, "length");
    }

    public void submit() {
        JS.invoke(peer, "submit");
    }

    public void requestSubmit(HTMLElement submitter) {
        JS.invoke(peer, "requestSubmit", JS.param(submitter));
    }

    public void requestSubmit() {
        JS.invoke(peer, "requestSubmit");
    }

    public void reset() {
        JS.invoke(peer, "reset");
    }

    public boolean checkValidity() {
        return JS.invoke(peer, "checkValidity");
    }

    public boolean reportValidity() {
        return JS.invoke(peer, "reportValidity");
    }
}
