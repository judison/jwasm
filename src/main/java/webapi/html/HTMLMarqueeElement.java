// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;

@SuppressWarnings({"unused"})
public class HTMLMarqueeElement extends HTMLElement {
    protected HTMLMarqueeElement(Object peer) {
        super(peer);
    }

    public static HTMLMarqueeElement wrap(Object peer) {
        return peer == null ? null : new HTMLMarqueeElement(peer);
    }

    public String getBehavior() {
        return JS.string(JS.get(peer, "behavior"));
    }

    public void setBehavior(String value) {
        JS.set(peer, "behavior", JS.param(value));
    }

    public String getBgColor() {
        return JS.string(JS.get(peer, "bgColor"));
    }

    public void setBgColor(String value) {
        JS.set(peer, "bgColor", JS.param(value));
    }

    public String getDirection() {
        return JS.string(JS.get(peer, "direction"));
    }

    public void setDirection(String value) {
        JS.set(peer, "direction", JS.param(value));
    }

    public String getHeight() {
        return JS.string(JS.get(peer, "height"));
    }

    public void setHeight(String value) {
        JS.set(peer, "height", JS.param(value));
    }

    public int getHspace() {
        return JS.getInt(peer, "hspace");
    }

    public void setHspace(int value) {
        JS.set(peer, "hspace", value);
    }

    public long getLoop() {
        return (long)JS.get(peer, "loop");
    }

    public void setLoop(long value) {
        JS.set(peer, "loop", value);
    }

    public int getScrollAmount() {
        return JS.getInt(peer, "scrollAmount");
    }

    public void setScrollAmount(int value) {
        JS.set(peer, "scrollAmount", value);
    }

    public int getScrollDelay() {
        return JS.getInt(peer, "scrollDelay");
    }

    public void setScrollDelay(int value) {
        JS.set(peer, "scrollDelay", value);
    }

    public boolean isTrueSpeed() {
        return JS.getBoolean(peer, "trueSpeed");
    }

    public void setTrueSpeed(boolean value) {
        JS.set(peer, "trueSpeed", value);
    }

    public int getVspace() {
        return JS.getInt(peer, "vspace");
    }

    public void setVspace(int value) {
        JS.set(peer, "vspace", value);
    }

    public String getWidth() {
        return JS.string(JS.get(peer, "width"));
    }

    public void setWidth(String value) {
        JS.set(peer, "width", JS.param(value));
    }

    public void start() {
        JS.invoke(peer, "start");
    }

    public void stop() {
        JS.invoke(peer, "stop");
    }
}
