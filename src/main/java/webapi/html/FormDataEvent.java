// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.dom.Event;
import webapi.xhr.FormData;

@SuppressWarnings({"unused"})
public class FormDataEvent extends Event {
    protected FormDataEvent(Object peer) {
        super(peer);
    }

    public static FormDataEvent wrap(Object peer) {
        return peer == null ? null : new FormDataEvent(peer);
    }

    public FormData getFormData() {
        return FormData.wrap(JS.get(peer, "formData"));
    }
}
