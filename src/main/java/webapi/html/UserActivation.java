// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class UserActivation extends WrappedObject {
    protected UserActivation(Object peer) {
        super(peer);
    }

    public static UserActivation wrap(Object peer) {
        return peer == null ? null : new UserActivation(peer);
    }

    public boolean isHasBeenActive() {
        return JS.getBoolean(peer, "hasBeenActive");
    }

    public boolean isIsActive() {
        return JS.getBoolean(peer, "isActive");
    }
}
