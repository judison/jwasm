// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;

@SuppressWarnings({"unused"})
public class HTMLTableCellElement extends HTMLElement {
    protected HTMLTableCellElement(Object peer) {
        super(peer);
    }

    public static HTMLTableCellElement wrap(Object peer) {
        return peer == null ? null : new HTMLTableCellElement(peer);
    }

    public int getColSpan() {
        return JS.getInt(peer, "colSpan");
    }

    public void setColSpan(int value) {
        JS.set(peer, "colSpan", value);
    }

    public int getRowSpan() {
        return JS.getInt(peer, "rowSpan");
    }

    public void setRowSpan(int value) {
        JS.set(peer, "rowSpan", value);
    }

    public String getHeaders() {
        return JS.string(JS.get(peer, "headers"));
    }

    public void setHeaders(String value) {
        JS.set(peer, "headers", JS.param(value));
    }

    public long getCellIndex() {
        return (long)JS.get(peer, "cellIndex");
    }

    public String getScope() {
        return JS.string(JS.get(peer, "scope"));
    }

    public void setScope(String value) {
        JS.set(peer, "scope", JS.param(value));
    }

    public String getAbbr() {
        return JS.string(JS.get(peer, "abbr"));
    }

    public void setAbbr(String value) {
        JS.set(peer, "abbr", JS.param(value));
    }

    public String getAlign() {
        return JS.string(JS.get(peer, "align"));
    }

    public void setAlign(String value) {
        JS.set(peer, "align", JS.param(value));
    }

    public String getAxis() {
        return JS.string(JS.get(peer, "axis"));
    }

    public void setAxis(String value) {
        JS.set(peer, "axis", JS.param(value));
    }

    public String getHeight() {
        return JS.string(JS.get(peer, "height"));
    }

    public void setHeight(String value) {
        JS.set(peer, "height", JS.param(value));
    }

    public String getWidth() {
        return JS.string(JS.get(peer, "width"));
    }

    public void setWidth(String value) {
        JS.set(peer, "width", JS.param(value));
    }

    public String getCh() {
        return JS.string(JS.get(peer, "ch"));
    }

    public void setCh(String value) {
        JS.set(peer, "ch", JS.param(value));
    }

    public String getChOff() {
        return JS.string(JS.get(peer, "chOff"));
    }

    public void setChOff(String value) {
        JS.set(peer, "chOff", JS.param(value));
    }

    public boolean isNoWrap() {
        return JS.getBoolean(peer, "noWrap");
    }

    public void setNoWrap(boolean value) {
        JS.set(peer, "noWrap", value);
    }

    public String getVAlign() {
        return JS.string(JS.get(peer, "vAlign"));
    }

    public void setVAlign(String value) {
        JS.set(peer, "vAlign", JS.param(value));
    }

    public String getBgColor() {
        return JS.string(JS.get(peer, "bgColor"));
    }

    public void setBgColor(String value) {
        JS.set(peer, "bgColor", JS.param(value));
    }
}
