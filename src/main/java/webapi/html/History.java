// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class History extends WrappedObject {
    protected History(Object peer) {
        super(peer);
    }

    public static History wrap(Object peer) {
        return peer == null ? null : new History(peer);
    }

    public int getLength() {
        return JS.getInt(peer, "length");
    }

    public WrappedObject getScrollRestoration() {
        return WrappedObject.wrap(JS.get(peer, "scrollRestoration"));
    }

    public void setScrollRestoration(WrappedObject value) {
        JS.set(peer, "scrollRestoration", value.peer);
    }

    public WrappedObject getState() {
        return WrappedObject.wrap(JS.get(peer, "state"));
    }

    public void go(long delta) {
        JS.invoke(peer, "go", delta);
    }

    public void go() {
        JS.invoke(peer, "go");
    }

    public void back() {
        JS.invoke(peer, "back");
    }

    public void forward() {
        JS.invoke(peer, "forward");
    }

    public void pushState(WrappedObject data, String unused, String url) {
        JS.invoke(peer, "pushState", JS.param(data), JS.param(unused), JS.param(url));
    }

    public void pushState(WrappedObject data, String unused) {
        JS.invoke(peer, "pushState", JS.param(data), JS.param(unused));
    }

    public void replaceState(WrappedObject data, String unused, String url) {
        JS.invoke(peer, "replaceState", JS.param(data), JS.param(unused), JS.param(url));
    }

    public void replaceState(WrappedObject data, String unused) {
        JS.invoke(peer, "replaceState", JS.param(data), JS.param(unused));
    }
}
