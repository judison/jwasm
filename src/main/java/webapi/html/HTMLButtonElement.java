// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.dom.NodeList;

@SuppressWarnings({"unused"})
public class HTMLButtonElement extends HTMLElement {
    protected HTMLButtonElement(Object peer) {
        super(peer);
    }

    public static HTMLButtonElement wrap(Object peer) {
        return peer == null ? null : new HTMLButtonElement(peer);
    }

    public boolean isDisabled() {
        return JS.getBoolean(peer, "disabled");
    }

    public void setDisabled(boolean value) {
        JS.set(peer, "disabled", value);
    }

    public HTMLFormElement getForm() {
        return HTMLFormElement.wrap(JS.get(peer, "form"));
    }

    public String getFormAction() {
        return JS.string(JS.get(peer, "formAction"));
    }

    public void setFormAction(String value) {
        JS.set(peer, "formAction", JS.param(value));
    }

    public String getFormEnctype() {
        return JS.string(JS.get(peer, "formEnctype"));
    }

    public void setFormEnctype(String value) {
        JS.set(peer, "formEnctype", JS.param(value));
    }

    public String getFormMethod() {
        return JS.string(JS.get(peer, "formMethod"));
    }

    public void setFormMethod(String value) {
        JS.set(peer, "formMethod", JS.param(value));
    }

    public boolean isFormNoValidate() {
        return JS.getBoolean(peer, "formNoValidate");
    }

    public void setFormNoValidate(boolean value) {
        JS.set(peer, "formNoValidate", value);
    }

    public String getFormTarget() {
        return JS.string(JS.get(peer, "formTarget"));
    }

    public void setFormTarget(String value) {
        JS.set(peer, "formTarget", JS.param(value));
    }

    public String getName() {
        return JS.string(JS.get(peer, "name"));
    }

    public void setName(String value) {
        JS.set(peer, "name", JS.param(value));
    }

    public String getType() {
        return JS.string(JS.get(peer, "type"));
    }

    public void setType(String value) {
        JS.set(peer, "type", JS.param(value));
    }

    public String getValue() {
        return JS.string(JS.get(peer, "value"));
    }

    public void setValue(String value) {
        JS.set(peer, "value", JS.param(value));
    }

    public boolean isWillValidate() {
        return JS.getBoolean(peer, "willValidate");
    }

    public ValidityState getValidity() {
        return ValidityState.wrap(JS.get(peer, "validity"));
    }

    public String getValidationMessage() {
        return JS.string(JS.get(peer, "validationMessage"));
    }

    public NodeList getLabels() {
        return NodeList.wrap(JS.get(peer, "labels"));
    }

    public boolean checkValidity() {
        return JS.invoke(peer, "checkValidity");
    }

    public boolean reportValidity() {
        return JS.invoke(peer, "reportValidity");
    }

    public void setCustomValidity(String error) {
        JS.invoke(peer, "setCustomValidity", JS.param(error));
    }
}
