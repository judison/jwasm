// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.dom.EventTarget;

@SuppressWarnings({"unused"})
public class TextTrackCue extends EventTarget {
    protected TextTrackCue(Object peer) {
        super(peer);
    }

    public static TextTrackCue wrap(Object peer) {
        return peer == null ? null : new TextTrackCue(peer);
    }

    public TextTrack getTrack() {
        return TextTrack.wrap(JS.get(peer, "track"));
    }

    public String getId() {
        return JS.string(JS.get(peer, "id"));
    }

    public void setId(String value) {
        JS.set(peer, "id", JS.param(value));
    }

    public double getStartTime() {
        return JS.getDouble(peer, "startTime");
    }

    public void setStartTime(double value) {
        JS.set(peer, "startTime", value);
    }

    public double getEndTime() {
        return JS.getDouble(peer, "endTime");
    }

    public void setEndTime(double value) {
        JS.set(peer, "endTime", value);
    }

    public boolean isPauseOnExit() {
        return JS.getBoolean(peer, "pauseOnExit");
    }

    public void setPauseOnExit(boolean value) {
        JS.set(peer, "pauseOnExit", value);
    }

    public EventHandler getOnenter() {
        return EventHandler.wrap(JS.get(peer, "onenter"));
    }

    public void setOnenter(EventHandler value) {
        JS.set(peer, "onenter", value.peer);
    }

    public EventHandler getOnexit() {
        return EventHandler.wrap(JS.get(peer, "onexit"));
    }

    public void setOnexit(EventHandler value) {
        JS.set(peer, "onexit", value.peer);
    }
}
