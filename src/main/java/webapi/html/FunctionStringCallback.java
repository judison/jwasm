// Generated file; DO NOT edit
package webapi.html;

import webapi.JSCallback;

@SuppressWarnings({"unused"})
public class FunctionStringCallback extends JSCallback {
    public FunctionStringCallback(Object peer) {
        super(peer);
    }

    public FunctionStringCallback(Callback javaCallback) {
        super(javaCallback);
    }

    public static FunctionStringCallback wrap(Object peer) {
        return peer == null ? null : new FunctionStringCallback(peer);
    }

    public interface Callback extends JSCallback.BaseCallback {
        void handle(String data);
    }
}
