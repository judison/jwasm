// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;

@SuppressWarnings({"unused"})
public class HTMLModElement extends HTMLElement {
    protected HTMLModElement(Object peer) {
        super(peer);
    }

    public static HTMLModElement wrap(Object peer) {
        return peer == null ? null : new HTMLModElement(peer);
    }

    public String getCite() {
        return JS.string(JS.get(peer, "cite"));
    }

    public void setCite(String value) {
        JS.set(peer, "cite", JS.param(value));
    }

    public String getDateTime() {
        return JS.string(JS.get(peer, "dateTime"));
    }

    public void setDateTime(String value) {
        JS.set(peer, "dateTime", JS.param(value));
    }
}
