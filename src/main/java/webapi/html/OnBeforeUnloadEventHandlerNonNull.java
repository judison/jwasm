// Generated file; DO NOT edit
package webapi.html;

import webapi.JSCallback;
import webapi.dom.Event;

@SuppressWarnings({"unused"})
public class OnBeforeUnloadEventHandlerNonNull extends JSCallback {
    public OnBeforeUnloadEventHandlerNonNull(Object peer) {
        super(peer);
    }

    public OnBeforeUnloadEventHandlerNonNull(Callback javaCallback) {
        super(javaCallback);
    }

    public static OnBeforeUnloadEventHandlerNonNull wrap(Object peer) {
        return peer == null ? null : new OnBeforeUnloadEventHandlerNonNull(peer);
    }

    public interface Callback extends JSCallback.BaseCallback {
        String handle(Event event);
    }
}
