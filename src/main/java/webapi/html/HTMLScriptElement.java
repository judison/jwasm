// Generated file; DO NOT edit
// Ignored static operation supports
package webapi.html;

import webapi.JS;
import webapi.dom.DOMTokenList;

@SuppressWarnings({"unused"})
public class HTMLScriptElement extends HTMLElement {
    protected HTMLScriptElement(Object peer) {
        super(peer);
    }

    public static HTMLScriptElement wrap(Object peer) {
        return peer == null ? null : new HTMLScriptElement(peer);
    }

    public String getSrc() {
        return JS.string(JS.get(peer, "src"));
    }

    public void setSrc(String value) {
        JS.set(peer, "src", JS.param(value));
    }

    public String getType() {
        return JS.string(JS.get(peer, "type"));
    }

    public void setType(String value) {
        JS.set(peer, "type", JS.param(value));
    }

    public boolean isNoModule() {
        return JS.getBoolean(peer, "noModule");
    }

    public void setNoModule(boolean value) {
        JS.set(peer, "noModule", value);
    }

    public boolean isAsync() {
        return JS.getBoolean(peer, "async");
    }

    public void setAsync(boolean value) {
        JS.set(peer, "async", value);
    }

    public boolean isDefer() {
        return JS.getBoolean(peer, "defer");
    }

    public void setDefer(boolean value) {
        JS.set(peer, "defer", value);
    }

    public String getCrossOrigin() {
        return JS.string(JS.get(peer, "crossOrigin"));
    }

    public void setCrossOrigin(String value) {
        JS.set(peer, "crossOrigin", JS.param(value));
    }

    public String getText() {
        return JS.string(JS.get(peer, "text"));
    }

    public void setText(String value) {
        JS.set(peer, "text", JS.param(value));
    }

    public String getIntegrity() {
        return JS.string(JS.get(peer, "integrity"));
    }

    public void setIntegrity(String value) {
        JS.set(peer, "integrity", JS.param(value));
    }

    public String getReferrerPolicy() {
        return JS.string(JS.get(peer, "referrerPolicy"));
    }

    public void setReferrerPolicy(String value) {
        JS.set(peer, "referrerPolicy", JS.param(value));
    }

    public DOMTokenList getBlocking() {
        return DOMTokenList.wrap(JS.get(peer, "blocking"));
    }

    public String getCharset() {
        return JS.string(JS.get(peer, "charset"));
    }

    public void setCharset(String value) {
        JS.set(peer, "charset", JS.param(value));
    }

    public String getEvent() {
        return JS.string(JS.get(peer, "event"));
    }

    public void setEvent(String value) {
        JS.set(peer, "event", JS.param(value));
    }

    public String getHtmlFor() {
        return JS.string(JS.get(peer, "htmlFor"));
    }

    public void setHtmlFor(String value) {
        JS.set(peer, "htmlFor", JS.param(value));
    }
}
