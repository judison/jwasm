// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;

@SuppressWarnings({"unused"})
public class HTMLOListElement extends HTMLElement {
    protected HTMLOListElement(Object peer) {
        super(peer);
    }

    public static HTMLOListElement wrap(Object peer) {
        return peer == null ? null : new HTMLOListElement(peer);
    }

    public boolean isReversed() {
        return JS.getBoolean(peer, "reversed");
    }

    public void setReversed(boolean value) {
        JS.set(peer, "reversed", value);
    }

    public long getStart() {
        return (long)JS.get(peer, "start");
    }

    public void setStart(long value) {
        JS.set(peer, "start", value);
    }

    public String getType() {
        return JS.string(JS.get(peer, "type"));
    }

    public void setType(String value) {
        JS.set(peer, "type", JS.param(value));
    }

    public boolean isCompact() {
        return JS.getBoolean(peer, "compact");
    }

    public void setCompact(boolean value) {
        JS.set(peer, "compact", value);
    }
}
