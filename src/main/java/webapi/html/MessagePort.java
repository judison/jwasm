// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.JSObject;
import webapi.JSSequence;
import webapi.WrappedObject;
import webapi.dom.EventTarget;

@SuppressWarnings({"unused"})
public class MessagePort extends EventTarget {
    protected MessagePort(Object peer) {
        super(peer);
    }

    public static MessagePort wrap(Object peer) {
        return peer == null ? null : new MessagePort(peer);
    }

    public EventHandler getOnmessage() {
        return EventHandler.wrap(JS.get(peer, "onmessage"));
    }

    public void setOnmessage(EventHandler value) {
        JS.set(peer, "onmessage", value.peer);
    }

    public EventHandler getOnmessageerror() {
        return EventHandler.wrap(JS.get(peer, "onmessageerror"));
    }

    public void setOnmessageerror(EventHandler value) {
        JS.set(peer, "onmessageerror", value.peer);
    }

    public void postMessage(WrappedObject message, JSSequence<WrappedObject> transfer) {
        JS.invoke(peer, "postMessage", JS.param(message), JS.param(transfer));
    }

    public void postMessage(WrappedObject message, JSObject options) {
        JS.invoke(peer, "postMessage", JS.param(message), JS.param(options));
    }

    public void postMessage(WrappedObject message) {
        JS.invoke(peer, "postMessage", JS.param(message));
    }

    public void start() {
        JS.invoke(peer, "start");
    }

    public void close() {
        JS.invoke(peer, "close");
    }
}
