// Generated file; DO NOT edit
package webapi.html;

import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class WorkletGlobalScope extends WrappedObject {
    protected WorkletGlobalScope(Object peer) {
        super(peer);
    }

    public static WorkletGlobalScope wrap(Object peer) {
        return peer == null ? null : new WorkletGlobalScope(peer);
    }
}
