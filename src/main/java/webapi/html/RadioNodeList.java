// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.dom.NodeList;

@SuppressWarnings({"unused"})
public class RadioNodeList extends NodeList {
    protected RadioNodeList(Object peer) {
        super(peer);
    }

    public static RadioNodeList wrap(Object peer) {
        return peer == null ? null : new RadioNodeList(peer);
    }

    public String getValue() {
        return JS.string(JS.get(peer, "value"));
    }

    public void setValue(String value) {
        JS.set(peer, "value", JS.param(value));
    }
}
