// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.JSObject;
import webapi.JSPromise;
import webapi.WrappedObject;
import webapi.dom.Node;

@SuppressWarnings({"unused"})
public class CustomElementRegistry extends WrappedObject {
    protected CustomElementRegistry(Object peer) {
        super(peer);
    }

    public static CustomElementRegistry wrap(Object peer) {
        return peer == null ? null : new CustomElementRegistry(peer);
    }

    public void define(String name, CustomElementConstructor constructor, JSObject options) {
        JS.invoke(peer, "define", JS.param(name), JS.param(constructor), JS.param(options));
    }

    public void define(String name, CustomElementConstructor constructor) {
        JS.invoke(peer, "define", JS.param(name), JS.param(constructor));
    }

    public WrappedObject get(String name) {
        return WrappedObject.wrap(JS.invoke(peer, "get", JS.param(name)));
    }

    public JSPromise<CustomElementConstructor> whenDefined(String name) {
        return JSPromise.wrap(CustomElementConstructor.class, JS.invoke(peer, "whenDefined", JS.param(name)));
    }

    public void upgrade(Node root) {
        JS.invoke(peer, "upgrade", JS.param(root));
    }
}
