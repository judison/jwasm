// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;

@SuppressWarnings({"unused"})
public class HTMLDataElement extends HTMLElement {
    protected HTMLDataElement(Object peer) {
        super(peer);
    }

    public static HTMLDataElement wrap(Object peer) {
        return peer == null ? null : new HTMLDataElement(peer);
    }

    public String getValue() {
        return JS.string(JS.get(peer, "value"));
    }

    public void setValue(String value) {
        JS.set(peer, "value", JS.param(value));
    }
}
