// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class TextMetrics extends WrappedObject {
    protected TextMetrics(Object peer) {
        super(peer);
    }

    public static TextMetrics wrap(Object peer) {
        return peer == null ? null : new TextMetrics(peer);
    }

    public double getWidth() {
        return JS.getDouble(peer, "width");
    }

    public double getActualBoundingBoxLeft() {
        return JS.getDouble(peer, "actualBoundingBoxLeft");
    }

    public double getActualBoundingBoxRight() {
        return JS.getDouble(peer, "actualBoundingBoxRight");
    }

    public double getFontBoundingBoxAscent() {
        return JS.getDouble(peer, "fontBoundingBoxAscent");
    }

    public double getFontBoundingBoxDescent() {
        return JS.getDouble(peer, "fontBoundingBoxDescent");
    }

    public double getActualBoundingBoxAscent() {
        return JS.getDouble(peer, "actualBoundingBoxAscent");
    }

    public double getActualBoundingBoxDescent() {
        return JS.getDouble(peer, "actualBoundingBoxDescent");
    }

    public double getEmHeightAscent() {
        return JS.getDouble(peer, "emHeightAscent");
    }

    public double getEmHeightDescent() {
        return JS.getDouble(peer, "emHeightDescent");
    }

    public double getHangingBaseline() {
        return JS.getDouble(peer, "hangingBaseline");
    }

    public double getAlphabeticBaseline() {
        return JS.getDouble(peer, "alphabeticBaseline");
    }

    public double getIdeographicBaseline() {
        return JS.getDouble(peer, "ideographicBaseline");
    }
}
