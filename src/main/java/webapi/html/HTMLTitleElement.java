// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;

@SuppressWarnings({"unused"})
public class HTMLTitleElement extends HTMLElement {
    protected HTMLTitleElement(Object peer) {
        super(peer);
    }

    public static HTMLTitleElement wrap(Object peer) {
        return peer == null ? null : new HTMLTitleElement(peer);
    }

    public String getText() {
        return JS.string(JS.get(peer, "text"));
    }

    public void setText(String value) {
        JS.set(peer, "text", JS.param(value));
    }
}
