// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class TextTrackCueList extends WrappedObject {
    protected TextTrackCueList(Object peer) {
        super(peer);
    }

    public static TextTrackCueList wrap(Object peer) {
        return peer == null ? null : new TextTrackCueList(peer);
    }

    public int getLength() {
        return JS.getInt(peer, "length");
    }

    public TextTrackCue getCueById(String id) {
        return TextTrackCue.wrap(JS.invoke(peer, "getCueById", JS.param(id)));
    }
}
