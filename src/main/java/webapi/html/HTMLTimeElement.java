// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;

@SuppressWarnings({"unused"})
public class HTMLTimeElement extends HTMLElement {
    protected HTMLTimeElement(Object peer) {
        super(peer);
    }

    public static HTMLTimeElement wrap(Object peer) {
        return peer == null ? null : new HTMLTimeElement(peer);
    }

    public String getDateTime() {
        return JS.string(JS.get(peer, "dateTime"));
    }

    public void setDateTime(String value) {
        JS.set(peer, "dateTime", JS.param(value));
    }
}
