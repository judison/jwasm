// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.JSObject;
import webapi.JSPromise;
import webapi.WrappedObject;
import webapi.dom.EventTarget;

@SuppressWarnings({"unused"})
public class WorkerGlobalScope extends EventTarget {
    protected WorkerGlobalScope(Object peer) {
        super(peer);
    }

    public static WorkerGlobalScope wrap(Object peer) {
        return peer == null ? null : new WorkerGlobalScope(peer);
    }

    public WorkerGlobalScope getSelf() {
        return WorkerGlobalScope.wrap(JS.get(peer, "self"));
    }

    public WorkerLocation getLocation() {
        return WorkerLocation.wrap(JS.get(peer, "location"));
    }

    public WorkerNavigator getNavigator() {
        return WorkerNavigator.wrap(JS.get(peer, "navigator"));
    }

    public WrappedObject getOnerror() {
        return WrappedObject.wrap(JS.get(peer, "onerror"));
    }

    public void setOnerror(WrappedObject value) {
        JS.set(peer, "onerror", value.peer);
    }

    public EventHandler getOnlanguagechange() {
        return EventHandler.wrap(JS.get(peer, "onlanguagechange"));
    }

    public void setOnlanguagechange(EventHandler value) {
        JS.set(peer, "onlanguagechange", value.peer);
    }

    public EventHandler getOnoffline() {
        return EventHandler.wrap(JS.get(peer, "onoffline"));
    }

    public void setOnoffline(EventHandler value) {
        JS.set(peer, "onoffline", value.peer);
    }

    public EventHandler getOnonline() {
        return EventHandler.wrap(JS.get(peer, "ononline"));
    }

    public void setOnonline(EventHandler value) {
        JS.set(peer, "ononline", value.peer);
    }

    public EventHandler getOnrejectionhandled() {
        return EventHandler.wrap(JS.get(peer, "onrejectionhandled"));
    }

    public void setOnrejectionhandled(EventHandler value) {
        JS.set(peer, "onrejectionhandled", value.peer);
    }

    public EventHandler getOnunhandledrejection() {
        return EventHandler.wrap(JS.get(peer, "onunhandledrejection"));
    }

    public void setOnunhandledrejection(EventHandler value) {
        JS.set(peer, "onunhandledrejection", value.peer);
    }

    public boolean isIsSecureContext() {
        return JS.getBoolean(peer, "isSecureContext");
    }

    public boolean isCrossOriginIsolated() {
        return JS.getBoolean(peer, "crossOriginIsolated");
    }

    public void importScripts(String urls) {
        JS.invoke(peer, "importScripts", JS.param(urls));
    }

    public void reportError(WrappedObject e) {
        JS.invoke(peer, "reportError", JS.param(e));
    }

    public String btoa(String data) {
        return JS.string(JS.invoke(peer, "btoa", JS.param(data)));
    }

    public String atob(String data) {
        return JS.string(JS.invoke(peer, "atob", JS.param(data)));
    }

    public long setTimeout(WrappedObject handler, long timeout, WrappedObject arguments) {
        return JS.invoke(peer, "setTimeout", JS.param(handler), timeout, JS.param(arguments));
    }

    public long setTimeout(WrappedObject handler, WrappedObject arguments) {
        return JS.invoke(peer, "setTimeout", JS.param(handler), JS.param(arguments));
    }

    public void clearTimeout(long id) {
        JS.invoke(peer, "clearTimeout", id);
    }

    public void clearTimeout() {
        JS.invoke(peer, "clearTimeout");
    }

    public long setInterval(WrappedObject handler, long timeout, WrappedObject arguments) {
        return JS.invoke(peer, "setInterval", JS.param(handler), timeout, JS.param(arguments));
    }

    public long setInterval(WrappedObject handler, WrappedObject arguments) {
        return JS.invoke(peer, "setInterval", JS.param(handler), JS.param(arguments));
    }

    public void clearInterval(long id) {
        JS.invoke(peer, "clearInterval", id);
    }

    public void clearInterval() {
        JS.invoke(peer, "clearInterval");
    }

    public void queueMicrotask(WrappedObject callback) {
        JS.invoke(peer, "queueMicrotask", JS.param(callback));
    }

    public JSPromise<ImageBitmap> createImageBitmap(WrappedObject image, JSObject options) {
        return JSPromise.wrap(ImageBitmap.class, JS.invoke(peer, "createImageBitmap", JS.param(image), JS.param(options)));
    }

    public JSPromise<ImageBitmap> createImageBitmap(WrappedObject image) {
        return JSPromise.wrap(ImageBitmap.class, JS.invoke(peer, "createImageBitmap", JS.param(image)));
    }

    public JSPromise<ImageBitmap> createImageBitmap(WrappedObject image, long sx, long sy, long sw,
            long sh, JSObject options) {
        return JSPromise.wrap(ImageBitmap.class, JS.invoke(peer, "createImageBitmap", JS.param(image), sx, sy, sw, sh, JS.param(options)));
    }

    public JSPromise<ImageBitmap> createImageBitmap(WrappedObject image, long sx, long sy, long sw,
            long sh) {
        return JSPromise.wrap(ImageBitmap.class, JS.invoke(peer, "createImageBitmap", JS.param(image), sx, sy, sw, sh));
    }

    public WrappedObject structuredClone(WrappedObject value, JSObject options) {
        return WrappedObject.wrap(JS.invoke(peer, "structuredClone", JS.param(value), JS.param(options)));
    }

    public WrappedObject structuredClone(WrappedObject value) {
        return WrappedObject.wrap(JS.invoke(peer, "structuredClone", JS.param(value)));
    }
}
