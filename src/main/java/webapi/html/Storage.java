// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class Storage extends WrappedObject {
    protected Storage(Object peer) {
        super(peer);
    }

    public static Storage wrap(Object peer) {
        return peer == null ? null : new Storage(peer);
    }

    public int getLength() {
        return JS.getInt(peer, "length");
    }

    public String key(int index) {
        return JS.string(JS.invoke(peer, "key", index));
    }

    public void clear() {
        JS.invoke(peer, "clear");
    }
}
