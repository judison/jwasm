// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;

@SuppressWarnings({"unused"})
public class HTMLSourceElement extends HTMLElement {
    protected HTMLSourceElement(Object peer) {
        super(peer);
    }

    public static HTMLSourceElement wrap(Object peer) {
        return peer == null ? null : new HTMLSourceElement(peer);
    }

    public String getSrc() {
        return JS.string(JS.get(peer, "src"));
    }

    public void setSrc(String value) {
        JS.set(peer, "src", JS.param(value));
    }

    public String getType() {
        return JS.string(JS.get(peer, "type"));
    }

    public void setType(String value) {
        JS.set(peer, "type", JS.param(value));
    }

    public String getSrcset() {
        return JS.string(JS.get(peer, "srcset"));
    }

    public void setSrcset(String value) {
        JS.set(peer, "srcset", JS.param(value));
    }

    public String getSizes() {
        return JS.string(JS.get(peer, "sizes"));
    }

    public void setSizes(String value) {
        JS.set(peer, "sizes", JS.param(value));
    }

    public String getMedia() {
        return JS.string(JS.get(peer, "media"));
    }

    public void setMedia(String value) {
        JS.set(peer, "media", JS.param(value));
    }

    public int getWidth() {
        return JS.getInt(peer, "width");
    }

    public void setWidth(int value) {
        JS.set(peer, "width", value);
    }

    public int getHeight() {
        return JS.getInt(peer, "height");
    }

    public void setHeight(int value) {
        JS.set(peer, "height", value);
    }
}
