// Generated file; DO NOT edit
package webapi.html;

import webapi.dom.HTMLCollection;

@SuppressWarnings({"unused"})
public class HTMLFormControlsCollection extends HTMLCollection {
    protected HTMLFormControlsCollection(Object peer) {
        super(peer);
    }

    public static HTMLFormControlsCollection wrap(Object peer) {
        return peer == null ? null : new HTMLFormControlsCollection(peer);
    }
}
