// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class Navigator extends WrappedObject {
    protected Navigator(Object peer) {
        super(peer);
    }

    public static Navigator wrap(Object peer) {
        return peer == null ? null : new Navigator(peer);
    }

    public UserActivation getUserActivation() {
        return UserActivation.wrap(JS.get(peer, "userActivation"));
    }

    public String getAppCodeName() {
        return JS.string(JS.get(peer, "appCodeName"));
    }

    public String getAppName() {
        return JS.string(JS.get(peer, "appName"));
    }

    public String getAppVersion() {
        return JS.string(JS.get(peer, "appVersion"));
    }

    public String getPlatform() {
        return JS.string(JS.get(peer, "platform"));
    }

    public String getProduct() {
        return JS.string(JS.get(peer, "product"));
    }

    public String getProductSub() {
        return JS.string(JS.get(peer, "productSub"));
    }

    public String getUserAgent() {
        return JS.string(JS.get(peer, "userAgent"));
    }

    public String getVendor() {
        return JS.string(JS.get(peer, "vendor"));
    }

    public String getVendorSub() {
        return JS.string(JS.get(peer, "vendorSub"));
    }

    public String getOscpu() {
        return JS.string(JS.get(peer, "oscpu"));
    }

    public String getLanguage() {
        return JS.string(JS.get(peer, "language"));
    }

    public WrappedObject getLanguages() {
        return WrappedObject.wrap(JS.get(peer, "languages"));
    }

    public boolean isOnLine() {
        return JS.getBoolean(peer, "onLine");
    }

    public boolean isCookieEnabled() {
        return JS.getBoolean(peer, "cookieEnabled");
    }

    public PluginArray getPlugins() {
        return PluginArray.wrap(JS.get(peer, "plugins"));
    }

    public MimeTypeArray getMimeTypes() {
        return MimeTypeArray.wrap(JS.get(peer, "mimeTypes"));
    }

    public boolean isPdfViewerEnabled() {
        return JS.getBoolean(peer, "pdfViewerEnabled");
    }

    public long getHardwareConcurrency() {
        return (long)JS.get(peer, "hardwareConcurrency");
    }

    public boolean taintEnabled() {
        return JS.invoke(peer, "taintEnabled");
    }

    public void registerProtocolHandler(String scheme, String url) {
        JS.invoke(peer, "registerProtocolHandler", JS.param(scheme), JS.param(url));
    }

    public void unregisterProtocolHandler(String scheme, String url) {
        JS.invoke(peer, "unregisterProtocolHandler", JS.param(scheme), JS.param(url));
    }

    public boolean javaEnabled() {
        return JS.invoke(peer, "javaEnabled");
    }
}
