// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.JSPromise;

@SuppressWarnings({"unused"})
public class HTMLImageElement extends HTMLElement {
    protected HTMLImageElement(Object peer) {
        super(peer);
    }

    public static HTMLImageElement wrap(Object peer) {
        return peer == null ? null : new HTMLImageElement(peer);
    }

    public String getAlt() {
        return JS.string(JS.get(peer, "alt"));
    }

    public void setAlt(String value) {
        JS.set(peer, "alt", JS.param(value));
    }

    public String getSrc() {
        return JS.string(JS.get(peer, "src"));
    }

    public void setSrc(String value) {
        JS.set(peer, "src", JS.param(value));
    }

    public String getSrcset() {
        return JS.string(JS.get(peer, "srcset"));
    }

    public void setSrcset(String value) {
        JS.set(peer, "srcset", JS.param(value));
    }

    public String getSizes() {
        return JS.string(JS.get(peer, "sizes"));
    }

    public void setSizes(String value) {
        JS.set(peer, "sizes", JS.param(value));
    }

    public String getCrossOrigin() {
        return JS.string(JS.get(peer, "crossOrigin"));
    }

    public void setCrossOrigin(String value) {
        JS.set(peer, "crossOrigin", JS.param(value));
    }

    public String getUseMap() {
        return JS.string(JS.get(peer, "useMap"));
    }

    public void setUseMap(String value) {
        JS.set(peer, "useMap", JS.param(value));
    }

    public boolean isIsMap() {
        return JS.getBoolean(peer, "isMap");
    }

    public void setIsMap(boolean value) {
        JS.set(peer, "isMap", value);
    }

    public int getWidth() {
        return JS.getInt(peer, "width");
    }

    public void setWidth(int value) {
        JS.set(peer, "width", value);
    }

    public int getHeight() {
        return JS.getInt(peer, "height");
    }

    public void setHeight(int value) {
        JS.set(peer, "height", value);
    }

    public int getNaturalWidth() {
        return JS.getInt(peer, "naturalWidth");
    }

    public int getNaturalHeight() {
        return JS.getInt(peer, "naturalHeight");
    }

    public boolean isComplete() {
        return JS.getBoolean(peer, "complete");
    }

    public String getCurrentSrc() {
        return JS.string(JS.get(peer, "currentSrc"));
    }

    public String getReferrerPolicy() {
        return JS.string(JS.get(peer, "referrerPolicy"));
    }

    public void setReferrerPolicy(String value) {
        JS.set(peer, "referrerPolicy", JS.param(value));
    }

    public String getDecoding() {
        return JS.string(JS.get(peer, "decoding"));
    }

    public void setDecoding(String value) {
        JS.set(peer, "decoding", JS.param(value));
    }

    public String getLoading() {
        return JS.string(JS.get(peer, "loading"));
    }

    public void setLoading(String value) {
        JS.set(peer, "loading", JS.param(value));
    }

    public String getName() {
        return JS.string(JS.get(peer, "name"));
    }

    public void setName(String value) {
        JS.set(peer, "name", JS.param(value));
    }

    public String getLowsrc() {
        return JS.string(JS.get(peer, "lowsrc"));
    }

    public void setLowsrc(String value) {
        JS.set(peer, "lowsrc", JS.param(value));
    }

    public String getAlign() {
        return JS.string(JS.get(peer, "align"));
    }

    public void setAlign(String value) {
        JS.set(peer, "align", JS.param(value));
    }

    public int getHspace() {
        return JS.getInt(peer, "hspace");
    }

    public void setHspace(int value) {
        JS.set(peer, "hspace", value);
    }

    public int getVspace() {
        return JS.getInt(peer, "vspace");
    }

    public void setVspace(int value) {
        JS.set(peer, "vspace", value);
    }

    public String getLongDesc() {
        return JS.string(JS.get(peer, "longDesc"));
    }

    public void setLongDesc(String value) {
        JS.set(peer, "longDesc", JS.param(value));
    }

    public String getBorder() {
        return JS.string(JS.get(peer, "border"));
    }

    public void setBorder(String value) {
        JS.set(peer, "border", JS.param(value));
    }

    public JSPromise<Void> decode() {
        return JSPromise.wrap(Void.class, JS.invoke(peer, "decode"));
    }
}
