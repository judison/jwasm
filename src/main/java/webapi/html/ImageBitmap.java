// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class ImageBitmap extends WrappedObject {
    protected ImageBitmap(Object peer) {
        super(peer);
    }

    public static ImageBitmap wrap(Object peer) {
        return peer == null ? null : new ImageBitmap(peer);
    }

    public int getWidth() {
        return JS.getInt(peer, "width");
    }

    public int getHeight() {
        return JS.getInt(peer, "height");
    }

    public void close() {
        JS.invoke(peer, "close");
    }
}
