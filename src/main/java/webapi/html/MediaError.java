// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class MediaError extends WrappedObject {
    protected MediaError(Object peer) {
        super(peer);
    }

    public static MediaError wrap(Object peer) {
        return peer == null ? null : new MediaError(peer);
    }

    public int getCode() {
        return JS.getInt(peer, "code");
    }

    public String getMessage() {
        return JS.string(JS.get(peer, "message"));
    }
}
