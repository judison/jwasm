// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;
import webapi.dom.Event;

@SuppressWarnings({"unused"})
public class ErrorEvent extends Event {
    protected ErrorEvent(Object peer) {
        super(peer);
    }

    public static ErrorEvent wrap(Object peer) {
        return peer == null ? null : new ErrorEvent(peer);
    }

    public String getMessage() {
        return JS.string(JS.get(peer, "message"));
    }

    public String getFilename() {
        return JS.string(JS.get(peer, "filename"));
    }

    public int getLineno() {
        return JS.getInt(peer, "lineno");
    }

    public int getColno() {
        return JS.getInt(peer, "colno");
    }

    public WrappedObject getError() {
        return WrappedObject.wrap(JS.get(peer, "error"));
    }
}
