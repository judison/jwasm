// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class VideoTrack extends WrappedObject {
    protected VideoTrack(Object peer) {
        super(peer);
    }

    public static VideoTrack wrap(Object peer) {
        return peer == null ? null : new VideoTrack(peer);
    }

    public String getId() {
        return JS.string(JS.get(peer, "id"));
    }

    public String getKind() {
        return JS.string(JS.get(peer, "kind"));
    }

    public String getLabel() {
        return JS.string(JS.get(peer, "label"));
    }

    public String getLanguage() {
        return JS.string(JS.get(peer, "language"));
    }

    public boolean isSelected() {
        return JS.getBoolean(peer, "selected");
    }

    public void setSelected(boolean value) {
        JS.set(peer, "selected", value);
    }
}
