// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.JSObject;
import webapi.JSSequence;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class Path2D extends WrappedObject {
    protected Path2D(Object peer) {
        super(peer);
    }

    public static Path2D wrap(Object peer) {
        return peer == null ? null : new Path2D(peer);
    }

    public void addPath(Path2D path, JSObject transform) {
        JS.invoke(peer, "addPath", JS.param(path), JS.param(transform));
    }

    public void addPath(Path2D path) {
        JS.invoke(peer, "addPath", JS.param(path));
    }

    public void closePath() {
        JS.invoke(peer, "closePath");
    }

    public void moveTo(double x, double y) {
        JS.invoke(peer, "moveTo", x, y);
    }

    public void lineTo(double x, double y) {
        JS.invoke(peer, "lineTo", x, y);
    }

    public void quadraticCurveTo(double cpx, double cpy, double x, double y) {
        JS.invoke(peer, "quadraticCurveTo", cpx, cpy, x, y);
    }

    public void bezierCurveTo(double cp1x, double cp1y, double cp2x, double cp2y, double x,
            double y) {
        JS.invoke(peer, "bezierCurveTo", cp1x, cp1y, cp2x, cp2y, x, y);
    }

    public void arcTo(double x1, double y1, double x2, double y2, double radius) {
        JS.invoke(peer, "arcTo", x1, y1, x2, y2, radius);
    }

    public void rect(double x, double y, double w, double h) {
        JS.invoke(peer, "rect", x, y, w, h);
    }

    public void roundRect(double x, double y, double w, double h, double radii) {
        JS.invoke(peer, "roundRect", x, y, w, h, radii);
    }

    public void roundRect(double x, double y, double w, double h, JSObject radii) {
        JS.invoke(peer, "roundRect", x, y, w, h, JS.param(radii));
    }

    public void roundRect(double x, double y, double w, double h, JSSequence<WrappedObject> radii) {
        JS.invoke(peer, "roundRect", x, y, w, h, JS.param(radii));
    }

    public void roundRect(double x, double y, double w, double h) {
        JS.invoke(peer, "roundRect", x, y, w, h);
    }

    public void arc(double x, double y, double radius, double startAngle, double endAngle,
            boolean counterclockwise) {
        JS.invoke(peer, "arc", x, y, radius, startAngle, endAngle, counterclockwise);
    }

    public void arc(double x, double y, double radius, double startAngle, double endAngle) {
        JS.invoke(peer, "arc", x, y, radius, startAngle, endAngle);
    }

    public void ellipse(double x, double y, double radiusX, double radiusY, double rotation,
            double startAngle, double endAngle, boolean counterclockwise) {
        JS.invoke(peer, "ellipse", x, y, radiusX, radiusY, rotation, startAngle, endAngle, counterclockwise);
    }

    public void ellipse(double x, double y, double radiusX, double radiusY, double rotation,
            double startAngle, double endAngle) {
        JS.invoke(peer, "ellipse", x, y, radiusX, radiusY, rotation, startAngle, endAngle);
    }
}
