// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;

@SuppressWarnings({"unused"})
public class HTMLHtmlElement extends HTMLElement {
    protected HTMLHtmlElement(Object peer) {
        super(peer);
    }

    public static HTMLHtmlElement wrap(Object peer) {
        return peer == null ? null : new HTMLHtmlElement(peer);
    }

    public String getVersion() {
        return JS.string(JS.get(peer, "version"));
    }

    public void setVersion(String value) {
        JS.set(peer, "version", JS.param(value));
    }
}
