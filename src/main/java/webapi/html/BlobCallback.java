// Generated file; DO NOT edit
package webapi.html;

import webapi.JSCallback;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class BlobCallback extends JSCallback {
    public BlobCallback(Object peer) {
        super(peer);
    }

    public BlobCallback(Callback javaCallback) {
        super(javaCallback);
    }

    public static BlobCallback wrap(Object peer) {
        return peer == null ? null : new BlobCallback(peer);
    }

    public interface Callback extends JSCallback.BaseCallback {
        void handle(WrappedObject blob);
    }
}
