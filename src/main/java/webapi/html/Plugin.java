// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class Plugin extends WrappedObject {
    protected Plugin(Object peer) {
        super(peer);
    }

    public static Plugin wrap(Object peer) {
        return peer == null ? null : new Plugin(peer);
    }

    public String getName() {
        return JS.string(JS.get(peer, "name"));
    }

    public String getDescription() {
        return JS.string(JS.get(peer, "description"));
    }

    public String getFilename() {
        return JS.string(JS.get(peer, "filename"));
    }

    public int getLength() {
        return JS.getInt(peer, "length");
    }
}
