// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class BarProp extends WrappedObject {
    protected BarProp(Object peer) {
        super(peer);
    }

    public static BarProp wrap(Object peer) {
        return peer == null ? null : new BarProp(peer);
    }

    public boolean isVisible() {
        return JS.getBoolean(peer, "visible");
    }
}
