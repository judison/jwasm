// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class MessageChannel extends WrappedObject {
    protected MessageChannel(Object peer) {
        super(peer);
    }

    public static MessageChannel wrap(Object peer) {
        return peer == null ? null : new MessageChannel(peer);
    }

    public MessagePort getPort1() {
        return MessagePort.wrap(JS.get(peer, "port1"));
    }

    public MessagePort getPort2() {
        return MessagePort.wrap(JS.get(peer, "port2"));
    }
}
