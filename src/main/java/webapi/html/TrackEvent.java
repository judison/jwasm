// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;
import webapi.dom.Event;

@SuppressWarnings({"unused"})
public class TrackEvent extends Event {
    protected TrackEvent(Object peer) {
        super(peer);
    }

    public static TrackEvent wrap(Object peer) {
        return peer == null ? null : new TrackEvent(peer);
    }

    public WrappedObject getTrack() {
        return WrappedObject.wrap(JS.get(peer, "track"));
    }
}
