// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;

@SuppressWarnings({"unused"})
public class HTMLFontElement extends HTMLElement {
    protected HTMLFontElement(Object peer) {
        super(peer);
    }

    public static HTMLFontElement wrap(Object peer) {
        return peer == null ? null : new HTMLFontElement(peer);
    }

    public String getColor() {
        return JS.string(JS.get(peer, "color"));
    }

    public void setColor(String value) {
        JS.set(peer, "color", JS.param(value));
    }

    public String getFace() {
        return JS.string(JS.get(peer, "face"));
    }

    public void setFace(String value) {
        JS.set(peer, "face", JS.param(value));
    }

    public String getSize() {
        return JS.string(JS.get(peer, "size"));
    }

    public void setSize(String value) {
        JS.set(peer, "size", JS.param(value));
    }
}
