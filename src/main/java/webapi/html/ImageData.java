// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class ImageData extends WrappedObject {
    protected ImageData(Object peer) {
        super(peer);
    }

    public static ImageData wrap(Object peer) {
        return peer == null ? null : new ImageData(peer);
    }

    public int getWidth() {
        return JS.getInt(peer, "width");
    }

    public int getHeight() {
        return JS.getInt(peer, "height");
    }

    public WrappedObject getData() {
        return WrappedObject.wrap(JS.get(peer, "data"));
    }

    public WrappedObject getColorSpace() {
        return WrappedObject.wrap(JS.get(peer, "colorSpace"));
    }
}
