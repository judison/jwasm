// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;

@SuppressWarnings({"unused"})
public class HTMLDListElement extends HTMLElement {
    protected HTMLDListElement(Object peer) {
        super(peer);
    }

    public static HTMLDListElement wrap(Object peer) {
        return peer == null ? null : new HTMLDListElement(peer);
    }

    public boolean isCompact() {
        return JS.getBoolean(peer, "compact");
    }

    public void setCompact(boolean value) {
        JS.set(peer, "compact", value);
    }
}
