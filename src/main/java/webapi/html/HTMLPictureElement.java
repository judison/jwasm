// Generated file; DO NOT edit
package webapi.html;

@SuppressWarnings({"unused"})
public class HTMLPictureElement extends HTMLElement {
    protected HTMLPictureElement(Object peer) {
        super(peer);
    }

    public static HTMLPictureElement wrap(Object peer) {
        return peer == null ? null : new HTMLPictureElement(peer);
    }
}
