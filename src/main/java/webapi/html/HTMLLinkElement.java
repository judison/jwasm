// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.css.CSSStyleSheet;
import webapi.dom.DOMTokenList;

@SuppressWarnings({"unused"})
public class HTMLLinkElement extends HTMLElement {
    protected HTMLLinkElement(Object peer) {
        super(peer);
    }

    public static HTMLLinkElement wrap(Object peer) {
        return peer == null ? null : new HTMLLinkElement(peer);
    }

    public String getHref() {
        return JS.string(JS.get(peer, "href"));
    }

    public void setHref(String value) {
        JS.set(peer, "href", JS.param(value));
    }

    public String getCrossOrigin() {
        return JS.string(JS.get(peer, "crossOrigin"));
    }

    public void setCrossOrigin(String value) {
        JS.set(peer, "crossOrigin", JS.param(value));
    }

    public String getRel() {
        return JS.string(JS.get(peer, "rel"));
    }

    public void setRel(String value) {
        JS.set(peer, "rel", JS.param(value));
    }

    public String getAs() {
        return JS.string(JS.get(peer, "as"));
    }

    public void setAs(String value) {
        JS.set(peer, "as", JS.param(value));
    }

    public DOMTokenList getRelList() {
        return DOMTokenList.wrap(JS.get(peer, "relList"));
    }

    public String getMedia() {
        return JS.string(JS.get(peer, "media"));
    }

    public void setMedia(String value) {
        JS.set(peer, "media", JS.param(value));
    }

    public String getIntegrity() {
        return JS.string(JS.get(peer, "integrity"));
    }

    public void setIntegrity(String value) {
        JS.set(peer, "integrity", JS.param(value));
    }

    public String getHreflang() {
        return JS.string(JS.get(peer, "hreflang"));
    }

    public void setHreflang(String value) {
        JS.set(peer, "hreflang", JS.param(value));
    }

    public String getType() {
        return JS.string(JS.get(peer, "type"));
    }

    public void setType(String value) {
        JS.set(peer, "type", JS.param(value));
    }

    public DOMTokenList getSizes() {
        return DOMTokenList.wrap(JS.get(peer, "sizes"));
    }

    public String getImageSrcset() {
        return JS.string(JS.get(peer, "imageSrcset"));
    }

    public void setImageSrcset(String value) {
        JS.set(peer, "imageSrcset", JS.param(value));
    }

    public String getImageSizes() {
        return JS.string(JS.get(peer, "imageSizes"));
    }

    public void setImageSizes(String value) {
        JS.set(peer, "imageSizes", JS.param(value));
    }

    public String getReferrerPolicy() {
        return JS.string(JS.get(peer, "referrerPolicy"));
    }

    public void setReferrerPolicy(String value) {
        JS.set(peer, "referrerPolicy", JS.param(value));
    }

    public DOMTokenList getBlocking() {
        return DOMTokenList.wrap(JS.get(peer, "blocking"));
    }

    public boolean isDisabled() {
        return JS.getBoolean(peer, "disabled");
    }

    public void setDisabled(boolean value) {
        JS.set(peer, "disabled", value);
    }

    public String getCharset() {
        return JS.string(JS.get(peer, "charset"));
    }

    public void setCharset(String value) {
        JS.set(peer, "charset", JS.param(value));
    }

    public String getRev() {
        return JS.string(JS.get(peer, "rev"));
    }

    public void setRev(String value) {
        JS.set(peer, "rev", JS.param(value));
    }

    public String getTarget() {
        return JS.string(JS.get(peer, "target"));
    }

    public void setTarget(String value) {
        JS.set(peer, "target", JS.param(value));
    }

    public CSSStyleSheet getSheet() {
        return CSSStyleSheet.wrap(JS.get(peer, "sheet"));
    }
}
