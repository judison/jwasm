// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;

@SuppressWarnings({"unused"})
public class HTMLLegendElement extends HTMLElement {
    protected HTMLLegendElement(Object peer) {
        super(peer);
    }

    public static HTMLLegendElement wrap(Object peer) {
        return peer == null ? null : new HTMLLegendElement(peer);
    }

    public HTMLFormElement getForm() {
        return HTMLFormElement.wrap(JS.get(peer, "form"));
    }

    public String getAlign() {
        return JS.string(JS.get(peer, "align"));
    }

    public void setAlign(String value) {
        JS.set(peer, "align", JS.param(value));
    }
}
