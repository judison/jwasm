// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.dom.EventTarget;

@SuppressWarnings({"unused"})
public class AudioTrackList extends EventTarget {
    protected AudioTrackList(Object peer) {
        super(peer);
    }

    public static AudioTrackList wrap(Object peer) {
        return peer == null ? null : new AudioTrackList(peer);
    }

    public int getLength() {
        return JS.getInt(peer, "length");
    }

    public EventHandler getOnchange() {
        return EventHandler.wrap(JS.get(peer, "onchange"));
    }

    public void setOnchange(EventHandler value) {
        JS.set(peer, "onchange", value.peer);
    }

    public EventHandler getOnaddtrack() {
        return EventHandler.wrap(JS.get(peer, "onaddtrack"));
    }

    public void setOnaddtrack(EventHandler value) {
        JS.set(peer, "onaddtrack", value.peer);
    }

    public EventHandler getOnremovetrack() {
        return EventHandler.wrap(JS.get(peer, "onremovetrack"));
    }

    public void setOnremovetrack(EventHandler value) {
        JS.set(peer, "onremovetrack", value.peer);
    }

    public AudioTrack getTrackById(String id) {
        return AudioTrack.wrap(JS.invoke(peer, "getTrackById", JS.param(id)));
    }
}
