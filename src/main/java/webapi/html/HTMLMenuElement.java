// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;

@SuppressWarnings({"unused"})
public class HTMLMenuElement extends HTMLElement {
    protected HTMLMenuElement(Object peer) {
        super(peer);
    }

    public static HTMLMenuElement wrap(Object peer) {
        return peer == null ? null : new HTMLMenuElement(peer);
    }

    public boolean isCompact() {
        return JS.getBoolean(peer, "compact");
    }

    public void setCompact(boolean value) {
        JS.set(peer, "compact", value);
    }
}
