// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.dom.DOMTokenList;

@SuppressWarnings({"unused"})
public class HTMLAnchorElement extends HTMLElement {
    protected HTMLAnchorElement(Object peer) {
        super(peer);
    }

    public static HTMLAnchorElement wrap(Object peer) {
        return peer == null ? null : new HTMLAnchorElement(peer);
    }

    public String getTarget() {
        return JS.string(JS.get(peer, "target"));
    }

    public void setTarget(String value) {
        JS.set(peer, "target", JS.param(value));
    }

    public String getDownload() {
        return JS.string(JS.get(peer, "download"));
    }

    public void setDownload(String value) {
        JS.set(peer, "download", JS.param(value));
    }

    public String getPing() {
        return JS.string(JS.get(peer, "ping"));
    }

    public void setPing(String value) {
        JS.set(peer, "ping", JS.param(value));
    }

    public String getRel() {
        return JS.string(JS.get(peer, "rel"));
    }

    public void setRel(String value) {
        JS.set(peer, "rel", JS.param(value));
    }

    public DOMTokenList getRelList() {
        return DOMTokenList.wrap(JS.get(peer, "relList"));
    }

    public String getHreflang() {
        return JS.string(JS.get(peer, "hreflang"));
    }

    public void setHreflang(String value) {
        JS.set(peer, "hreflang", JS.param(value));
    }

    public String getType() {
        return JS.string(JS.get(peer, "type"));
    }

    public void setType(String value) {
        JS.set(peer, "type", JS.param(value));
    }

    public String getText() {
        return JS.string(JS.get(peer, "text"));
    }

    public void setText(String value) {
        JS.set(peer, "text", JS.param(value));
    }

    public String getReferrerPolicy() {
        return JS.string(JS.get(peer, "referrerPolicy"));
    }

    public void setReferrerPolicy(String value) {
        JS.set(peer, "referrerPolicy", JS.param(value));
    }

    public String getCoords() {
        return JS.string(JS.get(peer, "coords"));
    }

    public void setCoords(String value) {
        JS.set(peer, "coords", JS.param(value));
    }

    public String getCharset() {
        return JS.string(JS.get(peer, "charset"));
    }

    public void setCharset(String value) {
        JS.set(peer, "charset", JS.param(value));
    }

    public String getName() {
        return JS.string(JS.get(peer, "name"));
    }

    public void setName(String value) {
        JS.set(peer, "name", JS.param(value));
    }

    public String getRev() {
        return JS.string(JS.get(peer, "rev"));
    }

    public void setRev(String value) {
        JS.set(peer, "rev", JS.param(value));
    }

    public String getShape() {
        return JS.string(JS.get(peer, "shape"));
    }

    public void setShape(String value) {
        JS.set(peer, "shape", JS.param(value));
    }

    public String getHref() {
        return JS.string(JS.get(peer, "href"));
    }

    public void setHref(String value) {
        JS.set(peer, "href", JS.param(value));
    }

    public String getOrigin() {
        return JS.string(JS.get(peer, "origin"));
    }

    public String getProtocol() {
        return JS.string(JS.get(peer, "protocol"));
    }

    public void setProtocol(String value) {
        JS.set(peer, "protocol", JS.param(value));
    }

    public String getUsername() {
        return JS.string(JS.get(peer, "username"));
    }

    public void setUsername(String value) {
        JS.set(peer, "username", JS.param(value));
    }

    public String getPassword() {
        return JS.string(JS.get(peer, "password"));
    }

    public void setPassword(String value) {
        JS.set(peer, "password", JS.param(value));
    }

    public String getHost() {
        return JS.string(JS.get(peer, "host"));
    }

    public void setHost(String value) {
        JS.set(peer, "host", JS.param(value));
    }

    public String getHostname() {
        return JS.string(JS.get(peer, "hostname"));
    }

    public void setHostname(String value) {
        JS.set(peer, "hostname", JS.param(value));
    }

    public String getPort() {
        return JS.string(JS.get(peer, "port"));
    }

    public void setPort(String value) {
        JS.set(peer, "port", JS.param(value));
    }

    public String getPathname() {
        return JS.string(JS.get(peer, "pathname"));
    }

    public void setPathname(String value) {
        JS.set(peer, "pathname", JS.param(value));
    }

    public String getSearch() {
        return JS.string(JS.get(peer, "search"));
    }

    public void setSearch(String value) {
        JS.set(peer, "search", JS.param(value));
    }

    public String getHash() {
        return JS.string(JS.get(peer, "hash"));
    }

    public void setHash(String value) {
        JS.set(peer, "hash", JS.param(value));
    }
}
