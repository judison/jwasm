// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.JSObject;
import webapi.JSSequence;
import webapi.dom.Element;
import webapi.dom.Node;
import webapi.dom.Text;

@SuppressWarnings({"unused"})
public class HTMLSlotElement extends HTMLElement {
    protected HTMLSlotElement(Object peer) {
        super(peer);
    }

    public static HTMLSlotElement wrap(Object peer) {
        return peer == null ? null : new HTMLSlotElement(peer);
    }

    public String getName() {
        return JS.string(JS.get(peer, "name"));
    }

    public void setName(String value) {
        JS.set(peer, "name", JS.param(value));
    }

    public JSSequence<Node> assignedNodes(JSObject options) {
        return JSSequence.wrap(Node.class, JS.invoke(peer, "assignedNodes", JS.param(options)));
    }

    public JSSequence<Node> assignedNodes() {
        return JSSequence.wrap(Node.class, JS.invoke(peer, "assignedNodes"));
    }

    public JSSequence<Element> assignedElements(JSObject options) {
        return JSSequence.wrap(Element.class, JS.invoke(peer, "assignedElements", JS.param(options)));
    }

    public JSSequence<Element> assignedElements() {
        return JSSequence.wrap(Element.class, JS.invoke(peer, "assignedElements"));
    }

    public void assign(Element nodes) {
        JS.invoke(peer, "assign", JS.param(nodes));
    }

    public void assign(Text nodes) {
        JS.invoke(peer, "assign", JS.param(nodes));
    }
}
