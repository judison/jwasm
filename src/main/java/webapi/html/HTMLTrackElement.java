// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;

@SuppressWarnings({"unused"})
public class HTMLTrackElement extends HTMLElement {
    protected HTMLTrackElement(Object peer) {
        super(peer);
    }

    public static HTMLTrackElement wrap(Object peer) {
        return peer == null ? null : new HTMLTrackElement(peer);
    }

    public String getKind() {
        return JS.string(JS.get(peer, "kind"));
    }

    public void setKind(String value) {
        JS.set(peer, "kind", JS.param(value));
    }

    public String getSrc() {
        return JS.string(JS.get(peer, "src"));
    }

    public void setSrc(String value) {
        JS.set(peer, "src", JS.param(value));
    }

    public String getSrclang() {
        return JS.string(JS.get(peer, "srclang"));
    }

    public void setSrclang(String value) {
        JS.set(peer, "srclang", JS.param(value));
    }

    public String getLabel() {
        return JS.string(JS.get(peer, "label"));
    }

    public void setLabel(String value) {
        JS.set(peer, "label", JS.param(value));
    }

    public boolean isDefault() {
        return JS.getBoolean(peer, "default");
    }

    public void setDefault(boolean value) {
        JS.set(peer, "default", value);
    }

    public int getReadyState() {
        return JS.getInt(peer, "readyState");
    }

    public TextTrack getTrack() {
        return TextTrack.wrap(JS.get(peer, "track"));
    }
}
