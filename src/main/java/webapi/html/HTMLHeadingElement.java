// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;

@SuppressWarnings({"unused"})
public class HTMLHeadingElement extends HTMLElement {
    protected HTMLHeadingElement(Object peer) {
        super(peer);
    }

    public static HTMLHeadingElement wrap(Object peer) {
        return peer == null ? null : new HTMLHeadingElement(peer);
    }

    public String getAlign() {
        return JS.string(JS.get(peer, "align"));
    }

    public void setAlign(String value) {
        JS.set(peer, "align", JS.param(value));
    }
}
