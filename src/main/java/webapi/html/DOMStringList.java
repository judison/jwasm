// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class DOMStringList extends WrappedObject {
    protected DOMStringList(Object peer) {
        super(peer);
    }

    public static DOMStringList wrap(Object peer) {
        return peer == null ? null : new DOMStringList(peer);
    }

    public int getLength() {
        return JS.getInt(peer, "length");
    }

    public boolean contains(String string) {
        return JS.invoke(peer, "contains", JS.param(string));
    }
}
