// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.dom.Event;

@SuppressWarnings({"unused"})
public class PageTransitionEvent extends Event {
    protected PageTransitionEvent(Object peer) {
        super(peer);
    }

    public static PageTransitionEvent wrap(Object peer) {
        return peer == null ? null : new PageTransitionEvent(peer);
    }

    public boolean isPersisted() {
        return JS.getBoolean(peer, "persisted");
    }
}
