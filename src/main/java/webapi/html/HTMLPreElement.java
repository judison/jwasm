// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;

@SuppressWarnings({"unused"})
public class HTMLPreElement extends HTMLElement {
    protected HTMLPreElement(Object peer) {
        super(peer);
    }

    public static HTMLPreElement wrap(Object peer) {
        return peer == null ? null : new HTMLPreElement(peer);
    }

    public long getWidth() {
        return (long)JS.get(peer, "width");
    }

    public void setWidth(long value) {
        JS.set(peer, "width", value);
    }
}
