// Generated file; DO NOT edit
package webapi.html;

import webapi.JSCallback;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class FrameRequestCallback extends JSCallback {
    public FrameRequestCallback(Object peer) {
        super(peer);
    }

    public FrameRequestCallback(Callback javaCallback) {
        super(javaCallback);
    }

    public static FrameRequestCallback wrap(Object peer) {
        return peer == null ? null : new FrameRequestCallback(peer);
    }

    public interface Callback extends JSCallback.BaseCallback {
        void handle(WrappedObject time);
    }
}
