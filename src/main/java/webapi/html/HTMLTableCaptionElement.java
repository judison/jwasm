// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;

@SuppressWarnings({"unused"})
public class HTMLTableCaptionElement extends HTMLElement {
    protected HTMLTableCaptionElement(Object peer) {
        super(peer);
    }

    public static HTMLTableCaptionElement wrap(Object peer) {
        return peer == null ? null : new HTMLTableCaptionElement(peer);
    }

    public String getAlign() {
        return JS.string(JS.get(peer, "align"));
    }

    public void setAlign(String value) {
        JS.set(peer, "align", JS.param(value));
    }
}
