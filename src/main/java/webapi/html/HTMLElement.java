// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.JSObject;
import webapi.WrappedObject;
import webapi.css.CSSStyleDeclaration;
import webapi.dom.Element;

@SuppressWarnings({"unused"})
public class HTMLElement extends Element {
    protected HTMLElement(Object peer) {
        super(peer);
    }

    public static HTMLElement wrap(Object peer) {
        return peer == null ? null : new HTMLElement(peer);
    }

    public String getTitle() {
        return JS.string(JS.get(peer, "title"));
    }

    public void setTitle(String value) {
        JS.set(peer, "title", JS.param(value));
    }

    public String getLang() {
        return JS.string(JS.get(peer, "lang"));
    }

    public void setLang(String value) {
        JS.set(peer, "lang", JS.param(value));
    }

    public boolean isTranslate() {
        return JS.getBoolean(peer, "translate");
    }

    public void setTranslate(boolean value) {
        JS.set(peer, "translate", value);
    }

    public String getDir() {
        return JS.string(JS.get(peer, "dir"));
    }

    public void setDir(String value) {
        JS.set(peer, "dir", JS.param(value));
    }

    public WrappedObject getHidden() {
        return WrappedObject.wrap(JS.get(peer, "hidden"));
    }

    public void setHidden(WrappedObject value) {
        JS.set(peer, "hidden", value.peer);
    }

    public boolean isInert() {
        return JS.getBoolean(peer, "inert");
    }

    public void setInert(boolean value) {
        JS.set(peer, "inert", value);
    }

    public String getAccessKey() {
        return JS.string(JS.get(peer, "accessKey"));
    }

    public void setAccessKey(String value) {
        JS.set(peer, "accessKey", JS.param(value));
    }

    public String getAccessKeyLabel() {
        return JS.string(JS.get(peer, "accessKeyLabel"));
    }

    public boolean isDraggable() {
        return JS.getBoolean(peer, "draggable");
    }

    public void setDraggable(boolean value) {
        JS.set(peer, "draggable", value);
    }

    public boolean isSpellcheck() {
        return JS.getBoolean(peer, "spellcheck");
    }

    public void setSpellcheck(boolean value) {
        JS.set(peer, "spellcheck", value);
    }

    public String getAutocapitalize() {
        return JS.string(JS.get(peer, "autocapitalize"));
    }

    public void setAutocapitalize(String value) {
        JS.set(peer, "autocapitalize", JS.param(value));
    }

    public String getInnerText() {
        return JS.string(JS.get(peer, "innerText"));
    }

    public void setInnerText(String value) {
        JS.set(peer, "innerText", JS.param(value));
    }

    public String getOuterText() {
        return JS.string(JS.get(peer, "outerText"));
    }

    public void setOuterText(String value) {
        JS.set(peer, "outerText", JS.param(value));
    }

    public EventHandler getOnabort() {
        return EventHandler.wrap(JS.get(peer, "onabort"));
    }

    public void setOnabort(EventHandler value) {
        JS.set(peer, "onabort", value.peer);
    }

    public EventHandler getOnauxclick() {
        return EventHandler.wrap(JS.get(peer, "onauxclick"));
    }

    public void setOnauxclick(EventHandler value) {
        JS.set(peer, "onauxclick", value.peer);
    }

    public EventHandler getOnbeforeinput() {
        return EventHandler.wrap(JS.get(peer, "onbeforeinput"));
    }

    public void setOnbeforeinput(EventHandler value) {
        JS.set(peer, "onbeforeinput", value.peer);
    }

    public EventHandler getOnbeforematch() {
        return EventHandler.wrap(JS.get(peer, "onbeforematch"));
    }

    public void setOnbeforematch(EventHandler value) {
        JS.set(peer, "onbeforematch", value.peer);
    }

    public EventHandler getOnblur() {
        return EventHandler.wrap(JS.get(peer, "onblur"));
    }

    public void setOnblur(EventHandler value) {
        JS.set(peer, "onblur", value.peer);
    }

    public EventHandler getOncancel() {
        return EventHandler.wrap(JS.get(peer, "oncancel"));
    }

    public void setOncancel(EventHandler value) {
        JS.set(peer, "oncancel", value.peer);
    }

    public EventHandler getOncanplay() {
        return EventHandler.wrap(JS.get(peer, "oncanplay"));
    }

    public void setOncanplay(EventHandler value) {
        JS.set(peer, "oncanplay", value.peer);
    }

    public EventHandler getOncanplaythrough() {
        return EventHandler.wrap(JS.get(peer, "oncanplaythrough"));
    }

    public void setOncanplaythrough(EventHandler value) {
        JS.set(peer, "oncanplaythrough", value.peer);
    }

    public EventHandler getOnchange() {
        return EventHandler.wrap(JS.get(peer, "onchange"));
    }

    public void setOnchange(EventHandler value) {
        JS.set(peer, "onchange", value.peer);
    }

    public EventHandler getOnclick() {
        return EventHandler.wrap(JS.get(peer, "onclick"));
    }

    public void setOnclick(EventHandler value) {
        JS.set(peer, "onclick", value.peer);
    }

    public EventHandler getOnclose() {
        return EventHandler.wrap(JS.get(peer, "onclose"));
    }

    public void setOnclose(EventHandler value) {
        JS.set(peer, "onclose", value.peer);
    }

    public EventHandler getOncontextlost() {
        return EventHandler.wrap(JS.get(peer, "oncontextlost"));
    }

    public void setOncontextlost(EventHandler value) {
        JS.set(peer, "oncontextlost", value.peer);
    }

    public EventHandler getOncontextmenu() {
        return EventHandler.wrap(JS.get(peer, "oncontextmenu"));
    }

    public void setOncontextmenu(EventHandler value) {
        JS.set(peer, "oncontextmenu", value.peer);
    }

    public EventHandler getOncontextrestored() {
        return EventHandler.wrap(JS.get(peer, "oncontextrestored"));
    }

    public void setOncontextrestored(EventHandler value) {
        JS.set(peer, "oncontextrestored", value.peer);
    }

    public EventHandler getOncuechange() {
        return EventHandler.wrap(JS.get(peer, "oncuechange"));
    }

    public void setOncuechange(EventHandler value) {
        JS.set(peer, "oncuechange", value.peer);
    }

    public EventHandler getOndblclick() {
        return EventHandler.wrap(JS.get(peer, "ondblclick"));
    }

    public void setOndblclick(EventHandler value) {
        JS.set(peer, "ondblclick", value.peer);
    }

    public EventHandler getOndrag() {
        return EventHandler.wrap(JS.get(peer, "ondrag"));
    }

    public void setOndrag(EventHandler value) {
        JS.set(peer, "ondrag", value.peer);
    }

    public EventHandler getOndragend() {
        return EventHandler.wrap(JS.get(peer, "ondragend"));
    }

    public void setOndragend(EventHandler value) {
        JS.set(peer, "ondragend", value.peer);
    }

    public EventHandler getOndragenter() {
        return EventHandler.wrap(JS.get(peer, "ondragenter"));
    }

    public void setOndragenter(EventHandler value) {
        JS.set(peer, "ondragenter", value.peer);
    }

    public EventHandler getOndragleave() {
        return EventHandler.wrap(JS.get(peer, "ondragleave"));
    }

    public void setOndragleave(EventHandler value) {
        JS.set(peer, "ondragleave", value.peer);
    }

    public EventHandler getOndragover() {
        return EventHandler.wrap(JS.get(peer, "ondragover"));
    }

    public void setOndragover(EventHandler value) {
        JS.set(peer, "ondragover", value.peer);
    }

    public EventHandler getOndragstart() {
        return EventHandler.wrap(JS.get(peer, "ondragstart"));
    }

    public void setOndragstart(EventHandler value) {
        JS.set(peer, "ondragstart", value.peer);
    }

    public EventHandler getOndrop() {
        return EventHandler.wrap(JS.get(peer, "ondrop"));
    }

    public void setOndrop(EventHandler value) {
        JS.set(peer, "ondrop", value.peer);
    }

    public EventHandler getOndurationchange() {
        return EventHandler.wrap(JS.get(peer, "ondurationchange"));
    }

    public void setOndurationchange(EventHandler value) {
        JS.set(peer, "ondurationchange", value.peer);
    }

    public EventHandler getOnemptied() {
        return EventHandler.wrap(JS.get(peer, "onemptied"));
    }

    public void setOnemptied(EventHandler value) {
        JS.set(peer, "onemptied", value.peer);
    }

    public EventHandler getOnended() {
        return EventHandler.wrap(JS.get(peer, "onended"));
    }

    public void setOnended(EventHandler value) {
        JS.set(peer, "onended", value.peer);
    }

    public WrappedObject getOnerror() {
        return WrappedObject.wrap(JS.get(peer, "onerror"));
    }

    public void setOnerror(WrappedObject value) {
        JS.set(peer, "onerror", value.peer);
    }

    public EventHandler getOnfocus() {
        return EventHandler.wrap(JS.get(peer, "onfocus"));
    }

    public void setOnfocus(EventHandler value) {
        JS.set(peer, "onfocus", value.peer);
    }

    public EventHandler getOnformdata() {
        return EventHandler.wrap(JS.get(peer, "onformdata"));
    }

    public void setOnformdata(EventHandler value) {
        JS.set(peer, "onformdata", value.peer);
    }

    public EventHandler getOninput() {
        return EventHandler.wrap(JS.get(peer, "oninput"));
    }

    public void setOninput(EventHandler value) {
        JS.set(peer, "oninput", value.peer);
    }

    public EventHandler getOninvalid() {
        return EventHandler.wrap(JS.get(peer, "oninvalid"));
    }

    public void setOninvalid(EventHandler value) {
        JS.set(peer, "oninvalid", value.peer);
    }

    public EventHandler getOnkeydown() {
        return EventHandler.wrap(JS.get(peer, "onkeydown"));
    }

    public void setOnkeydown(EventHandler value) {
        JS.set(peer, "onkeydown", value.peer);
    }

    public EventHandler getOnkeypress() {
        return EventHandler.wrap(JS.get(peer, "onkeypress"));
    }

    public void setOnkeypress(EventHandler value) {
        JS.set(peer, "onkeypress", value.peer);
    }

    public EventHandler getOnkeyup() {
        return EventHandler.wrap(JS.get(peer, "onkeyup"));
    }

    public void setOnkeyup(EventHandler value) {
        JS.set(peer, "onkeyup", value.peer);
    }

    public EventHandler getOnload() {
        return EventHandler.wrap(JS.get(peer, "onload"));
    }

    public void setOnload(EventHandler value) {
        JS.set(peer, "onload", value.peer);
    }

    public EventHandler getOnloadeddata() {
        return EventHandler.wrap(JS.get(peer, "onloadeddata"));
    }

    public void setOnloadeddata(EventHandler value) {
        JS.set(peer, "onloadeddata", value.peer);
    }

    public EventHandler getOnloadedmetadata() {
        return EventHandler.wrap(JS.get(peer, "onloadedmetadata"));
    }

    public void setOnloadedmetadata(EventHandler value) {
        JS.set(peer, "onloadedmetadata", value.peer);
    }

    public EventHandler getOnloadstart() {
        return EventHandler.wrap(JS.get(peer, "onloadstart"));
    }

    public void setOnloadstart(EventHandler value) {
        JS.set(peer, "onloadstart", value.peer);
    }

    public EventHandler getOnmousedown() {
        return EventHandler.wrap(JS.get(peer, "onmousedown"));
    }

    public void setOnmousedown(EventHandler value) {
        JS.set(peer, "onmousedown", value.peer);
    }

    public EventHandler getOnmouseenter() {
        return EventHandler.wrap(JS.get(peer, "onmouseenter"));
    }

    public void setOnmouseenter(EventHandler value) {
        JS.set(peer, "onmouseenter", value.peer);
    }

    public EventHandler getOnmouseleave() {
        return EventHandler.wrap(JS.get(peer, "onmouseleave"));
    }

    public void setOnmouseleave(EventHandler value) {
        JS.set(peer, "onmouseleave", value.peer);
    }

    public EventHandler getOnmousemove() {
        return EventHandler.wrap(JS.get(peer, "onmousemove"));
    }

    public void setOnmousemove(EventHandler value) {
        JS.set(peer, "onmousemove", value.peer);
    }

    public EventHandler getOnmouseout() {
        return EventHandler.wrap(JS.get(peer, "onmouseout"));
    }

    public void setOnmouseout(EventHandler value) {
        JS.set(peer, "onmouseout", value.peer);
    }

    public EventHandler getOnmouseover() {
        return EventHandler.wrap(JS.get(peer, "onmouseover"));
    }

    public void setOnmouseover(EventHandler value) {
        JS.set(peer, "onmouseover", value.peer);
    }

    public EventHandler getOnmouseup() {
        return EventHandler.wrap(JS.get(peer, "onmouseup"));
    }

    public void setOnmouseup(EventHandler value) {
        JS.set(peer, "onmouseup", value.peer);
    }

    public EventHandler getOnpause() {
        return EventHandler.wrap(JS.get(peer, "onpause"));
    }

    public void setOnpause(EventHandler value) {
        JS.set(peer, "onpause", value.peer);
    }

    public EventHandler getOnplay() {
        return EventHandler.wrap(JS.get(peer, "onplay"));
    }

    public void setOnplay(EventHandler value) {
        JS.set(peer, "onplay", value.peer);
    }

    public EventHandler getOnplaying() {
        return EventHandler.wrap(JS.get(peer, "onplaying"));
    }

    public void setOnplaying(EventHandler value) {
        JS.set(peer, "onplaying", value.peer);
    }

    public EventHandler getOnprogress() {
        return EventHandler.wrap(JS.get(peer, "onprogress"));
    }

    public void setOnprogress(EventHandler value) {
        JS.set(peer, "onprogress", value.peer);
    }

    public EventHandler getOnratechange() {
        return EventHandler.wrap(JS.get(peer, "onratechange"));
    }

    public void setOnratechange(EventHandler value) {
        JS.set(peer, "onratechange", value.peer);
    }

    public EventHandler getOnreset() {
        return EventHandler.wrap(JS.get(peer, "onreset"));
    }

    public void setOnreset(EventHandler value) {
        JS.set(peer, "onreset", value.peer);
    }

    public EventHandler getOnresize() {
        return EventHandler.wrap(JS.get(peer, "onresize"));
    }

    public void setOnresize(EventHandler value) {
        JS.set(peer, "onresize", value.peer);
    }

    public EventHandler getOnscroll() {
        return EventHandler.wrap(JS.get(peer, "onscroll"));
    }

    public void setOnscroll(EventHandler value) {
        JS.set(peer, "onscroll", value.peer);
    }

    public EventHandler getOnsecuritypolicyviolation() {
        return EventHandler.wrap(JS.get(peer, "onsecuritypolicyviolation"));
    }

    public void setOnsecuritypolicyviolation(EventHandler value) {
        JS.set(peer, "onsecuritypolicyviolation", value.peer);
    }

    public EventHandler getOnseeked() {
        return EventHandler.wrap(JS.get(peer, "onseeked"));
    }

    public void setOnseeked(EventHandler value) {
        JS.set(peer, "onseeked", value.peer);
    }

    public EventHandler getOnseeking() {
        return EventHandler.wrap(JS.get(peer, "onseeking"));
    }

    public void setOnseeking(EventHandler value) {
        JS.set(peer, "onseeking", value.peer);
    }

    public EventHandler getOnselect() {
        return EventHandler.wrap(JS.get(peer, "onselect"));
    }

    public void setOnselect(EventHandler value) {
        JS.set(peer, "onselect", value.peer);
    }

    public EventHandler getOnslotchange() {
        return EventHandler.wrap(JS.get(peer, "onslotchange"));
    }

    public void setOnslotchange(EventHandler value) {
        JS.set(peer, "onslotchange", value.peer);
    }

    public EventHandler getOnstalled() {
        return EventHandler.wrap(JS.get(peer, "onstalled"));
    }

    public void setOnstalled(EventHandler value) {
        JS.set(peer, "onstalled", value.peer);
    }

    public EventHandler getOnsubmit() {
        return EventHandler.wrap(JS.get(peer, "onsubmit"));
    }

    public void setOnsubmit(EventHandler value) {
        JS.set(peer, "onsubmit", value.peer);
    }

    public EventHandler getOnsuspend() {
        return EventHandler.wrap(JS.get(peer, "onsuspend"));
    }

    public void setOnsuspend(EventHandler value) {
        JS.set(peer, "onsuspend", value.peer);
    }

    public EventHandler getOntimeupdate() {
        return EventHandler.wrap(JS.get(peer, "ontimeupdate"));
    }

    public void setOntimeupdate(EventHandler value) {
        JS.set(peer, "ontimeupdate", value.peer);
    }

    public EventHandler getOntoggle() {
        return EventHandler.wrap(JS.get(peer, "ontoggle"));
    }

    public void setOntoggle(EventHandler value) {
        JS.set(peer, "ontoggle", value.peer);
    }

    public EventHandler getOnvolumechange() {
        return EventHandler.wrap(JS.get(peer, "onvolumechange"));
    }

    public void setOnvolumechange(EventHandler value) {
        JS.set(peer, "onvolumechange", value.peer);
    }

    public EventHandler getOnwaiting() {
        return EventHandler.wrap(JS.get(peer, "onwaiting"));
    }

    public void setOnwaiting(EventHandler value) {
        JS.set(peer, "onwaiting", value.peer);
    }

    public EventHandler getOnwebkitanimationend() {
        return EventHandler.wrap(JS.get(peer, "onwebkitanimationend"));
    }

    public void setOnwebkitanimationend(EventHandler value) {
        JS.set(peer, "onwebkitanimationend", value.peer);
    }

    public EventHandler getOnwebkitanimationiteration() {
        return EventHandler.wrap(JS.get(peer, "onwebkitanimationiteration"));
    }

    public void setOnwebkitanimationiteration(EventHandler value) {
        JS.set(peer, "onwebkitanimationiteration", value.peer);
    }

    public EventHandler getOnwebkitanimationstart() {
        return EventHandler.wrap(JS.get(peer, "onwebkitanimationstart"));
    }

    public void setOnwebkitanimationstart(EventHandler value) {
        JS.set(peer, "onwebkitanimationstart", value.peer);
    }

    public EventHandler getOnwebkittransitionend() {
        return EventHandler.wrap(JS.get(peer, "onwebkittransitionend"));
    }

    public void setOnwebkittransitionend(EventHandler value) {
        JS.set(peer, "onwebkittransitionend", value.peer);
    }

    public EventHandler getOnwheel() {
        return EventHandler.wrap(JS.get(peer, "onwheel"));
    }

    public void setOnwheel(EventHandler value) {
        JS.set(peer, "onwheel", value.peer);
    }

    public EventHandler getOncopy() {
        return EventHandler.wrap(JS.get(peer, "oncopy"));
    }

    public void setOncopy(EventHandler value) {
        JS.set(peer, "oncopy", value.peer);
    }

    public EventHandler getOncut() {
        return EventHandler.wrap(JS.get(peer, "oncut"));
    }

    public void setOncut(EventHandler value) {
        JS.set(peer, "oncut", value.peer);
    }

    public EventHandler getOnpaste() {
        return EventHandler.wrap(JS.get(peer, "onpaste"));
    }

    public void setOnpaste(EventHandler value) {
        JS.set(peer, "onpaste", value.peer);
    }

    public String getContentEditable() {
        return JS.string(JS.get(peer, "contentEditable"));
    }

    public void setContentEditable(String value) {
        JS.set(peer, "contentEditable", JS.param(value));
    }

    public String getEnterKeyHint() {
        return JS.string(JS.get(peer, "enterKeyHint"));
    }

    public void setEnterKeyHint(String value) {
        JS.set(peer, "enterKeyHint", JS.param(value));
    }

    public boolean isIsContentEditable() {
        return JS.getBoolean(peer, "isContentEditable");
    }

    public String getInputMode() {
        return JS.string(JS.get(peer, "inputMode"));
    }

    public void setInputMode(String value) {
        JS.set(peer, "inputMode", JS.param(value));
    }

    public DOMStringMap getDataset() {
        return DOMStringMap.wrap(JS.get(peer, "dataset"));
    }

    public String getNonce() {
        return JS.string(JS.get(peer, "nonce"));
    }

    public void setNonce(String value) {
        JS.set(peer, "nonce", JS.param(value));
    }

    public boolean isAutofocus() {
        return JS.getBoolean(peer, "autofocus");
    }

    public void setAutofocus(boolean value) {
        JS.set(peer, "autofocus", value);
    }

    public long getTabIndex() {
        return (long)JS.get(peer, "tabIndex");
    }

    public void setTabIndex(long value) {
        JS.set(peer, "tabIndex", value);
    }

    public CSSStyleDeclaration getStyle() {
        return CSSStyleDeclaration.wrap(JS.get(peer, "style"));
    }

    public void click() {
        JS.invoke(peer, "click");
    }

    public ElementInternals attachInternals() {
        return ElementInternals.wrap(JS.invoke(peer, "attachInternals"));
    }

    public void focus(JSObject options) {
        JS.invoke(peer, "focus", JS.param(options));
    }

    public void focus() {
        JS.invoke(peer, "focus");
    }

    public void blur() {
        JS.invoke(peer, "blur");
    }
}
