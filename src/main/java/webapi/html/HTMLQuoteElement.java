// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;

@SuppressWarnings({"unused"})
public class HTMLQuoteElement extends HTMLElement {
    protected HTMLQuoteElement(Object peer) {
        super(peer);
    }

    public static HTMLQuoteElement wrap(Object peer) {
        return peer == null ? null : new HTMLQuoteElement(peer);
    }

    public String getCite() {
        return JS.string(JS.get(peer, "cite"));
    }

    public void setCite(String value) {
        JS.set(peer, "cite", JS.param(value));
    }
}
