// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;

@SuppressWarnings({"unused"})
public class HTMLDialogElement extends HTMLElement {
    protected HTMLDialogElement(Object peer) {
        super(peer);
    }

    public static HTMLDialogElement wrap(Object peer) {
        return peer == null ? null : new HTMLDialogElement(peer);
    }

    public boolean isOpen() {
        return JS.getBoolean(peer, "open");
    }

    public void setOpen(boolean value) {
        JS.set(peer, "open", value);
    }

    public String getReturnValue() {
        return JS.string(JS.get(peer, "returnValue"));
    }

    public void setReturnValue(String value) {
        JS.set(peer, "returnValue", JS.param(value));
    }

    public void show() {
        JS.invoke(peer, "show");
    }

    public void showModal() {
        JS.invoke(peer, "showModal");
    }

    public void close(String returnValue) {
        JS.invoke(peer, "close", JS.param(returnValue));
    }

    public void close() {
        JS.invoke(peer, "close");
    }
}
