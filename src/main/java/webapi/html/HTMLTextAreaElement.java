// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;
import webapi.dom.NodeList;

@SuppressWarnings({"unused"})
public class HTMLTextAreaElement extends HTMLElement {
    protected HTMLTextAreaElement(Object peer) {
        super(peer);
    }

    public static HTMLTextAreaElement wrap(Object peer) {
        return peer == null ? null : new HTMLTextAreaElement(peer);
    }

    public String getAutocomplete() {
        return JS.string(JS.get(peer, "autocomplete"));
    }

    public void setAutocomplete(String value) {
        JS.set(peer, "autocomplete", JS.param(value));
    }

    public int getCols() {
        return JS.getInt(peer, "cols");
    }

    public void setCols(int value) {
        JS.set(peer, "cols", value);
    }

    public String getDirName() {
        return JS.string(JS.get(peer, "dirName"));
    }

    public void setDirName(String value) {
        JS.set(peer, "dirName", JS.param(value));
    }

    public boolean isDisabled() {
        return JS.getBoolean(peer, "disabled");
    }

    public void setDisabled(boolean value) {
        JS.set(peer, "disabled", value);
    }

    public HTMLFormElement getForm() {
        return HTMLFormElement.wrap(JS.get(peer, "form"));
    }

    public long getMaxLength() {
        return (long)JS.get(peer, "maxLength");
    }

    public void setMaxLength(long value) {
        JS.set(peer, "maxLength", value);
    }

    public long getMinLength() {
        return (long)JS.get(peer, "minLength");
    }

    public void setMinLength(long value) {
        JS.set(peer, "minLength", value);
    }

    public String getName() {
        return JS.string(JS.get(peer, "name"));
    }

    public void setName(String value) {
        JS.set(peer, "name", JS.param(value));
    }

    public String getPlaceholder() {
        return JS.string(JS.get(peer, "placeholder"));
    }

    public void setPlaceholder(String value) {
        JS.set(peer, "placeholder", JS.param(value));
    }

    public boolean isReadOnly() {
        return JS.getBoolean(peer, "readOnly");
    }

    public void setReadOnly(boolean value) {
        JS.set(peer, "readOnly", value);
    }

    public boolean isRequired() {
        return JS.getBoolean(peer, "required");
    }

    public void setRequired(boolean value) {
        JS.set(peer, "required", value);
    }

    public int getRows() {
        return JS.getInt(peer, "rows");
    }

    public void setRows(int value) {
        JS.set(peer, "rows", value);
    }

    public String getWrap() {
        return JS.string(JS.get(peer, "wrap"));
    }

    public void setWrap(String value) {
        JS.set(peer, "wrap", JS.param(value));
    }

    public String getType() {
        return JS.string(JS.get(peer, "type"));
    }

    public String getDefaultValue() {
        return JS.string(JS.get(peer, "defaultValue"));
    }

    public void setDefaultValue(String value) {
        JS.set(peer, "defaultValue", JS.param(value));
    }

    public String getValue() {
        return JS.string(JS.get(peer, "value"));
    }

    public void setValue(String value) {
        JS.set(peer, "value", JS.param(value));
    }

    public int getTextLength() {
        return JS.getInt(peer, "textLength");
    }

    public boolean isWillValidate() {
        return JS.getBoolean(peer, "willValidate");
    }

    public ValidityState getValidity() {
        return ValidityState.wrap(JS.get(peer, "validity"));
    }

    public String getValidationMessage() {
        return JS.string(JS.get(peer, "validationMessage"));
    }

    public NodeList getLabels() {
        return NodeList.wrap(JS.get(peer, "labels"));
    }

    public int getSelectionStart() {
        return JS.getInt(peer, "selectionStart");
    }

    public void setSelectionStart(int value) {
        JS.set(peer, "selectionStart", value);
    }

    public int getSelectionEnd() {
        return JS.getInt(peer, "selectionEnd");
    }

    public void setSelectionEnd(int value) {
        JS.set(peer, "selectionEnd", value);
    }

    public String getSelectionDirection() {
        return JS.string(JS.get(peer, "selectionDirection"));
    }

    public void setSelectionDirection(String value) {
        JS.set(peer, "selectionDirection", JS.param(value));
    }

    public boolean checkValidity() {
        return JS.invoke(peer, "checkValidity");
    }

    public boolean reportValidity() {
        return JS.invoke(peer, "reportValidity");
    }

    public void setCustomValidity(String error) {
        JS.invoke(peer, "setCustomValidity", JS.param(error));
    }

    public void select() {
        JS.invoke(peer, "select");
    }

    public void setRangeText(String replacement) {
        JS.invoke(peer, "setRangeText", JS.param(replacement));
    }

    public void setRangeText(String replacement, int start, int end, WrappedObject selectionMode) {
        JS.invoke(peer, "setRangeText", JS.param(replacement), start, end, JS.param(selectionMode));
    }

    public void setRangeText(String replacement, int start, int end) {
        JS.invoke(peer, "setRangeText", JS.param(replacement), start, end);
    }

    public void setSelectionRange(int start, int end, String direction) {
        JS.invoke(peer, "setSelectionRange", start, end, JS.param(direction));
    }

    public void setSelectionRange(int start, int end) {
        JS.invoke(peer, "setSelectionRange", start, end);
    }
}
