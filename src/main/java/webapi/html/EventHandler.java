// Generated file; DO NOT edit
package webapi.html;

import webapi.JSCallback;
import webapi.WrappedObject;
import webapi.dom.Event;

@SuppressWarnings({"unused"})
public class EventHandler extends JSCallback {
    public EventHandler(Object peer) {
        super(peer);
    }

    public EventHandler(Callback javaCallback) {
        super(javaCallback);
    }

    public static EventHandler wrap(Object peer) {
        return peer == null ? null : new EventHandler(peer);
    }

    public interface Callback extends JSCallback.BaseCallback {
        WrappedObject handle(Event event);
    }
}
