// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.dom.EventTarget;

@SuppressWarnings({"unused"})
public class SharedWorker extends EventTarget {
    protected SharedWorker(Object peer) {
        super(peer);
    }

    public static SharedWorker wrap(Object peer) {
        return peer == null ? null : new SharedWorker(peer);
    }

    public MessagePort getPort() {
        return MessagePort.wrap(JS.get(peer, "port"));
    }

    public EventHandler getOnerror() {
        return EventHandler.wrap(JS.get(peer, "onerror"));
    }

    public void setOnerror(EventHandler value) {
        JS.set(peer, "onerror", value.peer);
    }
}
