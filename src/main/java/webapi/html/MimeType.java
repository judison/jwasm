// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class MimeType extends WrappedObject {
    protected MimeType(Object peer) {
        super(peer);
    }

    public static MimeType wrap(Object peer) {
        return peer == null ? null : new MimeType(peer);
    }

    public String getType() {
        return JS.string(JS.get(peer, "type"));
    }

    public String getDescription() {
        return JS.string(JS.get(peer, "description"));
    }

    public String getSuffixes() {
        return JS.string(JS.get(peer, "suffixes"));
    }

    public Plugin getEnabledPlugin() {
        return Plugin.wrap(JS.get(peer, "enabledPlugin"));
    }
}
