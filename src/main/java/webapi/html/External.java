// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class External extends WrappedObject {
    protected External(Object peer) {
        super(peer);
    }

    public static External wrap(Object peer) {
        return peer == null ? null : new External(peer);
    }

    public void AddSearchProvider() {
        JS.invoke(peer, "AddSearchProvider");
    }

    public void IsSearchProviderInstalled() {
        JS.invoke(peer, "IsSearchProviderInstalled");
    }
}
