// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class HTMLCanvasElement extends HTMLElement {
    protected HTMLCanvasElement(Object peer) {
        super(peer);
    }

    public static HTMLCanvasElement wrap(Object peer) {
        return peer == null ? null : new HTMLCanvasElement(peer);
    }

    public int getWidth() {
        return JS.getInt(peer, "width");
    }

    public void setWidth(int value) {
        JS.set(peer, "width", value);
    }

    public int getHeight() {
        return JS.getInt(peer, "height");
    }

    public void setHeight(int value) {
        JS.set(peer, "height", value);
    }

    public WrappedObject getContext(String contextId, WrappedObject options) {
        return WrappedObject.wrap(JS.invoke(peer, "getContext", JS.param(contextId), JS.param(options)));
    }

    public WrappedObject getContext(String contextId) {
        return WrappedObject.wrap(JS.invoke(peer, "getContext", JS.param(contextId)));
    }

    public String toDataURL(String type, WrappedObject quality) {
        return JS.string(JS.invoke(peer, "toDataURL", JS.param(type), JS.param(quality)));
    }

    public String toDataURL(String type) {
        return JS.string(JS.invoke(peer, "toDataURL", JS.param(type)));
    }

    public String toDataURL() {
        return JS.string(JS.invoke(peer, "toDataURL"));
    }

    public void toBlob(BlobCallback _callback, String type, WrappedObject quality) {
        JS.invoke(peer, "toBlob", JS.param(_callback), JS.param(type), JS.param(quality));
    }

    public void toBlob(BlobCallback _callback, String type) {
        JS.invoke(peer, "toBlob", JS.param(_callback), JS.param(type));
    }

    public void toBlob(BlobCallback _callback) {
        JS.invoke(peer, "toBlob", JS.param(_callback));
    }

    public OffscreenCanvas transferControlToOffscreen() {
        return OffscreenCanvas.wrap(JS.invoke(peer, "transferControlToOffscreen"));
    }
}
