// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;

@SuppressWarnings({"unused"})
public class HTMLDivElement extends HTMLElement {
    protected HTMLDivElement(Object peer) {
        super(peer);
    }

    public static HTMLDivElement wrap(Object peer) {
        return peer == null ? null : new HTMLDivElement(peer);
    }

    public String getAlign() {
        return JS.string(JS.get(peer, "align"));
    }

    public void setAlign(String value) {
        JS.set(peer, "align", JS.param(value));
    }
}
