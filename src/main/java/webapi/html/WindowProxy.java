// Generated file; DO NOT edit
package webapi.html;

import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class WindowProxy extends WrappedObject {
    protected WindowProxy(Object peer) {
        super(peer);
    }

    public static WindowProxy wrap(Object peer) {
        return peer == null ? null : new WindowProxy(peer);
    }
}
