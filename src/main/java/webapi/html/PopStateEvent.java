// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;
import webapi.dom.Event;

@SuppressWarnings({"unused"})
public class PopStateEvent extends Event {
    protected PopStateEvent(Object peer) {
        super(peer);
    }

    public static PopStateEvent wrap(Object peer) {
        return peer == null ? null : new PopStateEvent(peer);
    }

    public WrappedObject getState() {
        return WrappedObject.wrap(JS.get(peer, "state"));
    }
}
