// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class TimeRanges extends WrappedObject {
    protected TimeRanges(Object peer) {
        super(peer);
    }

    public static TimeRanges wrap(Object peer) {
        return peer == null ? null : new TimeRanges(peer);
    }

    public int getLength() {
        return JS.getInt(peer, "length");
    }

    public double start(int index) {
        return JS.invoke(peer, "start", index);
    }

    public double end(int index) {
        return JS.invoke(peer, "end", index);
    }
}
