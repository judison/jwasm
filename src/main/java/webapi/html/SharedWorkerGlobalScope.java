// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;

@SuppressWarnings({"unused"})
public class SharedWorkerGlobalScope extends WorkerGlobalScope {
    protected SharedWorkerGlobalScope(Object peer) {
        super(peer);
    }

    public static SharedWorkerGlobalScope wrap(Object peer) {
        return peer == null ? null : new SharedWorkerGlobalScope(peer);
    }

    public EventHandler getOnconnect() {
        return EventHandler.wrap(JS.get(peer, "onconnect"));
    }

    public void setOnconnect(EventHandler value) {
        JS.set(peer, "onconnect", value.peer);
    }

    public void close() {
        JS.invoke(peer, "close");
    }
}
