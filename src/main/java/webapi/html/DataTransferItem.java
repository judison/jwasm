// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class DataTransferItem extends WrappedObject {
    protected DataTransferItem(Object peer) {
        super(peer);
    }

    public static DataTransferItem wrap(Object peer) {
        return peer == null ? null : new DataTransferItem(peer);
    }

    public String getKind() {
        return JS.string(JS.get(peer, "kind"));
    }

    public String getType() {
        return JS.string(JS.get(peer, "type"));
    }

    public void getAsString(FunctionStringCallback _callback) {
        JS.invoke(peer, "getAsString", JS.param(_callback));
    }

    public WrappedObject getAsFile() {
        return WrappedObject.wrap(JS.invoke(peer, "getAsFile"));
    }
}
