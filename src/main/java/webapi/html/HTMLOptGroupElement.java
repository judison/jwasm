// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;

@SuppressWarnings({"unused"})
public class HTMLOptGroupElement extends HTMLElement {
    protected HTMLOptGroupElement(Object peer) {
        super(peer);
    }

    public static HTMLOptGroupElement wrap(Object peer) {
        return peer == null ? null : new HTMLOptGroupElement(peer);
    }

    public boolean isDisabled() {
        return JS.getBoolean(peer, "disabled");
    }

    public void setDisabled(boolean value) {
        JS.set(peer, "disabled", value);
    }

    public String getLabel() {
        return JS.string(JS.get(peer, "label"));
    }

    public void setLabel(String value) {
        JS.set(peer, "label", JS.param(value));
    }
}
