// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;
import webapi.dom.EventTarget;

@SuppressWarnings({"unused"})
public class TextTrack extends EventTarget {
    protected TextTrack(Object peer) {
        super(peer);
    }

    public static TextTrack wrap(Object peer) {
        return peer == null ? null : new TextTrack(peer);
    }

    public WrappedObject getKind() {
        return WrappedObject.wrap(JS.get(peer, "kind"));
    }

    public String getLabel() {
        return JS.string(JS.get(peer, "label"));
    }

    public String getLanguage() {
        return JS.string(JS.get(peer, "language"));
    }

    public String getId() {
        return JS.string(JS.get(peer, "id"));
    }

    public String getInBandMetadataTrackDispatchType() {
        return JS.string(JS.get(peer, "inBandMetadataTrackDispatchType"));
    }

    public WrappedObject getMode() {
        return WrappedObject.wrap(JS.get(peer, "mode"));
    }

    public void setMode(WrappedObject value) {
        JS.set(peer, "mode", value.peer);
    }

    public TextTrackCueList getCues() {
        return TextTrackCueList.wrap(JS.get(peer, "cues"));
    }

    public TextTrackCueList getActiveCues() {
        return TextTrackCueList.wrap(JS.get(peer, "activeCues"));
    }

    public EventHandler getOncuechange() {
        return EventHandler.wrap(JS.get(peer, "oncuechange"));
    }

    public void setOncuechange(EventHandler value) {
        JS.set(peer, "oncuechange", value.peer);
    }

    public void addCue(TextTrackCue cue) {
        JS.invoke(peer, "addCue", JS.param(cue));
    }

    public void removeCue(TextTrackCue cue) {
        JS.invoke(peer, "removeCue", JS.param(cue));
    }
}
