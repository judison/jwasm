// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.dom.NodeList;

@SuppressWarnings({"unused"})
public class HTMLMeterElement extends HTMLElement {
    protected HTMLMeterElement(Object peer) {
        super(peer);
    }

    public static HTMLMeterElement wrap(Object peer) {
        return peer == null ? null : new HTMLMeterElement(peer);
    }

    public double getValue() {
        return JS.getDouble(peer, "value");
    }

    public void setValue(double value) {
        JS.set(peer, "value", value);
    }

    public double getMin() {
        return JS.getDouble(peer, "min");
    }

    public void setMin(double value) {
        JS.set(peer, "min", value);
    }

    public double getMax() {
        return JS.getDouble(peer, "max");
    }

    public void setMax(double value) {
        JS.set(peer, "max", value);
    }

    public double getLow() {
        return JS.getDouble(peer, "low");
    }

    public void setLow(double value) {
        JS.set(peer, "low", value);
    }

    public double getHigh() {
        return JS.getDouble(peer, "high");
    }

    public void setHigh(double value) {
        JS.set(peer, "high", value);
    }

    public double getOptimum() {
        return JS.getDouble(peer, "optimum");
    }

    public void setOptimum(double value) {
        JS.set(peer, "optimum", value);
    }

    public NodeList getLabels() {
        return NodeList.wrap(JS.get(peer, "labels"));
    }
}
