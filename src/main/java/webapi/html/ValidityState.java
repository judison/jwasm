// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class ValidityState extends WrappedObject {
    protected ValidityState(Object peer) {
        super(peer);
    }

    public static ValidityState wrap(Object peer) {
        return peer == null ? null : new ValidityState(peer);
    }

    public boolean isValueMissing() {
        return JS.getBoolean(peer, "valueMissing");
    }

    public boolean isTypeMismatch() {
        return JS.getBoolean(peer, "typeMismatch");
    }

    public boolean isPatternMismatch() {
        return JS.getBoolean(peer, "patternMismatch");
    }

    public boolean isTooLong() {
        return JS.getBoolean(peer, "tooLong");
    }

    public boolean isTooShort() {
        return JS.getBoolean(peer, "tooShort");
    }

    public boolean isRangeUnderflow() {
        return JS.getBoolean(peer, "rangeUnderflow");
    }

    public boolean isRangeOverflow() {
        return JS.getBoolean(peer, "rangeOverflow");
    }

    public boolean isStepMismatch() {
        return JS.getBoolean(peer, "stepMismatch");
    }

    public boolean isBadInput() {
        return JS.getBoolean(peer, "badInput");
    }

    public boolean isCustomError() {
        return JS.getBoolean(peer, "customError");
    }

    public boolean isValid() {
        return JS.getBoolean(peer, "valid");
    }
}
