// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.JSPromise;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class HTMLMediaElement extends HTMLElement {
    protected HTMLMediaElement(Object peer) {
        super(peer);
    }

    public static HTMLMediaElement wrap(Object peer) {
        return peer == null ? null : new HTMLMediaElement(peer);
    }

    public MediaError getError() {
        return MediaError.wrap(JS.get(peer, "error"));
    }

    public String getSrc() {
        return JS.string(JS.get(peer, "src"));
    }

    public void setSrc(String value) {
        JS.set(peer, "src", JS.param(value));
    }

    public WrappedObject getSrcObject() {
        return WrappedObject.wrap(JS.get(peer, "srcObject"));
    }

    public void setSrcObject(WrappedObject value) {
        JS.set(peer, "srcObject", value.peer);
    }

    public String getCurrentSrc() {
        return JS.string(JS.get(peer, "currentSrc"));
    }

    public String getCrossOrigin() {
        return JS.string(JS.get(peer, "crossOrigin"));
    }

    public void setCrossOrigin(String value) {
        JS.set(peer, "crossOrigin", JS.param(value));
    }

    public int getNetworkState() {
        return JS.getInt(peer, "networkState");
    }

    public String getPreload() {
        return JS.string(JS.get(peer, "preload"));
    }

    public void setPreload(String value) {
        JS.set(peer, "preload", JS.param(value));
    }

    public TimeRanges getBuffered() {
        return TimeRanges.wrap(JS.get(peer, "buffered"));
    }

    public int getReadyState() {
        return JS.getInt(peer, "readyState");
    }

    public boolean isSeeking() {
        return JS.getBoolean(peer, "seeking");
    }

    public double getCurrentTime() {
        return JS.getDouble(peer, "currentTime");
    }

    public void setCurrentTime(double value) {
        JS.set(peer, "currentTime", value);
    }

    public double getDuration() {
        return JS.getDouble(peer, "duration");
    }

    public boolean isPaused() {
        return JS.getBoolean(peer, "paused");
    }

    public double getDefaultPlaybackRate() {
        return JS.getDouble(peer, "defaultPlaybackRate");
    }

    public void setDefaultPlaybackRate(double value) {
        JS.set(peer, "defaultPlaybackRate", value);
    }

    public double getPlaybackRate() {
        return JS.getDouble(peer, "playbackRate");
    }

    public void setPlaybackRate(double value) {
        JS.set(peer, "playbackRate", value);
    }

    public boolean isPreservesPitch() {
        return JS.getBoolean(peer, "preservesPitch");
    }

    public void setPreservesPitch(boolean value) {
        JS.set(peer, "preservesPitch", value);
    }

    public TimeRanges getPlayed() {
        return TimeRanges.wrap(JS.get(peer, "played"));
    }

    public TimeRanges getSeekable() {
        return TimeRanges.wrap(JS.get(peer, "seekable"));
    }

    public boolean isEnded() {
        return JS.getBoolean(peer, "ended");
    }

    public boolean isAutoplay() {
        return JS.getBoolean(peer, "autoplay");
    }

    public void setAutoplay(boolean value) {
        JS.set(peer, "autoplay", value);
    }

    public boolean isLoop() {
        return JS.getBoolean(peer, "loop");
    }

    public void setLoop(boolean value) {
        JS.set(peer, "loop", value);
    }

    public boolean isControls() {
        return JS.getBoolean(peer, "controls");
    }

    public void setControls(boolean value) {
        JS.set(peer, "controls", value);
    }

    public double getVolume() {
        return JS.getDouble(peer, "volume");
    }

    public void setVolume(double value) {
        JS.set(peer, "volume", value);
    }

    public boolean isMuted() {
        return JS.getBoolean(peer, "muted");
    }

    public void setMuted(boolean value) {
        JS.set(peer, "muted", value);
    }

    public boolean isDefaultMuted() {
        return JS.getBoolean(peer, "defaultMuted");
    }

    public void setDefaultMuted(boolean value) {
        JS.set(peer, "defaultMuted", value);
    }

    public AudioTrackList getAudioTracks() {
        return AudioTrackList.wrap(JS.get(peer, "audioTracks"));
    }

    public VideoTrackList getVideoTracks() {
        return VideoTrackList.wrap(JS.get(peer, "videoTracks"));
    }

    public TextTrackList getTextTracks() {
        return TextTrackList.wrap(JS.get(peer, "textTracks"));
    }

    public void load() {
        JS.invoke(peer, "load");
    }

    public WrappedObject canPlayType(String type) {
        return WrappedObject.wrap(JS.invoke(peer, "canPlayType", JS.param(type)));
    }

    public void fastSeek(double time) {
        JS.invoke(peer, "fastSeek", time);
    }

    public WrappedObject getStartDate() {
        return WrappedObject.wrap(JS.invoke(peer, "getStartDate"));
    }

    public JSPromise<Void> play() {
        return JSPromise.wrap(Void.class, JS.invoke(peer, "play"));
    }

    public void pause() {
        JS.invoke(peer, "pause");
    }

    public TextTrack addTextTrack(WrappedObject kind, String label, String language) {
        return TextTrack.wrap(JS.invoke(peer, "addTextTrack", JS.param(kind), JS.param(label), JS.param(language)));
    }

    public TextTrack addTextTrack(WrappedObject kind, String label) {
        return TextTrack.wrap(JS.invoke(peer, "addTextTrack", JS.param(kind), JS.param(label)));
    }

    public TextTrack addTextTrack(WrappedObject kind) {
        return TextTrack.wrap(JS.invoke(peer, "addTextTrack", JS.param(kind)));
    }
}
