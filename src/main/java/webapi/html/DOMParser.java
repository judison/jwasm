// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;
import webapi.dom.Document;

@SuppressWarnings({"unused"})
public class DOMParser extends WrappedObject {
    protected DOMParser(Object peer) {
        super(peer);
    }

    public static DOMParser wrap(Object peer) {
        return peer == null ? null : new DOMParser(peer);
    }

    public Document parseFromString(String string, WrappedObject type) {
        return Document.wrap(JS.invoke(peer, "parseFromString", JS.param(string), JS.param(type)));
    }
}
