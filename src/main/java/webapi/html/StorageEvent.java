// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.dom.Event;

@SuppressWarnings({"unused"})
public class StorageEvent extends Event {
    protected StorageEvent(Object peer) {
        super(peer);
    }

    public static StorageEvent wrap(Object peer) {
        return peer == null ? null : new StorageEvent(peer);
    }

    public String getKey() {
        return JS.string(JS.get(peer, "key"));
    }

    public String getOldValue() {
        return JS.string(JS.get(peer, "oldValue"));
    }

    public String getNewValue() {
        return JS.string(JS.get(peer, "newValue"));
    }

    public String getUrl() {
        return JS.string(JS.get(peer, "url"));
    }

    public Storage getStorageArea() {
        return Storage.wrap(JS.get(peer, "storageArea"));
    }

    public void initStorageEvent(String type, boolean bubbles, boolean cancelable, String key,
            String oldValue, String newValue, String url, Storage storageArea) {
        JS.invoke(peer, "initStorageEvent", JS.param(type), bubbles, cancelable, JS.param(key), JS.param(oldValue), JS.param(newValue), JS.param(url), JS.param(storageArea));
    }

    public void initStorageEvent(String type, boolean bubbles, boolean cancelable, String key,
            String oldValue, String newValue, String url) {
        JS.invoke(peer, "initStorageEvent", JS.param(type), bubbles, cancelable, JS.param(key), JS.param(oldValue), JS.param(newValue), JS.param(url));
    }

    public void initStorageEvent(String type, boolean bubbles, boolean cancelable, String key,
            String oldValue, String newValue) {
        JS.invoke(peer, "initStorageEvent", JS.param(type), bubbles, cancelable, JS.param(key), JS.param(oldValue), JS.param(newValue));
    }

    public void initStorageEvent(String type, boolean bubbles, boolean cancelable, String key,
            String oldValue) {
        JS.invoke(peer, "initStorageEvent", JS.param(type), bubbles, cancelable, JS.param(key), JS.param(oldValue));
    }

    public void initStorageEvent(String type, boolean bubbles, boolean cancelable, String key) {
        JS.invoke(peer, "initStorageEvent", JS.param(type), bubbles, cancelable, JS.param(key));
    }

    public void initStorageEvent(String type, boolean bubbles, boolean cancelable) {
        JS.invoke(peer, "initStorageEvent", JS.param(type), bubbles, cancelable);
    }

    public void initStorageEvent(String type, boolean bubbles) {
        JS.invoke(peer, "initStorageEvent", JS.param(type), bubbles);
    }

    public void initStorageEvent(String type) {
        JS.invoke(peer, "initStorageEvent", JS.param(type));
    }
}
