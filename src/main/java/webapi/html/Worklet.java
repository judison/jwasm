// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.JSObject;
import webapi.JSPromise;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class Worklet extends WrappedObject {
    protected Worklet(Object peer) {
        super(peer);
    }

    public static Worklet wrap(Object peer) {
        return peer == null ? null : new Worklet(peer);
    }

    public JSPromise<Void> addModule(String moduleURL, JSObject options) {
        return JSPromise.wrap(Void.class, JS.invoke(peer, "addModule", JS.param(moduleURL), JS.param(options)));
    }

    public JSPromise<Void> addModule(String moduleURL) {
        return JSPromise.wrap(Void.class, JS.invoke(peer, "addModule", JS.param(moduleURL)));
    }
}
