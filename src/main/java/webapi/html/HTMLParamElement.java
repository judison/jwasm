// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;

@SuppressWarnings({"unused"})
public class HTMLParamElement extends HTMLElement {
    protected HTMLParamElement(Object peer) {
        super(peer);
    }

    public static HTMLParamElement wrap(Object peer) {
        return peer == null ? null : new HTMLParamElement(peer);
    }

    public String getName() {
        return JS.string(JS.get(peer, "name"));
    }

    public void setName(String value) {
        JS.set(peer, "name", JS.param(value));
    }

    public String getValue() {
        return JS.string(JS.get(peer, "value"));
    }

    public void setValue(String value) {
        JS.set(peer, "value", JS.param(value));
    }

    public String getType() {
        return JS.string(JS.get(peer, "type"));
    }

    public void setType(String value) {
        JS.set(peer, "type", JS.param(value));
    }

    public String getValueType() {
        return JS.string(JS.get(peer, "valueType"));
    }

    public void setValueType(String value) {
        JS.set(peer, "valueType", JS.param(value));
    }
}
