// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.JSPromise;
import webapi.WrappedObject;
import webapi.dom.Event;

@SuppressWarnings({"unused"})
public class PromiseRejectionEvent extends Event {
    protected PromiseRejectionEvent(Object peer) {
        super(peer);
    }

    public static PromiseRejectionEvent wrap(Object peer) {
        return peer == null ? null : new PromiseRejectionEvent(peer);
    }

    public JSPromise<WrappedObject> getPromise() {
        return JSPromise.wrap(WrappedObject.class, JS.get(peer, "promise"));
    }

    public WrappedObject getReason() {
        return WrappedObject.wrap(JS.get(peer, "reason"));
    }
}
