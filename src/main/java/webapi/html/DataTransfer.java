// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;
import webapi.dom.Element;

@SuppressWarnings({"unused"})
public class DataTransfer extends WrappedObject {
    protected DataTransfer(Object peer) {
        super(peer);
    }

    public static DataTransfer wrap(Object peer) {
        return peer == null ? null : new DataTransfer(peer);
    }

    public String getDropEffect() {
        return JS.string(JS.get(peer, "dropEffect"));
    }

    public void setDropEffect(String value) {
        JS.set(peer, "dropEffect", JS.param(value));
    }

    public String getEffectAllowed() {
        return JS.string(JS.get(peer, "effectAllowed"));
    }

    public void setEffectAllowed(String value) {
        JS.set(peer, "effectAllowed", JS.param(value));
    }

    public DataTransferItemList getItems() {
        return DataTransferItemList.wrap(JS.get(peer, "items"));
    }

    public WrappedObject getTypes() {
        return WrappedObject.wrap(JS.get(peer, "types"));
    }

    public WrappedObject getFiles() {
        return WrappedObject.wrap(JS.get(peer, "files"));
    }

    public void setDragImage(Element image, long x, long y) {
        JS.invoke(peer, "setDragImage", JS.param(image), x, y);
    }

    public String getData(String format) {
        return JS.string(JS.invoke(peer, "getData", JS.param(format)));
    }

    public void setData(String format, String data) {
        JS.invoke(peer, "setData", JS.param(format), JS.param(data));
    }

    public void clearData(String format) {
        JS.invoke(peer, "clearData", JS.param(format));
    }

    public void clearData() {
        JS.invoke(peer, "clearData");
    }
}
