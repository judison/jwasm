// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.JSObject;
import webapi.JSPromise;
import webapi.JSSequence;
import webapi.WrappedObject;
import webapi.css.CSSStyleDeclaration;
import webapi.dom.Document;
import webapi.dom.Element;
import webapi.dom.EventTarget;

@SuppressWarnings({"unused"})
public class Window extends EventTarget {
    protected Window(Object peer) {
        super(peer);
    }

    public static Window wrap(Object peer) {
        return peer == null ? null : new Window(peer);
    }

    public WindowProxy getWindow() {
        return WindowProxy.wrap(JS.get(peer, "window"));
    }

    public Document getDocument() {
        return Document.wrap(JS.get(peer, "document"));
    }

    public String getName() {
        return JS.string(JS.get(peer, "name"));
    }

    public void setName(String value) {
        JS.set(peer, "name", JS.param(value));
    }

    public Location getLocation() {
        return Location.wrap(JS.get(peer, "location"));
    }

    public History getHistory() {
        return History.wrap(JS.get(peer, "history"));
    }

    public CustomElementRegistry getCustomElements() {
        return CustomElementRegistry.wrap(JS.get(peer, "customElements"));
    }

    public String getStatus() {
        return JS.string(JS.get(peer, "status"));
    }

    public void setStatus(String value) {
        JS.set(peer, "status", JS.param(value));
    }

    public boolean isClosed() {
        return JS.getBoolean(peer, "closed");
    }

    public WindowProxy getTop() {
        return WindowProxy.wrap(JS.get(peer, "top"));
    }

    public WrappedObject getOpener() {
        return WrappedObject.wrap(JS.get(peer, "opener"));
    }

    public void setOpener(WrappedObject value) {
        JS.set(peer, "opener", value.peer);
    }

    public Element getFrameElement() {
        return Element.wrap(JS.get(peer, "frameElement"));
    }

    public Navigator getNavigator() {
        return Navigator.wrap(JS.get(peer, "navigator"));
    }

    public Navigator getClientInformation() {
        return Navigator.wrap(JS.get(peer, "clientInformation"));
    }

    public boolean isOriginAgentCluster() {
        return JS.getBoolean(peer, "originAgentCluster");
    }

    public EventHandler getOnabort() {
        return EventHandler.wrap(JS.get(peer, "onabort"));
    }

    public void setOnabort(EventHandler value) {
        JS.set(peer, "onabort", value.peer);
    }

    public EventHandler getOnauxclick() {
        return EventHandler.wrap(JS.get(peer, "onauxclick"));
    }

    public void setOnauxclick(EventHandler value) {
        JS.set(peer, "onauxclick", value.peer);
    }

    public EventHandler getOnbeforeinput() {
        return EventHandler.wrap(JS.get(peer, "onbeforeinput"));
    }

    public void setOnbeforeinput(EventHandler value) {
        JS.set(peer, "onbeforeinput", value.peer);
    }

    public EventHandler getOnbeforematch() {
        return EventHandler.wrap(JS.get(peer, "onbeforematch"));
    }

    public void setOnbeforematch(EventHandler value) {
        JS.set(peer, "onbeforematch", value.peer);
    }

    public EventHandler getOnblur() {
        return EventHandler.wrap(JS.get(peer, "onblur"));
    }

    public void setOnblur(EventHandler value) {
        JS.set(peer, "onblur", value.peer);
    }

    public EventHandler getOncancel() {
        return EventHandler.wrap(JS.get(peer, "oncancel"));
    }

    public void setOncancel(EventHandler value) {
        JS.set(peer, "oncancel", value.peer);
    }

    public EventHandler getOncanplay() {
        return EventHandler.wrap(JS.get(peer, "oncanplay"));
    }

    public void setOncanplay(EventHandler value) {
        JS.set(peer, "oncanplay", value.peer);
    }

    public EventHandler getOncanplaythrough() {
        return EventHandler.wrap(JS.get(peer, "oncanplaythrough"));
    }

    public void setOncanplaythrough(EventHandler value) {
        JS.set(peer, "oncanplaythrough", value.peer);
    }

    public EventHandler getOnchange() {
        return EventHandler.wrap(JS.get(peer, "onchange"));
    }

    public void setOnchange(EventHandler value) {
        JS.set(peer, "onchange", value.peer);
    }

    public EventHandler getOnclick() {
        return EventHandler.wrap(JS.get(peer, "onclick"));
    }

    public void setOnclick(EventHandler value) {
        JS.set(peer, "onclick", value.peer);
    }

    public EventHandler getOnclose() {
        return EventHandler.wrap(JS.get(peer, "onclose"));
    }

    public void setOnclose(EventHandler value) {
        JS.set(peer, "onclose", value.peer);
    }

    public EventHandler getOncontextlost() {
        return EventHandler.wrap(JS.get(peer, "oncontextlost"));
    }

    public void setOncontextlost(EventHandler value) {
        JS.set(peer, "oncontextlost", value.peer);
    }

    public EventHandler getOncontextmenu() {
        return EventHandler.wrap(JS.get(peer, "oncontextmenu"));
    }

    public void setOncontextmenu(EventHandler value) {
        JS.set(peer, "oncontextmenu", value.peer);
    }

    public EventHandler getOncontextrestored() {
        return EventHandler.wrap(JS.get(peer, "oncontextrestored"));
    }

    public void setOncontextrestored(EventHandler value) {
        JS.set(peer, "oncontextrestored", value.peer);
    }

    public EventHandler getOncuechange() {
        return EventHandler.wrap(JS.get(peer, "oncuechange"));
    }

    public void setOncuechange(EventHandler value) {
        JS.set(peer, "oncuechange", value.peer);
    }

    public EventHandler getOndblclick() {
        return EventHandler.wrap(JS.get(peer, "ondblclick"));
    }

    public void setOndblclick(EventHandler value) {
        JS.set(peer, "ondblclick", value.peer);
    }

    public EventHandler getOndrag() {
        return EventHandler.wrap(JS.get(peer, "ondrag"));
    }

    public void setOndrag(EventHandler value) {
        JS.set(peer, "ondrag", value.peer);
    }

    public EventHandler getOndragend() {
        return EventHandler.wrap(JS.get(peer, "ondragend"));
    }

    public void setOndragend(EventHandler value) {
        JS.set(peer, "ondragend", value.peer);
    }

    public EventHandler getOndragenter() {
        return EventHandler.wrap(JS.get(peer, "ondragenter"));
    }

    public void setOndragenter(EventHandler value) {
        JS.set(peer, "ondragenter", value.peer);
    }

    public EventHandler getOndragleave() {
        return EventHandler.wrap(JS.get(peer, "ondragleave"));
    }

    public void setOndragleave(EventHandler value) {
        JS.set(peer, "ondragleave", value.peer);
    }

    public EventHandler getOndragover() {
        return EventHandler.wrap(JS.get(peer, "ondragover"));
    }

    public void setOndragover(EventHandler value) {
        JS.set(peer, "ondragover", value.peer);
    }

    public EventHandler getOndragstart() {
        return EventHandler.wrap(JS.get(peer, "ondragstart"));
    }

    public void setOndragstart(EventHandler value) {
        JS.set(peer, "ondragstart", value.peer);
    }

    public EventHandler getOndrop() {
        return EventHandler.wrap(JS.get(peer, "ondrop"));
    }

    public void setOndrop(EventHandler value) {
        JS.set(peer, "ondrop", value.peer);
    }

    public EventHandler getOndurationchange() {
        return EventHandler.wrap(JS.get(peer, "ondurationchange"));
    }

    public void setOndurationchange(EventHandler value) {
        JS.set(peer, "ondurationchange", value.peer);
    }

    public EventHandler getOnemptied() {
        return EventHandler.wrap(JS.get(peer, "onemptied"));
    }

    public void setOnemptied(EventHandler value) {
        JS.set(peer, "onemptied", value.peer);
    }

    public EventHandler getOnended() {
        return EventHandler.wrap(JS.get(peer, "onended"));
    }

    public void setOnended(EventHandler value) {
        JS.set(peer, "onended", value.peer);
    }

    public WrappedObject getOnerror() {
        return WrappedObject.wrap(JS.get(peer, "onerror"));
    }

    public void setOnerror(WrappedObject value) {
        JS.set(peer, "onerror", value.peer);
    }

    public EventHandler getOnfocus() {
        return EventHandler.wrap(JS.get(peer, "onfocus"));
    }

    public void setOnfocus(EventHandler value) {
        JS.set(peer, "onfocus", value.peer);
    }

    public EventHandler getOnformdata() {
        return EventHandler.wrap(JS.get(peer, "onformdata"));
    }

    public void setOnformdata(EventHandler value) {
        JS.set(peer, "onformdata", value.peer);
    }

    public EventHandler getOninput() {
        return EventHandler.wrap(JS.get(peer, "oninput"));
    }

    public void setOninput(EventHandler value) {
        JS.set(peer, "oninput", value.peer);
    }

    public EventHandler getOninvalid() {
        return EventHandler.wrap(JS.get(peer, "oninvalid"));
    }

    public void setOninvalid(EventHandler value) {
        JS.set(peer, "oninvalid", value.peer);
    }

    public EventHandler getOnkeydown() {
        return EventHandler.wrap(JS.get(peer, "onkeydown"));
    }

    public void setOnkeydown(EventHandler value) {
        JS.set(peer, "onkeydown", value.peer);
    }

    public EventHandler getOnkeypress() {
        return EventHandler.wrap(JS.get(peer, "onkeypress"));
    }

    public void setOnkeypress(EventHandler value) {
        JS.set(peer, "onkeypress", value.peer);
    }

    public EventHandler getOnkeyup() {
        return EventHandler.wrap(JS.get(peer, "onkeyup"));
    }

    public void setOnkeyup(EventHandler value) {
        JS.set(peer, "onkeyup", value.peer);
    }

    public EventHandler getOnload() {
        return EventHandler.wrap(JS.get(peer, "onload"));
    }

    public void setOnload(EventHandler value) {
        JS.set(peer, "onload", value.peer);
    }

    public EventHandler getOnloadeddata() {
        return EventHandler.wrap(JS.get(peer, "onloadeddata"));
    }

    public void setOnloadeddata(EventHandler value) {
        JS.set(peer, "onloadeddata", value.peer);
    }

    public EventHandler getOnloadedmetadata() {
        return EventHandler.wrap(JS.get(peer, "onloadedmetadata"));
    }

    public void setOnloadedmetadata(EventHandler value) {
        JS.set(peer, "onloadedmetadata", value.peer);
    }

    public EventHandler getOnloadstart() {
        return EventHandler.wrap(JS.get(peer, "onloadstart"));
    }

    public void setOnloadstart(EventHandler value) {
        JS.set(peer, "onloadstart", value.peer);
    }

    public EventHandler getOnmousedown() {
        return EventHandler.wrap(JS.get(peer, "onmousedown"));
    }

    public void setOnmousedown(EventHandler value) {
        JS.set(peer, "onmousedown", value.peer);
    }

    public EventHandler getOnmouseenter() {
        return EventHandler.wrap(JS.get(peer, "onmouseenter"));
    }

    public void setOnmouseenter(EventHandler value) {
        JS.set(peer, "onmouseenter", value.peer);
    }

    public EventHandler getOnmouseleave() {
        return EventHandler.wrap(JS.get(peer, "onmouseleave"));
    }

    public void setOnmouseleave(EventHandler value) {
        JS.set(peer, "onmouseleave", value.peer);
    }

    public EventHandler getOnmousemove() {
        return EventHandler.wrap(JS.get(peer, "onmousemove"));
    }

    public void setOnmousemove(EventHandler value) {
        JS.set(peer, "onmousemove", value.peer);
    }

    public EventHandler getOnmouseout() {
        return EventHandler.wrap(JS.get(peer, "onmouseout"));
    }

    public void setOnmouseout(EventHandler value) {
        JS.set(peer, "onmouseout", value.peer);
    }

    public EventHandler getOnmouseover() {
        return EventHandler.wrap(JS.get(peer, "onmouseover"));
    }

    public void setOnmouseover(EventHandler value) {
        JS.set(peer, "onmouseover", value.peer);
    }

    public EventHandler getOnmouseup() {
        return EventHandler.wrap(JS.get(peer, "onmouseup"));
    }

    public void setOnmouseup(EventHandler value) {
        JS.set(peer, "onmouseup", value.peer);
    }

    public EventHandler getOnpause() {
        return EventHandler.wrap(JS.get(peer, "onpause"));
    }

    public void setOnpause(EventHandler value) {
        JS.set(peer, "onpause", value.peer);
    }

    public EventHandler getOnplay() {
        return EventHandler.wrap(JS.get(peer, "onplay"));
    }

    public void setOnplay(EventHandler value) {
        JS.set(peer, "onplay", value.peer);
    }

    public EventHandler getOnplaying() {
        return EventHandler.wrap(JS.get(peer, "onplaying"));
    }

    public void setOnplaying(EventHandler value) {
        JS.set(peer, "onplaying", value.peer);
    }

    public EventHandler getOnprogress() {
        return EventHandler.wrap(JS.get(peer, "onprogress"));
    }

    public void setOnprogress(EventHandler value) {
        JS.set(peer, "onprogress", value.peer);
    }

    public EventHandler getOnratechange() {
        return EventHandler.wrap(JS.get(peer, "onratechange"));
    }

    public void setOnratechange(EventHandler value) {
        JS.set(peer, "onratechange", value.peer);
    }

    public EventHandler getOnreset() {
        return EventHandler.wrap(JS.get(peer, "onreset"));
    }

    public void setOnreset(EventHandler value) {
        JS.set(peer, "onreset", value.peer);
    }

    public EventHandler getOnresize() {
        return EventHandler.wrap(JS.get(peer, "onresize"));
    }

    public void setOnresize(EventHandler value) {
        JS.set(peer, "onresize", value.peer);
    }

    public EventHandler getOnscroll() {
        return EventHandler.wrap(JS.get(peer, "onscroll"));
    }

    public void setOnscroll(EventHandler value) {
        JS.set(peer, "onscroll", value.peer);
    }

    public EventHandler getOnsecuritypolicyviolation() {
        return EventHandler.wrap(JS.get(peer, "onsecuritypolicyviolation"));
    }

    public void setOnsecuritypolicyviolation(EventHandler value) {
        JS.set(peer, "onsecuritypolicyviolation", value.peer);
    }

    public EventHandler getOnseeked() {
        return EventHandler.wrap(JS.get(peer, "onseeked"));
    }

    public void setOnseeked(EventHandler value) {
        JS.set(peer, "onseeked", value.peer);
    }

    public EventHandler getOnseeking() {
        return EventHandler.wrap(JS.get(peer, "onseeking"));
    }

    public void setOnseeking(EventHandler value) {
        JS.set(peer, "onseeking", value.peer);
    }

    public EventHandler getOnselect() {
        return EventHandler.wrap(JS.get(peer, "onselect"));
    }

    public void setOnselect(EventHandler value) {
        JS.set(peer, "onselect", value.peer);
    }

    public EventHandler getOnslotchange() {
        return EventHandler.wrap(JS.get(peer, "onslotchange"));
    }

    public void setOnslotchange(EventHandler value) {
        JS.set(peer, "onslotchange", value.peer);
    }

    public EventHandler getOnstalled() {
        return EventHandler.wrap(JS.get(peer, "onstalled"));
    }

    public void setOnstalled(EventHandler value) {
        JS.set(peer, "onstalled", value.peer);
    }

    public EventHandler getOnsubmit() {
        return EventHandler.wrap(JS.get(peer, "onsubmit"));
    }

    public void setOnsubmit(EventHandler value) {
        JS.set(peer, "onsubmit", value.peer);
    }

    public EventHandler getOnsuspend() {
        return EventHandler.wrap(JS.get(peer, "onsuspend"));
    }

    public void setOnsuspend(EventHandler value) {
        JS.set(peer, "onsuspend", value.peer);
    }

    public EventHandler getOntimeupdate() {
        return EventHandler.wrap(JS.get(peer, "ontimeupdate"));
    }

    public void setOntimeupdate(EventHandler value) {
        JS.set(peer, "ontimeupdate", value.peer);
    }

    public EventHandler getOntoggle() {
        return EventHandler.wrap(JS.get(peer, "ontoggle"));
    }

    public void setOntoggle(EventHandler value) {
        JS.set(peer, "ontoggle", value.peer);
    }

    public EventHandler getOnvolumechange() {
        return EventHandler.wrap(JS.get(peer, "onvolumechange"));
    }

    public void setOnvolumechange(EventHandler value) {
        JS.set(peer, "onvolumechange", value.peer);
    }

    public EventHandler getOnwaiting() {
        return EventHandler.wrap(JS.get(peer, "onwaiting"));
    }

    public void setOnwaiting(EventHandler value) {
        JS.set(peer, "onwaiting", value.peer);
    }

    public EventHandler getOnwebkitanimationend() {
        return EventHandler.wrap(JS.get(peer, "onwebkitanimationend"));
    }

    public void setOnwebkitanimationend(EventHandler value) {
        JS.set(peer, "onwebkitanimationend", value.peer);
    }

    public EventHandler getOnwebkitanimationiteration() {
        return EventHandler.wrap(JS.get(peer, "onwebkitanimationiteration"));
    }

    public void setOnwebkitanimationiteration(EventHandler value) {
        JS.set(peer, "onwebkitanimationiteration", value.peer);
    }

    public EventHandler getOnwebkitanimationstart() {
        return EventHandler.wrap(JS.get(peer, "onwebkitanimationstart"));
    }

    public void setOnwebkitanimationstart(EventHandler value) {
        JS.set(peer, "onwebkitanimationstart", value.peer);
    }

    public EventHandler getOnwebkittransitionend() {
        return EventHandler.wrap(JS.get(peer, "onwebkittransitionend"));
    }

    public void setOnwebkittransitionend(EventHandler value) {
        JS.set(peer, "onwebkittransitionend", value.peer);
    }

    public EventHandler getOnwheel() {
        return EventHandler.wrap(JS.get(peer, "onwheel"));
    }

    public void setOnwheel(EventHandler value) {
        JS.set(peer, "onwheel", value.peer);
    }

    public EventHandler getOnafterprint() {
        return EventHandler.wrap(JS.get(peer, "onafterprint"));
    }

    public void setOnafterprint(EventHandler value) {
        JS.set(peer, "onafterprint", value.peer);
    }

    public EventHandler getOnbeforeprint() {
        return EventHandler.wrap(JS.get(peer, "onbeforeprint"));
    }

    public void setOnbeforeprint(EventHandler value) {
        JS.set(peer, "onbeforeprint", value.peer);
    }

    public WrappedObject getOnbeforeunload() {
        return WrappedObject.wrap(JS.get(peer, "onbeforeunload"));
    }

    public void setOnbeforeunload(WrappedObject value) {
        JS.set(peer, "onbeforeunload", value.peer);
    }

    public EventHandler getOnhashchange() {
        return EventHandler.wrap(JS.get(peer, "onhashchange"));
    }

    public void setOnhashchange(EventHandler value) {
        JS.set(peer, "onhashchange", value.peer);
    }

    public EventHandler getOnlanguagechange() {
        return EventHandler.wrap(JS.get(peer, "onlanguagechange"));
    }

    public void setOnlanguagechange(EventHandler value) {
        JS.set(peer, "onlanguagechange", value.peer);
    }

    public EventHandler getOnmessage() {
        return EventHandler.wrap(JS.get(peer, "onmessage"));
    }

    public void setOnmessage(EventHandler value) {
        JS.set(peer, "onmessage", value.peer);
    }

    public EventHandler getOnmessageerror() {
        return EventHandler.wrap(JS.get(peer, "onmessageerror"));
    }

    public void setOnmessageerror(EventHandler value) {
        JS.set(peer, "onmessageerror", value.peer);
    }

    public EventHandler getOnoffline() {
        return EventHandler.wrap(JS.get(peer, "onoffline"));
    }

    public void setOnoffline(EventHandler value) {
        JS.set(peer, "onoffline", value.peer);
    }

    public EventHandler getOnonline() {
        return EventHandler.wrap(JS.get(peer, "ononline"));
    }

    public void setOnonline(EventHandler value) {
        JS.set(peer, "ononline", value.peer);
    }

    public EventHandler getOnpagehide() {
        return EventHandler.wrap(JS.get(peer, "onpagehide"));
    }

    public void setOnpagehide(EventHandler value) {
        JS.set(peer, "onpagehide", value.peer);
    }

    public EventHandler getOnpageshow() {
        return EventHandler.wrap(JS.get(peer, "onpageshow"));
    }

    public void setOnpageshow(EventHandler value) {
        JS.set(peer, "onpageshow", value.peer);
    }

    public EventHandler getOnpopstate() {
        return EventHandler.wrap(JS.get(peer, "onpopstate"));
    }

    public void setOnpopstate(EventHandler value) {
        JS.set(peer, "onpopstate", value.peer);
    }

    public EventHandler getOnrejectionhandled() {
        return EventHandler.wrap(JS.get(peer, "onrejectionhandled"));
    }

    public void setOnrejectionhandled(EventHandler value) {
        JS.set(peer, "onrejectionhandled", value.peer);
    }

    public EventHandler getOnstorage() {
        return EventHandler.wrap(JS.get(peer, "onstorage"));
    }

    public void setOnstorage(EventHandler value) {
        JS.set(peer, "onstorage", value.peer);
    }

    public EventHandler getOnunhandledrejection() {
        return EventHandler.wrap(JS.get(peer, "onunhandledrejection"));
    }

    public void setOnunhandledrejection(EventHandler value) {
        JS.set(peer, "onunhandledrejection", value.peer);
    }

    public EventHandler getOnunload() {
        return EventHandler.wrap(JS.get(peer, "onunload"));
    }

    public void setOnunload(EventHandler value) {
        JS.set(peer, "onunload", value.peer);
    }

    public boolean isIsSecureContext() {
        return JS.getBoolean(peer, "isSecureContext");
    }

    public boolean isCrossOriginIsolated() {
        return JS.getBoolean(peer, "crossOriginIsolated");
    }

    public Storage getSessionStorage() {
        return Storage.wrap(JS.get(peer, "sessionStorage"));
    }

    public Storage getLocalStorage() {
        return Storage.wrap(JS.get(peer, "localStorage"));
    }

    public void close() {
        JS.invoke(peer, "close");
    }

    public void stop() {
        JS.invoke(peer, "stop");
    }

    public void focus() {
        JS.invoke(peer, "focus");
    }

    public void blur() {
        JS.invoke(peer, "blur");
    }

    public WindowProxy open(String url, String target, String features) {
        return WindowProxy.wrap(JS.invoke(peer, "open", JS.param(url), JS.param(target), JS.param(features)));
    }

    public WindowProxy open(String url, String target) {
        return WindowProxy.wrap(JS.invoke(peer, "open", JS.param(url), JS.param(target)));
    }

    public WindowProxy open(String url) {
        return WindowProxy.wrap(JS.invoke(peer, "open", JS.param(url)));
    }

    public WindowProxy open() {
        return WindowProxy.wrap(JS.invoke(peer, "open"));
    }

    public void alert() {
        JS.invoke(peer, "alert");
    }

    public void alert(String message) {
        JS.invoke(peer, "alert", JS.param(message));
    }

    public boolean confirm(String message) {
        return JS.invoke(peer, "confirm", JS.param(message));
    }

    public boolean confirm() {
        return JS.invoke(peer, "confirm");
    }

    public String prompt(String message, String default_) {
        return JS.string(JS.invoke(peer, "prompt", JS.param(message), JS.param(default_)));
    }

    public String prompt(String message) {
        return JS.string(JS.invoke(peer, "prompt", JS.param(message)));
    }

    public String prompt() {
        return JS.string(JS.invoke(peer, "prompt"));
    }

    public void print() {
        JS.invoke(peer, "print");
    }

    public void postMessage(WrappedObject message, String targetOrigin,
            JSSequence<WrappedObject> transfer) {
        JS.invoke(peer, "postMessage", JS.param(message), JS.param(targetOrigin), JS.param(transfer));
    }

    public void postMessage(WrappedObject message, String targetOrigin) {
        JS.invoke(peer, "postMessage", JS.param(message), JS.param(targetOrigin));
    }

    public void postMessage(WrappedObject message, JSObject options) {
        JS.invoke(peer, "postMessage", JS.param(message), JS.param(options));
    }

    public void postMessage(WrappedObject message) {
        JS.invoke(peer, "postMessage", JS.param(message));
    }

    public void captureEvents() {
        JS.invoke(peer, "captureEvents");
    }

    public void releaseEvents() {
        JS.invoke(peer, "releaseEvents");
    }

    public CSSStyleDeclaration getComputedStyle(Element elt, String pseudoElt) {
        return CSSStyleDeclaration.wrap(JS.invoke(peer, "getComputedStyle", JS.param(elt), JS.param(pseudoElt)));
    }

    public CSSStyleDeclaration getComputedStyle(Element elt) {
        return CSSStyleDeclaration.wrap(JS.invoke(peer, "getComputedStyle", JS.param(elt)));
    }

    public void reportError(WrappedObject e) {
        JS.invoke(peer, "reportError", JS.param(e));
    }

    public String btoa(String data) {
        return JS.string(JS.invoke(peer, "btoa", JS.param(data)));
    }

    public String atob(String data) {
        return JS.string(JS.invoke(peer, "atob", JS.param(data)));
    }

    public long setTimeout(WrappedObject handler, long timeout, WrappedObject arguments) {
        return JS.invoke(peer, "setTimeout", JS.param(handler), timeout, JS.param(arguments));
    }

    public long setTimeout(WrappedObject handler, WrappedObject arguments) {
        return JS.invoke(peer, "setTimeout", JS.param(handler), JS.param(arguments));
    }

    public void clearTimeout(long id) {
        JS.invoke(peer, "clearTimeout", id);
    }

    public void clearTimeout() {
        JS.invoke(peer, "clearTimeout");
    }

    public long setInterval(WrappedObject handler, long timeout, WrappedObject arguments) {
        return JS.invoke(peer, "setInterval", JS.param(handler), timeout, JS.param(arguments));
    }

    public long setInterval(WrappedObject handler, WrappedObject arguments) {
        return JS.invoke(peer, "setInterval", JS.param(handler), JS.param(arguments));
    }

    public void clearInterval(long id) {
        JS.invoke(peer, "clearInterval", id);
    }

    public void clearInterval() {
        JS.invoke(peer, "clearInterval");
    }

    public void queueMicrotask(WrappedObject callback) {
        JS.invoke(peer, "queueMicrotask", JS.param(callback));
    }

    public JSPromise<ImageBitmap> createImageBitmap(WrappedObject image, JSObject options) {
        return JSPromise.wrap(ImageBitmap.class, JS.invoke(peer, "createImageBitmap", JS.param(image), JS.param(options)));
    }

    public JSPromise<ImageBitmap> createImageBitmap(WrappedObject image) {
        return JSPromise.wrap(ImageBitmap.class, JS.invoke(peer, "createImageBitmap", JS.param(image)));
    }

    public JSPromise<ImageBitmap> createImageBitmap(WrappedObject image, long sx, long sy, long sw,
            long sh, JSObject options) {
        return JSPromise.wrap(ImageBitmap.class, JS.invoke(peer, "createImageBitmap", JS.param(image), sx, sy, sw, sh, JS.param(options)));
    }

    public JSPromise<ImageBitmap> createImageBitmap(WrappedObject image, long sx, long sy, long sw,
            long sh) {
        return JSPromise.wrap(ImageBitmap.class, JS.invoke(peer, "createImageBitmap", JS.param(image), sx, sy, sw, sh));
    }

    public WrappedObject structuredClone(WrappedObject value, JSObject options) {
        return WrappedObject.wrap(JS.invoke(peer, "structuredClone", JS.param(value), JS.param(options)));
    }

    public WrappedObject structuredClone(WrappedObject value) {
        return WrappedObject.wrap(JS.invoke(peer, "structuredClone", JS.param(value)));
    }

    public int requestAnimationFrame(FrameRequestCallback callback) {
        return JS.invoke(peer, "requestAnimationFrame", JS.param(callback));
    }

    public void cancelAnimationFrame(int handle) {
        JS.invoke(peer, "cancelAnimationFrame", handle);
    }
}
