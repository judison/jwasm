// Generated file; DO NOT edit
package webapi.html;

import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class DOMStringMap extends WrappedObject {
    protected DOMStringMap(Object peer) {
        super(peer);
    }

    public static DOMStringMap wrap(Object peer) {
        return peer == null ? null : new DOMStringMap(peer);
    }
}
