// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.dom.NodeList;

@SuppressWarnings({"unused"})
public class HTMLProgressElement extends HTMLElement {
    protected HTMLProgressElement(Object peer) {
        super(peer);
    }

    public static HTMLProgressElement wrap(Object peer) {
        return peer == null ? null : new HTMLProgressElement(peer);
    }

    public double getValue() {
        return JS.getDouble(peer, "value");
    }

    public void setValue(double value) {
        JS.set(peer, "value", value);
    }

    public double getMax() {
        return JS.getDouble(peer, "max");
    }

    public void setMax(double value) {
        JS.set(peer, "max", value);
    }

    public double getPosition() {
        return JS.getDouble(peer, "position");
    }

    public NodeList getLabels() {
        return NodeList.wrap(JS.get(peer, "labels"));
    }
}
