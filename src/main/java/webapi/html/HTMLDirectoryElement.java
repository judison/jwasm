// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;

@SuppressWarnings({"unused"})
public class HTMLDirectoryElement extends HTMLElement {
    protected HTMLDirectoryElement(Object peer) {
        super(peer);
    }

    public static HTMLDirectoryElement wrap(Object peer) {
        return peer == null ? null : new HTMLDirectoryElement(peer);
    }

    public boolean isCompact() {
        return JS.getBoolean(peer, "compact");
    }

    public void setCompact(boolean value) {
        JS.set(peer, "compact", value);
    }
}
