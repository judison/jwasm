// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.dom.Document;

@SuppressWarnings({"unused"})
public class HTMLEmbedElement extends HTMLElement {
    protected HTMLEmbedElement(Object peer) {
        super(peer);
    }

    public static HTMLEmbedElement wrap(Object peer) {
        return peer == null ? null : new HTMLEmbedElement(peer);
    }

    public String getSrc() {
        return JS.string(JS.get(peer, "src"));
    }

    public void setSrc(String value) {
        JS.set(peer, "src", JS.param(value));
    }

    public String getType() {
        return JS.string(JS.get(peer, "type"));
    }

    public void setType(String value) {
        JS.set(peer, "type", JS.param(value));
    }

    public String getWidth() {
        return JS.string(JS.get(peer, "width"));
    }

    public void setWidth(String value) {
        JS.set(peer, "width", JS.param(value));
    }

    public String getHeight() {
        return JS.string(JS.get(peer, "height"));
    }

    public void setHeight(String value) {
        JS.set(peer, "height", JS.param(value));
    }

    public String getAlign() {
        return JS.string(JS.get(peer, "align"));
    }

    public void setAlign(String value) {
        JS.set(peer, "align", JS.param(value));
    }

    public String getName() {
        return JS.string(JS.get(peer, "name"));
    }

    public void setName(String value) {
        JS.set(peer, "name", JS.param(value));
    }

    public Document getSVGDocument() {
        return Document.wrap(JS.invoke(peer, "getSVGDocument"));
    }
}
