// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class ImageBitmapRenderingContext extends WrappedObject {
    protected ImageBitmapRenderingContext(Object peer) {
        super(peer);
    }

    public static ImageBitmapRenderingContext wrap(Object peer) {
        return peer == null ? null : new ImageBitmapRenderingContext(peer);
    }

    public WrappedObject getCanvas() {
        return WrappedObject.wrap(JS.get(peer, "canvas"));
    }

    public void transferFromImageBitmap(ImageBitmap bitmap) {
        JS.invoke(peer, "transferFromImageBitmap", JS.param(bitmap));
    }
}
