// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.dom.EventTarget;

@SuppressWarnings({"unused"})
public class VideoTrackList extends EventTarget {
    protected VideoTrackList(Object peer) {
        super(peer);
    }

    public static VideoTrackList wrap(Object peer) {
        return peer == null ? null : new VideoTrackList(peer);
    }

    public int getLength() {
        return JS.getInt(peer, "length");
    }

    public long getSelectedIndex() {
        return (long)JS.get(peer, "selectedIndex");
    }

    public EventHandler getOnchange() {
        return EventHandler.wrap(JS.get(peer, "onchange"));
    }

    public void setOnchange(EventHandler value) {
        JS.set(peer, "onchange", value.peer);
    }

    public EventHandler getOnaddtrack() {
        return EventHandler.wrap(JS.get(peer, "onaddtrack"));
    }

    public void setOnaddtrack(EventHandler value) {
        JS.set(peer, "onaddtrack", value.peer);
    }

    public EventHandler getOnremovetrack() {
        return EventHandler.wrap(JS.get(peer, "onremovetrack"));
    }

    public void setOnremovetrack(EventHandler value) {
        JS.set(peer, "onremovetrack", value.peer);
    }

    public VideoTrack getTrackById(String id) {
        return VideoTrack.wrap(JS.invoke(peer, "getTrackById", JS.param(id)));
    }
}
