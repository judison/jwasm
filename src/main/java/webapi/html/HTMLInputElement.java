// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;
import webapi.dom.NodeList;

@SuppressWarnings({"unused"})
public class HTMLInputElement extends HTMLElement {
    protected HTMLInputElement(Object peer) {
        super(peer);
    }

    public static HTMLInputElement wrap(Object peer) {
        return peer == null ? null : new HTMLInputElement(peer);
    }

    public String getAccept() {
        return JS.string(JS.get(peer, "accept"));
    }

    public void setAccept(String value) {
        JS.set(peer, "accept", JS.param(value));
    }

    public String getAlt() {
        return JS.string(JS.get(peer, "alt"));
    }

    public void setAlt(String value) {
        JS.set(peer, "alt", JS.param(value));
    }

    public String getAutocomplete() {
        return JS.string(JS.get(peer, "autocomplete"));
    }

    public void setAutocomplete(String value) {
        JS.set(peer, "autocomplete", JS.param(value));
    }

    public boolean isDefaultChecked() {
        return JS.getBoolean(peer, "defaultChecked");
    }

    public void setDefaultChecked(boolean value) {
        JS.set(peer, "defaultChecked", value);
    }

    public boolean isChecked() {
        return JS.getBoolean(peer, "checked");
    }

    public void setChecked(boolean value) {
        JS.set(peer, "checked", value);
    }

    public String getDirName() {
        return JS.string(JS.get(peer, "dirName"));
    }

    public void setDirName(String value) {
        JS.set(peer, "dirName", JS.param(value));
    }

    public boolean isDisabled() {
        return JS.getBoolean(peer, "disabled");
    }

    public void setDisabled(boolean value) {
        JS.set(peer, "disabled", value);
    }

    public HTMLFormElement getForm() {
        return HTMLFormElement.wrap(JS.get(peer, "form"));
    }

    public WrappedObject getFiles() {
        return WrappedObject.wrap(JS.get(peer, "files"));
    }

    public void setFiles(WrappedObject value) {
        JS.set(peer, "files", value.peer);
    }

    public String getFormAction() {
        return JS.string(JS.get(peer, "formAction"));
    }

    public void setFormAction(String value) {
        JS.set(peer, "formAction", JS.param(value));
    }

    public String getFormEnctype() {
        return JS.string(JS.get(peer, "formEnctype"));
    }

    public void setFormEnctype(String value) {
        JS.set(peer, "formEnctype", JS.param(value));
    }

    public String getFormMethod() {
        return JS.string(JS.get(peer, "formMethod"));
    }

    public void setFormMethod(String value) {
        JS.set(peer, "formMethod", JS.param(value));
    }

    public boolean isFormNoValidate() {
        return JS.getBoolean(peer, "formNoValidate");
    }

    public void setFormNoValidate(boolean value) {
        JS.set(peer, "formNoValidate", value);
    }

    public String getFormTarget() {
        return JS.string(JS.get(peer, "formTarget"));
    }

    public void setFormTarget(String value) {
        JS.set(peer, "formTarget", JS.param(value));
    }

    public int getHeight() {
        return JS.getInt(peer, "height");
    }

    public void setHeight(int value) {
        JS.set(peer, "height", value);
    }

    public boolean isIndeterminate() {
        return JS.getBoolean(peer, "indeterminate");
    }

    public void setIndeterminate(boolean value) {
        JS.set(peer, "indeterminate", value);
    }

    public HTMLElement getList() {
        return HTMLElement.wrap(JS.get(peer, "list"));
    }

    public String getMax() {
        return JS.string(JS.get(peer, "max"));
    }

    public void setMax(String value) {
        JS.set(peer, "max", JS.param(value));
    }

    public long getMaxLength() {
        return (long)JS.get(peer, "maxLength");
    }

    public void setMaxLength(long value) {
        JS.set(peer, "maxLength", value);
    }

    public String getMin() {
        return JS.string(JS.get(peer, "min"));
    }

    public void setMin(String value) {
        JS.set(peer, "min", JS.param(value));
    }

    public long getMinLength() {
        return (long)JS.get(peer, "minLength");
    }

    public void setMinLength(long value) {
        JS.set(peer, "minLength", value);
    }

    public boolean isMultiple() {
        return JS.getBoolean(peer, "multiple");
    }

    public void setMultiple(boolean value) {
        JS.set(peer, "multiple", value);
    }

    public String getName() {
        return JS.string(JS.get(peer, "name"));
    }

    public void setName(String value) {
        JS.set(peer, "name", JS.param(value));
    }

    public String getPattern() {
        return JS.string(JS.get(peer, "pattern"));
    }

    public void setPattern(String value) {
        JS.set(peer, "pattern", JS.param(value));
    }

    public String getPlaceholder() {
        return JS.string(JS.get(peer, "placeholder"));
    }

    public void setPlaceholder(String value) {
        JS.set(peer, "placeholder", JS.param(value));
    }

    public boolean isReadOnly() {
        return JS.getBoolean(peer, "readOnly");
    }

    public void setReadOnly(boolean value) {
        JS.set(peer, "readOnly", value);
    }

    public boolean isRequired() {
        return JS.getBoolean(peer, "required");
    }

    public void setRequired(boolean value) {
        JS.set(peer, "required", value);
    }

    public int getSize() {
        return JS.getInt(peer, "size");
    }

    public void setSize(int value) {
        JS.set(peer, "size", value);
    }

    public String getSrc() {
        return JS.string(JS.get(peer, "src"));
    }

    public void setSrc(String value) {
        JS.set(peer, "src", JS.param(value));
    }

    public String getStep() {
        return JS.string(JS.get(peer, "step"));
    }

    public void setStep(String value) {
        JS.set(peer, "step", JS.param(value));
    }

    public String getType() {
        return JS.string(JS.get(peer, "type"));
    }

    public void setType(String value) {
        JS.set(peer, "type", JS.param(value));
    }

    public String getDefaultValue() {
        return JS.string(JS.get(peer, "defaultValue"));
    }

    public void setDefaultValue(String value) {
        JS.set(peer, "defaultValue", JS.param(value));
    }

    public String getValue() {
        return JS.string(JS.get(peer, "value"));
    }

    public void setValue(String value) {
        JS.set(peer, "value", JS.param(value));
    }

    public WrappedObject getValueAsDate() {
        return WrappedObject.wrap(JS.get(peer, "valueAsDate"));
    }

    public void setValueAsDate(WrappedObject value) {
        JS.set(peer, "valueAsDate", value.peer);
    }

    public double getValueAsNumber() {
        return JS.getDouble(peer, "valueAsNumber");
    }

    public void setValueAsNumber(double value) {
        JS.set(peer, "valueAsNumber", value);
    }

    public int getWidth() {
        return JS.getInt(peer, "width");
    }

    public void setWidth(int value) {
        JS.set(peer, "width", value);
    }

    public boolean isWillValidate() {
        return JS.getBoolean(peer, "willValidate");
    }

    public ValidityState getValidity() {
        return ValidityState.wrap(JS.get(peer, "validity"));
    }

    public String getValidationMessage() {
        return JS.string(JS.get(peer, "validationMessage"));
    }

    public NodeList getLabels() {
        return NodeList.wrap(JS.get(peer, "labels"));
    }

    public int getSelectionStart() {
        return JS.getInt(peer, "selectionStart");
    }

    public void setSelectionStart(int value) {
        JS.set(peer, "selectionStart", value);
    }

    public int getSelectionEnd() {
        return JS.getInt(peer, "selectionEnd");
    }

    public void setSelectionEnd(int value) {
        JS.set(peer, "selectionEnd", value);
    }

    public String getSelectionDirection() {
        return JS.string(JS.get(peer, "selectionDirection"));
    }

    public void setSelectionDirection(String value) {
        JS.set(peer, "selectionDirection", JS.param(value));
    }

    public String getAlign() {
        return JS.string(JS.get(peer, "align"));
    }

    public void setAlign(String value) {
        JS.set(peer, "align", JS.param(value));
    }

    public String getUseMap() {
        return JS.string(JS.get(peer, "useMap"));
    }

    public void setUseMap(String value) {
        JS.set(peer, "useMap", JS.param(value));
    }

    public void stepUp(long n) {
        JS.invoke(peer, "stepUp", n);
    }

    public void stepUp() {
        JS.invoke(peer, "stepUp");
    }

    public void stepDown(long n) {
        JS.invoke(peer, "stepDown", n);
    }

    public void stepDown() {
        JS.invoke(peer, "stepDown");
    }

    public boolean checkValidity() {
        return JS.invoke(peer, "checkValidity");
    }

    public boolean reportValidity() {
        return JS.invoke(peer, "reportValidity");
    }

    public void setCustomValidity(String error) {
        JS.invoke(peer, "setCustomValidity", JS.param(error));
    }

    public void select() {
        JS.invoke(peer, "select");
    }

    public void setRangeText(String replacement) {
        JS.invoke(peer, "setRangeText", JS.param(replacement));
    }

    public void setRangeText(String replacement, int start, int end, WrappedObject selectionMode) {
        JS.invoke(peer, "setRangeText", JS.param(replacement), start, end, JS.param(selectionMode));
    }

    public void setRangeText(String replacement, int start, int end) {
        JS.invoke(peer, "setRangeText", JS.param(replacement), start, end);
    }

    public void setSelectionRange(int start, int end, String direction) {
        JS.invoke(peer, "setSelectionRange", start, end, JS.param(direction));
    }

    public void setSelectionRange(int start, int end) {
        JS.invoke(peer, "setSelectionRange", start, end);
    }

    public void showPicker() {
        JS.invoke(peer, "showPicker");
    }
}
