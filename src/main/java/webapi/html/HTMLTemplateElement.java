// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.dom.DocumentFragment;

@SuppressWarnings({"unused"})
public class HTMLTemplateElement extends HTMLElement {
    protected HTMLTemplateElement(Object peer) {
        super(peer);
    }

    public static HTMLTemplateElement wrap(Object peer) {
        return peer == null ? null : new HTMLTemplateElement(peer);
    }

    public DocumentFragment getContent() {
        return DocumentFragment.wrap(JS.get(peer, "content"));
    }
}
