// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class HTMLAllCollection extends WrappedObject {
    protected HTMLAllCollection(Object peer) {
        super(peer);
    }

    public static HTMLAllCollection wrap(Object peer) {
        return peer == null ? null : new HTMLAllCollection(peer);
    }

    public int getLength() {
        return JS.getInt(peer, "length");
    }

    public WrappedObject item(String nameOrIndex) {
        return WrappedObject.wrap(JS.invoke(peer, "item", JS.param(nameOrIndex)));
    }

    public WrappedObject item() {
        return WrappedObject.wrap(JS.invoke(peer, "item"));
    }
}
