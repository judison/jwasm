// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class DataTransferItemList extends WrappedObject {
    protected DataTransferItemList(Object peer) {
        super(peer);
    }

    public static DataTransferItemList wrap(Object peer) {
        return peer == null ? null : new DataTransferItemList(peer);
    }

    public int getLength() {
        return JS.getInt(peer, "length");
    }

    public DataTransferItem add(String data, String type) {
        return DataTransferItem.wrap(JS.invoke(peer, "add", JS.param(data), JS.param(type)));
    }

    public DataTransferItem add(WrappedObject data) {
        return DataTransferItem.wrap(JS.invoke(peer, "add", JS.param(data)));
    }

    public void remove(int index) {
        JS.invoke(peer, "remove", index);
    }

    public void clear() {
        JS.invoke(peer, "clear");
    }
}
