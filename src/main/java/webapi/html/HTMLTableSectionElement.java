// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.dom.HTMLCollection;

@SuppressWarnings({"unused"})
public class HTMLTableSectionElement extends HTMLElement {
    protected HTMLTableSectionElement(Object peer) {
        super(peer);
    }

    public static HTMLTableSectionElement wrap(Object peer) {
        return peer == null ? null : new HTMLTableSectionElement(peer);
    }

    public HTMLCollection getRows() {
        return HTMLCollection.wrap(JS.get(peer, "rows"));
    }

    public String getAlign() {
        return JS.string(JS.get(peer, "align"));
    }

    public void setAlign(String value) {
        JS.set(peer, "align", JS.param(value));
    }

    public String getCh() {
        return JS.string(JS.get(peer, "ch"));
    }

    public void setCh(String value) {
        JS.set(peer, "ch", JS.param(value));
    }

    public String getChOff() {
        return JS.string(JS.get(peer, "chOff"));
    }

    public void setChOff(String value) {
        JS.set(peer, "chOff", JS.param(value));
    }

    public String getVAlign() {
        return JS.string(JS.get(peer, "vAlign"));
    }

    public void setVAlign(String value) {
        JS.set(peer, "vAlign", JS.param(value));
    }

    public HTMLTableRowElement insertRow(long index) {
        return HTMLTableRowElement.wrap(JS.invoke(peer, "insertRow", index));
    }

    public HTMLTableRowElement insertRow() {
        return HTMLTableRowElement.wrap(JS.invoke(peer, "insertRow"));
    }

    public void deleteRow(long index) {
        JS.invoke(peer, "deleteRow", index);
    }
}
