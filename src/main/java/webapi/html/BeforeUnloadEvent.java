// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.dom.Event;

@SuppressWarnings({"unused"})
public class BeforeUnloadEvent extends Event {
    protected BeforeUnloadEvent(Object peer) {
        super(peer);
    }

    public static BeforeUnloadEvent wrap(Object peer) {
        return peer == null ? null : new BeforeUnloadEvent(peer);
    }

    public String getReturnValue() {
        return JS.string(JS.get(peer, "returnValue"));
    }

    public void setReturnValue(String value) {
        JS.set(peer, "returnValue", JS.param(value));
    }
}
