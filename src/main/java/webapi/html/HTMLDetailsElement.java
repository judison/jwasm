// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;

@SuppressWarnings({"unused"})
public class HTMLDetailsElement extends HTMLElement {
    protected HTMLDetailsElement(Object peer) {
        super(peer);
    }

    public static HTMLDetailsElement wrap(Object peer) {
        return peer == null ? null : new HTMLDetailsElement(peer);
    }

    public boolean isOpen() {
        return JS.getBoolean(peer, "open");
    }

    public void setOpen(boolean value) {
        JS.set(peer, "open", value);
    }
}
