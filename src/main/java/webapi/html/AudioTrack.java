// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class AudioTrack extends WrappedObject {
    protected AudioTrack(Object peer) {
        super(peer);
    }

    public static AudioTrack wrap(Object peer) {
        return peer == null ? null : new AudioTrack(peer);
    }

    public String getId() {
        return JS.string(JS.get(peer, "id"));
    }

    public String getKind() {
        return JS.string(JS.get(peer, "kind"));
    }

    public String getLabel() {
        return JS.string(JS.get(peer, "label"));
    }

    public String getLanguage() {
        return JS.string(JS.get(peer, "language"));
    }

    public boolean isEnabled() {
        return JS.getBoolean(peer, "enabled");
    }

    public void setEnabled(boolean value) {
        JS.set(peer, "enabled", value);
    }
}
