// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.JSSequence;
import webapi.WrappedObject;
import webapi.dom.Event;

@SuppressWarnings({"unused"})
public class MessageEvent extends Event {
    protected MessageEvent(Object peer) {
        super(peer);
    }

    public static MessageEvent wrap(Object peer) {
        return peer == null ? null : new MessageEvent(peer);
    }

    public WrappedObject getData() {
        return WrappedObject.wrap(JS.get(peer, "data"));
    }

    public String getOrigin() {
        return JS.string(JS.get(peer, "origin"));
    }

    public String getLastEventId() {
        return JS.string(JS.get(peer, "lastEventId"));
    }

    public WrappedObject getSource() {
        return WrappedObject.wrap(JS.get(peer, "source"));
    }

    public WrappedObject getPorts() {
        return WrappedObject.wrap(JS.get(peer, "ports"));
    }

    public void initMessageEvent(String type, boolean bubbles, boolean cancelable,
            WrappedObject data, String origin, String lastEventId, WrappedObject source,
            JSSequence<MessagePort> ports) {
        JS.invoke(peer, "initMessageEvent", JS.param(type), bubbles, cancelable, JS.param(data), JS.param(origin), JS.param(lastEventId), JS.param(source), JS.param(ports));
    }

    public void initMessageEvent(String type, boolean bubbles, boolean cancelable,
            WrappedObject data, String origin, String lastEventId, WrappedObject source) {
        JS.invoke(peer, "initMessageEvent", JS.param(type), bubbles, cancelable, JS.param(data), JS.param(origin), JS.param(lastEventId), JS.param(source));
    }

    public void initMessageEvent(String type, boolean bubbles, boolean cancelable,
            WrappedObject data, String origin, String lastEventId) {
        JS.invoke(peer, "initMessageEvent", JS.param(type), bubbles, cancelable, JS.param(data), JS.param(origin), JS.param(lastEventId));
    }

    public void initMessageEvent(String type, boolean bubbles, boolean cancelable,
            WrappedObject data, String origin) {
        JS.invoke(peer, "initMessageEvent", JS.param(type), bubbles, cancelable, JS.param(data), JS.param(origin));
    }

    public void initMessageEvent(String type, boolean bubbles, boolean cancelable,
            WrappedObject data) {
        JS.invoke(peer, "initMessageEvent", JS.param(type), bubbles, cancelable, JS.param(data));
    }

    public void initMessageEvent(String type, boolean bubbles, boolean cancelable) {
        JS.invoke(peer, "initMessageEvent", JS.param(type), bubbles, cancelable);
    }

    public void initMessageEvent(String type, boolean bubbles) {
        JS.invoke(peer, "initMessageEvent", JS.param(type), bubbles);
    }

    public void initMessageEvent(String type) {
        JS.invoke(peer, "initMessageEvent", JS.param(type));
    }
}
