// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.dom.Event;

@SuppressWarnings({"unused"})
public class HashChangeEvent extends Event {
    protected HashChangeEvent(Object peer) {
        super(peer);
    }

    public static HashChangeEvent wrap(Object peer) {
        return peer == null ? null : new HashChangeEvent(peer);
    }

    public String getOldURL() {
        return JS.string(JS.get(peer, "oldURL"));
    }

    public String getNewURL() {
        return JS.string(JS.get(peer, "newURL"));
    }
}
