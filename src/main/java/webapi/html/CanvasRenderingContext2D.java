// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.JSObject;
import webapi.JSSequence;
import webapi.WrappedObject;
import webapi.dom.DOMMatrix;
import webapi.dom.Element;

@SuppressWarnings({"unused"})
public class CanvasRenderingContext2D extends WrappedObject {
    protected CanvasRenderingContext2D(Object peer) {
        super(peer);
    }

    public static CanvasRenderingContext2D wrap(Object peer) {
        return peer == null ? null : new CanvasRenderingContext2D(peer);
    }

    public HTMLCanvasElement getCanvas() {
        return HTMLCanvasElement.wrap(JS.get(peer, "canvas"));
    }

    public double getGlobalAlpha() {
        return JS.getDouble(peer, "globalAlpha");
    }

    public void setGlobalAlpha(double value) {
        JS.set(peer, "globalAlpha", value);
    }

    public String getGlobalCompositeOperation() {
        return JS.string(JS.get(peer, "globalCompositeOperation"));
    }

    public void setGlobalCompositeOperation(String value) {
        JS.set(peer, "globalCompositeOperation", JS.param(value));
    }

    public boolean isImageSmoothingEnabled() {
        return JS.getBoolean(peer, "imageSmoothingEnabled");
    }

    public void setImageSmoothingEnabled(boolean value) {
        JS.set(peer, "imageSmoothingEnabled", value);
    }

    public WrappedObject getImageSmoothingQuality() {
        return WrappedObject.wrap(JS.get(peer, "imageSmoothingQuality"));
    }

    public void setImageSmoothingQuality(WrappedObject value) {
        JS.set(peer, "imageSmoothingQuality", value.peer);
    }

    public WrappedObject getStrokeStyle() {
        return WrappedObject.wrap(JS.get(peer, "strokeStyle"));
    }

    public void setStrokeStyle(WrappedObject value) {
        JS.set(peer, "strokeStyle", value.peer);
    }

    public WrappedObject getFillStyle() {
        return WrappedObject.wrap(JS.get(peer, "fillStyle"));
    }

    public void setFillStyle(WrappedObject value) {
        JS.set(peer, "fillStyle", value.peer);
    }

    public double getShadowOffsetX() {
        return JS.getDouble(peer, "shadowOffsetX");
    }

    public void setShadowOffsetX(double value) {
        JS.set(peer, "shadowOffsetX", value);
    }

    public double getShadowOffsetY() {
        return JS.getDouble(peer, "shadowOffsetY");
    }

    public void setShadowOffsetY(double value) {
        JS.set(peer, "shadowOffsetY", value);
    }

    public double getShadowBlur() {
        return JS.getDouble(peer, "shadowBlur");
    }

    public void setShadowBlur(double value) {
        JS.set(peer, "shadowBlur", value);
    }

    public String getShadowColor() {
        return JS.string(JS.get(peer, "shadowColor"));
    }

    public void setShadowColor(String value) {
        JS.set(peer, "shadowColor", JS.param(value));
    }

    public String getFilter() {
        return JS.string(JS.get(peer, "filter"));
    }

    public void setFilter(String value) {
        JS.set(peer, "filter", JS.param(value));
    }

    public double getLineWidth() {
        return JS.getDouble(peer, "lineWidth");
    }

    public void setLineWidth(double value) {
        JS.set(peer, "lineWidth", value);
    }

    public WrappedObject getLineCap() {
        return WrappedObject.wrap(JS.get(peer, "lineCap"));
    }

    public void setLineCap(WrappedObject value) {
        JS.set(peer, "lineCap", value.peer);
    }

    public WrappedObject getLineJoin() {
        return WrappedObject.wrap(JS.get(peer, "lineJoin"));
    }

    public void setLineJoin(WrappedObject value) {
        JS.set(peer, "lineJoin", value.peer);
    }

    public double getMiterLimit() {
        return JS.getDouble(peer, "miterLimit");
    }

    public void setMiterLimit(double value) {
        JS.set(peer, "miterLimit", value);
    }

    public double getLineDashOffset() {
        return JS.getDouble(peer, "lineDashOffset");
    }

    public void setLineDashOffset(double value) {
        JS.set(peer, "lineDashOffset", value);
    }

    public String getFont() {
        return JS.string(JS.get(peer, "font"));
    }

    public void setFont(String value) {
        JS.set(peer, "font", JS.param(value));
    }

    public WrappedObject getTextAlign() {
        return WrappedObject.wrap(JS.get(peer, "textAlign"));
    }

    public void setTextAlign(WrappedObject value) {
        JS.set(peer, "textAlign", value.peer);
    }

    public WrappedObject getTextBaseline() {
        return WrappedObject.wrap(JS.get(peer, "textBaseline"));
    }

    public void setTextBaseline(WrappedObject value) {
        JS.set(peer, "textBaseline", value.peer);
    }

    public WrappedObject getDirection() {
        return WrappedObject.wrap(JS.get(peer, "direction"));
    }

    public void setDirection(WrappedObject value) {
        JS.set(peer, "direction", value.peer);
    }

    public String getLetterSpacing() {
        return JS.string(JS.get(peer, "letterSpacing"));
    }

    public void setLetterSpacing(String value) {
        JS.set(peer, "letterSpacing", JS.param(value));
    }

    public WrappedObject getFontKerning() {
        return WrappedObject.wrap(JS.get(peer, "fontKerning"));
    }

    public void setFontKerning(WrappedObject value) {
        JS.set(peer, "fontKerning", value.peer);
    }

    public WrappedObject getFontStretch() {
        return WrappedObject.wrap(JS.get(peer, "fontStretch"));
    }

    public void setFontStretch(WrappedObject value) {
        JS.set(peer, "fontStretch", value.peer);
    }

    public WrappedObject getFontVariantCaps() {
        return WrappedObject.wrap(JS.get(peer, "fontVariantCaps"));
    }

    public void setFontVariantCaps(WrappedObject value) {
        JS.set(peer, "fontVariantCaps", value.peer);
    }

    public WrappedObject getTextRendering() {
        return WrappedObject.wrap(JS.get(peer, "textRendering"));
    }

    public void setTextRendering(WrappedObject value) {
        JS.set(peer, "textRendering", value.peer);
    }

    public String getWordSpacing() {
        return JS.string(JS.get(peer, "wordSpacing"));
    }

    public void setWordSpacing(String value) {
        JS.set(peer, "wordSpacing", JS.param(value));
    }

    public JSObject getContextAttributes() {
        return JSObject.wrap(JS.invoke(peer, "getContextAttributes"));
    }

    public void save() {
        JS.invoke(peer, "save");
    }

    public void restore() {
        JS.invoke(peer, "restore");
    }

    public void reset() {
        JS.invoke(peer, "reset");
    }

    public boolean isContextLost() {
        return JS.invoke(peer, "isContextLost");
    }

    public void scale(double x, double y) {
        JS.invoke(peer, "scale", x, y);
    }

    public void rotate(double angle) {
        JS.invoke(peer, "rotate", angle);
    }

    public void translate(double x, double y) {
        JS.invoke(peer, "translate", x, y);
    }

    public void transform(double a, double b, double c, double d, double e, double f) {
        JS.invoke(peer, "transform", a, b, c, d, e, f);
    }

    public DOMMatrix getTransform() {
        return DOMMatrix.wrap(JS.invoke(peer, "getTransform"));
    }

    public void setTransform(double a, double b, double c, double d, double e, double f) {
        JS.invoke(peer, "setTransform", a, b, c, d, e, f);
    }

    public void setTransform(JSObject transform) {
        JS.invoke(peer, "setTransform", JS.param(transform));
    }

    public void setTransform() {
        JS.invoke(peer, "setTransform");
    }

    public void resetTransform() {
        JS.invoke(peer, "resetTransform");
    }

    public CanvasGradient createLinearGradient(double x0, double y0, double x1, double y1) {
        return CanvasGradient.wrap(JS.invoke(peer, "createLinearGradient", x0, y0, x1, y1));
    }

    public CanvasGradient createRadialGradient(double x0, double y0, double r0, double x1,
            double y1, double r1) {
        return CanvasGradient.wrap(JS.invoke(peer, "createRadialGradient", x0, y0, r0, x1, y1, r1));
    }

    public CanvasGradient createConicGradient(double startAngle, double x, double y) {
        return CanvasGradient.wrap(JS.invoke(peer, "createConicGradient", startAngle, x, y));
    }

    public CanvasPattern createPattern(WrappedObject image, String repetition) {
        return CanvasPattern.wrap(JS.invoke(peer, "createPattern", JS.param(image), JS.param(repetition)));
    }

    public void clearRect(double x, double y, double w, double h) {
        JS.invoke(peer, "clearRect", x, y, w, h);
    }

    public void fillRect(double x, double y, double w, double h) {
        JS.invoke(peer, "fillRect", x, y, w, h);
    }

    public void strokeRect(double x, double y, double w, double h) {
        JS.invoke(peer, "strokeRect", x, y, w, h);
    }

    public void beginPath() {
        JS.invoke(peer, "beginPath");
    }

    public void fill(WrappedObject fillRule) {
        JS.invoke(peer, "fill", JS.param(fillRule));
    }

    public void fill() {
        JS.invoke(peer, "fill");
    }

    public void fill(Path2D path, WrappedObject fillRule) {
        JS.invoke(peer, "fill", JS.param(path), JS.param(fillRule));
    }

    public void fill(Path2D path) {
        JS.invoke(peer, "fill", JS.param(path));
    }

    public void stroke() {
        JS.invoke(peer, "stroke");
    }

    public void stroke(Path2D path) {
        JS.invoke(peer, "stroke", JS.param(path));
    }

    public void clip(WrappedObject fillRule) {
        JS.invoke(peer, "clip", JS.param(fillRule));
    }

    public void clip() {
        JS.invoke(peer, "clip");
    }

    public void clip(Path2D path, WrappedObject fillRule) {
        JS.invoke(peer, "clip", JS.param(path), JS.param(fillRule));
    }

    public void clip(Path2D path) {
        JS.invoke(peer, "clip", JS.param(path));
    }

    public boolean isPointInPath(double x, double y, WrappedObject fillRule) {
        return JS.invoke(peer, "isPointInPath", x, y, JS.param(fillRule));
    }

    public boolean isPointInPath(double x, double y) {
        return JS.invoke(peer, "isPointInPath", x, y);
    }

    public boolean isPointInPath(Path2D path, double x, double y, WrappedObject fillRule) {
        return JS.invoke(peer, "isPointInPath", JS.param(path), x, y, JS.param(fillRule));
    }

    public boolean isPointInPath(Path2D path, double x, double y) {
        return JS.invoke(peer, "isPointInPath", JS.param(path), x, y);
    }

    public boolean isPointInStroke(double x, double y) {
        return JS.invoke(peer, "isPointInStroke", x, y);
    }

    public boolean isPointInStroke(Path2D path, double x, double y) {
        return JS.invoke(peer, "isPointInStroke", JS.param(path), x, y);
    }

    public void drawFocusIfNeeded(Element element) {
        JS.invoke(peer, "drawFocusIfNeeded", JS.param(element));
    }

    public void drawFocusIfNeeded(Path2D path, Element element) {
        JS.invoke(peer, "drawFocusIfNeeded", JS.param(path), JS.param(element));
    }

    public void scrollPathIntoView() {
        JS.invoke(peer, "scrollPathIntoView");
    }

    public void scrollPathIntoView(Path2D path) {
        JS.invoke(peer, "scrollPathIntoView", JS.param(path));
    }

    public void fillText(String text, double x, double y, double maxWidth) {
        JS.invoke(peer, "fillText", JS.param(text), x, y, maxWidth);
    }

    public void fillText(String text, double x, double y) {
        JS.invoke(peer, "fillText", JS.param(text), x, y);
    }

    public void strokeText(String text, double x, double y, double maxWidth) {
        JS.invoke(peer, "strokeText", JS.param(text), x, y, maxWidth);
    }

    public void strokeText(String text, double x, double y) {
        JS.invoke(peer, "strokeText", JS.param(text), x, y);
    }

    public TextMetrics measureText(String text) {
        return TextMetrics.wrap(JS.invoke(peer, "measureText", JS.param(text)));
    }

    public void drawImage(WrappedObject image, double dx, double dy) {
        JS.invoke(peer, "drawImage", JS.param(image), dx, dy);
    }

    public void drawImage(WrappedObject image, double dx, double dy, double dw, double dh) {
        JS.invoke(peer, "drawImage", JS.param(image), dx, dy, dw, dh);
    }

    public void drawImage(WrappedObject image, double sx, double sy, double sw, double sh,
            double dx, double dy, double dw, double dh) {
        JS.invoke(peer, "drawImage", JS.param(image), sx, sy, sw, sh, dx, dy, dw, dh);
    }

    public ImageData createImageData(long sw, long sh, JSObject settings) {
        return ImageData.wrap(JS.invoke(peer, "createImageData", sw, sh, JS.param(settings)));
    }

    public ImageData createImageData(long sw, long sh) {
        return ImageData.wrap(JS.invoke(peer, "createImageData", sw, sh));
    }

    public ImageData createImageData(ImageData imagedata) {
        return ImageData.wrap(JS.invoke(peer, "createImageData", JS.param(imagedata)));
    }

    public ImageData getImageData(long sx, long sy, long sw, long sh, JSObject settings) {
        return ImageData.wrap(JS.invoke(peer, "getImageData", sx, sy, sw, sh, JS.param(settings)));
    }

    public ImageData getImageData(long sx, long sy, long sw, long sh) {
        return ImageData.wrap(JS.invoke(peer, "getImageData", sx, sy, sw, sh));
    }

    public void putImageData(ImageData imagedata, long dx, long dy) {
        JS.invoke(peer, "putImageData", JS.param(imagedata), dx, dy);
    }

    public void putImageData(ImageData imagedata, long dx, long dy, long dirtyX, long dirtyY,
            long dirtyWidth, long dirtyHeight) {
        JS.invoke(peer, "putImageData", JS.param(imagedata), dx, dy, dirtyX, dirtyY, dirtyWidth, dirtyHeight);
    }

    public void setLineDash(JSSequence<Double> segments) {
        JS.invoke(peer, "setLineDash", JS.param(segments));
    }

    public JSSequence<Double> getLineDash() {
        return JSSequence.wrap(Double.class, JS.invoke(peer, "getLineDash"));
    }

    public void closePath() {
        JS.invoke(peer, "closePath");
    }

    public void moveTo(double x, double y) {
        JS.invoke(peer, "moveTo", x, y);
    }

    public void lineTo(double x, double y) {
        JS.invoke(peer, "lineTo", x, y);
    }

    public void quadraticCurveTo(double cpx, double cpy, double x, double y) {
        JS.invoke(peer, "quadraticCurveTo", cpx, cpy, x, y);
    }

    public void bezierCurveTo(double cp1x, double cp1y, double cp2x, double cp2y, double x,
            double y) {
        JS.invoke(peer, "bezierCurveTo", cp1x, cp1y, cp2x, cp2y, x, y);
    }

    public void arcTo(double x1, double y1, double x2, double y2, double radius) {
        JS.invoke(peer, "arcTo", x1, y1, x2, y2, radius);
    }

    public void rect(double x, double y, double w, double h) {
        JS.invoke(peer, "rect", x, y, w, h);
    }

    public void roundRect(double x, double y, double w, double h, double radii) {
        JS.invoke(peer, "roundRect", x, y, w, h, radii);
    }

    public void roundRect(double x, double y, double w, double h, JSObject radii) {
        JS.invoke(peer, "roundRect", x, y, w, h, JS.param(radii));
    }

    public void roundRect(double x, double y, double w, double h, JSSequence<WrappedObject> radii) {
        JS.invoke(peer, "roundRect", x, y, w, h, JS.param(radii));
    }

    public void roundRect(double x, double y, double w, double h) {
        JS.invoke(peer, "roundRect", x, y, w, h);
    }

    public void arc(double x, double y, double radius, double startAngle, double endAngle,
            boolean counterclockwise) {
        JS.invoke(peer, "arc", x, y, radius, startAngle, endAngle, counterclockwise);
    }

    public void arc(double x, double y, double radius, double startAngle, double endAngle) {
        JS.invoke(peer, "arc", x, y, radius, startAngle, endAngle);
    }

    public void ellipse(double x, double y, double radiusX, double radiusY, double rotation,
            double startAngle, double endAngle, boolean counterclockwise) {
        JS.invoke(peer, "ellipse", x, y, radiusX, radiusY, rotation, startAngle, endAngle, counterclockwise);
    }

    public void ellipse(double x, double y, double radiusX, double radiusY, double rotation,
            double startAngle, double endAngle) {
        JS.invoke(peer, "ellipse", x, y, radiusX, radiusY, rotation, startAngle, endAngle);
    }
}
