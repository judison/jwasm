// Generated file; DO NOT edit
package webapi.html;

import webapi.JSCallback;
import webapi.WrappedObject;
import webapi.dom.Event;

@SuppressWarnings({"unused"})
public class EventHandlerNonNull extends JSCallback {
    public EventHandlerNonNull(Object peer) {
        super(peer);
    }

    public EventHandlerNonNull(Callback javaCallback) {
        super(javaCallback);
    }

    public static EventHandlerNonNull wrap(Object peer) {
        return peer == null ? null : new EventHandlerNonNull(peer);
    }

    public interface Callback extends JSCallback.BaseCallback {
        WrappedObject handle(Event event);
    }
}
