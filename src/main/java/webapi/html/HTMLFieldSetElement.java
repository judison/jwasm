// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.dom.HTMLCollection;

@SuppressWarnings({"unused"})
public class HTMLFieldSetElement extends HTMLElement {
    protected HTMLFieldSetElement(Object peer) {
        super(peer);
    }

    public static HTMLFieldSetElement wrap(Object peer) {
        return peer == null ? null : new HTMLFieldSetElement(peer);
    }

    public boolean isDisabled() {
        return JS.getBoolean(peer, "disabled");
    }

    public void setDisabled(boolean value) {
        JS.set(peer, "disabled", value);
    }

    public HTMLFormElement getForm() {
        return HTMLFormElement.wrap(JS.get(peer, "form"));
    }

    public String getName() {
        return JS.string(JS.get(peer, "name"));
    }

    public void setName(String value) {
        JS.set(peer, "name", JS.param(value));
    }

    public String getType() {
        return JS.string(JS.get(peer, "type"));
    }

    public HTMLCollection getElements() {
        return HTMLCollection.wrap(JS.get(peer, "elements"));
    }

    public boolean isWillValidate() {
        return JS.getBoolean(peer, "willValidate");
    }

    public ValidityState getValidity() {
        return ValidityState.wrap(JS.get(peer, "validity"));
    }

    public String getValidationMessage() {
        return JS.string(JS.get(peer, "validationMessage"));
    }

    public boolean checkValidity() {
        return JS.invoke(peer, "checkValidity");
    }

    public boolean reportValidity() {
        return JS.invoke(peer, "reportValidity");
    }

    public void setCustomValidity(String error) {
        JS.invoke(peer, "setCustomValidity", JS.param(error));
    }
}
