// Generated file; DO NOT edit
package webapi.html;

@SuppressWarnings({"unused"})
public class HTMLUnknownElement extends HTMLElement {
    protected HTMLUnknownElement(Object peer) {
        super(peer);
    }

    public static HTMLUnknownElement wrap(Object peer) {
        return peer == null ? null : new HTMLUnknownElement(peer);
    }
}
