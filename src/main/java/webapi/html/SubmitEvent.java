// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.dom.Event;

@SuppressWarnings({"unused"})
public class SubmitEvent extends Event {
    protected SubmitEvent(Object peer) {
        super(peer);
    }

    public static SubmitEvent wrap(Object peer) {
        return peer == null ? null : new SubmitEvent(peer);
    }

    public HTMLElement getSubmitter() {
        return HTMLElement.wrap(JS.get(peer, "submitter"));
    }
}
