// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;

@SuppressWarnings({"unused"})
public class HTMLMetaElement extends HTMLElement {
    protected HTMLMetaElement(Object peer) {
        super(peer);
    }

    public static HTMLMetaElement wrap(Object peer) {
        return peer == null ? null : new HTMLMetaElement(peer);
    }

    public String getName() {
        return JS.string(JS.get(peer, "name"));
    }

    public void setName(String value) {
        JS.set(peer, "name", JS.param(value));
    }

    public String getHttpEquiv() {
        return JS.string(JS.get(peer, "httpEquiv"));
    }

    public void setHttpEquiv(String value) {
        JS.set(peer, "httpEquiv", JS.param(value));
    }

    public String getContent() {
        return JS.string(JS.get(peer, "content"));
    }

    public void setContent(String value) {
        JS.set(peer, "content", JS.param(value));
    }

    public String getMedia() {
        return JS.string(JS.get(peer, "media"));
    }

    public void setMedia(String value) {
        JS.set(peer, "media", JS.param(value));
    }

    public String getScheme() {
        return JS.string(JS.get(peer, "scheme"));
    }

    public void setScheme(String value) {
        JS.set(peer, "scheme", JS.param(value));
    }
}
