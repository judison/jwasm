// Generated file; DO NOT edit
package webapi.html;

import webapi.JSCallback;

@SuppressWarnings({"unused"})
public class CustomElementConstructor extends JSCallback {
    public CustomElementConstructor(Object peer) {
        super(peer);
    }

    public CustomElementConstructor(Callback javaCallback) {
        super(javaCallback);
    }

    public static CustomElementConstructor wrap(Object peer) {
        return peer == null ? null : new CustomElementConstructor(peer);
    }

    public interface Callback extends JSCallback.BaseCallback {
        HTMLElement handle();
    }
}
