// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class MimeTypeArray extends WrappedObject {
    protected MimeTypeArray(Object peer) {
        super(peer);
    }

    public static MimeTypeArray wrap(Object peer) {
        return peer == null ? null : new MimeTypeArray(peer);
    }

    public int getLength() {
        return JS.getInt(peer, "length");
    }
}
