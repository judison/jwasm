// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;

@SuppressWarnings({"unused"})
public class HTMLVideoElement extends HTMLMediaElement {
    protected HTMLVideoElement(Object peer) {
        super(peer);
    }

    public static HTMLVideoElement wrap(Object peer) {
        return peer == null ? null : new HTMLVideoElement(peer);
    }

    public int getWidth() {
        return JS.getInt(peer, "width");
    }

    public void setWidth(int value) {
        JS.set(peer, "width", value);
    }

    public int getHeight() {
        return JS.getInt(peer, "height");
    }

    public void setHeight(int value) {
        JS.set(peer, "height", value);
    }

    public int getVideoWidth() {
        return JS.getInt(peer, "videoWidth");
    }

    public int getVideoHeight() {
        return JS.getInt(peer, "videoHeight");
    }

    public String getPoster() {
        return JS.string(JS.get(peer, "poster"));
    }

    public void setPoster(String value) {
        JS.set(peer, "poster", JS.param(value));
    }

    public boolean isPlaysInline() {
        return JS.getBoolean(peer, "playsInline");
    }

    public void setPlaysInline(boolean value) {
        JS.set(peer, "playsInline", value);
    }
}
