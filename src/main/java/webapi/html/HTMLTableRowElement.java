// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.dom.HTMLCollection;

@SuppressWarnings({"unused"})
public class HTMLTableRowElement extends HTMLElement {
    protected HTMLTableRowElement(Object peer) {
        super(peer);
    }

    public static HTMLTableRowElement wrap(Object peer) {
        return peer == null ? null : new HTMLTableRowElement(peer);
    }

    public long getRowIndex() {
        return (long)JS.get(peer, "rowIndex");
    }

    public long getSectionRowIndex() {
        return (long)JS.get(peer, "sectionRowIndex");
    }

    public HTMLCollection getCells() {
        return HTMLCollection.wrap(JS.get(peer, "cells"));
    }

    public String getAlign() {
        return JS.string(JS.get(peer, "align"));
    }

    public void setAlign(String value) {
        JS.set(peer, "align", JS.param(value));
    }

    public String getCh() {
        return JS.string(JS.get(peer, "ch"));
    }

    public void setCh(String value) {
        JS.set(peer, "ch", JS.param(value));
    }

    public String getChOff() {
        return JS.string(JS.get(peer, "chOff"));
    }

    public void setChOff(String value) {
        JS.set(peer, "chOff", JS.param(value));
    }

    public String getVAlign() {
        return JS.string(JS.get(peer, "vAlign"));
    }

    public void setVAlign(String value) {
        JS.set(peer, "vAlign", JS.param(value));
    }

    public String getBgColor() {
        return JS.string(JS.get(peer, "bgColor"));
    }

    public void setBgColor(String value) {
        JS.set(peer, "bgColor", JS.param(value));
    }

    public HTMLTableCellElement insertCell(long index) {
        return HTMLTableCellElement.wrap(JS.invoke(peer, "insertCell", index));
    }

    public HTMLTableCellElement insertCell() {
        return HTMLTableCellElement.wrap(JS.invoke(peer, "insertCell"));
    }

    public void deleteCell(long index) {
        JS.invoke(peer, "deleteCell", index);
    }
}
