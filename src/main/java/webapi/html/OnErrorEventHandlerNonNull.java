// Generated file; DO NOT edit
package webapi.html;

import webapi.JSCallback;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class OnErrorEventHandlerNonNull extends JSCallback {
    public OnErrorEventHandlerNonNull(Object peer) {
        super(peer);
    }

    public OnErrorEventHandlerNonNull(Callback javaCallback) {
        super(javaCallback);
    }

    public static OnErrorEventHandlerNonNull wrap(Object peer) {
        return peer == null ? null : new OnErrorEventHandlerNonNull(peer);
    }

    public interface Callback extends JSCallback.BaseCallback {
        WrappedObject handle(WrappedObject event, String source, int lineno, int colno,
                WrappedObject error);
    }
}
