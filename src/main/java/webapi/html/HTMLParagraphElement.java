// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;

@SuppressWarnings({"unused"})
public class HTMLParagraphElement extends HTMLElement {
    protected HTMLParagraphElement(Object peer) {
        super(peer);
    }

    public static HTMLParagraphElement wrap(Object peer) {
        return peer == null ? null : new HTMLParagraphElement(peer);
    }

    public String getAlign() {
        return JS.string(JS.get(peer, "align"));
    }

    public void setAlign(String value) {
        JS.set(peer, "align", JS.param(value));
    }
}
