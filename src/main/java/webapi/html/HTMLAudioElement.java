// Generated file; DO NOT edit
package webapi.html;

@SuppressWarnings({"unused"})
public class HTMLAudioElement extends HTMLMediaElement {
    protected HTMLAudioElement(Object peer) {
        super(peer);
    }

    public static HTMLAudioElement wrap(Object peer) {
        return peer == null ? null : new HTMLAudioElement(peer);
    }
}
