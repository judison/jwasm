// Generated file; DO NOT edit
package webapi.html;

@SuppressWarnings({"unused"})
public class HTMLSpanElement extends HTMLElement {
    protected HTMLSpanElement(Object peer) {
        super(peer);
    }

    public static HTMLSpanElement wrap(Object peer) {
        return peer == null ? null : new HTMLSpanElement(peer);
    }
}
