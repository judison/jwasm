// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class DragEvent extends WrappedObject {
    protected DragEvent(Object peer) {
        super(peer);
    }

    public static DragEvent wrap(Object peer) {
        return peer == null ? null : new DragEvent(peer);
    }

    public DataTransfer getDataTransfer() {
        return DataTransfer.wrap(JS.get(peer, "dataTransfer"));
    }
}
