// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.dom.HTMLCollection;

@SuppressWarnings({"unused"})
public class HTMLOptionsCollection extends HTMLCollection {
    protected HTMLOptionsCollection(Object peer) {
        super(peer);
    }

    public static HTMLOptionsCollection wrap(Object peer) {
        return peer == null ? null : new HTMLOptionsCollection(peer);
    }

    public int getLength() {
        return JS.getInt(peer, "length");
    }

    public void setLength(int value) {
        JS.set(peer, "length", value);
    }

    public long getSelectedIndex() {
        return (long)JS.get(peer, "selectedIndex");
    }

    public void setSelectedIndex(long value) {
        JS.set(peer, "selectedIndex", value);
    }

    public void add(HTMLOptionElement element, HTMLElement before) {
        JS.invoke(peer, "add", JS.param(element), JS.param(before));
    }

    public void add(HTMLOptGroupElement element, HTMLElement before) {
        JS.invoke(peer, "add", JS.param(element), JS.param(before));
    }

    public void add(HTMLOptionElement element, long before) {
        JS.invoke(peer, "add", JS.param(element), before);
    }

    public void add(HTMLOptGroupElement element, long before) {
        JS.invoke(peer, "add", JS.param(element), before);
    }

    public void add(HTMLOptionElement element) {
        JS.invoke(peer, "add", JS.param(element));
    }

    public void add(HTMLOptGroupElement element) {
        JS.invoke(peer, "add", JS.param(element));
    }

    public void remove(long index) {
        JS.invoke(peer, "remove", index);
    }
}
