// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.JSObject;
import webapi.WrappedObject;
import webapi.dom.Element;
import webapi.dom.NodeList;
import webapi.dom.ShadowRoot;
import webapi.xhr.FormData;

@SuppressWarnings({"unused"})
public class ElementInternals extends WrappedObject {
    protected ElementInternals(Object peer) {
        super(peer);
    }

    public static ElementInternals wrap(Object peer) {
        return peer == null ? null : new ElementInternals(peer);
    }

    public ShadowRoot getShadowRoot() {
        return ShadowRoot.wrap(JS.get(peer, "shadowRoot"));
    }

    public HTMLFormElement getForm() {
        return HTMLFormElement.wrap(JS.get(peer, "form"));
    }

    public boolean isWillValidate() {
        return JS.getBoolean(peer, "willValidate");
    }

    public ValidityState getValidity() {
        return ValidityState.wrap(JS.get(peer, "validity"));
    }

    public String getValidationMessage() {
        return JS.string(JS.get(peer, "validationMessage"));
    }

    public NodeList getLabels() {
        return NodeList.wrap(JS.get(peer, "labels"));
    }

    public String getRole() {
        return JS.string(JS.get(peer, "role"));
    }

    public void setRole(String value) {
        JS.set(peer, "role", JS.param(value));
    }

    public Element getAriaActiveDescendantElement() {
        return Element.wrap(JS.get(peer, "ariaActiveDescendantElement"));
    }

    public void setAriaActiveDescendantElement(Element value) {
        JS.set(peer, "ariaActiveDescendantElement", value.peer);
    }

    public String getAriaAtomic() {
        return JS.string(JS.get(peer, "ariaAtomic"));
    }

    public void setAriaAtomic(String value) {
        JS.set(peer, "ariaAtomic", JS.param(value));
    }

    public String getAriaAutoComplete() {
        return JS.string(JS.get(peer, "ariaAutoComplete"));
    }

    public void setAriaAutoComplete(String value) {
        JS.set(peer, "ariaAutoComplete", JS.param(value));
    }

    public String getAriaBusy() {
        return JS.string(JS.get(peer, "ariaBusy"));
    }

    public void setAriaBusy(String value) {
        JS.set(peer, "ariaBusy", JS.param(value));
    }

    public String getAriaChecked() {
        return JS.string(JS.get(peer, "ariaChecked"));
    }

    public void setAriaChecked(String value) {
        JS.set(peer, "ariaChecked", JS.param(value));
    }

    public String getAriaColCount() {
        return JS.string(JS.get(peer, "ariaColCount"));
    }

    public void setAriaColCount(String value) {
        JS.set(peer, "ariaColCount", JS.param(value));
    }

    public String getAriaColIndex() {
        return JS.string(JS.get(peer, "ariaColIndex"));
    }

    public void setAriaColIndex(String value) {
        JS.set(peer, "ariaColIndex", JS.param(value));
    }

    public String getAriaColIndexText() {
        return JS.string(JS.get(peer, "ariaColIndexText"));
    }

    public void setAriaColIndexText(String value) {
        JS.set(peer, "ariaColIndexText", JS.param(value));
    }

    public String getAriaColSpan() {
        return JS.string(JS.get(peer, "ariaColSpan"));
    }

    public void setAriaColSpan(String value) {
        JS.set(peer, "ariaColSpan", JS.param(value));
    }

    public WrappedObject getAriaControlsElements() {
        return WrappedObject.wrap(JS.get(peer, "ariaControlsElements"));
    }

    public void setAriaControlsElements(WrappedObject value) {
        JS.set(peer, "ariaControlsElements", value.peer);
    }

    public String getAriaCurrent() {
        return JS.string(JS.get(peer, "ariaCurrent"));
    }

    public void setAriaCurrent(String value) {
        JS.set(peer, "ariaCurrent", JS.param(value));
    }

    public WrappedObject getAriaDescribedByElements() {
        return WrappedObject.wrap(JS.get(peer, "ariaDescribedByElements"));
    }

    public void setAriaDescribedByElements(WrappedObject value) {
        JS.set(peer, "ariaDescribedByElements", value.peer);
    }

    public String getAriaDescription() {
        return JS.string(JS.get(peer, "ariaDescription"));
    }

    public void setAriaDescription(String value) {
        JS.set(peer, "ariaDescription", JS.param(value));
    }

    public WrappedObject getAriaDetailsElements() {
        return WrappedObject.wrap(JS.get(peer, "ariaDetailsElements"));
    }

    public void setAriaDetailsElements(WrappedObject value) {
        JS.set(peer, "ariaDetailsElements", value.peer);
    }

    public String getAriaDisabled() {
        return JS.string(JS.get(peer, "ariaDisabled"));
    }

    public void setAriaDisabled(String value) {
        JS.set(peer, "ariaDisabled", JS.param(value));
    }

    public Element getAriaErrorMessageElement() {
        return Element.wrap(JS.get(peer, "ariaErrorMessageElement"));
    }

    public void setAriaErrorMessageElement(Element value) {
        JS.set(peer, "ariaErrorMessageElement", value.peer);
    }

    public String getAriaExpanded() {
        return JS.string(JS.get(peer, "ariaExpanded"));
    }

    public void setAriaExpanded(String value) {
        JS.set(peer, "ariaExpanded", JS.param(value));
    }

    public WrappedObject getAriaFlowToElements() {
        return WrappedObject.wrap(JS.get(peer, "ariaFlowToElements"));
    }

    public void setAriaFlowToElements(WrappedObject value) {
        JS.set(peer, "ariaFlowToElements", value.peer);
    }

    public String getAriaHasPopup() {
        return JS.string(JS.get(peer, "ariaHasPopup"));
    }

    public void setAriaHasPopup(String value) {
        JS.set(peer, "ariaHasPopup", JS.param(value));
    }

    public String getAriaHidden() {
        return JS.string(JS.get(peer, "ariaHidden"));
    }

    public void setAriaHidden(String value) {
        JS.set(peer, "ariaHidden", JS.param(value));
    }

    public String getAriaInvalid() {
        return JS.string(JS.get(peer, "ariaInvalid"));
    }

    public void setAriaInvalid(String value) {
        JS.set(peer, "ariaInvalid", JS.param(value));
    }

    public String getAriaKeyShortcuts() {
        return JS.string(JS.get(peer, "ariaKeyShortcuts"));
    }

    public void setAriaKeyShortcuts(String value) {
        JS.set(peer, "ariaKeyShortcuts", JS.param(value));
    }

    public String getAriaLabel() {
        return JS.string(JS.get(peer, "ariaLabel"));
    }

    public void setAriaLabel(String value) {
        JS.set(peer, "ariaLabel", JS.param(value));
    }

    public WrappedObject getAriaLabelledByElements() {
        return WrappedObject.wrap(JS.get(peer, "ariaLabelledByElements"));
    }

    public void setAriaLabelledByElements(WrappedObject value) {
        JS.set(peer, "ariaLabelledByElements", value.peer);
    }

    public String getAriaLevel() {
        return JS.string(JS.get(peer, "ariaLevel"));
    }

    public void setAriaLevel(String value) {
        JS.set(peer, "ariaLevel", JS.param(value));
    }

    public String getAriaLive() {
        return JS.string(JS.get(peer, "ariaLive"));
    }

    public void setAriaLive(String value) {
        JS.set(peer, "ariaLive", JS.param(value));
    }

    public String getAriaModal() {
        return JS.string(JS.get(peer, "ariaModal"));
    }

    public void setAriaModal(String value) {
        JS.set(peer, "ariaModal", JS.param(value));
    }

    public String getAriaMultiLine() {
        return JS.string(JS.get(peer, "ariaMultiLine"));
    }

    public void setAriaMultiLine(String value) {
        JS.set(peer, "ariaMultiLine", JS.param(value));
    }

    public String getAriaMultiSelectable() {
        return JS.string(JS.get(peer, "ariaMultiSelectable"));
    }

    public void setAriaMultiSelectable(String value) {
        JS.set(peer, "ariaMultiSelectable", JS.param(value));
    }

    public String getAriaOrientation() {
        return JS.string(JS.get(peer, "ariaOrientation"));
    }

    public void setAriaOrientation(String value) {
        JS.set(peer, "ariaOrientation", JS.param(value));
    }

    public WrappedObject getAriaOwnsElements() {
        return WrappedObject.wrap(JS.get(peer, "ariaOwnsElements"));
    }

    public void setAriaOwnsElements(WrappedObject value) {
        JS.set(peer, "ariaOwnsElements", value.peer);
    }

    public String getAriaPlaceholder() {
        return JS.string(JS.get(peer, "ariaPlaceholder"));
    }

    public void setAriaPlaceholder(String value) {
        JS.set(peer, "ariaPlaceholder", JS.param(value));
    }

    public String getAriaPosInSet() {
        return JS.string(JS.get(peer, "ariaPosInSet"));
    }

    public void setAriaPosInSet(String value) {
        JS.set(peer, "ariaPosInSet", JS.param(value));
    }

    public String getAriaPressed() {
        return JS.string(JS.get(peer, "ariaPressed"));
    }

    public void setAriaPressed(String value) {
        JS.set(peer, "ariaPressed", JS.param(value));
    }

    public String getAriaReadOnly() {
        return JS.string(JS.get(peer, "ariaReadOnly"));
    }

    public void setAriaReadOnly(String value) {
        JS.set(peer, "ariaReadOnly", JS.param(value));
    }

    public String getAriaRequired() {
        return JS.string(JS.get(peer, "ariaRequired"));
    }

    public void setAriaRequired(String value) {
        JS.set(peer, "ariaRequired", JS.param(value));
    }

    public String getAriaRoleDescription() {
        return JS.string(JS.get(peer, "ariaRoleDescription"));
    }

    public void setAriaRoleDescription(String value) {
        JS.set(peer, "ariaRoleDescription", JS.param(value));
    }

    public String getAriaRowCount() {
        return JS.string(JS.get(peer, "ariaRowCount"));
    }

    public void setAriaRowCount(String value) {
        JS.set(peer, "ariaRowCount", JS.param(value));
    }

    public String getAriaRowIndex() {
        return JS.string(JS.get(peer, "ariaRowIndex"));
    }

    public void setAriaRowIndex(String value) {
        JS.set(peer, "ariaRowIndex", JS.param(value));
    }

    public String getAriaRowIndexText() {
        return JS.string(JS.get(peer, "ariaRowIndexText"));
    }

    public void setAriaRowIndexText(String value) {
        JS.set(peer, "ariaRowIndexText", JS.param(value));
    }

    public String getAriaRowSpan() {
        return JS.string(JS.get(peer, "ariaRowSpan"));
    }

    public void setAriaRowSpan(String value) {
        JS.set(peer, "ariaRowSpan", JS.param(value));
    }

    public String getAriaSelected() {
        return JS.string(JS.get(peer, "ariaSelected"));
    }

    public void setAriaSelected(String value) {
        JS.set(peer, "ariaSelected", JS.param(value));
    }

    public String getAriaSetSize() {
        return JS.string(JS.get(peer, "ariaSetSize"));
    }

    public void setAriaSetSize(String value) {
        JS.set(peer, "ariaSetSize", JS.param(value));
    }

    public String getAriaSort() {
        return JS.string(JS.get(peer, "ariaSort"));
    }

    public void setAriaSort(String value) {
        JS.set(peer, "ariaSort", JS.param(value));
    }

    public String getAriaValueMax() {
        return JS.string(JS.get(peer, "ariaValueMax"));
    }

    public void setAriaValueMax(String value) {
        JS.set(peer, "ariaValueMax", JS.param(value));
    }

    public String getAriaValueMin() {
        return JS.string(JS.get(peer, "ariaValueMin"));
    }

    public void setAriaValueMin(String value) {
        JS.set(peer, "ariaValueMin", JS.param(value));
    }

    public String getAriaValueNow() {
        return JS.string(JS.get(peer, "ariaValueNow"));
    }

    public void setAriaValueNow(String value) {
        JS.set(peer, "ariaValueNow", JS.param(value));
    }

    public String getAriaValueText() {
        return JS.string(JS.get(peer, "ariaValueText"));
    }

    public void setAriaValueText(String value) {
        JS.set(peer, "ariaValueText", JS.param(value));
    }

    public void setFormValue(WrappedObject value, WrappedObject state) {
        JS.invoke(peer, "setFormValue", JS.param(value), JS.param(state));
    }

    public void setFormValue(String value, WrappedObject state) {
        JS.invoke(peer, "setFormValue", JS.param(value), JS.param(state));
    }

    public void setFormValue(FormData value, WrappedObject state) {
        JS.invoke(peer, "setFormValue", JS.param(value), JS.param(state));
    }

    public void setFormValue(WrappedObject value, String state) {
        JS.invoke(peer, "setFormValue", JS.param(value), JS.param(state));
    }

    public void setFormValue(String value, String state) {
        JS.invoke(peer, "setFormValue", JS.param(value), JS.param(state));
    }

    public void setFormValue(FormData value, String state) {
        JS.invoke(peer, "setFormValue", JS.param(value), JS.param(state));
    }

    public void setFormValue(WrappedObject value, FormData state) {
        JS.invoke(peer, "setFormValue", JS.param(value), JS.param(state));
    }

    public void setFormValue(String value, FormData state) {
        JS.invoke(peer, "setFormValue", JS.param(value), JS.param(state));
    }

    public void setFormValue(FormData value, FormData state) {
        JS.invoke(peer, "setFormValue", JS.param(value), JS.param(state));
    }

    public void setFormValue(WrappedObject value) {
        JS.invoke(peer, "setFormValue", JS.param(value));
    }

    public void setFormValue(String value) {
        JS.invoke(peer, "setFormValue", JS.param(value));
    }

    public void setFormValue(FormData value) {
        JS.invoke(peer, "setFormValue", JS.param(value));
    }

    public void setValidity(JSObject flags, String message, HTMLElement anchor) {
        JS.invoke(peer, "setValidity", JS.param(flags), JS.param(message), JS.param(anchor));
    }

    public void setValidity(JSObject flags, String message) {
        JS.invoke(peer, "setValidity", JS.param(flags), JS.param(message));
    }

    public void setValidity(JSObject flags) {
        JS.invoke(peer, "setValidity", JS.param(flags));
    }

    public void setValidity() {
        JS.invoke(peer, "setValidity");
    }

    public boolean checkValidity() {
        return JS.invoke(peer, "checkValidity");
    }

    public boolean reportValidity() {
        return JS.invoke(peer, "reportValidity");
    }
}
