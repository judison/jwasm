// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.dom.Document;

@SuppressWarnings({"unused"})
public class HTMLFrameElement extends HTMLElement {
    protected HTMLFrameElement(Object peer) {
        super(peer);
    }

    public static HTMLFrameElement wrap(Object peer) {
        return peer == null ? null : new HTMLFrameElement(peer);
    }

    public String getName() {
        return JS.string(JS.get(peer, "name"));
    }

    public void setName(String value) {
        JS.set(peer, "name", JS.param(value));
    }

    public String getScrolling() {
        return JS.string(JS.get(peer, "scrolling"));
    }

    public void setScrolling(String value) {
        JS.set(peer, "scrolling", JS.param(value));
    }

    public String getSrc() {
        return JS.string(JS.get(peer, "src"));
    }

    public void setSrc(String value) {
        JS.set(peer, "src", JS.param(value));
    }

    public String getFrameBorder() {
        return JS.string(JS.get(peer, "frameBorder"));
    }

    public void setFrameBorder(String value) {
        JS.set(peer, "frameBorder", JS.param(value));
    }

    public String getLongDesc() {
        return JS.string(JS.get(peer, "longDesc"));
    }

    public void setLongDesc(String value) {
        JS.set(peer, "longDesc", JS.param(value));
    }

    public boolean isNoResize() {
        return JS.getBoolean(peer, "noResize");
    }

    public void setNoResize(boolean value) {
        JS.set(peer, "noResize", value);
    }

    public Document getContentDocument() {
        return Document.wrap(JS.get(peer, "contentDocument"));
    }

    public WindowProxy getContentWindow() {
        return WindowProxy.wrap(JS.get(peer, "contentWindow"));
    }

    public String getMarginHeight() {
        return JS.string(JS.get(peer, "marginHeight"));
    }

    public void setMarginHeight(String value) {
        JS.set(peer, "marginHeight", JS.param(value));
    }

    public String getMarginWidth() {
        return JS.string(JS.get(peer, "marginWidth"));
    }

    public void setMarginWidth(String value) {
        JS.set(peer, "marginWidth", JS.param(value));
    }
}
