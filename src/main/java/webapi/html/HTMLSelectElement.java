// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.dom.HTMLCollection;
import webapi.dom.NodeList;

@SuppressWarnings({"unused"})
public class HTMLSelectElement extends HTMLElement {
    protected HTMLSelectElement(Object peer) {
        super(peer);
    }

    public static HTMLSelectElement wrap(Object peer) {
        return peer == null ? null : new HTMLSelectElement(peer);
    }

    public String getAutocomplete() {
        return JS.string(JS.get(peer, "autocomplete"));
    }

    public void setAutocomplete(String value) {
        JS.set(peer, "autocomplete", JS.param(value));
    }

    public boolean isDisabled() {
        return JS.getBoolean(peer, "disabled");
    }

    public void setDisabled(boolean value) {
        JS.set(peer, "disabled", value);
    }

    public HTMLFormElement getForm() {
        return HTMLFormElement.wrap(JS.get(peer, "form"));
    }

    public boolean isMultiple() {
        return JS.getBoolean(peer, "multiple");
    }

    public void setMultiple(boolean value) {
        JS.set(peer, "multiple", value);
    }

    public String getName() {
        return JS.string(JS.get(peer, "name"));
    }

    public void setName(String value) {
        JS.set(peer, "name", JS.param(value));
    }

    public boolean isRequired() {
        return JS.getBoolean(peer, "required");
    }

    public void setRequired(boolean value) {
        JS.set(peer, "required", value);
    }

    public int getSize() {
        return JS.getInt(peer, "size");
    }

    public void setSize(int value) {
        JS.set(peer, "size", value);
    }

    public String getType() {
        return JS.string(JS.get(peer, "type"));
    }

    public HTMLOptionsCollection getOptions() {
        return HTMLOptionsCollection.wrap(JS.get(peer, "options"));
    }

    public int getLength() {
        return JS.getInt(peer, "length");
    }

    public void setLength(int value) {
        JS.set(peer, "length", value);
    }

    public HTMLCollection getSelectedOptions() {
        return HTMLCollection.wrap(JS.get(peer, "selectedOptions"));
    }

    public long getSelectedIndex() {
        return (long)JS.get(peer, "selectedIndex");
    }

    public void setSelectedIndex(long value) {
        JS.set(peer, "selectedIndex", value);
    }

    public String getValue() {
        return JS.string(JS.get(peer, "value"));
    }

    public void setValue(String value) {
        JS.set(peer, "value", JS.param(value));
    }

    public boolean isWillValidate() {
        return JS.getBoolean(peer, "willValidate");
    }

    public ValidityState getValidity() {
        return ValidityState.wrap(JS.get(peer, "validity"));
    }

    public String getValidationMessage() {
        return JS.string(JS.get(peer, "validationMessage"));
    }

    public NodeList getLabels() {
        return NodeList.wrap(JS.get(peer, "labels"));
    }

    public HTMLOptionElement namedItem(String name) {
        return HTMLOptionElement.wrap(JS.invoke(peer, "namedItem", JS.param(name)));
    }

    public void add(HTMLOptionElement element, HTMLElement before) {
        JS.invoke(peer, "add", JS.param(element), JS.param(before));
    }

    public void add(HTMLOptGroupElement element, HTMLElement before) {
        JS.invoke(peer, "add", JS.param(element), JS.param(before));
    }

    public void add(HTMLOptionElement element, long before) {
        JS.invoke(peer, "add", JS.param(element), before);
    }

    public void add(HTMLOptGroupElement element, long before) {
        JS.invoke(peer, "add", JS.param(element), before);
    }

    public void add(HTMLOptionElement element) {
        JS.invoke(peer, "add", JS.param(element));
    }

    public void add(HTMLOptGroupElement element) {
        JS.invoke(peer, "add", JS.param(element));
    }

    public void remove() {
        JS.invoke(peer, "remove");
    }

    public void remove(long index) {
        JS.invoke(peer, "remove", index);
    }

    public boolean checkValidity() {
        return JS.invoke(peer, "checkValidity");
    }

    public boolean reportValidity() {
        return JS.invoke(peer, "reportValidity");
    }

    public void setCustomValidity(String error) {
        JS.invoke(peer, "setCustomValidity", JS.param(error));
    }
}
