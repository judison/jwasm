// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.dom.HTMLCollection;

@SuppressWarnings({"unused"})
public class HTMLTableElement extends HTMLElement {
    protected HTMLTableElement(Object peer) {
        super(peer);
    }

    public static HTMLTableElement wrap(Object peer) {
        return peer == null ? null : new HTMLTableElement(peer);
    }

    public HTMLTableCaptionElement getCaption() {
        return HTMLTableCaptionElement.wrap(JS.get(peer, "caption"));
    }

    public void setCaption(HTMLTableCaptionElement value) {
        JS.set(peer, "caption", value.peer);
    }

    public HTMLTableSectionElement getTHead() {
        return HTMLTableSectionElement.wrap(JS.get(peer, "tHead"));
    }

    public void setTHead(HTMLTableSectionElement value) {
        JS.set(peer, "tHead", value.peer);
    }

    public HTMLTableSectionElement getTFoot() {
        return HTMLTableSectionElement.wrap(JS.get(peer, "tFoot"));
    }

    public void setTFoot(HTMLTableSectionElement value) {
        JS.set(peer, "tFoot", value.peer);
    }

    public HTMLCollection getTBodies() {
        return HTMLCollection.wrap(JS.get(peer, "tBodies"));
    }

    public HTMLCollection getRows() {
        return HTMLCollection.wrap(JS.get(peer, "rows"));
    }

    public String getAlign() {
        return JS.string(JS.get(peer, "align"));
    }

    public void setAlign(String value) {
        JS.set(peer, "align", JS.param(value));
    }

    public String getBorder() {
        return JS.string(JS.get(peer, "border"));
    }

    public void setBorder(String value) {
        JS.set(peer, "border", JS.param(value));
    }

    public String getFrame() {
        return JS.string(JS.get(peer, "frame"));
    }

    public void setFrame(String value) {
        JS.set(peer, "frame", JS.param(value));
    }

    public String getRules() {
        return JS.string(JS.get(peer, "rules"));
    }

    public void setRules(String value) {
        JS.set(peer, "rules", JS.param(value));
    }

    public String getSummary() {
        return JS.string(JS.get(peer, "summary"));
    }

    public void setSummary(String value) {
        JS.set(peer, "summary", JS.param(value));
    }

    public String getWidth() {
        return JS.string(JS.get(peer, "width"));
    }

    public void setWidth(String value) {
        JS.set(peer, "width", JS.param(value));
    }

    public String getBgColor() {
        return JS.string(JS.get(peer, "bgColor"));
    }

    public void setBgColor(String value) {
        JS.set(peer, "bgColor", JS.param(value));
    }

    public String getCellPadding() {
        return JS.string(JS.get(peer, "cellPadding"));
    }

    public void setCellPadding(String value) {
        JS.set(peer, "cellPadding", JS.param(value));
    }

    public String getCellSpacing() {
        return JS.string(JS.get(peer, "cellSpacing"));
    }

    public void setCellSpacing(String value) {
        JS.set(peer, "cellSpacing", JS.param(value));
    }

    public HTMLTableCaptionElement createCaption() {
        return HTMLTableCaptionElement.wrap(JS.invoke(peer, "createCaption"));
    }

    public void deleteCaption() {
        JS.invoke(peer, "deleteCaption");
    }

    public HTMLTableSectionElement createTHead() {
        return HTMLTableSectionElement.wrap(JS.invoke(peer, "createTHead"));
    }

    public void deleteTHead() {
        JS.invoke(peer, "deleteTHead");
    }

    public HTMLTableSectionElement createTFoot() {
        return HTMLTableSectionElement.wrap(JS.invoke(peer, "createTFoot"));
    }

    public void deleteTFoot() {
        JS.invoke(peer, "deleteTFoot");
    }

    public HTMLTableSectionElement createTBody() {
        return HTMLTableSectionElement.wrap(JS.invoke(peer, "createTBody"));
    }

    public HTMLTableRowElement insertRow(long index) {
        return HTMLTableRowElement.wrap(JS.invoke(peer, "insertRow", index));
    }

    public HTMLTableRowElement insertRow() {
        return HTMLTableRowElement.wrap(JS.invoke(peer, "insertRow"));
    }

    public void deleteRow(long index) {
        JS.invoke(peer, "deleteRow", index);
    }
}
