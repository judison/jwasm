// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;

@SuppressWarnings({"unused"})
public class HTMLUListElement extends HTMLElement {
    protected HTMLUListElement(Object peer) {
        super(peer);
    }

    public static HTMLUListElement wrap(Object peer) {
        return peer == null ? null : new HTMLUListElement(peer);
    }

    public boolean isCompact() {
        return JS.getBoolean(peer, "compact");
    }

    public void setCompact(boolean value) {
        JS.set(peer, "compact", value);
    }

    public String getType() {
        return JS.string(JS.get(peer, "type"));
    }

    public void setType(String value) {
        JS.set(peer, "type", JS.param(value));
    }
}
