// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class WorkerLocation extends WrappedObject {
    protected WorkerLocation(Object peer) {
        super(peer);
    }

    public static WorkerLocation wrap(Object peer) {
        return peer == null ? null : new WorkerLocation(peer);
    }

    public String getHref() {
        return JS.string(JS.get(peer, "href"));
    }

    public String getOrigin() {
        return JS.string(JS.get(peer, "origin"));
    }

    public String getProtocol() {
        return JS.string(JS.get(peer, "protocol"));
    }

    public String getHost() {
        return JS.string(JS.get(peer, "host"));
    }

    public String getHostname() {
        return JS.string(JS.get(peer, "hostname"));
    }

    public String getPort() {
        return JS.string(JS.get(peer, "port"));
    }

    public String getPathname() {
        return JS.string(JS.get(peer, "pathname"));
    }

    public String getSearch() {
        return JS.string(JS.get(peer, "search"));
    }

    public String getHash() {
        return JS.string(JS.get(peer, "hash"));
    }
}
