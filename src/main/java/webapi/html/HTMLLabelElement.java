// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;

@SuppressWarnings({"unused"})
public class HTMLLabelElement extends HTMLElement {
    protected HTMLLabelElement(Object peer) {
        super(peer);
    }

    public static HTMLLabelElement wrap(Object peer) {
        return peer == null ? null : new HTMLLabelElement(peer);
    }

    public HTMLFormElement getForm() {
        return HTMLFormElement.wrap(JS.get(peer, "form"));
    }

    public String getHtmlFor() {
        return JS.string(JS.get(peer, "htmlFor"));
    }

    public void setHtmlFor(String value) {
        JS.set(peer, "htmlFor", JS.param(value));
    }

    public HTMLElement getControl() {
        return HTMLElement.wrap(JS.get(peer, "control"));
    }
}
