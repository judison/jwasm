// Generated file; DO NOT edit
package webapi.html;

import webapi.JS;
import webapi.dom.DOMTokenList;
import webapi.dom.Document;

@SuppressWarnings({"unused"})
public class HTMLIFrameElement extends HTMLElement {
    protected HTMLIFrameElement(Object peer) {
        super(peer);
    }

    public static HTMLIFrameElement wrap(Object peer) {
        return peer == null ? null : new HTMLIFrameElement(peer);
    }

    public String getSrc() {
        return JS.string(JS.get(peer, "src"));
    }

    public void setSrc(String value) {
        JS.set(peer, "src", JS.param(value));
    }

    public String getSrcdoc() {
        return JS.string(JS.get(peer, "srcdoc"));
    }

    public void setSrcdoc(String value) {
        JS.set(peer, "srcdoc", JS.param(value));
    }

    public String getName() {
        return JS.string(JS.get(peer, "name"));
    }

    public void setName(String value) {
        JS.set(peer, "name", JS.param(value));
    }

    public DOMTokenList getSandbox() {
        return DOMTokenList.wrap(JS.get(peer, "sandbox"));
    }

    public String getAllow() {
        return JS.string(JS.get(peer, "allow"));
    }

    public void setAllow(String value) {
        JS.set(peer, "allow", JS.param(value));
    }

    public boolean isAllowFullscreen() {
        return JS.getBoolean(peer, "allowFullscreen");
    }

    public void setAllowFullscreen(boolean value) {
        JS.set(peer, "allowFullscreen", value);
    }

    public String getWidth() {
        return JS.string(JS.get(peer, "width"));
    }

    public void setWidth(String value) {
        JS.set(peer, "width", JS.param(value));
    }

    public String getHeight() {
        return JS.string(JS.get(peer, "height"));
    }

    public void setHeight(String value) {
        JS.set(peer, "height", JS.param(value));
    }

    public String getReferrerPolicy() {
        return JS.string(JS.get(peer, "referrerPolicy"));
    }

    public void setReferrerPolicy(String value) {
        JS.set(peer, "referrerPolicy", JS.param(value));
    }

    public String getLoading() {
        return JS.string(JS.get(peer, "loading"));
    }

    public void setLoading(String value) {
        JS.set(peer, "loading", JS.param(value));
    }

    public Document getContentDocument() {
        return Document.wrap(JS.get(peer, "contentDocument"));
    }

    public WindowProxy getContentWindow() {
        return WindowProxy.wrap(JS.get(peer, "contentWindow"));
    }

    public String getAlign() {
        return JS.string(JS.get(peer, "align"));
    }

    public void setAlign(String value) {
        JS.set(peer, "align", JS.param(value));
    }

    public String getScrolling() {
        return JS.string(JS.get(peer, "scrolling"));
    }

    public void setScrolling(String value) {
        JS.set(peer, "scrolling", JS.param(value));
    }

    public String getFrameBorder() {
        return JS.string(JS.get(peer, "frameBorder"));
    }

    public void setFrameBorder(String value) {
        JS.set(peer, "frameBorder", JS.param(value));
    }

    public String getLongDesc() {
        return JS.string(JS.get(peer, "longDesc"));
    }

    public void setLongDesc(String value) {
        JS.set(peer, "longDesc", JS.param(value));
    }

    public String getMarginHeight() {
        return JS.string(JS.get(peer, "marginHeight"));
    }

    public void setMarginHeight(String value) {
        JS.set(peer, "marginHeight", JS.param(value));
    }

    public String getMarginWidth() {
        return JS.string(JS.get(peer, "marginWidth"));
    }

    public void setMarginWidth(String value) {
        JS.set(peer, "marginWidth", JS.param(value));
    }

    public Document getSVGDocument() {
        return Document.wrap(JS.invoke(peer, "getSVGDocument"));
    }
}
