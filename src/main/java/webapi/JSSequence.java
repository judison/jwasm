package webapi;

public class JSSequence<T> extends WrappedObject {

	public JSSequence() {
		super(JS.newObject());
	}

	public JSSequence(Object peer) {
		super(peer);
	}
	
	public static JSSequence<?> wrap(Object peer) {
		return peer == null ? null : new JSSequence<>(peer);
	}

	//TODO
}
