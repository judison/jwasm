package webapi;

import browser.dom.Wrap;
import browser.javascript.Peered;

public class JSObject extends WrappedObject {

	public JSObject() {
		super(JS.newObject());
	}

	public JSObject(Object peer) {
		super(peer);
	}

	public String getString(String propName) {
		return Wrap.String(JS.get(peer, propName));
	}

	public int getInt(String propName) {
		return JS.getInt(peer, propName);
	}

	public double getDouble(String propName) {
		return JS.getDouble(peer, propName);
	}

	public boolean getBoolean(String propName) {
		return JS.getBoolean(peer, propName);
	}

	public Object getRaw(String propName) {
		return JS.get(peer, propName);
	}

	public JSObject getJSObject(String propName) {
		return new JSObject(JS.get(peer, propName));
	}

	public void set(String propName, String value) {
		JS.set(peer, propName, value);
	}

	public void set(String propName, int value) {
		JS.set(peer, propName, value);
	}

	public void set(String propName, double value) {
		JS.set(peer, propName, value);
	}

	public void set(String propName, boolean value) {
		JS.set(peer, propName, value);
	}

	public void set(String propName, Peered value) {
		JS.set(peer, propName, value.peer);
	}

	public boolean has(String propName) {
		return JS.in(peer, propName);
	}

	public void remove(String propName) {
		JS.delete(peer, propName);
	}

}
