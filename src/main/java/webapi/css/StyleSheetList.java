// Generated file; DO NOT edit
package webapi.css;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class StyleSheetList extends WrappedObject {
    protected StyleSheetList(Object peer) {
        super(peer);
    }

    public static StyleSheetList wrap(Object peer) {
        return peer == null ? null : new StyleSheetList(peer);
    }

    public int getLength() {
        return JS.getInt(peer, "length");
    }
}
