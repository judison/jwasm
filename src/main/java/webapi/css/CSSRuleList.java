// Generated file; DO NOT edit
package webapi.css;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class CSSRuleList extends WrappedObject {
    protected CSSRuleList(Object peer) {
        super(peer);
    }

    public static CSSRuleList wrap(Object peer) {
        return peer == null ? null : new CSSRuleList(peer);
    }

    public int getLength() {
        return JS.getInt(peer, "length");
    }
}
