// Generated file; DO NOT edit
package webapi.css;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class MediaList extends WrappedObject {
    protected MediaList(Object peer) {
        super(peer);
    }

    public static MediaList wrap(Object peer) {
        return peer == null ? null : new MediaList(peer);
    }

    public String getMediaText() {
        return JS.string(JS.get(peer, "mediaText"));
    }

    public void setMediaText(String value) {
        JS.set(peer, "mediaText", JS.param(value));
    }

    public int getLength() {
        return JS.getInt(peer, "length");
    }

    public void appendMedium(String medium) {
        JS.invoke(peer, "appendMedium", JS.param(medium));
    }

    public void deleteMedium(String medium) {
        JS.invoke(peer, "deleteMedium", JS.param(medium));
    }
}
