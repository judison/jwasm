// Generated file; DO NOT edit
package webapi.css;

import webapi.JS;

@SuppressWarnings({"unused"})
public class CSSImportRule extends CSSRule {
    protected CSSImportRule(Object peer) {
        super(peer);
    }

    public static CSSImportRule wrap(Object peer) {
        return peer == null ? null : new CSSImportRule(peer);
    }

    public String getHref() {
        return JS.string(JS.get(peer, "href"));
    }

    public MediaList getMedia() {
        return MediaList.wrap(JS.get(peer, "media"));
    }

    public CSSStyleSheet getStyleSheet() {
        return CSSStyleSheet.wrap(JS.get(peer, "styleSheet"));
    }
}
