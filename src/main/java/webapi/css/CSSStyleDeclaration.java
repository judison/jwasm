// Generated file; DO NOT edit
package webapi.css;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class CSSStyleDeclaration extends WrappedObject {
    protected CSSStyleDeclaration(Object peer) {
        super(peer);
    }

    public static CSSStyleDeclaration wrap(Object peer) {
        return peer == null ? null : new CSSStyleDeclaration(peer);
    }

    public String getCssText() {
        return JS.string(JS.get(peer, "cssText"));
    }

    public void setCssText(String value) {
        JS.set(peer, "cssText", JS.param(value));
    }

    public int getLength() {
        return JS.getInt(peer, "length");
    }

    public CSSRule getParentRule() {
        return CSSRule.wrap(JS.get(peer, "parentRule"));
    }

    public String getCssFloat() {
        return JS.string(JS.get(peer, "cssFloat"));
    }

    public void setCssFloat(String value) {
        JS.set(peer, "cssFloat", JS.param(value));
    }

    public String getPropertyValue(String property) {
        return JS.string(JS.invoke(peer, "getPropertyValue", JS.param(property)));
    }

    public String getPropertyPriority(String property) {
        return JS.string(JS.invoke(peer, "getPropertyPriority", JS.param(property)));
    }

    public void setProperty(String property, String value, String priority) {
        JS.invoke(peer, "setProperty", JS.param(property), JS.param(value), JS.param(priority));
    }

    public void setProperty(String property, String value) {
        JS.invoke(peer, "setProperty", JS.param(property), JS.param(value));
    }

    public String removeProperty(String property) {
        return JS.string(JS.invoke(peer, "removeProperty", JS.param(property)));
    }
}
