// Generated file; DO NOT edit
package webapi.css;

import webapi.JS;

@SuppressWarnings({"unused"})
public class CSSMarginRule extends CSSRule {
    protected CSSMarginRule(Object peer) {
        super(peer);
    }

    public static CSSMarginRule wrap(Object peer) {
        return peer == null ? null : new CSSMarginRule(peer);
    }

    public String getName() {
        return JS.string(JS.get(peer, "name"));
    }

    public CSSStyleDeclaration getStyle() {
        return CSSStyleDeclaration.wrap(JS.get(peer, "style"));
    }
}
