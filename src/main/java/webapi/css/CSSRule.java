// Generated file; DO NOT edit
package webapi.css;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class CSSRule extends WrappedObject {
    protected CSSRule(Object peer) {
        super(peer);
    }

    public static CSSRule wrap(Object peer) {
        return peer == null ? null : new CSSRule(peer);
    }

    public String getCssText() {
        return JS.string(JS.get(peer, "cssText"));
    }

    public void setCssText(String value) {
        JS.set(peer, "cssText", JS.param(value));
    }

    public CSSRule getParentRule() {
        return CSSRule.wrap(JS.get(peer, "parentRule"));
    }

    public CSSStyleSheet getParentStyleSheet() {
        return CSSStyleSheet.wrap(JS.get(peer, "parentStyleSheet"));
    }

    public int getType() {
        return JS.getInt(peer, "type");
    }
}
