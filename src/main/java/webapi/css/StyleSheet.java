// Generated file; DO NOT edit
package webapi.css;

import webapi.JS;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class StyleSheet extends WrappedObject {
    protected StyleSheet(Object peer) {
        super(peer);
    }

    public static StyleSheet wrap(Object peer) {
        return peer == null ? null : new StyleSheet(peer);
    }

    public String getType() {
        return JS.string(JS.get(peer, "type"));
    }

    public String getHref() {
        return JS.string(JS.get(peer, "href"));
    }

    public WrappedObject getOwnerNode() {
        return WrappedObject.wrap(JS.get(peer, "ownerNode"));
    }

    public CSSStyleSheet getParentStyleSheet() {
        return CSSStyleSheet.wrap(JS.get(peer, "parentStyleSheet"));
    }

    public String getTitle() {
        return JS.string(JS.get(peer, "title"));
    }

    public MediaList getMedia() {
        return MediaList.wrap(JS.get(peer, "media"));
    }

    public boolean isDisabled() {
        return JS.getBoolean(peer, "disabled");
    }

    public void setDisabled(boolean value) {
        JS.set(peer, "disabled", value);
    }
}
