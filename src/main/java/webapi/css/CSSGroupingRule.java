// Generated file; DO NOT edit
package webapi.css;

import webapi.JS;

@SuppressWarnings({"unused"})
public class CSSGroupingRule extends CSSRule {
    protected CSSGroupingRule(Object peer) {
        super(peer);
    }

    public static CSSGroupingRule wrap(Object peer) {
        return peer == null ? null : new CSSGroupingRule(peer);
    }

    public CSSRuleList getCssRules() {
        return CSSRuleList.wrap(JS.get(peer, "cssRules"));
    }

    public int insertRule(String rule, int index) {
        return JS.invoke(peer, "insertRule", JS.param(rule), index);
    }

    public int insertRule(String rule) {
        return JS.invoke(peer, "insertRule", JS.param(rule));
    }

    public void deleteRule(int index) {
        JS.invoke(peer, "deleteRule", index);
    }
}
