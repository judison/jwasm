// Generated file; DO NOT edit
package webapi.css;

import webapi.JS;

@SuppressWarnings({"unused"})
public class CSSPageRule extends CSSGroupingRule {
    protected CSSPageRule(Object peer) {
        super(peer);
    }

    public static CSSPageRule wrap(Object peer) {
        return peer == null ? null : new CSSPageRule(peer);
    }

    public String getSelectorText() {
        return JS.string(JS.get(peer, "selectorText"));
    }

    public void setSelectorText(String value) {
        JS.set(peer, "selectorText", JS.param(value));
    }

    public CSSStyleDeclaration getStyle() {
        return CSSStyleDeclaration.wrap(JS.get(peer, "style"));
    }
}
