// Generated file; DO NOT edit
package webapi.css;

import webapi.JS;
import webapi.JSPromise;

@SuppressWarnings({"unused"})
public class CSSStyleSheet extends StyleSheet {
    protected CSSStyleSheet(Object peer) {
        super(peer);
    }

    public static CSSStyleSheet wrap(Object peer) {
        return peer == null ? null : new CSSStyleSheet(peer);
    }

    public CSSRule getOwnerRule() {
        return CSSRule.wrap(JS.get(peer, "ownerRule"));
    }

    public CSSRuleList getCssRules() {
        return CSSRuleList.wrap(JS.get(peer, "cssRules"));
    }

    public CSSRuleList getRules() {
        return CSSRuleList.wrap(JS.get(peer, "rules"));
    }

    public int insertRule(String rule, int index) {
        return JS.invoke(peer, "insertRule", JS.param(rule), index);
    }

    public int insertRule(String rule) {
        return JS.invoke(peer, "insertRule", JS.param(rule));
    }

    public void deleteRule(int index) {
        JS.invoke(peer, "deleteRule", index);
    }

    public JSPromise<CSSStyleSheet> replace(String text) {
        return JSPromise.wrap(CSSStyleSheet.class, JS.invoke(peer, "replace", JS.param(text)));
    }

    public void replaceSync(String text) {
        JS.invoke(peer, "replaceSync", JS.param(text));
    }

    public long addRule(String selector, String style, int index) {
        return JS.invoke(peer, "addRule", JS.param(selector), JS.param(style), index);
    }

    public long addRule(String selector, String style) {
        return JS.invoke(peer, "addRule", JS.param(selector), JS.param(style));
    }

    public long addRule(String selector) {
        return JS.invoke(peer, "addRule", JS.param(selector));
    }

    public long addRule() {
        return JS.invoke(peer, "addRule");
    }

    public void removeRule(int index) {
        JS.invoke(peer, "removeRule", index);
    }

    public void removeRule() {
        JS.invoke(peer, "removeRule");
    }
}
