// Generated file; DO NOT edit
package webapi.css;

import webapi.JS;

@SuppressWarnings({"unused"})
public class CSSNamespaceRule extends CSSRule {
    protected CSSNamespaceRule(Object peer) {
        super(peer);
    }

    public static CSSNamespaceRule wrap(Object peer) {
        return peer == null ? null : new CSSNamespaceRule(peer);
    }

    public String getNamespaceURI() {
        return JS.string(JS.get(peer, "namespaceURI"));
    }

    public String getPrefix() {
        return JS.string(JS.get(peer, "prefix"));
    }
}
