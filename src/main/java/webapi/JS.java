package webapi;

import javax.annotation.Nonnull;

import de.inetsoftware.jwebassembly.api.annotation.Import;
import de.inetsoftware.jwebassembly.web.DOMString;

public class JS {

	public static final String MODULE = "JS";

	public static String string(Object peer) {
		return peer == null ? null : de.inetsoftware.jwebassembly.web.JSObject.toJavaString(peer);
	}

	//=========================
	// Param Adapters
	//=========================

	public static DOMString param(String str) {
		return str == null ? null : de.inetsoftware.jwebassembly.web.JSObject.domString(str);
	}

	public static Object param(WrappedObject wrappedObject) {
		return wrappedObject == null ? null : wrappedObject.peer;
	}

	public static Object param(Object object) {
		if (object instanceof WrappedObject)
			return ((WrappedObject)object).peer;
		return object;
	}

	//=========================
	// newObject -> new Object()
	//=========================

	@Import(module = MODULE, js = "()=>new Object()")
	private static native Object newObject0();

	public static Object newObject() {
		return newObject0();
	}

	//=========================
	// get -> o[p]
	//=========================

	@Import(module = MODULE, js = "(o,p)=>o[p]")
	private static native <T> T get0(@Nonnull Object peer, @Nonnull DOMString propName);

	public static Object get(@Nonnull Object peer, @Nonnull String propName) {
		return get0(peer, param(propName));
	}

	public static int getInt(@Nonnull Object peer, @Nonnull String propName) {
		return get0(peer, param(propName));
	}

	public static double getDouble(@Nonnull Object peer, @Nonnull String propName) {
		return get0(peer, param(propName));
	}

	public static boolean getBoolean(@Nonnull Object peer, @Nonnull String propName) {
		return get0(peer, param(propName));
	}

	//=========================
	// set -> o[p]=v
	//=========================

	@Import(module = MODULE, js = "(o,p,v)=>o[p]=v")
	private static native void set0(Object peer, @Nonnull DOMString propName, Object value);

	public static void set(@Nonnull Object peer, @Nonnull String propName, Object value) {
		set0(peer, param(propName), value);
	}

	public static void set(@Nonnull Object peer, @Nonnull String propName, String value) {
		set0(peer, param(propName), param(value));
	}

	//=========================
	// delete -> delete o[p]
	//=========================

	@Import(module = MODULE, js = "(o,p)=>delete o[p]")
	private static native void delete0(Object peer, @Nonnull DOMString propName);

	public static void delete(Object peer, String propName) {
		delete0(peer, param(propName));
	}

	//=========================
	// in -> p in o
	//=========================

	@Import(module = MODULE, js = "(o,p)=>p in o")
	private static native boolean in0(Object peer, @Nonnull DOMString propName);

	public static boolean in(Object peer, String propName) {
		return in0(peer, param(propName));
	}

	//=========================
	// invoke -> o[m](p1,p2,...)
	//=========================

	@Import(module = MODULE, js = "(o,m)=>o[m]()")
	private static native <T> T invoke0(Object peer, DOMString methodName);

	@Import(module = MODULE, js = "(o,m,p1)=>o[m](p1)")
	private static native <T> T invoke1(Object peer, DOMString methodName, Object param1);

	@Import(module = MODULE, js = "(o,m,p1,p2)=>o[m](p1,p2)")
	private static native <T> T invoke2(Object peer, DOMString methodName, Object param1, Object param2);

	@Import(module = MODULE, js = "(o,m,p1,p2,p3)=>o[m](p1,p2,p3)")
	private static native <T> T invoke3(Object peer, DOMString methodName, Object param1, Object param2, Object param3);

	@Import(module = MODULE, js = "(o,m,p1,p2,p3,p4)=>o[m](p1,p2,p3,p4)")
	private static native <T> T invoke4(Object peer, DOMString methodName, Object param1, Object param2, Object param3, Object param4);

	@Import(module = MODULE, js = "(o,m,p1,p2,p3,p4,p5)=>o[m](p1,p2,p3,p4,p5)")
	private static native <T> T invoke5(Object peer, DOMString methodName, Object param1, Object param2, Object param3, Object param4, Object param5);

	@Import(module = MODULE, js = "(o,m,p1,p2,p3,p4,p5,p6)=>o[m](p1,p2,p3,p4,p5,p6)")
	private static native <T> T invoke6(Object peer, DOMString methodName, Object param1, Object param2, Object param3, Object param4, Object param5, Object param6);

	@Import(module = MODULE, js = "(o,m,p1,p2,p3,p4,p5,p6,p7)=>o[m](p1,p2,p3,p4,p5,p6,p7)")
	private static native <T> T invoke7(Object peer, DOMString methodName, Object param1, Object param2, Object param3, Object param4, Object param5, Object param6, Object param7);

	@Import(module = MODULE, js = "(o,m,p1,p2,p3,p4,p5,p6,p7,p8)=>o[m](p1,p2,p3,p4,p5,p6,p7,p8)")
	private static native <T> T invoke8(Object peer, DOMString methodName, Object param1, Object param2, Object param3, Object param4, Object param5, Object param6, Object param7, Object param8);

	public static <T> T invoke(Object peer, String methodName) {
		return invoke0(peer, param(methodName));
	}

	public static <T> T invoke(Object peer, String methodName, Object param1) {
		return invoke1(peer, param(methodName), param1);
	}

	public static <T> T invoke(Object peer, String methodName, Object param1, Object param2) {
		return invoke2(peer, param(methodName), param1, param2);
	}

	public static <T> T invoke(Object peer, String methodName, Object param1, Object param2, Object param3) {
		return invoke3(peer, param(methodName), param1, param2, param3);
	}

	public static <T> T invoke(Object peer, String methodName, Object param1, Object param2, Object param3, Object param4) {
		return invoke4(peer, param(methodName), param1, param2, param3, param4);
	}

	public static <T> T invoke(Object peer, String methodName, Object param1, Object param2, Object param3, Object param4, Object param5) {
		return invoke5(peer, param(methodName), param1, param2, param3, param4, param5);
	}

	public static <T> T invoke(Object peer, String methodName, Object param1, Object param2, Object param3, Object param4, Object param5, Object param6) {
		return invoke6(peer, param(methodName), param1, param2, param3, param4, param5, param6);
	}

	public static <T> T invoke(Object peer, String methodName, Object param1, Object param2, Object param3, Object param4, Object param5, Object param6, Object param7) {
		return invoke7(peer, param(methodName), param1, param2, param3, param4, param5, param6, param7);
	}

	public static <T> T invoke(Object peer, String methodName, Object param1, Object param2, Object param3, Object param4, Object param5, Object param6, Object param7, Object param8) {
		return invoke8(peer, param(methodName), param1, param2, param3, param4, param5, param6, param7, param8);
	}

	//=========================
	// TODO Callbacks
	//=========================

	//=========================
	// Other Stuff
	//=========================

	@Import(module = MODULE, js = "()=>window")
	public static native Object window();

	@Import(module = MODULE, js = "()=>document")
	public static native Object document();

	@Import(module = MODULE, js = "()=>console")
	public static native Object console();


}
