// Generated file; DO NOT edit
package webapi.xhr;

@SuppressWarnings({"unused"})
public class XMLHttpRequestUpload extends XMLHttpRequestEventTarget {
    protected XMLHttpRequestUpload(Object peer) {
        super(peer);
    }

    public static XMLHttpRequestUpload wrap(Object peer) {
        return peer == null ? null : new XMLHttpRequestUpload(peer);
    }
}
