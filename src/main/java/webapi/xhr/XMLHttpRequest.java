// Generated file; DO NOT edit
package webapi.xhr;

import webapi.JS;
import webapi.WrappedObject;
import webapi.dom.Document;
import webapi.html.EventHandler;

@SuppressWarnings({"unused"})
public class XMLHttpRequest extends XMLHttpRequestEventTarget {
    protected XMLHttpRequest(Object peer) {
        super(peer);
    }

    public static XMLHttpRequest wrap(Object peer) {
        return peer == null ? null : new XMLHttpRequest(peer);
    }

    public EventHandler getOnreadystatechange() {
        return EventHandler.wrap(JS.get(peer, "onreadystatechange"));
    }

    public void setOnreadystatechange(EventHandler value) {
        JS.set(peer, "onreadystatechange", value.peer);
    }

    public int getReadyState() {
        return JS.getInt(peer, "readyState");
    }

    public int getTimeout() {
        return JS.getInt(peer, "timeout");
    }

    public void setTimeout(int value) {
        JS.set(peer, "timeout", value);
    }

    public boolean isWithCredentials() {
        return JS.getBoolean(peer, "withCredentials");
    }

    public void setWithCredentials(boolean value) {
        JS.set(peer, "withCredentials", value);
    }

    public XMLHttpRequestUpload getUpload() {
        return XMLHttpRequestUpload.wrap(JS.get(peer, "upload"));
    }

    public String getResponseURL() {
        return JS.string(JS.get(peer, "responseURL"));
    }

    public int getStatus() {
        return JS.getInt(peer, "status");
    }

    public String getStatusText() {
        return JS.string(JS.get(peer, "statusText"));
    }

    public WrappedObject getResponseType() {
        return WrappedObject.wrap(JS.get(peer, "responseType"));
    }

    public void setResponseType(WrappedObject value) {
        JS.set(peer, "responseType", value.peer);
    }

    public WrappedObject getResponse() {
        return WrappedObject.wrap(JS.get(peer, "response"));
    }

    public String getResponseText() {
        return JS.string(JS.get(peer, "responseText"));
    }

    public Document getResponseXML() {
        return Document.wrap(JS.get(peer, "responseXML"));
    }

    public void open(String method, String url) {
        JS.invoke(peer, "open", JS.param(method), JS.param(url));
    }

    public void open(String method, String url, boolean async, String username, String password) {
        JS.invoke(peer, "open", JS.param(method), JS.param(url), async, JS.param(username), JS.param(password));
    }

    public void open(String method, String url, boolean async, String username) {
        JS.invoke(peer, "open", JS.param(method), JS.param(url), async, JS.param(username));
    }

    public void open(String method, String url, boolean async) {
        JS.invoke(peer, "open", JS.param(method), JS.param(url), async);
    }

    public void setRequestHeader(String name, String value) {
        JS.invoke(peer, "setRequestHeader", JS.param(name), JS.param(value));
    }

    public void send(Document body) {
        JS.invoke(peer, "send", JS.param(body));
    }

    public void send(WrappedObject body) {
        JS.invoke(peer, "send", JS.param(body));
    }

    public void send() {
        JS.invoke(peer, "send");
    }

    public void abort() {
        JS.invoke(peer, "abort");
    }

    public String getResponseHeader(String name) {
        return JS.string(JS.invoke(peer, "getResponseHeader", JS.param(name)));
    }

    public String getAllResponseHeaders() {
        return JS.string(JS.invoke(peer, "getAllResponseHeaders"));
    }

    public void overrideMimeType(String mime) {
        JS.invoke(peer, "overrideMimeType", JS.param(mime));
    }
}
