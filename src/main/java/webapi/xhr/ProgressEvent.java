// Generated file; DO NOT edit
package webapi.xhr;

import webapi.JS;
import webapi.dom.Event;

@SuppressWarnings({"unused"})
public class ProgressEvent extends Event {
    protected ProgressEvent(Object peer) {
        super(peer);
    }

    public static ProgressEvent wrap(Object peer) {
        return peer == null ? null : new ProgressEvent(peer);
    }

    public boolean isLengthComputable() {
        return JS.getBoolean(peer, "lengthComputable");
    }

    public long getLoaded() {
        return (long)JS.get(peer, "loaded");
    }

    public long getTotal() {
        return (long)JS.get(peer, "total");
    }
}
