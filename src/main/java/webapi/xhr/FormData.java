// Generated file; DO NOT edit
package webapi.xhr;

import webapi.JS;
import webapi.JSSequence;
import webapi.WrappedObject;

@SuppressWarnings({"unused"})
public class FormData extends WrappedObject {
    protected FormData(Object peer) {
        super(peer);
    }

    public static FormData wrap(Object peer) {
        return peer == null ? null : new FormData(peer);
    }

    public void append(String name, String value) {
        JS.invoke(peer, "append", JS.param(name), JS.param(value));
    }

    public void append(String name, WrappedObject blobValue, String filename) {
        JS.invoke(peer, "append", JS.param(name), JS.param(blobValue), JS.param(filename));
    }

    public void append(String name, WrappedObject blobValue) {
        JS.invoke(peer, "append", JS.param(name), JS.param(blobValue));
    }

    public void delete(String name) {
        JS.invoke(peer, "delete", JS.param(name));
    }

    public WrappedObject get(String name) {
        return WrappedObject.wrap(JS.invoke(peer, "get", JS.param(name)));
    }

    public JSSequence<WrappedObject> getAll(String name) {
        return JSSequence.wrap(WrappedObject.class, JS.invoke(peer, "getAll", JS.param(name)));
    }

    public boolean has(String name) {
        return JS.invoke(peer, "has", JS.param(name));
    }

    public void set(String name, String value) {
        JS.invoke(peer, "set", JS.param(name), JS.param(value));
    }

    public void set(String name, WrappedObject blobValue, String filename) {
        JS.invoke(peer, "set", JS.param(name), JS.param(blobValue), JS.param(filename));
    }

    public void set(String name, WrappedObject blobValue) {
        JS.invoke(peer, "set", JS.param(name), JS.param(blobValue));
    }
}
