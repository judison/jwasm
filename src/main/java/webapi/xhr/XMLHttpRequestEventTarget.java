// Generated file; DO NOT edit
package webapi.xhr;

import webapi.JS;
import webapi.dom.EventTarget;
import webapi.html.EventHandler;

@SuppressWarnings({"unused"})
public class XMLHttpRequestEventTarget extends EventTarget {
    protected XMLHttpRequestEventTarget(Object peer) {
        super(peer);
    }

    public static XMLHttpRequestEventTarget wrap(Object peer) {
        return peer == null ? null : new XMLHttpRequestEventTarget(peer);
    }

    public EventHandler getOnloadstart() {
        return EventHandler.wrap(JS.get(peer, "onloadstart"));
    }

    public void setOnloadstart(EventHandler value) {
        JS.set(peer, "onloadstart", value.peer);
    }

    public EventHandler getOnprogress() {
        return EventHandler.wrap(JS.get(peer, "onprogress"));
    }

    public void setOnprogress(EventHandler value) {
        JS.set(peer, "onprogress", value.peer);
    }

    public EventHandler getOnabort() {
        return EventHandler.wrap(JS.get(peer, "onabort"));
    }

    public void setOnabort(EventHandler value) {
        JS.set(peer, "onabort", value.peer);
    }

    public EventHandler getOnerror() {
        return EventHandler.wrap(JS.get(peer, "onerror"));
    }

    public void setOnerror(EventHandler value) {
        JS.set(peer, "onerror", value.peer);
    }

    public EventHandler getOnload() {
        return EventHandler.wrap(JS.get(peer, "onload"));
    }

    public void setOnload(EventHandler value) {
        JS.set(peer, "onload", value.peer);
    }

    public EventHandler getOntimeout() {
        return EventHandler.wrap(JS.get(peer, "ontimeout"));
    }

    public void setOntimeout(EventHandler value) {
        JS.set(peer, "ontimeout", value.peer);
    }

    public EventHandler getOnloadend() {
        return EventHandler.wrap(JS.get(peer, "onloadend"));
    }

    public void setOnloadend(EventHandler value) {
        JS.set(peer, "onloadend", value.peer);
    }
}
