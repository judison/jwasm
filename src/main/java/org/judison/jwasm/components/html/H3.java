package org.judison.jwasm.components.html;

import org.judison.jwasm.components.Classes;

import browser.dom.HTMLElement;
import browser.javascript.Browser;

public final class H3 extends AbstractBasic<H3> implements Classes.Owner<H3> {

	@Override
	protected HTMLElement create() {
		return Browser.document.createElement("h3");
	}

}
