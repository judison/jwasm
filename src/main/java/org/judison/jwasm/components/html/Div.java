package org.judison.jwasm.components.html;

import org.judison.jwasm.components.Classes;

import browser.dom.HTMLElement;
import browser.javascript.Browser;

public final class Div extends AbstractBasic<Div> implements Classes.Owner<Div> {

	@Override
	protected HTMLElement create() {
		return Browser.document.createElement("div");
	}

}
