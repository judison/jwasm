package org.judison.jwasm.components.html;

import org.judison.jwasm.components.Classes;

import browser.dom.HTMLElement;
import browser.javascript.Browser;

public final class H2 extends AbstractBasic<H2> implements Classes.Owner<H2> {

	@Override
	protected HTMLElement create() {
		return Browser.document.createElement("h2");
	}

}
