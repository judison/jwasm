package org.judison.jwasm.components.html;

import org.judison.jwasm.components.Classes;

import browser.dom.HTMLElement;
import browser.javascript.Browser;

public final class Br extends AbstractBasic<Br> implements Classes.Owner<Br> {

	@Override
	protected HTMLElement create() {
		return Browser.document.createElement("br");
	}

}
