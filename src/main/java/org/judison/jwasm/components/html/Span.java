package org.judison.jwasm.components.html;

import org.judison.jwasm.components.Classes;

import browser.dom.HTMLElement;
import browser.javascript.Browser;

public final class Span extends AbstractBasic<Span> implements Classes.Owner<Span> {

	@Override
	protected HTMLElement create() {
		return Browser.document.createElement("span");
	}

}
