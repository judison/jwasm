package org.judison.jwasm.components.html;

import org.judison.jwasm.components.Classes;

import browser.dom.HTMLElement;
import browser.javascript.Browser;

public final class H6 extends AbstractBasic<H6> implements Classes.Owner<H6> {

	@Override
	protected HTMLElement create() {
		return Browser.document.createElement("h6");
	}

}
