package org.judison.jwasm.components.html;

import org.judison.jwasm.components.Classes;

import browser.dom.HTMLElement;
import browser.javascript.Browser;

public final class P extends AbstractBasic<P> implements Classes.Owner<P> {

	@Override
	protected HTMLElement create() {
		return Browser.document.createElement("p");
	}

}
