package org.judison.jwasm.components.html;

import org.judison.jwasm.components.Classes;

import browser.dom.HTMLElement;
import browser.javascript.Browser;

public final class H5 extends AbstractBasic<H5> implements Classes.Owner<H5> {

	@Override
	protected HTMLElement create() {
		return Browser.document.createElement("h5");
	}

}
