package org.judison.jwasm.components.html;

import org.judison.jwasm.components.Classes;

import browser.dom.HTMLElement;
import browser.javascript.Browser;

public final class H4 extends AbstractBasic<H4> implements Classes.Owner<H4> {

	@Override
	protected HTMLElement create() {
		return Browser.document.createElement("h4");
	}

}
