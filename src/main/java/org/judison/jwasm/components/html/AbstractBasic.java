package org.judison.jwasm.components.html;

import org.judison.jwasm.components.Classes;
import org.judison.jwasm.components.Parent;

import browser.dom.HTMLElement;

public abstract class AbstractBasic<This> extends Parent<This> implements Classes.Owner<This> {

	private Classes classes;

	@Override
	protected void doUpdate() {
		HTMLElement element = getRoot();
		element.setClassName(classes == null ? "" : classes.toString());
	}

	public Classes getClasses() {
		if (classes == null)
			classes = new Classes(this);
		return classes;
	}


}
