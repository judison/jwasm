package org.judison.jwasm.components.html;

public class Html {

	public static Br br() {
		return new Br();
	}

	public static Div div() {
		return new Div();
	}

	public static H1 h1() {
		return new H1();
	}

	public static H2 h2() {
		return new H2();
	}

	public static H3 h3() {
		return new H3();
	}

	public static H4 h4() {
		return new H4();
	}

	public static H5 h5() {
		return new H5();
	}

	public static H6 h6() {
		return new H6();
	}

	public static Hr hr() {
		return new Hr();
	}

	public static P p() {
		return new P();
	}

	public static Span span() {
		return new Span();
	}

}
