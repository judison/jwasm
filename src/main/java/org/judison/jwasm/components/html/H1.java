package org.judison.jwasm.components.html;

import org.judison.jwasm.components.Classes;

import browser.dom.HTMLElement;
import browser.javascript.Browser;

public final class H1 extends AbstractBasic<H1> implements Classes.Owner<H1> {

	@Override
	protected HTMLElement create() {
		return Browser.document.createElement("h1");
	}

}
