package org.judison.jwasm.components;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

@SuppressWarnings({"UnusedReturnValue", "unused", "unchecked"})
public class Classes {

	private final Component<?> owner;
	private final List<String> list = new ArrayList<>();

	public Classes(Component<?> owner) {
		this.owner = owner;
	}

	public boolean has(String cls) {
		return list.contains(cls);
	}

	public boolean hasAny(String... classes) {
		for (String cls: classes)
			if (list.contains(cls))
				return true;
		return false;
	}

	public boolean hasAll(String... classes) {
		for (String cls: classes)
			if (!list.contains(cls))
				return false;
		return true;
	}

	public Classes addAll(String... classes) {
		boolean mod = false;
		for (String cls: classes) {
			if (list.contains(cls))
				continue;
			list.add(cls);
			mod = true;
		}
		if (mod)
			owner.touch();
		return this;
	}

	public Classes add(String cls) {
		if (list.contains(cls))
			return this;
		list.add(cls);
		owner.touch();
		return this;
	}

	public Classes remove(String cls) {
		if (list.remove(cls))
			owner.touch();
		return this;
	}

	public Classes removeAll(String... clsNames) {
		boolean mod = false;
		for (String cls: clsNames)
			if (list.remove(cls))
				mod = true;
		if (mod)
			owner.touch();
		return this;
	}

	public Classes toggle(String cls) {
		if (!list.remove(cls))
			list.add(cls);
		owner.touch();
		return this;
	}

	public boolean isEmpty() {
		return list.isEmpty();
	}

	@SuppressWarnings("StringConcatenationInLoop")
	@Override
	public String toString() {
		String str = "";
		for (int i = 0, size = list.size(); i < size; i++)
			if (i == 0)
				str = list.get(i);
			else
				str = str + " " + list.get(i);
		return str;
	}

	public interface Owner<This> {

		Classes getClasses();

		default This withClasses(Consumer<Classes> consumer) {
			consumer.accept(getClasses());
			return (This)this;
		}

		default boolean hasClass(String cls) {
			return getClasses().has(cls);
		}

		default boolean hasAllClasses(String... cls) {
			return getClasses().hasAll(cls);
		}

		default boolean hasAnyClasses(String... cls) {
			return getClasses().hasAny(cls);
		}

		default This addClass(String cls) {
			getClasses().add(cls);
			return (This)this;
		}

		default This addClasses(String... cls) {
			getClasses().addAll(cls);
			return (This)this;
		}

		default This removeClass(String cls) {
			getClasses().remove(cls);
			return (This)this;
		}

		default This removeClasses(String cls) {
			getClasses().removeAll(cls);
			return (This)this;
		}

		default This toggleClass(String cls) {
			getClasses().toggle(cls);
			return (This)this;
		}

	}

}
