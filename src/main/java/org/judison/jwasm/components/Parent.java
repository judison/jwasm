package org.judison.jwasm.components;

import browser.dom.HTMLElement;
import browser.javascript.Browser;

@SuppressWarnings("unchecked")
public abstract class Parent<This> extends Component<This> {

	public This append(Component<?> child) {
		getElement().appendChild(child.getElement());
		return (This)this;
	}

	public This append(String text) {
		getElement().appendChild(Browser.document.createTextNode(text));
		return (This)this;
	}

	public This append(HTMLElement element) {
		getElement().appendChild(element);
		return (This)this;
	}

}
