package org.judison.jwasm.components;

import browser.dom.HTMLElement;

@SuppressWarnings("unchecked")
public abstract class Component<This> {

	private HTMLElement element;
	private boolean dirty;

	public void touch() {
		dirty = true;
		Components.notifyDirty(this);
	}

	protected <T> T set(T oldValue, T newValue) {
		if (oldValue != newValue)
			touch();
		return newValue;
	}

	private void checkElement() {
		if (element == null) {
			element = create();
			touch();
		}
	}

	public final HTMLElement getElement() {
		checkElement();
		return element;
	}

	protected abstract HTMLElement create();

	protected abstract void doUpdate();

	protected final <T extends HTMLElement> T getRoot() {
		checkElement();
		return (T)element;
	}

	This update() {
		checkElement();
		if (dirty) {
			doUpdate();
			dirty = false;
		}
		return (This)this;
	}

}
