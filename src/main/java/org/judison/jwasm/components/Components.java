package org.judison.jwasm.components;

import java.util.ArrayList;

public class Components {

	private static ArrayList<Component<?>> dirtyComponents = new ArrayList<>();

	static void notifyDirty(Component<?> component) {
		if (dirtyComponents.contains(component))
			return;
		dirtyComponents.add(component);
	}

	public static void update() {
		while (!dirtyComponents.isEmpty()) {
			Component<?> component = dirtyComponents.remove(dirtyComponents.size() - 1);
			component.update();
		}
	}

}
