package org.judison.jwasm.components.bootstrap;

import org.judison.jwasm.components.Parent;

import browser.dom.HTMLDivElement;
import browser.dom.HTMLElement;
import browser.javascript.Browser;

public final class ButtonGroup extends Parent<ButtonGroup> {

	private Bootstrap.Size size = Bootstrap.Size.NORMAL;
	private boolean vertical;

	@Override
	protected HTMLElement create() {
		HTMLDivElement div = Browser.document.createElement("div");
		div.setAttribute("role", "group");
		return div;
	}

	@Override
	protected void doUpdate() {
		HTMLDivElement div = getRoot();

		// Base + Variant
		String className;
		if (vertical)
			className = "btn-group-vertical";
		else
			className = "btn-group";

		// Size
		if (size != Bootstrap.Size.NORMAL)
			className += " btn-group" + size.suffix;

		div.setClassName(className);
	}

	public Bootstrap.Size getSize() {
		return size;
	}

	public ButtonGroup setSize(Bootstrap.Size size) {
		this.size = set(this.size, size);
		return this;
	}

	public boolean isVertical() {
		return vertical;
	}

	public ButtonGroup setVertical(boolean vertical) {
		this.vertical = set(this.vertical, vertical);
		return this;
	}

}
