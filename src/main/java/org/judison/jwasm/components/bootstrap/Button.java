package org.judison.jwasm.components.bootstrap;

import org.judison.jwasm.components.Parent;

import browser.dom.HTMLButtonElement;
import browser.dom.HTMLElement;
import browser.javascript.Browser;

public final class Button extends Parent<Button> {

	private Bootstrap.ThemeColor themeColor = Bootstrap.ThemeColor.PRIMARY;
	private Bootstrap.Size size = Bootstrap.Size.NORMAL;
	private boolean outline;
	private boolean disabled;

	@Override
	protected HTMLElement create() {
		HTMLButtonElement btn = Browser.document.createElement("button");
		btn.setType("button");
		return btn;
	}

	@Override
	protected void doUpdate() {
		HTMLButtonElement btn = getRoot();

		// Base
		String className = "btn";

		// Theme Color + Outline
		if (outline)
			className += " btn-outline" + themeColor.suffix;
		else
			className += " btn" + themeColor.suffix;

		// Size
		if (size != Bootstrap.Size.NORMAL)
			className += " btn" + size.suffix;

		btn.setClassName(className);

		btn.setDisabled(disabled);
	}

	public Bootstrap.ThemeColor getThemeColor() {
		return themeColor;
	}

	public Button setThemeColor(Bootstrap.ThemeColor themeColor) {
		this.themeColor = set(this.themeColor, themeColor);
		return this;
	}

	public Bootstrap.Size getSize() {
		return size;
	}

	public Button setSize(Bootstrap.Size size) {
		this.size = set(this.size, size);
		return this;
	}

	public boolean isOutline() {
		return outline;
	}

	public Button setOutline(boolean outline) {
		this.outline = set(this.outline, outline);
		return this;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public Button setDisabled(boolean disabled) {
		this.disabled = set(this.disabled, disabled);
		return this;
	}

}
