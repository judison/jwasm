package org.judison.jwasm.components.bootstrap;

import org.judison.jwasm.components.Classes;
import org.judison.jwasm.components.Parent;

import browser.dom.HTMLDivElement;
import browser.dom.HTMLElement;
import browser.javascript.Browser;

public final class Container extends Parent<Container> implements Classes.Owner<Container> {

	private Classes classes;
	private Type type;

	public Container() {
		this(Type.NORMAL);
	}

	public Container(Type type) {
		this.type = type;
	}

	@Override
	protected HTMLElement create() {
		return Browser.document.createElement("div");
	}

	@Override
	protected void doUpdate() {
		HTMLDivElement div = getRoot();
		div.setClassName(classes == null || classes.isEmpty() ? type.cls : type.cls + " " + classes);
	}

	@Override
	public Classes getClasses() {
		if (classes == null)
			classes = new Classes(this);
		return classes;
	}

	public Type getType() {
		return type;
	}

	public Container setType(Type type) {
		this.type = set(this.type, type);
		return this;
	}

	public enum Type {
		NORMAL("container"),
		SMALL("container-sm"),
		MEDIUM("container-md"),
		LARGE("container-lg"),
		X_LARGE("container-xl"),
		XX_LARGE("container-xxl"),
		FLUID("container-fluid");

		private final String cls;

		Type(String cls) {
			this.cls = cls;
		}
	}

}
