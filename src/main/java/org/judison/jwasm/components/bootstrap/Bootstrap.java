package org.judison.jwasm.components.bootstrap;

public class Bootstrap {


	public enum ThemeColor {
		PRIMARY("-primary"),
		SECONDARY("-secondary"),
		SUCCESS("-success"),
		DANGER("-danger"),
		WARNING("-warning"),
		INFO("-info"),
		LIGHT("-light"),
		DARK("-dark");

		public final String suffix;

		ThemeColor(String suffix) {
			this.suffix = suffix;
		}
	}

	public enum Size {
		SMALL("-sm"),
		NORMAL(""),
		LARGE("-lg");

		public final String suffix;

		Size(String suffix) {
			this.suffix = suffix;
		}
	}

	public static Button button() {
		return new Button();
	}

	public static ButtonGroup buttonGroup() {
		return new ButtonGroup();
	}

	public static Container container() {
		return new Container();
	}

}
