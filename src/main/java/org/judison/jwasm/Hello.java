package org.judison.jwasm;

import org.judison.jwasm.components.Components;
import org.judison.jwasm.components.bootstrap.Bootstrap;
import org.judison.jwasm.components.bootstrap.Button;
import org.judison.jwasm.components.bootstrap.ButtonGroup;
import org.judison.jwasm.components.bootstrap.Container;
import org.judison.jwasm.components.html.Html;

import browser.dom.Document;
import browser.javascript.Browser;
import de.inetsoftware.jwebassembly.api.annotation.Export;

public class Hello {

	@Export
	public static void main() {
		Document document = Browser.document;

		Container container = Bootstrap.container().addClasses("text-center", "px-4", "border");

		document.body().appendChild(container.getElement());

		ButtonGroup grp = Bootstrap.buttonGroup();
		for (Bootstrap.ThemeColor themeColor: Bootstrap.ThemeColor.values())
			grp.append(
				new Button()
					.setThemeColor(themeColor)
					.append(themeColor.name())
			);
		container.append(grp).append(Html.br()).append(Html.br());

		grp = Bootstrap.buttonGroup();
		for (Bootstrap.ThemeColor themeColor: Bootstrap.ThemeColor.values())
			grp.append(
				new Button()
					.setOutline(true)
					.setThemeColor(themeColor)
					.append(themeColor.name())
			);
		container.append(grp);

		Components.update();
	}

}
