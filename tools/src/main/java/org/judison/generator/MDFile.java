package org.judison.generator;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

public class MDFile {

	public final File file;
	public String id;
	public final String[] lines;
	public final Map<String, String> props = new HashMap<>();
	public final List<String> main = new ArrayList<>();
	public final List<String> tags = new ArrayList<>();
	public String type = "String";
	public boolean readOnly = false;

	public MDFile(File file) throws IOException {
		this.file = file;
		this.lines = Files.lines(file.toPath()).toArray(String[]::new);

		String block = "init";

		for (String line: lines) {
			if (line.equals("---")) {
				if (block.equals("init"))
					block = "props";
				else if (block.equals("props"))
					block = "main";

			} else if (line.startsWith("##")) {
				if (line.equals("## Value")) {
					block = "value";
					main.add(line);
				} else if (line.equals("### Parameters") || line.equals("### Return value")) {
					block = "main";
					main.add(line);
				} else
					block = "ignore";
			} else if (line.trim().isEmpty()) {
				// ignore
			} else {
				if (block.equals("props")) {
					if (line.trim().startsWith("-"))
						tags.add(line.trim().substring(2));
					else {
						String[] p = line.split(": ", 2);
						if (p.length == 2 && !p[1].trim().isEmpty()) {
							props.put(p[0].trim(), p[1].trim());
							id = props.get("title");
						}
					}
				} else if (block.equals("main") || block.equals("value")) {
					if (block.equals("value")) {
						if (line.contains("Boolean") || line.contains("boolean"))
							type = "boolean";
						if (line.contains("Integer") || line.contains("integer"))
							type = "int";
						if (line.contains("Number") || line.contains("number") || line.contains("Double") || line.contains("double") || line.contains("Float") || line.contains("float"))
							type = "double";
						for (String objectType: objectTypes)
							if (type.equals("String") && line.contains(objectType)) {
								type = objectType;
								break;
							}
					}
					if (line.contains("read-only"))
						readOnly = true;

					if (line.contains("{{InheritanceDiagram}}") ||
						line.matches("\\{\\{\\s*APIRef(.*)\\s*}}") ||
						line.matches("\\{\\{\\s*ApiRef(.*)\\s*}}") ||
						line.matches("\\{\\{\\s*DefaultAPISidebar(.*)}}")) {
						if (main.size() > 0 && main.get(main.size() - 1).isEmpty())
							main.remove(main.size() - 1);
						continue;
					}

					if (line.contains("{{Non-standard_header()}}"))
						tags.add("Non-standard");

					line = line.replaceAll("\\[.*?]\\(/en-US/docs/Web/API/(.*?)\\)", "{@link $1}");
					line = line.replaceAll("\\[.*?]\\(/en-US/docs/Web/HTML/Element/(.*?)\\)", "$1");
					line = line.replaceAll("\\[(.*?)]\\(/en-US/docs/.*?\\)", "$1");
					line = line.replaceAll("\\{\\{domxref\\(\"(.*?)\",.*?\\)}}", "{@link $1}");
					line = line.replaceAll("\\{\\{htmlattrxref\\(\"(.*?)\".*?\\)}}", "`$1`");
					line = line.replaceAll("\\{\\{domxref\\(['\"](.*?)\\.(.*?)['\"]\\)}}", "{@link $1#$2}");
					line = line.replaceAll("\\{\\{domxref\\(['\"](.*?)['\"]\\)}}", "{@link $1}");
					line = line.replace("{{InheritanceDiagram}}", "");
					line = line.replaceAll("\\{\\{.*?\\(['\"](.*?)['\"]\\)}}", "`$1`");
					line = line.replaceAll("\\{\\{(.*?)}}", "$1");
					main.add(line);
				}
			}
		}

		if (props.get("title").contains(".aria"))
			type = "String";

		while (main.size() > 0 && main.get(0).trim().isEmpty())
			main.remove(0);

		applyOverrides();
	}

	private static final List<String> objectTypes;

	static {
		objectTypes = new ArrayList<>();
		for (File file: Objects.requireNonNull(Generator.javaDir.listFiles()))
			if (file.getName().endsWith(".java"))
				objectTypes.add(file.getName().replace(".java", ""));

	}

	public String getterName(String name) {
		if ("boolean".equals(type))
			if (name.startsWith("is"))
				return name;
			else
				return "is" + name.toUpperCase().charAt(0) + name.substring(1);
		return "get" + name.toUpperCase().charAt(0) + name.substring(1);
	}

	public String setterName(String name) {
		return "set" + name.toUpperCase().charAt(0) + name.substring(1);
	}

	public boolean wrapType() {
		return Character.isUpperCase(type.charAt(0));
	}

	private static final Properties overrides;

	static {
		overrides = new Properties();
		try {
			overrides.load(MDFile.class.getResourceAsStream("/overrides.properties"));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private void applyOverrides() {
		if (overrides.containsKey(id + "#type"))
			type = overrides.getProperty(id + "#type");
		if (overrides.containsKey(id + "#readOnly"))
			readOnly = Boolean.parseBoolean(overrides.getProperty(id + "#readOnly"));
	}

}
