package org.judison.generator;

import java.util.ArrayList;

import org.judison.generator.model.Arg;
import org.judison.generator.model.Attribute;
import org.judison.generator.model.Callback;
import org.judison.generator.model.Intf;
import org.judison.generator.model.Operation;
import org.judison.generator.model.Spec;
import org.judison.generator.model.Type;

import webidl.WebIDLBaseListener;
import webidl.WebIDLLexer;
import webidl.WebIDLParser;

public class IDLLoader extends WebIDLBaseListener {

	public final Spec spec;
	public final String pack;

	private Intf intf;

	public IDLLoader(Spec spec, String pack) {
		this.spec = spec;
		this.pack = pack;
	}

	@Override
	public void enterInterfaceDef(WebIDLParser.InterfaceDefContext ctx) {
		String name = ctx.IDENTIFIER_WEBIDL().getText();

		intf = spec.getIntfOrCreate(name, Intf.Kind.INTERFACE);
		if (ctx.getToken(WebIDLLexer.PARTIAL, 0) == null)
			intf.pack = pack;

		WebIDLParser.InheritanceContext inheritance = ctx.inheritance();
		if (inheritance != null)
			intf.parent = inheritance.IDENTIFIER_WEBIDL().getText();
	}

	@Override
	public void enterCallbackInterfaceDef(WebIDLParser.CallbackInterfaceDefContext ctx) {
		String name = ctx.IDENTIFIER_WEBIDL().getText();

		intf = spec.getIntfOrCreate(name, Intf.Kind.CALLBACK_INTERFACE);
		intf.pack = pack;
	}

	@Override
	public void enterDictionaryDef(WebIDLParser.DictionaryDefContext ctx) {
		spec.dictionaries.add(ctx.IDENTIFIER_WEBIDL().getText());
	}

	@Override
	public void enterMixinDef(WebIDLParser.MixinDefContext ctx) {
		String name = ctx.IDENTIFIER_WEBIDL().getText();

		intf = spec.getIntfOrCreate(name, Intf.Kind.MIXIN);
		if (ctx.getToken(WebIDLLexer.PARTIAL, 0) == null)
			intf.pack = pack;
	}

	@Override
	public void enterCallbackDef(WebIDLParser.CallbackDefContext ctx) {
		Callback callback = new Callback(pack, ctx.IDENTIFIER_WEBIDL().getText(), type(ctx.type_()));
		if (ctx.arguments() != null)
			for (WebIDLParser.ArgumentContext argCtx: ctx.arguments().argument()) {
				String name = argCtx.argumentName().getText();
				Type type;
				if (argCtx.typeWithExtendedAttributes() != null)
					type = type(argCtx.typeWithExtendedAttributes().type_());
				else
					type = type(argCtx.type_());
				Arg arg = new Arg(type, name);
				arg.optional = argCtx.getToken(WebIDLLexer.OPTIONAL, 0) != null;
				callback.args.add(arg);
			}
		spec.callbacks.put(callback.name, callback);
	}

	public boolean hasExtendedAttribute(WebIDLParser.ExtendedAttributesContext ctx, String text) {
		if (ctx != null)
			for (WebIDLParser.ExtendedAttributeContext att: ctx.extendedAttribute())
				if (text.equals(att.getText()))
					return true;
		return false;
	}

	@Override
	public void enterAttributeDecl(WebIDLParser.AttributeDeclContext ctx) {
		if (hasExtendedAttribute(ctx.extendedAttributes(), "Replaceable"))
			return;

		Attribute attr = new Attribute();
		attr.name = ctx.attributeName().getText();
		attr.type = type(ctx.typeWithExtendedAttributes().type_());
		attr.readonly = ctx.getToken(WebIDLParser.READONLY, 0) != null;

		intf.attrs.add(attr);
//		System.out.println("  " + attr.type + " " + attr.name + " " + attr.readonly);
	}

	private Type type(WebIDLParser.Type_Context ctx) {
		if (ctx.getToken(WebIDLLexer.ANY, 0) != null)
			return Type.any().setStart(ctx.start);

		// Promise type
		{
			WebIDLParser.PromiseTypeContext promiseType = ctx.promiseType();
			if (promiseType != null) {
				Type paramType = type(promiseType.type_());
				if (paramType == null) {
					System.out.println("linha: " + ctx.start.getLine() + " " + ctx.getText());
					paramType = new Type("Void");
				}
				Type type = new Type("JSPromise", paramType);
				type.param = paramType;
				return type.setStart(ctx.start);
			}
		}
		// Union type
		{
			WebIDLParser.UnionTypeContext unionType = ctx.unionType();
			if (unionType != null) {
				Type type = new Type("Union");
				type.union = new ArrayList<>();
				for (WebIDLParser.UnionMemberTypeContext unionMemberType: unionType.unionMemberType()) {
					type.union.add(type(unionMemberType.distinguishableType()));
				}
				return type.setStart(ctx.start);
			}
		}
		// Distinguishable type
		WebIDLParser.DistinguishableTypeContext distinguishableType = ctx.distinguishableType();
		if (distinguishableType != null) {
			return type(distinguishableType);
		}
		System.out.println("Loader Unknown type line: " + ctx.start.getLine());
		return new Type("Unknown").setStart(ctx.start);
	}

	private Type type(WebIDLParser.DistinguishableTypeContext distinguishableType) {
		boolean nullable = distinguishableType.orNull() != null;
		// Primitive Type
		{
			WebIDLParser.PrimitiveTypeContext primitiveType = distinguishableType.primitiveType();
			if (primitiveType != null)
				if (primitiveType.getToken(WebIDLLexer.UNDEFINED, 0) != null)
					return null;
				else
					switch (primitiveType.getText()) {
						case "unsignedshort":
							return new Type("short", nullable).setStart(distinguishableType.start);
						case "unsignedlong":
							return new Type("int", nullable).setStart(distinguishableType.start);
						case "unsignedlonglong":
						case "longlong":
							return new Type("long", nullable).setStart(distinguishableType.start);
						case "unrestrictedfloat":
							return new Type("float", nullable).setStart(distinguishableType.start);
						case "unrestricteddouble":
							return new Type("double", nullable).setStart(distinguishableType.start);
						default:
							return new Type(primitiveType.getText()).setStart(distinguishableType.start);
					}
		}
		// String Type
		{
			WebIDLParser.StringTypeContext stringType = distinguishableType.stringType();
			if (stringType != null)
				return new Type("String", nullable).setStart(distinguishableType.start);
		}
		// object Type
		{
			if (distinguishableType.getToken(WebIDLLexer.OBJECT, 0) != null)
				return Type.object(nullable).setStart(distinguishableType.start);
		}
		// sequence Type
		{
			WebIDLParser.SequenceTypeContext sequenceType = distinguishableType.sequenceType();
			if (sequenceType != null) {
				Type param = type(sequenceType.typeWithExtendedAttributes().type_());
				return new Type("JSSequence", param).setStart(distinguishableType.start);
			}
		}
		// Named
		{
			if (distinguishableType.IDENTIFIER_WEBIDL() != null)
				return new Type(distinguishableType.IDENTIFIER_WEBIDL().getText(), nullable).setStart(distinguishableType.start);
		}
		return new Type("Unknown").setStart(distinguishableType.start);
	}


	@Override
	public void enterOperationDecl(WebIDLParser.OperationDeclContext ctx) {
		if (intf == null)
			return;
		if (ctx.getToken(WebIDLLexer.STATIC, 0) != null) {
			intf.msgs.add("Ignored static operation " + ctx.operationName().getText());
			return;
		}

		if (ctx.operationName() == null) {
			intf.msgs.add("Operation without name at line" + ctx.start.getLine());
			System.out.println("Operation without name at line" + ctx.start.getLine());
			return;
		}

		Operation operation = new Operation(ctx.operationName().getText(), type(ctx.type_()));

		if (ctx.arguments() != null)
			for (WebIDLParser.ArgumentContext argCtx: ctx.arguments().argument()) {
				String name = argCtx.argumentName().getText();
				Type type;
				if (argCtx.typeWithExtendedAttributes() != null)
					type = type(argCtx.typeWithExtendedAttributes().type_());
				else
					type = type(argCtx.type_());
				Arg arg = new Arg(type, name);
				arg.optional = argCtx.getToken(WebIDLLexer.OPTIONAL, 0) != null;
				operation.args.add(arg);
			}


		intf.operations.add(operation);
	}

	@Override
	public void exitInterfaceDef(WebIDLParser.InterfaceDefContext ctx) {
		intf = null;
	}

	@Override
	public void exitCallbackInterfaceDef(WebIDLParser.CallbackInterfaceDefContext ctx) {
		intf = null;
	}

	@Override
	public void exitMixinDef(WebIDLParser.MixinDefContext ctx) {
		intf = null;
	}

}
