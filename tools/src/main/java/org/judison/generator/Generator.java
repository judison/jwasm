package org.judison.generator;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import com.vladsch.flexmark.html.HtmlRenderer;
import com.vladsch.flexmark.parser.Parser;
import com.vladsch.flexmark.util.ast.Node;
import com.vladsch.flexmark.util.data.MutableDataSet;

public class Generator {

	private static PrintWriter out;

	public static final File javaDir = new File("src/main/java/browser/dom");

	public static void main(String[] args) throws IOException {
		String parent = "Peered";
		String clsName = "DOMTokenList";
		File dest = new File(javaDir, clsName + ".java");

		File root = new File("tools/content/files/en-us/web/api");
		File dir = new File(root, clsName.toLowerCase());

		MDFile cls = new MDFile(new File(dir, "index.md"));

		out = new PrintWriter(new FileWriter(dest));

		out.println("package browser.dom;");
		out.println();
		out.println("import browser.javascript.JS;");
		out.println();
		out.println("/**");
		printJDoc(" * ", cls.main);
		out.println(" */");
		out.println("@SuppressWarnings(\"unused\")");
		out.println("public class " + clsName + " extends " + parent + " {");
		out.println();
		out.println("\tprotected " + clsName + "(Object peer) {");
		out.println("\t\tsuper(peer);");
		out.println("\t}");

		List<MDFile> children = new ArrayList<>();
		for (File file: Objects.requireNonNull(dir.listFiles()))
			if (file.isDirectory()) {
				MDFile md = new MDFile(new File(file, "index.md"));
				children.add(md);
			}
		children.sort(Comparator.comparing(md -> md.props.get("title")));

		out.println();
		out.println("\t//=========================");
		out.println("\t// Properties");
		out.println("\t//=========================");

		for (MDFile md: children)
			if ("web-api-instance-property".equals(md.props.get("page-type"))) {
				String name = md.props.get("title").split("\\.")[1];
				String type = md.type;

				out.println();
				if (md.tags.contains("Deprecated")) {
					out.println("\t// " + name + " DEPRECATED");
					continue;
				} else if (md.tags.contains("Non-standard")) {
					out.println("\t// " + name + " NON-STANDARD");
					continue;
				}
				out.println("\t/**");
				printJDoc("\t * ", md.main);
				out.println("\t */");
				out.println("\tpublic " + type + " " + md.getterName(name) + "() {");
				if ("int".equals(md.type))
					out.println("\t\treturn JS.getInt(peer, \"" + name + "\");");
				else if ("double".equals(md.type))
					out.println("\t\treturn JS.getDouble(peer, \"" + name + "\");");
				else if ("boolean".equals(md.type))
					out.println("\t\treturn JS.getBoolean(peer, \"" + name + "\");");
				else
					out.println("\t\treturn Wrap." + md.type + "(JS.get(peer, \"" + name + "\"));");
				out.println("\t}");

				if (!md.readOnly) {
					out.println();
					out.println("\t/**");
					printJDoc("\t * ", md.main);
					out.println("\t */");
					out.println("\tpublic void " + md.setterName(name) + "(" + type + " value) {");
					if ("int".equals(md.type) || "double".equals(md.type) || "boolean".equals(md.type))
						out.println("\t\tJS.set(peer, \"" + name + "\", value);");
					else
						out.println("\t\tJS.set(peer, \"" + name + "\", JS.param(value));");
					out.println("\t}");
				}

			}

		out.println();
		out.println("\t//=========================");
		out.println("\t// Methods");
		out.println("\t//=========================");

		for (MDFile md: children)
			if ("web-api-instance-method".equals(md.props.get("page-type"))) {
				String name = md.props.get("title").split("\\.")[1].replace("()", "");

				if (md.tags.contains("Deprecated")) {
					out.println("\n    // " + name + " DEPRECATED");
					continue;
				} else if (md.tags.contains("Non-standard")) {
					out.println("\n    // " + name + " NON-STANDARD");
					continue;
				}

				if (name.equals("toString"))
					continue;

				out.println();
				out.println("\t/**");
				printJDoc("\t * ", md.main);
				out.println("\t */");
				out.println("\t//TODO check it");
				out.println("\tpublic void " + name + "() {");
				out.println("\t\tJS.invoke(peer, \"" + name + "\");");
				out.println("\t}");
			}


		out.println();
		out.println("}");
		out.close();
	}

	/**
	 * directionality of its parent element.
	 * <pre>
	 * > **Note:** Browsers might allow users to change the directionality of
	 * > and s in order to assist with authoring content. Chrome
	 * > and Safari provide a directionality option in the contextual menu of input fields
	 * > while Internet Explorer and Edge use the key combinations <kbd>Ctrl</kbd> + <kbd>Left Shift</kbd> and <kbd>Ctrl</kbd> + <kbd>Right Shift</kbd>. Firefox uses <kbd>Ctrl</kbd>/<kbd>Cmd</kbd> + <kbd>Shift</kbd> + <kbd>X</kbd> but does NOT update
	 * > the dir attribute value.
	 * </pre>
	 */
	public static void printJDoc(String prefix, List<String> lines) {
		String md = String.join("\n", lines);

		MutableDataSet options = new MutableDataSet();
		Parser parser = Parser.builder(options).build();
		HtmlRenderer renderer = HtmlRenderer.builder(options).build();
		Node document = parser.parse(md);

		String html = renderer.render(document);
		for (String l: html.split("\n"))
			out.println(prefix + l);


	}

}
