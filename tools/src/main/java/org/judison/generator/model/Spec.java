package org.judison.generator.model;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import webidl.WebIDLParser;

public class Spec {

	public final Map<String, Intf> interfaces = new LinkedHashMap<>();
	public final List<String> enums = new ArrayList<>();
	public final List<String> dictionaries = new ArrayList<>();
	public final Map<String, Callback> callbacks = new LinkedHashMap<>();
	public final List<WebIDLParser.WebIDLContext> rootCtxs = new ArrayList<>();

	public Intf getIntfOrCreate(String name, Intf.Kind kind) {
		Intf intf = interfaces.get(name);
		if (intf == null) {
			intf = new Intf(kind, name);
			interfaces.put(name, intf);
		}
		if (intf.kind != kind)
			System.out.println("Intf type mismatch " + name + " " + intf.kind + " != " + kind);
		return intf;
	}

}
