package org.judison.generator.model;

import java.util.Arrays;
import java.util.List;

public class Arg {

	public Type type;
	public String name;
	public boolean optional;

	public Arg(Type type, String name) {
		this.type = type;
		this.name = normalizeName(name);
	}

	public Arg(Arg other) {
		this.type = other.type;
		this.name = other.name;
		this.optional = other.optional;
	}

	private static final List<String> reservedNames = Arrays.asList("interface", "default");

	public static String normalizeName(String name) {
		if (reservedNames.contains(name))
			return name + "_";
		return name;
	}

}
