package org.judison.generator.model;

import java.util.ArrayList;
import java.util.List;

public class Operation {

	public final String name;
	public final Type type;
	public final List<Arg> args = new ArrayList<>();

	public Operation(String name, Type type) {
		this.name = name;
		this.type = type;
	}

	public Operation(Operation op) {
		this.name = op.name;
		this.type = op.type;
		this.args.addAll(op.args);
	}


}
