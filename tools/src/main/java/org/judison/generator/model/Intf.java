package org.judison.generator.model;

import java.util.ArrayList;
import java.util.List;

public class Intf {

	public enum Kind {INTERFACE, CALLBACK_INTERFACE, MIXIN}

	public final Kind kind;
	public final String name;
	public String pack = "webapi.partial";
	public String parent;

	public List<Attribute> attrs = new ArrayList<>();
	public List<Operation> operations = new ArrayList<>();
	public List<Intf> mixins = new ArrayList<>();

	public List<String> msgs = new ArrayList<>();

	public Intf(Kind kind, String name) {
		this.kind = kind;
		this.name = name;
		if (name.equals("Document"))
			pack = "webapi.dom";
	}

}
