package org.judison.generator.model;

import java.util.ArrayList;
import java.util.List;

public class Callback {

	public final String pack;
	public final String name;
	public final Type type;
	public final List<Arg> args = new ArrayList<>();

	public Callback(String pack, String name, Type type) {
		this.pack = pack;
		this.name = name;
		this.type = type;
	}

	public Callback(Callback cb) {
		this.pack = cb.pack;
		this.name = cb.name;
		this.type = cb.type;
		this.args.addAll(cb.args);
	}


}
