package org.judison.generator.model;

import java.util.Arrays;
import java.util.List;

import org.antlr.v4.runtime.Token;

public class Type {

	public String name;
	public boolean nullable;
	public Type param;
	public boolean promise;
	public boolean sequence;
	public boolean optional;
	public boolean any;
	public boolean object;
	public List<Type> union;

	public Token start;

	public Type(String name) {
		this.name = name;
	}

	public Type(String name, Type param) {
		this.name = name + "<" + param.name + ">";
		this.param = param;
		this.promise = name.equals("JSPromise");
		this.sequence = name.equals("JSSequence");
		this.optional = name.equals("optional");
	}

	public Type(String name, boolean nullable) {
		this.name = name;
		this.nullable = nullable;
	}

	public static Type any() {
		Type any = new Type("any");
		any.any = true;
		return any;
	}

	public static Type object(boolean nullable) {
		Type object = new Type("object");
		object.object = true;
		return object;
	}

	public boolean isBoolean() {
		return name.equals("boolean");
	}

	public boolean isString() {
		return name.equals("String");
	}

	public boolean isPrimitive() {
		return primitiveTypes.contains(name);
	}

	private static final List<String> primitiveTypes = Arrays.asList("int", "long", "double", "float", "boolean");

	public Type setStart(Token start) {
		this.start = start;
		return this;
	}

}
