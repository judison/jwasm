package org.judison.generator;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import javax.lang.model.element.Modifier;

import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.judison.generator.model.Arg;
import org.judison.generator.model.Attribute;
import org.judison.generator.model.Callback;
import org.judison.generator.model.Intf;
import org.judison.generator.model.Operation;
import org.judison.generator.model.Spec;
import org.judison.generator.model.Type;

import com.squareup.javapoet.AnnotationSpec;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import webidl.WebIDLBaseListener;
import webidl.WebIDLLexer;
import webidl.WebIDLParser;

public class IDLGen {

	private static final String[] idls = {
		"geometry",
		"dom",
		"html",
		"xhr",
//		"SVG",
		"cssom",
		"wai-aria"
	};

	private static final String IDL_BASE_URL = "https://raw.githubusercontent.com/w3c/webref/main/ed/idl/";

	private static final TypeName TN_STRING = ClassName.get(String.class);
	private static final TypeName TN_JS = ClassName.get("webapi", "JS");
	private static final TypeName TN_WRAPPED_OBJECT = ClassName.get("webapi", "WrappedObject");
	private static final ClassName TN_JSPROMISE = ClassName.get("webapi", "JSPromise");
	private static final ClassName TN_JSSEQUENCE = ClassName.get("webapi", "JSSequence");
	private static final ClassName TN_JSCALLBACK = ClassName.get("webapi", "JSCallback");
	private static final ClassName TN_JSCALLBACK_BASE_CALLBACK = ClassName.get("webapi", "JSCallback", "BaseCallback");
	private static final ClassName TN_JSOBJECT = ClassName.get("webapi", "JSObject");

	private final Spec spec = new Spec();

	public static void main(String[] args) throws IOException, URISyntaxException {
		new IDLGen().run();
	}

	private void run() throws IOException, URISyntaxException {
		Path specsDir = Paths.get("specs");
		if (!Files.isDirectory(specsDir))
			Files.createDirectory(specsDir);
		for (String name: idls) {
			Path file = specsDir.resolve(name + ".idl");
			if (!Files.exists(file)) {
				String url = IDL_BASE_URL + name + ".idl";
				System.out.println("Downloading " + url + " to " + file);
				Files.copy(new URI(url).toURL().openStream(), file);
			}
		}

		for (String name: idls) {
			Path file = specsDir.resolve(name + ".idl");
			loadSpec(file, id(name));
			Path override = specsDir.resolve(name + ".override.idl");
			if (Files.exists(override))
				loadSpec(override, id(name));
		}

		// Apply includes
		for (WebIDLParser.WebIDLContext rootCtx: spec.rootCtxs) {
			new ParseTreeWalker().walk(new WebIDLBaseListener() {
				@Override
				public void enterIncludesDef(WebIDLParser.IncludesDefContext ctx) {
					String intfName = ctx.IDENTIFIER_WEBIDL(0).getText();
					String mixinName = ctx.IDENTIFIER_WEBIDL(1).getText();
					Intf intf = getIntf(intfName);
					if (intf == null)
						System.out.println("Interface " + intfName + " not found");
					Intf mixin = getIntf(mixinName);
					if (mixin == null)
						System.out.println("Mixin " + mixinName + " not found");
					if (intf != null && mixin != null)
						intf.mixins.add(mixin);
				}
			}, rootCtx);
		}


		Path dst = Paths.get("src", "main", "java");
		for (Intf intf: spec.interfaces.values()) {
			switch (intf.kind) {
				case INTERFACE:
				case CALLBACK_INTERFACE:
					genInterface(intf, dst);
					break;
			}
		}

		for (Callback callback: spec.callbacks.values()) {
			genCallback(callback, dst);
		}
	}

	private String id(String name) {
		switch (name) {
			case "geometry":
				return "dom";
			case "cssom":
				return "css";
			case "wai-aria":
				return "aria";
			default:
				return name.toLowerCase();
		}
	}

	private Intf getIntf(String name) {
		return spec.interfaces.get(name);
	}

	public static class Lexer extends WebIDLLexer {

		public Path file;

		public Lexer(Path file) throws IOException {
			super(new ANTLRFileStream(file.toString()));
			this.file = file;
		}

	}

	private void loadSpec(Path idlFile, String id) {
		try {
			System.out.println("Parsing " + id + " from " + idlFile);

			WebIDLLexer lexer = new Lexer(idlFile);
			WebIDLParser parser = new WebIDLParser(new CommonTokenStream(lexer));
			WebIDLParser.WebIDLContext rootCtx = parser.webIDL();
			spec.rootCtxs.add(rootCtx);

			ParseTreeWalker walker = new ParseTreeWalker();
			walker.walk(new IDLLoader(spec, "webapi." + id), rootCtx);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private void genInterface(Intf intf, Path dir) throws IOException {
		List<String> comments = new ArrayList<>(intf.msgs);

		TypeSpec.Builder classBuilder;
		ClassName className = ClassName.get(intf.pack, intf.name);
		switch (intf.kind) {
			case INTERFACE:
				classBuilder = TypeSpec.classBuilder(className);
				break;
			case CALLBACK_INTERFACE:
				classBuilder = TypeSpec.interfaceBuilder(className);
				break;
			default:
				return;
		}

//		System.out.println(intf.name);

		classBuilder.addAnnotation(AnnotationSpec.builder(SuppressWarnings.class).addMember("value", "{\"unused\"}").build());
		classBuilder.addModifiers(Modifier.PUBLIC);
		if (intf.kind == Intf.Kind.INTERFACE)
			if (intf.parent == null)
				classBuilder.superclass(TN_WRAPPED_OBJECT);
			else
				classBuilder.superclass(typeName(new Type(intf.parent)));
		else if (intf.parent != null)
			classBuilder.addSuperinterface(typeName(new Type(intf.parent)));

		// Constructor
		if (intf.kind == Intf.Kind.INTERFACE) {
			MethodSpec.Builder constructorBuilder = MethodSpec.constructorBuilder();
			constructorBuilder.addModifiers(Modifier.PROTECTED);
			constructorBuilder.addParameter(ParameterSpec.builder(TypeName.OBJECT, "peer").build());
			constructorBuilder.addCode("super(peer);");
			classBuilder.addMethod(constructorBuilder.build());

		}

		{
			//== Wrap
			MethodSpec.Builder wrapBuilder = MethodSpec.methodBuilder("wrap");
			wrapBuilder.addModifiers(Modifier.PUBLIC, Modifier.STATIC);
			wrapBuilder.returns(className);
			wrapBuilder.addParameter(ParameterSpec.builder(TypeName.OBJECT, "peer").build());
			if (intf.kind == Intf.Kind.INTERFACE)
				wrapBuilder.addCode("return peer == null ? null : new $T(peer);", className);
			else
				wrapBuilder.addCode("return null; //TODO not implemented");
			classBuilder.addMethod(wrapBuilder.build());
		}


		genAttributes(intf, comments, classBuilder);

		genOperations(intf, classBuilder);

		String comment = String.join("\n", comments);

		JavaFile.builder(intf.pack, classBuilder.build())
			.indent("    ")
			.skipJavaLangImports(true)
			.addFileComment("Generated file; DO NOT edit")
			.addFileComment(comment.length() > 0 ? "\n" + comment : "")
			.build()
			.writeTo(dir);
	}


	private void genCallback(Callback callback, Path dir) throws IOException {
		ClassName className = ClassName.get(callback.pack, callback.name);
		TypeSpec.Builder classBuilder = TypeSpec.classBuilder(className);

		classBuilder.addAnnotation(AnnotationSpec.builder(SuppressWarnings.class).addMember("value", "{\"unused\"}").build());
		classBuilder.addModifiers(Modifier.PUBLIC);
		classBuilder.superclass(TN_JSCALLBACK);

		// Constructor
		{
			MethodSpec.Builder constructorBuilder = MethodSpec.constructorBuilder();
			constructorBuilder.addModifiers(Modifier.PUBLIC);
			constructorBuilder.addParameter(ParameterSpec.builder(TypeName.OBJECT, "peer").build());
			constructorBuilder.addCode("super(peer);");
			classBuilder.addMethod(constructorBuilder.build());
		}
		{
			MethodSpec.Builder constructorBuilder = MethodSpec.constructorBuilder();
			constructorBuilder.addModifiers(Modifier.PUBLIC);
			constructorBuilder.addParameter(ParameterSpec.builder(ClassName.get(callback.pack, callback.name, "Callback"), "javaCallback").build());
			constructorBuilder.addCode("super(javaCallback);");
			classBuilder.addMethod(constructorBuilder.build());
		}

		{
			//== Wrap
			MethodSpec.Builder wrapBuilder = MethodSpec.methodBuilder("wrap");
			wrapBuilder.addModifiers(Modifier.PUBLIC, Modifier.STATIC);
			wrapBuilder.returns(className);
			wrapBuilder.addParameter(ParameterSpec.builder(TypeName.OBJECT, "peer").build());
			wrapBuilder.addCode("return peer == null ? null : new $T(peer);", className);
			classBuilder.addMethod(wrapBuilder.build());
		}

		{
			TypeSpec.Builder intfBuilder = TypeSpec.interfaceBuilder("Callback");

			intfBuilder.addModifiers(Modifier.PUBLIC);
			intfBuilder.addSuperinterface(TN_JSCALLBACK_BASE_CALLBACK);

			MethodSpec.Builder methodBuilder = MethodSpec.methodBuilder("handle");
			TypeName returnType = typeName(callback.type);
			methodBuilder.returns(returnType);

			methodBuilder.addModifiers(Modifier.PUBLIC, Modifier.ABSTRACT);

			for (Arg arg: callback.args)
				methodBuilder.addParameter(typeName(arg.type), arg.name);

			intfBuilder.addMethod(methodBuilder.build());

			classBuilder.addType(intfBuilder.build());
		}

		JavaFile.builder(callback.pack, classBuilder.build())
			.indent("    ")
			.skipJavaLangImports(true)
			.addFileComment("Generated file; DO NOT edit")
			.build()
			.writeTo(dir);
	}

	private void genAttributes(Intf intf, List<String> comments, TypeSpec.Builder classBuilder) {
		for (Attribute attr: intf.attrs) {
			String getterName = (attr.type.isBoolean() ? "is" : "get") + Character.toUpperCase(attr.name.charAt(0)) + attr.name.substring(1);
			String setterName = "set" + Character.toUpperCase(attr.name.charAt(0)) + attr.name.substring(1);
			TypeName type = typeName(attr.type);

			if (type == TypeName.VOID) {
				comments.add("TODO Unknown type for attribute " + attr.name);
				continue;
			}
			// Getter
			{
				MethodSpec.Builder getBuilder = MethodSpec.methodBuilder(getterName);
				getBuilder.addModifiers(Modifier.PUBLIC);
				getBuilder.returns(type);
				if (type.isPrimitive())
					switch (type.toString()) {
						case "int":
							getBuilder.addCode("return $T.getInt(peer, $S);", TN_JS, attr.name);
							break;
						case "double":
							getBuilder.addCode("return $T.getDouble(peer, $S);", TN_JS, attr.name);
							break;
						case "boolean":
							getBuilder.addCode("return $T.getBoolean(peer, $S);", TN_JS, attr.name);
							break;
						default:
							getBuilder.addCode("return ($T)$T.get(peer, $S);", type, TN_JS, attr.name);
							break;
					}
				else if (type == TN_STRING)
					getBuilder.addCode("return $T.string($T.get(peer, $S));", TN_JS, TN_JS, attr.name);
				else {
					if (type instanceof ParameterizedTypeName) {
						ParameterizedTypeName ptn = (ParameterizedTypeName)type;
						getBuilder.addCode("return $T.wrap($T.class, ", ptn.rawType, ptn.typeArguments.get(0));
					} else
						getBuilder.addCode("return $T.wrap(", type);
					getBuilder.addCode("$T.get(peer, $S));", TN_JS, attr.name);
				}
				classBuilder.addMethod(getBuilder.build());
			}

			// Setter
			if (!attr.readonly) {
				MethodSpec.Builder setBuilder = MethodSpec.methodBuilder(setterName);
				setBuilder.addModifiers(Modifier.PUBLIC);
				setBuilder.addParameter(type, "value");
				if (type.isPrimitive())
					setBuilder.addCode("$T.set(peer, $S, value);", TN_JS, attr.name);
				else if (type == TN_STRING)
					setBuilder.addCode("$T.set(peer, $S, $T.param(value));", TN_JS, attr.name, TN_JS);
				else
					setBuilder.addCode("$T.set(peer, $S, value.peer);", TN_JS, attr.name);
				classBuilder.addMethod(setBuilder.build());
			}
		}
		for (Intf mixin: intf.mixins)
			genAttributes(mixin, comments, classBuilder);
	}

	private void genOperations(Intf intf, TypeSpec.Builder classBuilder) {
		for (Operation operation: intf.operations) {
			List<Operation> permutations = permute(operation);
			for (Operation permutation: permutations)
				genOperation(intf, classBuilder, permutation);
		}
		for (Intf mixin: intf.mixins)
			genOperations(mixin, classBuilder);
	}

	private void genOperation(Intf intf, TypeSpec.Builder classBuilder, Operation operation) {
		MethodSpec.Builder methodBuilder = MethodSpec.methodBuilder(operation.name);
		TypeName returnType = typeName(operation.type);
		methodBuilder.returns(returnType);

		if (intf.kind == Intf.Kind.CALLBACK_INTERFACE)
			methodBuilder.addModifiers(Modifier.PUBLIC, Modifier.ABSTRACT);
		else
			methodBuilder.addModifiers(Modifier.PUBLIC);

		for (Arg arg: operation.args)
			methodBuilder.addParameter(typeName(arg.type), arg.name);

		if (intf.kind != Intf.Kind.CALLBACK_INTERFACE) {
			CodeBlock.Builder code = CodeBlock.builder();
			if (returnType != TypeName.VOID) {
				code.add("return ");
				if (operation.type.isString()) {
					code.add("$T.string(", TN_JS);
				} else if (!operation.type.isPrimitive()) {
					if (returnType instanceof ParameterizedTypeName) {
						ParameterizedTypeName ptn = (ParameterizedTypeName)returnType;
						code.add("$T.wrap($T.class, ", ptn.rawType, ptn.typeArguments.get(0));
					} else
						code.add("$T.wrap(", returnType);
				}
			}

			code.add("$T.invoke(peer, $S", TN_JS, operation.name);
			for (Arg arg: operation.args) {
				if (arg.type.isPrimitive())
					code.add(", $N", arg.name);
				else
					code.add(", $T.param($N)", TN_JS, arg.name);
			}
			code.add(")");

			if (returnType != TypeName.VOID) {
				if (operation.type.isString()) {
					code.add(")");
				} else if (!operation.type.isPrimitive()) {
					code.add(")");
				}
			}
			code.add(";");


			methodBuilder.addCode(code.build());
		}

		classBuilder.addMethod(methodBuilder.build());
	}

	private TypeName typeName(Type type) {
		if (type == null)
			return TypeName.VOID;

		if (type.any || type.object)
			return TN_WRAPPED_OBJECT;

		if (type.promise) {
			TypeName param = typeName(type.param);
			if (param == TypeName.VOID)
				param = TN_WRAPPED_OBJECT;
			return ParameterizedTypeName.get(TN_JSPROMISE, param);
		}
		if (type.sequence) {
			TypeName param = typeName(type.param);
			if (param == TypeName.VOID)
				param = TN_WRAPPED_OBJECT;
			if (param.isPrimitive())
				param = param.box();
			return ParameterizedTypeName.get(TN_JSSEQUENCE, param);
		}

		String str = type.name;

		if (str.equals("boolean"))
			return TypeName.BOOLEAN;
		if (str.equals("short") || str.equals("int"))
			return TypeName.INT;
		if (str.equals("long"))
			return TypeName.LONG;
		if (str.equals("double"))
			return TypeName.DOUBLE;
		if (str.equals("float"))
			return TypeName.FLOAT;
		if (str.equals("String"))
			return TN_STRING;
		if (str.equals("JSObject"))
			return TN_JSOBJECT;
		if (str.equals("Void"))
			return ClassName.get(Void.class);

		Intf intf = spec.interfaces.get(str);
		if (intf != null)
			return ClassName.get(intf.pack, intf.name);
		if (spec.enums.contains(str))
			return TN_STRING;
		if (spec.dictionaries.contains(str))
			return TN_JSOBJECT;
		Callback callback = spec.callbacks.get(str);
		if (callback != null)
			return ClassName.get(callback.pack, callback.name);

		System.out.println("type " + str + " -> WrappedObject " + sourceRef(type.start));
		return TN_WRAPPED_OBJECT;

	}

	public String sourceRef(Token token) {
		if (token == null)
			return "";
		StringBuilder sb = new StringBuilder();
		sb.append("at ");
		if (token.getTokenSource() instanceof Lexer)
			sb.append(((Lexer)token.getTokenSource()).file.getFileName().toString()).append(":");
		sb.append(token.getLine());
		return sb.toString();
	}

	public List<Operation> permute(Operation operation) {
		ArrayList<Operation> in = new ArrayList<>();
		in.add(operation);
		ArrayList<Operation> out = null;
		boolean needed = true;
		while (needed) {
			needed = false;
			out = new ArrayList<>();
			for (Operation inOp: in)
				if (permute(inOp, out))
					needed = true;
			in = out;
		}
		return out;
	}

	private boolean permute(Operation operation, List<Operation> res) {
		for (int i = operation.args.size() - 1; i >= 0; i--) {
			Arg arg = operation.args.get(i);
			if (arg.optional) {
				// with optional
				Operation opWith = new Operation(operation);
				Arg nonOptionalArg = new Arg(arg);
				nonOptionalArg.optional = false;
				opWith.args.set(i, nonOptionalArg);
				for (int i1 = 0; i1 < opWith.args.size(); i1++) {
					// remove optional flag from other args
					Arg arg1 = opWith.args.get(i1);
					if (arg1.optional) {
						arg1 = new Arg(arg1);
						arg1.optional = false;
						opWith.args.set(i1, arg1);
					}
				}
				res.add(opWith);
				// without optional
				Operation opWithout = new Operation(operation);
				opWithout.args.remove(i);
				res.add(opWithout);
				return true;
			}
			if (arg.type.union != null) {
				for (Type type: arg.type.union) {
					Operation newOp = new Operation(operation);
					Arg newArg = new Arg(arg);
					newArg.type = type;
					newOp.args.set(i, newArg);
					res.add(newOp);
				}
				return true;
			}
		}
		res.add(operation);
		return false;
	}

}
