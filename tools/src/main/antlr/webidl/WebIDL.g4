/*
BSD License
Copyright (c) 2013,2015 Rainer Schuster
Copyright (c) 2021 ethanmdavidson
All rights reserved.
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of Rainer Schuster nor the names of its contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
Web IDL grammar derived from:
    http://heycam.github.io/webidl/
    Web IDL (Second Edition)
    Editor’s Draft, 3 May 2021
 */
grammar WebIDL;

/* Note: appended underscore to keywords for CPP compat:
    const, default, enum, namespace, null
    see github.com/antlr/grammars-v4/issues/2099 for more info
*/
// Note: Added "wrapper" rule webIDL with EOF token.

@header {
package webidl;
}

webIDL
	: definition* EOF
;

definition
	: callbackDef
	| callbackInterfaceDef
    | interfaceDef
	| mixinDef
	| namespaceDef
	| dictionaryDef
	| enumDef
	| typedefDef
	| includesDef
    ;

//====================================================
// Defs
//====================================================

callbackDef
    : extendedAttributes? 'callback' IDENTIFIER_WEBIDL '=' type_ '(' arguments? ')' ';'
    ;

callbackInterfaceDef
    : extendedAttributes? 'callback' 'interface' IDENTIFIER_WEBIDL '{' callbackInterfaceMember* '}' ';'
    ;

interfaceDef
    : extendedAttributes? 'partial'? 'interface' IDENTIFIER_WEBIDL inheritance? '{' interfaceMember* '}' ';'
    ;

mixinDef
    : extendedAttributes? 'partial'? 'interface' 'mixin' IDENTIFIER_WEBIDL '{' mixinMember* '}' ';'
    ;

namespaceDef
    : extendedAttributes? 'partial'? 'namespace' IDENTIFIER_WEBIDL '{' namespaceMember* '}' ';'
    ;

dictionaryDef
    : extendedAttributes? 'partial'? 'dictionary' IDENTIFIER_WEBIDL inheritance? '{' dictionaryMember* '}' ';'
    ;

enumDef
    : extendedAttributes? 'enum' IDENTIFIER_WEBIDL '{' enumValueList '}' ';'
    ;

enumValueList
	: STRING_WEBIDL (',' STRING_WEBIDL)*
    ;

typedefDef
    : extendedAttributes? 'typedef' typeWithExtendedAttributes IDENTIFIER_WEBIDL ';'
    ;

includesDef
    : extendedAttributes? IDENTIFIER_WEBIDL 'includes' IDENTIFIER_WEBIDL ';'
    ;

//====================================================
// Members
//====================================================

interfaceMember
    : constDecl
    | stringifierDecl
    | operationDecl
    | specialOperationDecl
    | iterableDecl
    | asyncIterableDecl
    | attributeDecl
    | maplikeDecl
    | setlikeDecl
    | constructorDecl
;

mixinMember
    : constDecl
    | stringifierDecl
    | operationDecl
    | specialOperationDecl
    | attributeDecl
;

callbackInterfaceMember
    : constDecl
    | operationDecl
    | specialOperationDecl
;

namespaceMember
    : constDecl
    | operationDecl
    | specialOperationDecl
    | attributeDecl
;

dictionaryMember
	: dictReqDecl
    | dictDecl
;

//====================================================
// Decls
//====================================================

constDecl: extendedAttributes? 'const' constType IDENTIFIER_WEBIDL '=' constValue ';';
operationDecl: extendedAttributes? 'stringifier'? ('static')? type_ operationName '(' arguments? ')' ';';
specialOperationDecl: extendedAttributes? 'stringifier'? ('getter' | 'setter' | 'deleter') type_ operationName? '(' arguments? ')' ';';
stringifierDecl: extendedAttributes? 'stringifier' ';';
iterableDecl: extendedAttributes? 'iterable' '<' iterableTypes '>' ';';
asyncIterableDecl: extendedAttributes? 'async' 'iterable' '<' iterableTypes '>' optionalArgumentList ';';
attributeDecl: extendedAttributes? ('static' | 'stringifier')? ('readonly'|'inherit')? 'attribute' typeWithExtendedAttributes attributeName ';';
maplikeDecl: extendedAttributes? 'readonly'? 'maplike' '<' typeWithExtendedAttributes ',' typeWithExtendedAttributes '>' ';';
setlikeDecl: extendedAttributes? 'readonly'? 'setlike' '<' typeWithExtendedAttributes '>' ';';
constructorDecl: extendedAttributes? 'constructor' '(' arguments? ')' ';';
dictReqDecl: extendedAttributes? 'required' typeWithExtendedAttributes IDENTIFIER_WEBIDL ';';
dictDecl: extendedAttributes? type_ IDENTIFIER_WEBIDL default_? ';';

//====================================================

inheritance
	: ':' IDENTIFIER_WEBIDL
    ;

constValue
	: 'true'
    | 'false'
	| DECIMAL_WEBIDL
	| '-Infinity'
	| 'Infinity'
	| 'NaN'
	| INTEGER_WEBIDL
;

constType
	: primitiveType
	| IDENTIFIER_WEBIDL
;

attributeName
	: 'async'
	| 'required'
	| IDENTIFIER_WEBIDL
;

operationName
    : 'includes'
    | IDENTIFIER_WEBIDL
;

arguments
	: argument (',' argument)*
	;

argument
	: extendedAttributes? (
          'optional' typeWithExtendedAttributes argumentName default_?
        | type_ ellipsis? argumentName
    )
;

argumentName
	: ('async'
	| 'attribute'
	| 'callback'
	| 'const'
	| 'constructor'
	| 'deleter'
	| 'dictionary'
	| 'enum'
	| 'getter'
	| 'includes'
	| 'inherit'
	| 'interface'
	| 'iterable'
	| 'maplike'
	| 'mixin'
	| 'namespace'
	| 'partial'
	| 'readonly'
	| 'required'
	| 'setlike'
	| 'setter'
	| 'static'
	| 'stringifier'
	| 'typedef'
	| 'unrestricted')
	| IDENTIFIER_WEBIDL
;


ellipsis
	: '...'
;


iterableTypes
    : typeWithExtendedAttributes ( ',' typeWithExtendedAttributes )*
    ;

optionalArgumentList
    : '(' arguments? ')'
    | /* empty */
;



default_
	: '='
	    ( constValue
	    | STRING_WEBIDL
	    | '[' ']'
	    | '{' '}'
	    | 'null'
	    )
;


type_
	: distinguishableType
	| 'any'
	| promiseType
	| unionType orNull?
;

typeWithExtendedAttributes
    : extendedAttributes? type_
;

unionType
	: '(' unionMemberType ('or' unionMemberType)+  ')'
;

unionMemberType
	: extendedAttributes? distinguishableType
	//| unionType orNull?
;

distinguishableType
    : primitiveType orNull?
    | stringType orNull?
    | IDENTIFIER_WEBIDL orNull?
    | sequenceType
    | 'object' orNull?
    | 'symbol' orNull?
    | bufferRelatedType orNull?
    | 'FrozenArray' '<' typeWithExtendedAttributes '>' orNull?
    | 'ObservableArray' '<' typeWithExtendedAttributes '>' orNull?
    | recordType orNull?
;

sequenceType
    : 'sequence' '<' typeWithExtendedAttributes '>' orNull?
    ;

primitiveType
	: integerType
	| floatType
	| 'undefined'
	| 'boolean'
	| 'byte'
	| 'octet'
	| 'bigint'
;

floatType
	: 'unrestricted'? 'float'
	| 'unrestricted'? 'double'
;

integerType
	: 'unsigned'? 'short'
	| 'unsigned'? 'long' extraLong?
;


extraLong
	: 'long'
;

stringType
    : 'ByteString'
    | 'DOMString'
    | 'CSSOMString'
    | 'USVString'
;

promiseType
	: 'Promise' '<' type_ '>'
;

recordType
    : 'record' '<' stringType ',' typeWithExtendedAttributes '>'
;

orNull
	: '?'
;

bufferRelatedType
	: 'ArrayBuffer'
	| 'DataView'
	| 'Int8Array'
	| 'Int16Array'
	| 'Int32Array'
	| 'Uint8Array'
	| 'Uint16Array'
	| 'Uint32Array'
	| 'Uint8ClampedArray'
	| 'Float32Array'
	| 'Float64Array'
;

extendedAttributes
	: '[' extendedAttribute (',' extendedAttribute)* ']'
    ;

/* https://heycam.github.io/webidl/#idl-extended-attributes
   "The ExtendedAttribute grammar symbol matches nearly any sequence of tokens,
   however the extended attributes defined in this document only accept a more
   restricted syntax."
   I use the more restrictive syntax here because it will be more useful for
   parsing things in the real world.
*/
extendedAttribute
    : IDENTIFIER_WEBIDL
    | IDENTIFIER_WEBIDL '(' arguments? ')'
    | IDENTIFIER_WEBIDL '=' IDENTIFIER_WEBIDL '(' arguments? ')'
    | IDENTIFIER_WEBIDL '=' IDENTIFIER_WEBIDL
    | IDENTIFIER_WEBIDL '=' '*'
    | IDENTIFIER_WEBIDL '=' '(' identifiers ')'
    | IDENTIFIER_WEBIDL '=' STRING_WEBIDL
    | IDENTIFIER_WEBIDL '=' '(' strings ')'
;

identifiers
	: IDENTIFIER_WEBIDL (',' IDENTIFIER_WEBIDL)*
;

strings
    : STRING_WEBIDL (',' STRING_WEBIDL)*
;

READONLY: 'readonly';
ANY: 'any';
OBJECT: 'object';
STATIC: 'static';
OPTIONAL: 'optional';
UNDEFINED: 'undefined';
PARTIAL: 'partial';

INTEGER_WEBIDL
	: '-'?('0'([Xx][0-9A-Fa-f]+|[0-7]*)|[1-9][0-9]*)
;

DECIMAL_WEBIDL
	: '-'?(([0-9]+'.'[0-9]*|[0-9]*'.'[0-9]+)([Ee][+\-]?[0-9]+)?|[0-9]+[Ee][+\-]?[0-9]+)
;

IDENTIFIER_WEBIDL
	: [_-]?[A-Z_a-z][0-9A-Z_a-z]*
;

STRING_WEBIDL
	: '"' ~["]* '"'
;

WHITESPACE_WEBIDL
	: [\t\n\r ]+ -> channel(HIDDEN)
;

COMMENT_WEBIDL
	: ('//'~[\n\r]*|'/*'(.|'\n')*?'*/')+ -> channel(HIDDEN)
; // Note: '/''/'~[\n\r]* instead of '/''/'.* (non-greedy because of wildcard).

OTHER_WEBIDL
	: ~[\t\n\r 0-9A-Z_a-z]
;
